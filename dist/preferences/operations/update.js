"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const defaultAccess_1 = __importDefault(require("../../auth/defaultAccess"));
const executeAccess_1 = __importDefault(require("../../auth/executeAccess"));
const UnathorizedError_1 = __importDefault(require("../../errors/UnathorizedError"));
async function update(args) {
    const { overrideAccess, user, req, req: { payload: { config: { admin: { locale: adminUILocale } }, preferences: { Model } } }, key, value } = args;
    if (!user) {
        throw new UnathorizedError_1.default(adminUILocale.errors.UnauthorizedErrorLabel);
    }
    if (!overrideAccess) {
        await (0, executeAccess_1.default)({ req }, defaultAccess_1.default);
    }
    const filter = { user: user.id, key, userCollection: user.collection };
    const preference = { ...filter, value };
    await Model.updateOne(filter, { ...preference }, { upsert: true });
    return preference;
}
exports.default = update;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3ByZWZlcmVuY2VzL29wZXJhdGlvbnMvdXBkYXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0EsNkVBQXFEO0FBQ3JELDZFQUFxRDtBQUNyRCxxRkFBOEQ7QUFFOUQsS0FBSyxVQUFVLE1BQU0sQ0FBQyxJQUE2QjtJQUMvQyxNQUFNLEVBQ0YsY0FBYyxFQUNkLElBQUksRUFDSixHQUFHLEVBQ0gsR0FBRyxFQUFFLEVBQ0QsT0FBTyxFQUFFLEVBQ0wsTUFBTSxFQUFFLEVBQ0osS0FBSyxFQUFFLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxFQUNuQyxFQUNELFdBQVcsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUN6QixFQUNKLEVBQ0QsR0FBRyxFQUNILEtBQUssRUFDUixHQUFHLElBQUksQ0FBQztJQUVULElBQUksQ0FBQyxJQUFJLEVBQUU7UUFDUCxNQUFNLElBQUksMEJBQWlCLENBQ3ZCLGFBQWEsQ0FBQyxNQUFNLENBQUMsc0JBQXNCLENBQzlDLENBQUM7S0FDTDtJQUVELElBQUksQ0FBQyxjQUFjLEVBQUU7UUFDakIsTUFBTSxJQUFBLHVCQUFhLEVBQUMsRUFBRSxHQUFHLEVBQUUsRUFBRSx1QkFBYSxDQUFDLENBQUM7S0FDL0M7SUFFRCxNQUFNLE1BQU0sR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3ZFLE1BQU0sVUFBVSxHQUFlLEVBQUUsR0FBRyxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7SUFDcEQsTUFBTSxLQUFLLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxFQUFFLEdBQUcsVUFBVSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUVuRSxPQUFPLFVBQVUsQ0FBQztBQUN0QixDQUFDO0FBRUQsa0JBQWUsTUFBTSxDQUFDIn0=