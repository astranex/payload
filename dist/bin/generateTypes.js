"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateTypes = void 0;
/* eslint-disable no-nested-ternary */
const fs_1 = __importDefault(require("fs"));
const json_schema_to_typescript_1 = require("json-schema-to-typescript");
const logger_1 = __importDefault(require("../utilities/logger"));
const types_1 = require("../fields/config/types");
const load_1 = __importDefault(require("../config/load"));
const deepCopyObject_1 = __importDefault(require("../utilities/deepCopyObject"));
const groupOrTabHasRequiredSubfield_1 = require("../utilities/groupOrTabHasRequiredSubfield");
const nonOptionalFieldTypes = ['group', 'array', 'blocks'];
const propertyIsOptional = (field) => {
    return (0, types_1.fieldAffectsData)(field) && (field.required === true || nonOptionalFieldTypes.includes(field.type));
};
function getCollectionIDType(collections, slug) {
    const matchedCollection = collections.find((collection) => collection.slug === slug);
    const customIdField = matchedCollection.fields.find((field) => 'name' in field && field.name === 'id');
    if (customIdField && customIdField.type === 'number') {
        return 'number';
    }
    return 'string';
}
function returnOptionEnums(options) {
    return options.map((option) => {
        if (typeof option === 'object' && 'value' in option) {
            return option.value;
        }
        return option;
    });
}
function generateFieldTypes(config, fields) {
    let topLevelProps = [];
    let requiredTopLevelProps = [];
    return {
        properties: Object.fromEntries(fields.reduce((properties, field) => {
            let fieldSchema;
            switch (field.type) {
                case 'text':
                case 'textarea':
                case 'code':
                case 'email':
                case 'date': {
                    fieldSchema = { type: 'string' };
                    break;
                }
                case 'number': {
                    fieldSchema = { type: 'number' };
                    break;
                }
                case 'checkbox': {
                    fieldSchema = { type: 'boolean' };
                    break;
                }
                case 'richText': {
                    fieldSchema = {
                        type: 'array',
                        items: {
                            type: 'object',
                        },
                    };
                    break;
                }
                case 'radio': {
                    fieldSchema = {
                        type: 'string',
                        enum: returnOptionEnums(field.options),
                    };
                    break;
                }
                case 'select': {
                    const selectType = {
                        type: 'string',
                        enum: returnOptionEnums(field.options),
                    };
                    if (field.hasMany) {
                        fieldSchema = {
                            type: 'array',
                            items: selectType,
                        };
                    }
                    else {
                        fieldSchema = selectType;
                    }
                    break;
                }
                case 'point': {
                    fieldSchema = {
                        type: 'array',
                        minItems: 2,
                        maxItems: 2,
                        items: [
                            {
                                type: 'number',
                            },
                            {
                                type: 'number',
                            },
                        ],
                    };
                    break;
                }
                case 'relationship': {
                    if (Array.isArray(field.relationTo)) {
                        if (field.hasMany) {
                            fieldSchema = {
                                oneOf: [
                                    {
                                        type: 'array',
                                        items: {
                                            oneOf: field.relationTo.map((relation) => {
                                                const idFieldType = getCollectionIDType(config.collections, relation);
                                                return {
                                                    type: 'object',
                                                    additionalProperties: false,
                                                    properties: {
                                                        value: {
                                                            type: idFieldType,
                                                        },
                                                        relationTo: {
                                                            const: relation,
                                                        },
                                                    },
                                                    required: ['value', 'relationTo'],
                                                };
                                            }),
                                        },
                                    },
                                    {
                                        type: 'array',
                                        items: {
                                            oneOf: field.relationTo.map((relation) => {
                                                return {
                                                    type: 'object',
                                                    additionalProperties: false,
                                                    properties: {
                                                        value: {
                                                            $ref: `#/definitions/${relation}`,
                                                        },
                                                        relationTo: {
                                                            const: relation,
                                                        },
                                                    },
                                                    required: ['value', 'relationTo'],
                                                };
                                            }),
                                        },
                                    },
                                ],
                            };
                        }
                        else {
                            fieldSchema = {
                                oneOf: field.relationTo.map((relation) => {
                                    const idFieldType = getCollectionIDType(config.collections, relation);
                                    return {
                                        type: 'object',
                                        additionalProperties: false,
                                        properties: {
                                            value: {
                                                oneOf: [
                                                    {
                                                        type: idFieldType,
                                                    },
                                                    {
                                                        $ref: `#/definitions/${relation}`,
                                                    },
                                                ],
                                            },
                                            relationTo: {
                                                const: relation,
                                            },
                                        },
                                        required: ['value', 'relationTo'],
                                    };
                                }),
                            };
                        }
                    }
                    else {
                        const idFieldType = getCollectionIDType(config.collections, field.relationTo);
                        if (field.hasMany) {
                            fieldSchema = {
                                oneOf: [
                                    {
                                        type: 'array',
                                        items: {
                                            type: idFieldType,
                                        },
                                    },
                                    {
                                        type: 'array',
                                        items: {
                                            $ref: `#/definitions/${field.relationTo}`,
                                        },
                                    },
                                ],
                            };
                        }
                        else {
                            fieldSchema = {
                                oneOf: [
                                    {
                                        type: idFieldType,
                                    },
                                    {
                                        $ref: `#/definitions/${field.relationTo}`,
                                    },
                                ],
                            };
                        }
                    }
                    break;
                }
                case 'upload': {
                    const idFieldType = getCollectionIDType(config.collections, field.relationTo);
                    fieldSchema = {
                        oneOf: [
                            {
                                type: idFieldType,
                            },
                            {
                                $ref: `#/definitions/${field.relationTo}`,
                            },
                        ],
                    };
                    break;
                }
                case 'blocks': {
                    fieldSchema = {
                        type: 'array',
                        items: {
                            oneOf: field.blocks.map((block) => {
                                const blockSchema = generateFieldTypes(config, block.fields);
                                return {
                                    type: 'object',
                                    additionalProperties: false,
                                    properties: {
                                        ...blockSchema.properties,
                                        blockType: {
                                            const: block.slug,
                                        },
                                    },
                                    required: [
                                        'blockType',
                                        ...blockSchema.required,
                                    ],
                                };
                            }),
                        },
                    };
                    break;
                }
                case 'array': {
                    fieldSchema = {
                        type: 'array',
                        items: {
                            type: 'object',
                            additionalProperties: false,
                            ...generateFieldTypes(config, field.fields),
                        },
                    };
                    break;
                }
                case 'row':
                case 'collapsible': {
                    const topLevelFields = generateFieldTypes(config, field.fields);
                    requiredTopLevelProps = requiredTopLevelProps.concat(topLevelFields.required);
                    topLevelProps = topLevelProps.concat(Object.entries(topLevelFields.properties).map((prop) => prop));
                    break;
                }
                case 'tabs': {
                    field.tabs.forEach((tab) => {
                        if ((0, types_1.tabHasName)(tab)) {
                            const hasRequiredSubfields = (0, groupOrTabHasRequiredSubfield_1.groupOrTabHasRequiredSubfield)(tab);
                            if (hasRequiredSubfields)
                                requiredTopLevelProps.push(tab.name);
                            topLevelProps.push([
                                tab.name,
                                {
                                    type: 'object',
                                    additionalProperties: false,
                                    ...generateFieldTypes(config, tab.fields),
                                },
                            ]);
                        }
                        else {
                            const topLevelFields = generateFieldTypes(config, tab.fields);
                            requiredTopLevelProps = requiredTopLevelProps.concat(topLevelFields.required);
                            topLevelProps = topLevelProps.concat(Object.entries(topLevelFields.properties).map((prop) => prop));
                        }
                    });
                    break;
                }
                case 'group': {
                    fieldSchema = {
                        type: 'object',
                        additionalProperties: false,
                        ...generateFieldTypes(config, field.fields),
                    };
                    break;
                }
                default: {
                    break;
                }
            }
            if (fieldSchema && (0, types_1.fieldAffectsData)(field)) {
                return [
                    ...properties,
                    [
                        field.name,
                        {
                            ...fieldSchema,
                        },
                    ],
                ];
            }
            return [
                ...properties,
                ...topLevelProps,
            ];
        }, [])),
        required: [
            ...fields
                .filter(propertyIsOptional)
                .map((field) => ((0, types_1.fieldAffectsData)(field) ? field.name : '')),
            ...requiredTopLevelProps,
        ],
    };
}
function entityToJsonSchema(config, incomingEntity) {
    const entity = (0, deepCopyObject_1.default)(incomingEntity);
    const title = 'label' in entity ? entity.label : entity.labels.singular;
    const idField = { type: 'text', name: 'id', required: true };
    const customIdField = entity.fields.find((field) => (0, types_1.fieldAffectsData)(field) && field.name === 'id');
    if (customIdField) {
        customIdField.required = true;
    }
    else {
        entity.fields.unshift(idField);
    }
    if ('timestamps' in entity && entity.timestamps !== false) {
        entity.fields.push({
            type: 'text',
            name: 'createdAt',
            required: true,
        }, {
            type: 'text',
            name: 'updatedAt',
            required: true,
        });
    }
    return {
        title,
        type: 'object',
        additionalProperties: false,
        ...generateFieldTypes(config, entity.fields),
    };
}
function configToJsonSchema(config) {
    return {
        definitions: Object.fromEntries([
            ...config.globals.map((global) => [
                global.slug,
                entityToJsonSchema(config, global),
            ]),
            ...config.collections.map((collection) => [
                collection.slug,
                entityToJsonSchema(config, collection),
            ]),
        ]),
        additionalProperties: false,
    };
}
function generateTypes() {
    const logger = (0, logger_1.default)();
    const config = (0, load_1.default)();
    const outputFile = process.env.PAYLOAD_TS_OUTPUT_PATH || config.typescript.outputFile;
    logger.info('Compiling TS types for Collections and Globals...');
    const jsonSchema = configToJsonSchema(config);
    (0, json_schema_to_typescript_1.compile)(jsonSchema, 'Config', {
        unreachableDefinitions: true,
        bannerComment: '/* tslint:disable */\n/**\n* This file was automatically generated by Payload CMS.\n* DO NOT MODIFY IT BY HAND. Instead, modify your source Payload config,\n* and re-run `payload generate:types` to regenerate this file.\n*/',
        style: {
            singleQuote: true,
        },
    }).then((compiled) => {
        fs_1.default.writeFileSync(outputFile, compiled);
        logger.info(`Types written to ${outputFile}`);
    });
}
exports.generateTypes = generateTypes;
// when generateTypes.js is launched directly
if (module.id === require.main.id) {
    generateTypes();
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VuZXJhdGVUeXBlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9iaW4vZ2VuZXJhdGVUeXBlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxzQ0FBc0M7QUFDdEMsNENBQW9CO0FBRXBCLHlFQUFvRDtBQUNwRCxpRUFBeUM7QUFDekMsa0RBQXlHO0FBR3pHLDBEQUF3QztBQUV4QyxpRkFBeUQ7QUFDekQsOEZBQTJGO0FBRTNGLE1BQU0scUJBQXFCLEdBQUcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0FBRTNELE1BQU0sa0JBQWtCLEdBQUcsQ0FBQyxLQUFZLEVBQUUsRUFBRTtJQUMxQyxPQUFPLElBQUEsd0JBQWdCLEVBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxLQUFLLElBQUksSUFBSSxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7QUFDNUcsQ0FBQyxDQUFDO0FBRUYsU0FBUyxtQkFBbUIsQ0FBQyxXQUF3QyxFQUFFLElBQVk7SUFDakYsTUFBTSxpQkFBaUIsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDO0lBQ3JGLE1BQU0sYUFBYSxHQUFHLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLE1BQU0sSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztJQUV2RyxJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFBRTtRQUNwRCxPQUFPLFFBQVEsQ0FBQztLQUNqQjtJQUVELE9BQU8sUUFBUSxDQUFDO0FBQ2xCLENBQUM7QUFFRCxTQUFTLGlCQUFpQixDQUFDLE9BQWlCO0lBQzFDLE9BQU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO1FBQzVCLElBQUksT0FBTyxNQUFNLEtBQUssUUFBUSxJQUFJLE9BQU8sSUFBSSxNQUFNLEVBQUU7WUFDbkQsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQ3JCO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDO0FBRUQsU0FBUyxrQkFBa0IsQ0FBQyxNQUF1QixFQUFFLE1BQWU7SUFNbEUsSUFBSSxhQUFhLEdBQUcsRUFBRSxDQUFDO0lBQ3ZCLElBQUkscUJBQXFCLEdBQUcsRUFBRSxDQUFDO0lBRS9CLE9BQU87UUFDTCxVQUFVLEVBQUUsTUFBTSxDQUFDLFdBQVcsQ0FDNUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFVBQVUsRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUNsQyxJQUFJLFdBQXdCLENBQUM7WUFFN0IsUUFBUSxLQUFLLENBQUMsSUFBSSxFQUFFO2dCQUNsQixLQUFLLE1BQU0sQ0FBQztnQkFDWixLQUFLLFVBQVUsQ0FBQztnQkFDaEIsS0FBSyxNQUFNLENBQUM7Z0JBQ1osS0FBSyxPQUFPLENBQUM7Z0JBQ2IsS0FBSyxNQUFNLENBQUMsQ0FBQztvQkFDWCxXQUFXLEdBQUcsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLENBQUM7b0JBQ2pDLE1BQU07aUJBQ1A7Z0JBRUQsS0FBSyxRQUFRLENBQUMsQ0FBQztvQkFDYixXQUFXLEdBQUcsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLENBQUM7b0JBQ2pDLE1BQU07aUJBQ1A7Z0JBRUQsS0FBSyxVQUFVLENBQUMsQ0FBQztvQkFDZixXQUFXLEdBQUcsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUM7b0JBQ2xDLE1BQU07aUJBQ1A7Z0JBRUQsS0FBSyxVQUFVLENBQUMsQ0FBQztvQkFDZixXQUFXLEdBQUc7d0JBQ1osSUFBSSxFQUFFLE9BQU87d0JBQ2IsS0FBSyxFQUFFOzRCQUNMLElBQUksRUFBRSxRQUFRO3lCQUNmO3FCQUNGLENBQUM7b0JBRUYsTUFBTTtpQkFDUDtnQkFFRCxLQUFLLE9BQU8sQ0FBQyxDQUFDO29CQUNaLFdBQVcsR0FBRzt3QkFDWixJQUFJLEVBQUUsUUFBUTt3QkFDZCxJQUFJLEVBQUUsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztxQkFDdkMsQ0FBQztvQkFFRixNQUFNO2lCQUNQO2dCQUVELEtBQUssUUFBUSxDQUFDLENBQUM7b0JBQ2IsTUFBTSxVQUFVLEdBQWdCO3dCQUM5QixJQUFJLEVBQUUsUUFBUTt3QkFDZCxJQUFJLEVBQUUsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztxQkFDdkMsQ0FBQztvQkFFRixJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7d0JBQ2pCLFdBQVcsR0FBRzs0QkFDWixJQUFJLEVBQUUsT0FBTzs0QkFDYixLQUFLLEVBQUUsVUFBVTt5QkFDbEIsQ0FBQztxQkFDSDt5QkFBTTt3QkFDTCxXQUFXLEdBQUcsVUFBVSxDQUFDO3FCQUMxQjtvQkFFRCxNQUFNO2lCQUNQO2dCQUVELEtBQUssT0FBTyxDQUFDLENBQUM7b0JBQ1osV0FBVyxHQUFHO3dCQUNaLElBQUksRUFBRSxPQUFPO3dCQUNiLFFBQVEsRUFBRSxDQUFDO3dCQUNYLFFBQVEsRUFBRSxDQUFDO3dCQUNYLEtBQUssRUFBRTs0QkFDTDtnQ0FDRSxJQUFJLEVBQUUsUUFBUTs2QkFDZjs0QkFDRDtnQ0FDRSxJQUFJLEVBQUUsUUFBUTs2QkFDZjt5QkFDRjtxQkFDRixDQUFDO29CQUNGLE1BQU07aUJBQ1A7Z0JBRUQsS0FBSyxjQUFjLENBQUMsQ0FBQztvQkFDbkIsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRTt3QkFDbkMsSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFOzRCQUNqQixXQUFXLEdBQUc7Z0NBQ1osS0FBSyxFQUFFO29DQUNMO3dDQUNFLElBQUksRUFBRSxPQUFPO3dDQUNiLEtBQUssRUFBRTs0Q0FDTCxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtnREFDdkMsTUFBTSxXQUFXLEdBQUcsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxRQUFRLENBQUMsQ0FBQztnREFFdEUsT0FBTztvREFDTCxJQUFJLEVBQUUsUUFBUTtvREFDZCxvQkFBb0IsRUFBRSxLQUFLO29EQUMzQixVQUFVLEVBQUU7d0RBQ1YsS0FBSyxFQUFFOzREQUNMLElBQUksRUFBRSxXQUFXO3lEQUNsQjt3REFDRCxVQUFVLEVBQUU7NERBQ1YsS0FBSyxFQUFFLFFBQVE7eURBQ2hCO3FEQUNGO29EQUNELFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUM7aURBQ2xDLENBQUM7NENBQ0osQ0FBQyxDQUFDO3lDQUNIO3FDQUNGO29DQUNEO3dDQUNFLElBQUksRUFBRSxPQUFPO3dDQUNiLEtBQUssRUFBRTs0Q0FDTCxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtnREFDdkMsT0FBTztvREFDTCxJQUFJLEVBQUUsUUFBUTtvREFDZCxvQkFBb0IsRUFBRSxLQUFLO29EQUMzQixVQUFVLEVBQUU7d0RBQ1YsS0FBSyxFQUFFOzREQUNMLElBQUksRUFBRSxpQkFBaUIsUUFBUSxFQUFFO3lEQUNsQzt3REFDRCxVQUFVLEVBQUU7NERBQ1YsS0FBSyxFQUFFLFFBQVE7eURBQ2hCO3FEQUNGO29EQUNELFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUM7aURBQ2xDLENBQUM7NENBQ0osQ0FBQyxDQUFDO3lDQUNIO3FDQUNGO2lDQUNGOzZCQUNGLENBQUM7eUJBQ0g7NkJBQU07NEJBQ0wsV0FBVyxHQUFHO2dDQUNaLEtBQUssRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO29DQUN2QyxNQUFNLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDO29DQUV0RSxPQUFPO3dDQUNMLElBQUksRUFBRSxRQUFRO3dDQUNkLG9CQUFvQixFQUFFLEtBQUs7d0NBQzNCLFVBQVUsRUFBRTs0Q0FDVixLQUFLLEVBQUU7Z0RBQ0wsS0FBSyxFQUFFO29EQUNMO3dEQUNFLElBQUksRUFBRSxXQUFXO3FEQUNsQjtvREFDRDt3REFDRSxJQUFJLEVBQUUsaUJBQWlCLFFBQVEsRUFBRTtxREFDbEM7aURBQ0Y7NkNBQ0Y7NENBQ0QsVUFBVSxFQUFFO2dEQUNWLEtBQUssRUFBRSxRQUFROzZDQUNoQjt5Q0FDRjt3Q0FDRCxRQUFRLEVBQUUsQ0FBQyxPQUFPLEVBQUUsWUFBWSxDQUFDO3FDQUNsQyxDQUFDO2dDQUNKLENBQUMsQ0FBQzs2QkFDSCxDQUFDO3lCQUNIO3FCQUNGO3lCQUFNO3dCQUNMLE1BQU0sV0FBVyxHQUFHLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3dCQUU5RSxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7NEJBQ2pCLFdBQVcsR0FBRztnQ0FDWixLQUFLLEVBQUU7b0NBQ0w7d0NBQ0UsSUFBSSxFQUFFLE9BQU87d0NBQ2IsS0FBSyxFQUFFOzRDQUNMLElBQUksRUFBRSxXQUFXO3lDQUNsQjtxQ0FDRjtvQ0FDRDt3Q0FDRSxJQUFJLEVBQUUsT0FBTzt3Q0FDYixLQUFLLEVBQUU7NENBQ0wsSUFBSSxFQUFFLGlCQUFpQixLQUFLLENBQUMsVUFBVSxFQUFFO3lDQUMxQztxQ0FDRjtpQ0FDRjs2QkFDRixDQUFDO3lCQUNIOzZCQUFNOzRCQUNMLFdBQVcsR0FBRztnQ0FDWixLQUFLLEVBQUU7b0NBQ0w7d0NBQ0UsSUFBSSxFQUFFLFdBQVc7cUNBQ2xCO29DQUNEO3dDQUNFLElBQUksRUFBRSxpQkFBaUIsS0FBSyxDQUFDLFVBQVUsRUFBRTtxQ0FDMUM7aUNBQ0Y7NkJBQ0YsQ0FBQzt5QkFDSDtxQkFDRjtvQkFFRCxNQUFNO2lCQUNQO2dCQUVELEtBQUssUUFBUSxDQUFDLENBQUM7b0JBQ2IsTUFBTSxXQUFXLEdBQUcsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBRTlFLFdBQVcsR0FBRzt3QkFDWixLQUFLLEVBQUU7NEJBQ0w7Z0NBQ0UsSUFBSSxFQUFFLFdBQVc7NkJBQ2xCOzRCQUNEO2dDQUNFLElBQUksRUFBRSxpQkFBaUIsS0FBSyxDQUFDLFVBQVUsRUFBRTs2QkFDMUM7eUJBQ0Y7cUJBQ0YsQ0FBQztvQkFDRixNQUFNO2lCQUNQO2dCQUVELEtBQUssUUFBUSxDQUFDLENBQUM7b0JBQ2IsV0FBVyxHQUFHO3dCQUNaLElBQUksRUFBRSxPQUFPO3dCQUNiLEtBQUssRUFBRTs0QkFDTCxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQ0FDaEMsTUFBTSxXQUFXLEdBQUcsa0JBQWtCLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztnQ0FFN0QsT0FBTztvQ0FDTCxJQUFJLEVBQUUsUUFBUTtvQ0FDZCxvQkFBb0IsRUFBRSxLQUFLO29DQUMzQixVQUFVLEVBQUU7d0NBQ1YsR0FBRyxXQUFXLENBQUMsVUFBVTt3Q0FDekIsU0FBUyxFQUFFOzRDQUNULEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSTt5Q0FDbEI7cUNBQ0Y7b0NBQ0QsUUFBUSxFQUFFO3dDQUNSLFdBQVc7d0NBQ1gsR0FBRyxXQUFXLENBQUMsUUFBUTtxQ0FDeEI7aUNBQ0YsQ0FBQzs0QkFDSixDQUFDLENBQUM7eUJBQ0g7cUJBQ0YsQ0FBQztvQkFDRixNQUFNO2lCQUNQO2dCQUVELEtBQUssT0FBTyxDQUFDLENBQUM7b0JBQ1osV0FBVyxHQUFHO3dCQUNaLElBQUksRUFBRSxPQUFPO3dCQUNiLEtBQUssRUFBRTs0QkFDTCxJQUFJLEVBQUUsUUFBUTs0QkFDZCxvQkFBb0IsRUFBRSxLQUFLOzRCQUMzQixHQUFHLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDO3lCQUM1QztxQkFDRixDQUFDO29CQUNGLE1BQU07aUJBQ1A7Z0JBRUQsS0FBSyxLQUFLLENBQUM7Z0JBQ1gsS0FBSyxhQUFhLENBQUMsQ0FBQztvQkFDbEIsTUFBTSxjQUFjLEdBQUcsa0JBQWtCLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDaEUscUJBQXFCLEdBQUcscUJBQXFCLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDOUUsYUFBYSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNwRyxNQUFNO2lCQUNQO2dCQUVELEtBQUssTUFBTSxDQUFDLENBQUM7b0JBQ1gsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTt3QkFDekIsSUFBSSxJQUFBLGtCQUFVLEVBQUMsR0FBRyxDQUFDLEVBQUU7NEJBQ25CLE1BQU0sb0JBQW9CLEdBQUcsSUFBQSw2REFBNkIsRUFBQyxHQUFHLENBQUMsQ0FBQzs0QkFDaEUsSUFBSSxvQkFBb0I7Z0NBQUUscUJBQXFCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFFL0QsYUFBYSxDQUFDLElBQUksQ0FBQztnQ0FDakIsR0FBRyxDQUFDLElBQUk7Z0NBQ1I7b0NBQ0UsSUFBSSxFQUFFLFFBQVE7b0NBQ2Qsb0JBQW9CLEVBQUUsS0FBSztvQ0FDM0IsR0FBRyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQztpQ0FDMUM7NkJBQ0YsQ0FBQyxDQUFDO3lCQUNKOzZCQUFNOzRCQUNMLE1BQU0sY0FBYyxHQUFHLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7NEJBQzlELHFCQUFxQixHQUFHLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQzlFLGFBQWEsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzt5QkFDckc7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsTUFBTTtpQkFDUDtnQkFFRCxLQUFLLE9BQU8sQ0FBQyxDQUFDO29CQUNaLFdBQVcsR0FBRzt3QkFDWixJQUFJLEVBQUUsUUFBUTt3QkFDZCxvQkFBb0IsRUFBRSxLQUFLO3dCQUMzQixHQUFHLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDO3FCQUM1QyxDQUFDO29CQUNGLE1BQU07aUJBQ1A7Z0JBRUQsT0FBTyxDQUFDLENBQUM7b0JBQ1AsTUFBTTtpQkFDUDthQUNGO1lBRUQsSUFBSSxXQUFXLElBQUksSUFBQSx3QkFBZ0IsRUFBQyxLQUFLLENBQUMsRUFBRTtnQkFDMUMsT0FBTztvQkFDTCxHQUFHLFVBQVU7b0JBQ2I7d0JBQ0UsS0FBSyxDQUFDLElBQUk7d0JBQ1Y7NEJBQ0UsR0FBRyxXQUFXO3lCQUNmO3FCQUNGO2lCQUNGLENBQUM7YUFDSDtZQUVELE9BQU87Z0JBQ0wsR0FBRyxVQUFVO2dCQUNiLEdBQUcsYUFBYTthQUNqQixDQUFDO1FBQ0osQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUNQO1FBQ0QsUUFBUSxFQUFFO1lBQ1IsR0FBRyxNQUFNO2lCQUNOLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQztpQkFDMUIsR0FBRyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLElBQUEsd0JBQWdCLEVBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzlELEdBQUcscUJBQXFCO1NBQ3pCO0tBQ0YsQ0FBQztBQUNKLENBQUM7QUFFRCxTQUFTLGtCQUFrQixDQUFDLE1BQXVCLEVBQUUsY0FBaUU7SUFDcEgsTUFBTSxNQUFNLEdBQUcsSUFBQSx3QkFBYyxFQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQzlDLE1BQU0sS0FBSyxHQUFHLE9BQU8sSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO0lBRXhFLE1BQU0sT0FBTyxHQUF1QixFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUM7SUFDakYsTUFBTSxhQUFhLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUEsd0JBQWdCLEVBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQXVCLENBQUM7SUFFMUgsSUFBSSxhQUFhLEVBQUU7UUFDakIsYUFBYSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7S0FDL0I7U0FBTTtRQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQ2hDO0lBRUQsSUFBSSxZQUFZLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO1FBQ3pELE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUNoQjtZQUNFLElBQUksRUFBRSxNQUFNO1lBQ1osSUFBSSxFQUFFLFdBQVc7WUFDakIsUUFBUSxFQUFFLElBQUk7U0FDZixFQUNEO1lBQ0UsSUFBSSxFQUFFLE1BQU07WUFDWixJQUFJLEVBQUUsV0FBVztZQUNqQixRQUFRLEVBQUUsSUFBSTtTQUNmLENBQ0YsQ0FBQztLQUNIO0lBRUQsT0FBTztRQUNMLEtBQUs7UUFDTCxJQUFJLEVBQUUsUUFBUTtRQUNkLG9CQUFvQixFQUFFLEtBQUs7UUFDM0IsR0FBRyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQztLQUM3QyxDQUFDO0FBQ0osQ0FBQztBQUVELFNBQVMsa0JBQWtCLENBQUMsTUFBdUI7SUFDakQsT0FBTztRQUNMLFdBQVcsRUFBRSxNQUFNLENBQUMsV0FBVyxDQUM3QjtZQUNFLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDO2dCQUNoQyxNQUFNLENBQUMsSUFBSTtnQkFDWCxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDO2FBQ25DLENBQUM7WUFDRixHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQztnQkFDeEMsVUFBVSxDQUFDLElBQUk7Z0JBQ2Ysa0JBQWtCLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQzthQUN2QyxDQUFDO1NBQ0gsQ0FDRjtRQUNELG9CQUFvQixFQUFFLEtBQUs7S0FDNUIsQ0FBQztBQUNKLENBQUM7QUFFRCxTQUFnQixhQUFhO0lBQzNCLE1BQU0sTUFBTSxHQUFHLElBQUEsZ0JBQU0sR0FBRSxDQUFDO0lBQ3hCLE1BQU0sTUFBTSxHQUFHLElBQUEsY0FBVSxHQUFFLENBQUM7SUFDNUIsTUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsSUFBSSxNQUFNLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQztJQUV0RixNQUFNLENBQUMsSUFBSSxDQUFDLG1EQUFtRCxDQUFDLENBQUM7SUFFakUsTUFBTSxVQUFVLEdBQUcsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFFOUMsSUFBQSxtQ0FBTyxFQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUU7UUFDNUIsc0JBQXNCLEVBQUUsSUFBSTtRQUM1QixhQUFhLEVBQUUsaU9BQWlPO1FBQ2hQLEtBQUssRUFBRTtZQUNMLFdBQVcsRUFBRSxJQUFJO1NBQ2xCO0tBQ0YsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1FBQ25CLFlBQUUsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZDLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLFVBQVUsRUFBRSxDQUFDLENBQUM7SUFDaEQsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDO0FBbkJELHNDQW1CQztBQUVELDZDQUE2QztBQUM3QyxJQUFJLE1BQU0sQ0FBQyxFQUFFLEtBQUssT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUU7SUFDakMsYUFBYSxFQUFFLENBQUM7Q0FDakIifQ==