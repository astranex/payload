"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.fieldIsLocalized = exports.tabHasName = exports.fieldAffectsData = exports.fieldIsPresentationalOnly = exports.fieldHasMaxDepth = exports.fieldSupportsMany = exports.optionIsValue = exports.optionsAreObjects = exports.optionIsObject = exports.fieldIsBlockType = exports.fieldIsArrayType = exports.fieldHasSubFields = exports.valueIsValueWithRelation = void 0;
function valueIsValueWithRelation(value) {
    return (typeof value === 'object' && 'relationTo' in value && 'value' in value);
}
exports.valueIsValueWithRelation = valueIsValueWithRelation;
function fieldHasSubFields(field) {
    return (field.type === 'group' ||
        field.type === 'array' ||
        field.type === 'row' ||
        field.type === 'collapsible');
}
exports.fieldHasSubFields = fieldHasSubFields;
function fieldIsArrayType(field) {
    return field.type === 'array';
}
exports.fieldIsArrayType = fieldIsArrayType;
function fieldIsBlockType(field) {
    return field.type === 'blocks';
}
exports.fieldIsBlockType = fieldIsBlockType;
function optionIsObject(option) {
    return typeof option === 'object';
}
exports.optionIsObject = optionIsObject;
function optionsAreObjects(options) {
    return Array.isArray(options) && typeof (options === null || options === void 0 ? void 0 : options[0]) === 'object';
}
exports.optionsAreObjects = optionsAreObjects;
function optionIsValue(option) {
    return typeof option === 'string';
}
exports.optionIsValue = optionIsValue;
function fieldSupportsMany(field) {
    return field.type === 'select' || field.type === 'relationship';
}
exports.fieldSupportsMany = fieldSupportsMany;
function fieldHasMaxDepth(field) {
    return ((field.type === 'upload' || field.type === 'relationship') &&
        typeof field.maxDepth === 'number');
}
exports.fieldHasMaxDepth = fieldHasMaxDepth;
function fieldIsPresentationalOnly(field) {
    return field.type === 'ui';
}
exports.fieldIsPresentationalOnly = fieldIsPresentationalOnly;
function fieldAffectsData(field) {
    return 'name' in field && !fieldIsPresentationalOnly(field);
}
exports.fieldAffectsData = fieldAffectsData;
function tabHasName(tab) {
    return 'name' in tab;
}
exports.tabHasName = tabHasName;
function fieldIsLocalized(field) {
    return 'localized' in field && field.localized;
}
exports.fieldIsLocalized = fieldIsLocalized;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvZmllbGRzL2NvbmZpZy90eXBlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFzU0EsU0FBZ0Isd0JBQXdCLENBQ3BDLEtBQWM7SUFFZCxPQUFPLENBQ0gsT0FBTyxLQUFLLEtBQUssUUFBUSxJQUFJLFlBQVksSUFBSSxLQUFLLElBQUksT0FBTyxJQUFJLEtBQUssQ0FDekUsQ0FBQztBQUNOLENBQUM7QUFORCw0REFNQztBQTRMRCxTQUFnQixpQkFBaUIsQ0FBQyxLQUFZO0lBQzFDLE9BQU8sQ0FDSCxLQUFLLENBQUMsSUFBSSxLQUFLLE9BQU87UUFDdEIsS0FBSyxDQUFDLElBQUksS0FBSyxPQUFPO1FBQ3RCLEtBQUssQ0FBQyxJQUFJLEtBQUssS0FBSztRQUNwQixLQUFLLENBQUMsSUFBSSxLQUFLLGFBQWEsQ0FDL0IsQ0FBQztBQUNOLENBQUM7QUFQRCw4Q0FPQztBQUVELFNBQWdCLGdCQUFnQixDQUFDLEtBQVk7SUFDekMsT0FBTyxLQUFLLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQztBQUNsQyxDQUFDO0FBRkQsNENBRUM7QUFFRCxTQUFnQixnQkFBZ0IsQ0FBQyxLQUFZO0lBQ3pDLE9BQU8sS0FBSyxDQUFDLElBQUksS0FBSyxRQUFRLENBQUM7QUFDbkMsQ0FBQztBQUZELDRDQUVDO0FBRUQsU0FBZ0IsY0FBYyxDQUFDLE1BQWM7SUFDekMsT0FBTyxPQUFPLE1BQU0sS0FBSyxRQUFRLENBQUM7QUFDdEMsQ0FBQztBQUZELHdDQUVDO0FBRUQsU0FBZ0IsaUJBQWlCLENBQzdCLE9BQWlCO0lBRWpCLE9BQU8sS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUEsT0FBTyxhQUFQLE9BQU8sdUJBQVAsT0FBTyxDQUFHLENBQUMsQ0FBQyxDQUFBLEtBQUssUUFBUSxDQUFDO0FBQ3RFLENBQUM7QUFKRCw4Q0FJQztBQUVELFNBQWdCLGFBQWEsQ0FBQyxNQUFjO0lBQ3hDLE9BQU8sT0FBTyxNQUFNLEtBQUssUUFBUSxDQUFDO0FBQ3RDLENBQUM7QUFGRCxzQ0FFQztBQUVELFNBQWdCLGlCQUFpQixDQUFDLEtBQVk7SUFDMUMsT0FBTyxLQUFLLENBQUMsSUFBSSxLQUFLLFFBQVEsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQztBQUNwRSxDQUFDO0FBRkQsOENBRUM7QUFFRCxTQUFnQixnQkFBZ0IsQ0FBQyxLQUFZO0lBQ3pDLE9BQU8sQ0FDSCxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssUUFBUSxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDO1FBQzFELE9BQU8sS0FBSyxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQ3JDLENBQUM7QUFDTixDQUFDO0FBTEQsNENBS0M7QUFFRCxTQUFnQix5QkFBeUIsQ0FDckMsS0FBeUI7SUFFekIsT0FBTyxLQUFLLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQztBQUMvQixDQUFDO0FBSkQsOERBSUM7QUFFRCxTQUFnQixnQkFBZ0IsQ0FDNUIsS0FBeUI7SUFFekIsT0FBTyxNQUFNLElBQUksS0FBSyxJQUFJLENBQUMseUJBQXlCLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDaEUsQ0FBQztBQUpELDRDQUlDO0FBRUQsU0FBZ0IsVUFBVSxDQUFDLEdBQVE7SUFDL0IsT0FBTyxNQUFNLElBQUksR0FBRyxDQUFDO0FBQ3pCLENBQUM7QUFGRCxnQ0FFQztBQUVELFNBQWdCLGdCQUFnQixDQUFDLEtBQWtCO0lBQy9DLE9BQU8sV0FBVyxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsU0FBUyxDQUFDO0FBQ25ELENBQUM7QUFGRCw0Q0FFQyJ9