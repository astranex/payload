"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.point = exports.blocks = exports.radio = exports.select = exports.array = exports.relationship = exports.upload = exports.date = exports.checkbox = exports.richText = exports.code = exports.textarea = exports.email = exports.password = exports.text = exports.number = void 0;
const defaultValue_1 = __importDefault(require("./richText/defaultValue"));
const types_1 = require("./config/types");
const canUseDOM_1 = __importDefault(require("../utilities/canUseDOM"));
const isValidID_1 = require("../utilities/isValidID");
const getIDType_1 = require("../utilities/getIDType");
const number = (value, { required, min, max, adminUILocale }) => {
    const parsedValue = parseFloat(value);
    if ((value && typeof parsedValue !== 'number') ||
        (required && Number.isNaN(parsedValue)) ||
        (value && Number.isNaN(parsedValue))) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.EnterValidNumberLabel) ||
            'Please enter a valid number.');
    }
    if (typeof max === 'number' && parsedValue > max) {
        return `"${value}" ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.NumberGreaterLabel) ||
            'is greater than the max allowed value of'} ${max}.`;
    }
    if (typeof min === 'number' && parsedValue < min) {
        return `"${value}" ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.NumberLessLabel) ||
            'is less than the min allowed value of'} ${min}.`;
    }
    if (required && typeof parsedValue !== 'number') {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
            'This field is required.');
    }
    return true;
};
exports.number = number;
const text = (value, { minLength, maxLength, required, adminUILocale }) => {
    if (value && maxLength && value.length > maxLength) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.TextShorterLabels[0]) ||
            'This value must be shorter than the max length of'} ${maxLength} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.TextShorterLabels[1]) ||
            'characters.'}`;
    }
    if (value && minLength && (value === null || value === void 0 ? void 0 : value.length) < minLength) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.TextLongerLabels[0]) ||
            'This value must be longer than the minimum length of'} ${minLength} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.TextLongerLabels[1]) ||
            'characters.'}`;
    }
    if (required) {
        if (typeof value !== 'string' || (value === null || value === void 0 ? void 0 : value.length) === 0) {
            return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
                'This field is required.');
        }
    }
    return true;
};
exports.text = text;
const password = (value, { required, maxLength, minLength, adminUILocale }) => {
    if (value && maxLength && value.length > maxLength) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.PasswordShorterLabels[0]) ||
            'This value must be shorter than the max length of'} ${maxLength} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.PasswordShorterLabels[1]) ||
            'characters.'}`;
    }
    if (value && minLength && value.length < minLength) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.PasswordLongerLabels[0]) ||
            'This value must be longer than the minimum length of'} ${minLength} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.PasswordLongerLabels[1]) ||
            'characters.'}`;
    }
    if (required && !value) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
            'This field is required.');
    }
    return true;
};
exports.password = password;
const email = (value, { required, adminUILocale }) => {
    if ((value && !/\S+@\S+\.\S+/.test(value)) || (!value && required)) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.EnterValidEmailLabel) ||
            'Please enter a valid email address.');
    }
    return true;
};
exports.email = email;
const textarea = (value, { required, maxLength, minLength, adminUILocale }) => {
    if (value && maxLength && value.length > maxLength) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.TextareaShorterLabels[0]) ||
            'This value must be shorter than the max length of'} ${maxLength} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.TextareaShorterLabels[1]) ||
            'characters.'}`;
    }
    if (value && minLength && value.length < minLength) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.TextareaLongerLabels[0]) ||
            'This value must be longer than the minimum length of'} ${minLength} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.TextareaLongerLabels[1]) ||
            'characters.'}`;
    }
    if (required && !value) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
            'This field is required.');
    }
    return true;
};
exports.textarea = textarea;
const code = (value, { required, adminUILocale }) => {
    if (required && value === undefined) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
            'This field is required.');
    }
    return true;
};
exports.code = code;
const richText = (value, { required, adminUILocale }) => {
    if (required) {
        const stringifiedDefaultValue = JSON.stringify(defaultValue_1.default);
        if (value && JSON.stringify(value) !== stringifiedDefaultValue)
            return true;
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
            'This field is required.');
    }
    return true;
};
exports.richText = richText;
const checkbox = (value, { required, adminUILocale }) => {
    if ((value && typeof value !== 'boolean') ||
        (required && typeof value !== 'boolean')) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.InvalidCheckboxLabel) ||
            'This field can only be equal to true or false.');
    }
    return true;
};
exports.checkbox = checkbox;
const date = (value, { required, adminUILocale }) => {
    if (value && !isNaN(Date.parse(value.toString()))) {
        /* eslint-disable-line */
        return true;
    }
    if (value) {
        return `"${value}" ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.InvalidDateLabel) ||
            'is not a valid date.'}`;
    }
    if (required) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
            'This field is required.');
    }
    return true;
};
exports.date = date;
const validateFilterOptions = async (value, { filterOptions, id, user, data, siblingData, relationTo, payload }) => {
    if (!canUseDOM_1.default && typeof filterOptions !== 'undefined' && value) {
        const options = {};
        const collections = typeof relationTo === 'string' ? [relationTo] : relationTo;
        const values = Array.isArray(value) ? value : [value];
        await Promise.all(collections.map(async (collection) => {
            const optionFilter = typeof filterOptions === 'function'
                ? filterOptions({
                    id,
                    data,
                    siblingData,
                    user,
                    relationTo: collection
                })
                : filterOptions;
            const valueIDs = [];
            values.forEach((val) => {
                if (typeof val === 'object' && (val === null || val === void 0 ? void 0 : val.value)) {
                    valueIDs.push(val.value);
                }
                if (typeof val === 'string' || typeof val === 'number') {
                    valueIDs.push(val);
                }
            });
            const result = await payload.find({
                collection,
                depth: 0,
                where: {
                    and: [{ id: { in: valueIDs } }, optionFilter]
                }
            });
            options[collection] = result.docs.map((doc) => doc.id);
        }));
        const invalidRelationships = values.filter((val) => {
            let collection;
            let requestedID;
            if (typeof relationTo === 'string') {
                collection = relationTo;
                if (typeof val === 'string' || typeof val === 'number') {
                    requestedID = val;
                }
            }
            if (Array.isArray(relationTo) &&
                typeof val === 'object' &&
                (val === null || val === void 0 ? void 0 : val.relationTo)) {
                collection = val.relationTo;
                requestedID = val.value;
            }
            return options[collection].indexOf(requestedID) === -1;
        });
        if (invalidRelationships.length > 0) {
            return invalidRelationships.reduce((err, invalid, i) => {
                return `${err} ${JSON.stringify(invalid)}${invalidRelationships.length === i + 1 ? ',' : ''} `;
            }, 'This field has the following invalid selections:');
        }
        return true;
    }
    return true;
};
const upload = (value, options) => {
    const { adminUILocale } = options;
    if (!value && options.required) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
            'This field is required.');
    }
    if (!canUseDOM_1.default && typeof value !== 'undefined' && value !== null) {
        const idField = options.payload.collections[options.relationTo].config.fields.find((field) => (0, types_1.fieldAffectsData)(field) && field.name === 'id');
        const type = (0, getIDType_1.getIDType)(idField);
        if (!(0, isValidID_1.isValidID)(value, type)) {
            return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.InvalidUploadLabel) ||
                'This field is not a valid upload ID');
        }
    }
    return validateFilterOptions(value, options);
};
exports.upload = upload;
const relationship = async (value, options) => {
    const { adminUILocale } = options;
    if ((!value || (Array.isArray(value) && value.length === 0)) &&
        options.required) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
            'This field is required.');
    }
    if (!canUseDOM_1.default && typeof value !== 'undefined' && value !== null) {
        const values = Array.isArray(value) ? value : [value];
        const invalidRelationships = values.filter((val) => {
            let collection;
            let requestedID;
            if (typeof options.relationTo === 'string') {
                collection = options.relationTo;
                // custom id
                if (typeof val === 'string' || typeof val === 'number') {
                    requestedID = val;
                }
            }
            if (Array.isArray(options.relationTo) &&
                typeof val === 'object' &&
                (val === null || val === void 0 ? void 0 : val.relationTo)) {
                collection = val.relationTo;
                requestedID = val.value;
            }
            const idField = options.payload.collections[collection].config.fields.find((field) => (0, types_1.fieldAffectsData)(field) && field.name === 'id');
            let type;
            if (idField) {
                type = idField.type === 'number' ? 'number' : 'text';
            }
            else {
                type = 'ObjectID';
            }
            return !(0, isValidID_1.isValidID)(requestedID, type);
        });
        if (invalidRelationships.length > 0) {
            return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.InvalidSelectionsLabel) ||
                'This field has the following invalid selections:'} ${invalidRelationships
                .map((err, invalid) => {
                return `${err} ${JSON.stringify(invalid)}`;
            })
                .join(', ')}`;
        }
    }
    return validateFilterOptions(value, options);
};
exports.relationship = relationship;
const array = (value, { minRows, maxRows, required, adminUILocale }) => {
    if (minRows && value < minRows) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.ArrayRequiresAtLeastLabels[0]) ||
            'This field requires at least'} ${minRows} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.ArrayRequiresAtLeastLabels[1]) ||
            'row(s).'}`;
    }
    if (maxRows && value > maxRows) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.ArrayRequiresNoMoreLabels[0]) ||
            'This field requires no more than'} ${maxRows} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.ArrayRequiresNoMoreLabels[1]) ||
            'row(s).'}`;
    }
    if (!value && required) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.ArrayRequiresOneRowLabel) ||
            'This field requires at least one row.');
    }
    return true;
};
exports.array = array;
const select = (value, { options, hasMany, required, adminUILocale }) => {
    if (Array.isArray(value) &&
        value.some((input) => !options.some((option) => option === input ||
            (typeof option !== 'string' && (option === null || option === void 0 ? void 0 : option.value) === input)))) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.InvalidSelectionLabel) ||
            'This field has an invalid selection');
    }
    if (typeof value === 'string' &&
        !options.some((option) => option === value ||
            (typeof option !== 'string' && option.value === value))) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.InvalidSelectionLabel) ||
            'This field has an invalid selection');
    }
    if (required &&
        (typeof value === 'undefined' ||
            value === null ||
            (hasMany && Array.isArray(value) && (value === null || value === void 0 ? void 0 : value.length) === 0))) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
            'This field is required.');
    }
    return true;
};
exports.select = select;
const radio = (value, { options, required, adminUILocale }) => {
    const stringValue = String(value);
    if ((typeof value !== 'undefined' || !required) &&
        options.find((option) => String(typeof option !== 'string' && (option === null || option === void 0 ? void 0 : option.value)) ===
            stringValue))
        return true;
    return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.DefaultMessageLabel) ||
        'This field is required.');
};
exports.radio = radio;
const blocks = (value, { maxRows, minRows, required, adminUILocale }) => {
    if (minRows && value < minRows) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.BlockRequiresAtLeastLabels[0]) ||
            'This field requires at least'} ${minRows} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.BlockRequiresAtLeastLabels[1]) ||
            'row(s).'}`;
    }
    if (maxRows && value > maxRows) {
        return `${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.BlockRequiresNoMoreLabels[0]) ||
            'This field requires no more than'} ${maxRows} ${(adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.BlockRequiresNoMoreLabels[1]) ||
            'row(s).'}`;
    }
    if (!value && required) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.BlockRequiresOneRowLabel) ||
            'This field requires at least one row.');
    }
    return true;
};
exports.blocks = blocks;
const point = (value = ['', ''], { required, adminUILocale }) => {
    const lng = parseFloat(String(value[0]));
    const lat = parseFloat(String(value[1]));
    if (required &&
        ((value[0] &&
            value[1] &&
            typeof lng !== 'number' &&
            typeof lat !== 'number') ||
            Number.isNaN(lng) ||
            Number.isNaN(lat) ||
            (Array.isArray(value) && value.length !== 2))) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.PointRequiresTwoNumbersLabel) ||
            'This field requires two numbers');
    }
    if ((value[1] && Number.isNaN(lng)) || (value[0] && Number.isNaN(lat))) {
        return ((adminUILocale === null || adminUILocale === void 0 ? void 0 : adminUILocale.fields.validations.PointInvalidInputLabel) ||
            'This field has an invalid input');
    }
    return true;
};
exports.point = point;
exports.default = {
    number: exports.number,
    text: exports.text,
    password: exports.password,
    email: exports.email,
    textarea: exports.textarea,
    code: exports.code,
    richText: exports.richText,
    checkbox: exports.checkbox,
    date: exports.date,
    upload: exports.upload,
    relationship: exports.relationship,
    array: exports.array,
    select: exports.select,
    radio: exports.radio,
    blocks: exports.blocks,
    point: exports.point
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGlvbnMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvZmllbGRzL3ZhbGlkYXRpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLDJFQUEyRDtBQUMzRCwwQ0FtQndCO0FBRXhCLHVFQUErQztBQUMvQyxzREFBbUQ7QUFDbkQsc0RBQW1EO0FBRTVDLE1BQU0sTUFBTSxHQUE0QyxDQUMzRCxLQUFhLEVBQ2IsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsRUFDdkMsRUFBRTtJQUNBLE1BQU0sV0FBVyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUV0QyxJQUNJLENBQUMsS0FBSyxJQUFJLE9BQU8sV0FBVyxLQUFLLFFBQVEsQ0FBQztRQUMxQyxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsS0FBSyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsRUFDdEM7UUFDRSxPQUFPLENBQ0gsQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUI7WUFDdkQsOEJBQThCLENBQ2pDLENBQUM7S0FDTDtJQUVELElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxJQUFJLFdBQVcsR0FBRyxHQUFHLEVBQUU7UUFDOUMsT0FBTyxJQUFJLEtBQUssS0FDWixDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLGtCQUFrQjtZQUNwRCwwQ0FDSixJQUFJLEdBQUcsR0FBRyxDQUFDO0tBQ2Q7SUFFRCxJQUFJLE9BQU8sR0FBRyxLQUFLLFFBQVEsSUFBSSxXQUFXLEdBQUcsR0FBRyxFQUFFO1FBQzlDLE9BQU8sSUFBSSxLQUFLLEtBQ1osQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxlQUFlO1lBQ2pELHVDQUNKLElBQUksR0FBRyxHQUFHLENBQUM7S0FDZDtJQUVELElBQUksUUFBUSxJQUFJLE9BQU8sV0FBVyxLQUFLLFFBQVEsRUFBRTtRQUM3QyxPQUFPLENBQ0gsQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUI7WUFDckQseUJBQXlCLENBQzVCLENBQUM7S0FDTDtJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2hCLENBQUMsQ0FBQztBQXZDVyxRQUFBLE1BQU0sVUF1Q2pCO0FBRUssTUFBTSxJQUFJLEdBQTBDLENBQ3ZELEtBQWEsRUFDYixFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUNuRCxFQUFFO0lBQ0EsSUFBSSxLQUFLLElBQUksU0FBUyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsU0FBUyxFQUFFO1FBQ2hELE9BQU8sR0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztZQUN0RCxtREFDSixJQUFJLFNBQVMsSUFDVCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztZQUN0RCxhQUNKLEVBQUUsQ0FBQztLQUNOO0lBRUQsSUFBSSxLQUFLLElBQUksU0FBUyxJQUFJLENBQUEsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLE1BQU0sSUFBRyxTQUFTLEVBQUU7UUFDakQsT0FBTyxHQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQ3JELHNEQUNKLElBQUksU0FBUyxJQUNULENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQ3JELGFBQ0osRUFBRSxDQUFDO0tBQ047SUFFRCxJQUFJLFFBQVEsRUFBRTtRQUNWLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxJQUFJLENBQUEsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLE1BQU0sTUFBSyxDQUFDLEVBQUU7WUFDbEQsT0FBTyxDQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsbUJBQW1CO2dCQUNyRCx5QkFBeUIsQ0FDNUIsQ0FBQztTQUNMO0tBQ0o7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNoQixDQUFDLENBQUM7QUFsQ1csUUFBQSxJQUFJLFFBa0NmO0FBRUssTUFBTSxRQUFRLEdBQTBDLENBQzNELEtBQWEsRUFDYixFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxFQUNuRCxFQUFFO0lBQ0EsSUFBSSxLQUFLLElBQUksU0FBUyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsU0FBUyxFQUFFO1FBQ2hELE9BQU8sR0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztZQUMxRCxtREFDSixJQUFJLFNBQVMsSUFDVCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztZQUMxRCxhQUNKLEVBQUUsQ0FBQztLQUNOO0lBRUQsSUFBSSxLQUFLLElBQUksU0FBUyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsU0FBUyxFQUFFO1FBQ2hELE9BQU8sR0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUN6RCxzREFDSixJQUFJLFNBQVMsSUFDVCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUN6RCxhQUNKLEVBQUUsQ0FBQztLQUNOO0lBRUQsSUFBSSxRQUFRLElBQUksQ0FBQyxLQUFLLEVBQUU7UUFDcEIsT0FBTyxDQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsbUJBQW1CO1lBQ3JELHlCQUF5QixDQUM1QixDQUFDO0tBQ0w7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNoQixDQUFDLENBQUM7QUFoQ1csUUFBQSxRQUFRLFlBZ0NuQjtBQUVLLE1BQU0sS0FBSyxHQUEyQyxDQUN6RCxLQUFhLEVBQ2IsRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLEVBQzdCLEVBQUU7SUFDQSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEVBQUU7UUFDaEUsT0FBTyxDQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsb0JBQW9CO1lBQ3RELHFDQUFxQyxDQUN4QyxDQUFDO0tBQ0w7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNoQixDQUFDLENBQUM7QUFaVyxRQUFBLEtBQUssU0FZaEI7QUFFSyxNQUFNLFFBQVEsR0FBOEMsQ0FDL0QsS0FBYSxFQUNiLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLEVBQ25ELEVBQUU7SUFDQSxJQUFJLEtBQUssSUFBSSxTQUFTLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxTQUFTLEVBQUU7UUFDaEQsT0FBTyxHQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO1lBQzFELG1EQUNKLElBQUksU0FBUyxJQUNULENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO1lBQzFELGFBQ0osRUFBRSxDQUFDO0tBQ047SUFFRCxJQUFJLEtBQUssSUFBSSxTQUFTLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxTQUFTLEVBQUU7UUFDaEQsT0FBTyxHQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1lBQ3pELHNEQUNKLElBQUksU0FBUyxJQUNULENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1lBQ3pELGFBQ0osRUFBRSxDQUFDO0tBQ047SUFFRCxJQUFJLFFBQVEsSUFBSSxDQUFDLEtBQUssRUFBRTtRQUNwQixPQUFPLENBQ0gsQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUI7WUFDckQseUJBQXlCLENBQzVCLENBQUM7S0FDTDtJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2hCLENBQUMsQ0FBQztBQWhDVyxRQUFBLFFBQVEsWUFnQ25CO0FBRUssTUFBTSxJQUFJLEdBQTBDLENBQ3ZELEtBQWEsRUFDYixFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUUsRUFDN0IsRUFBRTtJQUNBLElBQUksUUFBUSxJQUFJLEtBQUssS0FBSyxTQUFTLEVBQUU7UUFDakMsT0FBTyxDQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsbUJBQW1CO1lBQ3JELHlCQUF5QixDQUM1QixDQUFDO0tBQ0w7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNoQixDQUFDLENBQUM7QUFaVyxRQUFBLElBQUksUUFZZjtBQUVLLE1BQU0sUUFBUSxHQUE4QyxDQUMvRCxLQUFLLEVBQ0wsRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLEVBQzdCLEVBQUU7SUFDQSxJQUFJLFFBQVEsRUFBRTtRQUNWLE1BQU0sdUJBQXVCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBb0IsQ0FBQyxDQUFDO1FBQ3JFLElBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssdUJBQXVCO1lBQzFELE9BQU8sSUFBSSxDQUFDO1FBQ2hCLE9BQU8sQ0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLG1CQUFtQjtZQUNyRCx5QkFBeUIsQ0FDNUIsQ0FBQztLQUNMO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDaEIsQ0FBQyxDQUFDO0FBZlcsUUFBQSxRQUFRLFlBZW5CO0FBRUssTUFBTSxRQUFRLEdBQThDLENBQy9ELEtBQWMsRUFDZCxFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUUsRUFDN0IsRUFBRTtJQUNBLElBQ0ksQ0FBQyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssU0FBUyxDQUFDO1FBQ3JDLENBQUMsUUFBUSxJQUFJLE9BQU8sS0FBSyxLQUFLLFNBQVMsQ0FBQyxFQUMxQztRQUNFLE9BQU8sQ0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLG9CQUFvQjtZQUN0RCxnREFBZ0QsQ0FDbkQsQ0FBQztLQUNMO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDaEIsQ0FBQyxDQUFDO0FBZlcsUUFBQSxRQUFRLFlBZW5CO0FBRUssTUFBTSxJQUFJLEdBQTBDLENBQ3ZELEtBQUssRUFDTCxFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUUsRUFDN0IsRUFBRTtJQUNBLElBQUksS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRTtRQUMvQyx5QkFBeUI7UUFDekIsT0FBTyxJQUFJLENBQUM7S0FDZjtJQUVELElBQUksS0FBSyxFQUFFO1FBQ1AsT0FBTyxJQUFJLEtBQUssS0FDWixDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLGdCQUFnQjtZQUNsRCxzQkFDSixFQUFFLENBQUM7S0FDTjtJQUVELElBQUksUUFBUSxFQUFFO1FBQ1YsT0FBTyxDQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsbUJBQW1CO1lBQ3JELHlCQUF5QixDQUM1QixDQUFDO0tBQ0w7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNoQixDQUFDLENBQUM7QUF4QlcsUUFBQSxJQUFJLFFBd0JmO0FBRUYsTUFBTSxxQkFBcUIsR0FBYSxLQUFLLEVBQ3pDLEtBQUssRUFDTCxFQUFFLGFBQWEsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxFQUNyRSxFQUFFO0lBQ0EsSUFBSSxDQUFDLG1CQUFTLElBQUksT0FBTyxhQUFhLEtBQUssV0FBVyxJQUFJLEtBQUssRUFBRTtRQUM3RCxNQUFNLE9BQU8sR0FFVCxFQUFFLENBQUM7UUFFUCxNQUFNLFdBQVcsR0FDYixPQUFPLFVBQVUsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztRQUMvRCxNQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFdEQsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUNiLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxFQUFFO1lBQ2pDLE1BQU0sWUFBWSxHQUNkLE9BQU8sYUFBYSxLQUFLLFVBQVU7Z0JBQy9CLENBQUMsQ0FBQyxhQUFhLENBQUM7b0JBQ1YsRUFBRTtvQkFDRixJQUFJO29CQUNKLFdBQVc7b0JBQ1gsSUFBSTtvQkFDSixVQUFVLEVBQUUsVUFBVTtpQkFDekIsQ0FBQztnQkFDSixDQUFDLENBQUMsYUFBYSxDQUFDO1lBRXhCLE1BQU0sUUFBUSxHQUF3QixFQUFFLENBQUM7WUFFekMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNuQixJQUFJLE9BQU8sR0FBRyxLQUFLLFFBQVEsS0FBSSxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsS0FBSyxDQUFBLEVBQUU7b0JBQ3ZDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM1QjtnQkFFRCxJQUFJLE9BQU8sR0FBRyxLQUFLLFFBQVEsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLEVBQUU7b0JBQ3BELFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ3RCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFSCxNQUFNLE1BQU0sR0FBRyxNQUFNLE9BQU8sQ0FBQyxJQUFJLENBQWE7Z0JBQzFDLFVBQVU7Z0JBQ1YsS0FBSyxFQUFFLENBQUM7Z0JBQ1IsS0FBSyxFQUFFO29CQUNILEdBQUcsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsWUFBWSxDQUFDO2lCQUNoRDthQUNKLENBQUMsQ0FBQztZQUVILE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzNELENBQUMsQ0FBQyxDQUNMLENBQUM7UUFFRixNQUFNLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUMvQyxJQUFJLFVBQWtCLENBQUM7WUFDdkIsSUFBSSxXQUE0QixDQUFDO1lBRWpDLElBQUksT0FBTyxVQUFVLEtBQUssUUFBUSxFQUFFO2dCQUNoQyxVQUFVLEdBQUcsVUFBVSxDQUFDO2dCQUV4QixJQUFJLE9BQU8sR0FBRyxLQUFLLFFBQVEsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLEVBQUU7b0JBQ3BELFdBQVcsR0FBRyxHQUFHLENBQUM7aUJBQ3JCO2FBQ0o7WUFFRCxJQUNJLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDO2dCQUN6QixPQUFPLEdBQUcsS0FBSyxRQUFRO2lCQUN2QixHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsVUFBVSxDQUFBLEVBQ2pCO2dCQUNFLFVBQVUsR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFDO2dCQUM1QixXQUFXLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQzthQUMzQjtZQUVELE9BQU8sT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUMzRCxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksb0JBQW9CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNqQyxPQUFPLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ25ELE9BQU8sR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FDcEMsb0JBQW9CLENBQUMsTUFBTSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFDbEQsR0FBRyxDQUFDO1lBQ1IsQ0FBQyxFQUFFLGtEQUFrRCxDQUFXLENBQUM7U0FDcEU7UUFFRCxPQUFPLElBQUksQ0FBQztLQUNmO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDaEIsQ0FBQyxDQUFDO0FBRUssTUFBTSxNQUFNLEdBQTRDLENBQzNELEtBQWEsRUFDYixPQUFPLEVBQ1QsRUFBRTtJQUNBLE1BQU0sRUFBRSxhQUFhLEVBQUUsR0FBRyxPQUFPLENBQUM7SUFFbEMsSUFBSSxDQUFDLEtBQUssSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO1FBQzVCLE9BQU8sQ0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLG1CQUFtQjtZQUNyRCx5QkFBeUIsQ0FDNUIsQ0FBQztLQUNMO0lBRUQsSUFBSSxDQUFDLG1CQUFTLElBQUksT0FBTyxLQUFLLEtBQUssV0FBVyxJQUFJLEtBQUssS0FBSyxJQUFJLEVBQUU7UUFDOUQsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQ3ZDLE9BQU8sQ0FBQyxVQUFVLENBQ3JCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQ2hCLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFBLHdCQUFnQixFQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUM1RCxDQUFDO1FBQ0YsTUFBTSxJQUFJLEdBQUcsSUFBQSxxQkFBUyxFQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRWhDLElBQUksQ0FBQyxJQUFBLHFCQUFTLEVBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ3pCLE9BQU8sQ0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLGtCQUFrQjtnQkFDcEQscUNBQXFDLENBQ3hDLENBQUM7U0FDTDtLQUNKO0lBRUQsT0FBTyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFDakQsQ0FBQyxDQUFDO0FBOUJXLFFBQUEsTUFBTSxVQThCakI7QUFFSyxNQUFNLFlBQVksR0FJckIsS0FBSyxFQUFFLEtBQXdCLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDNUMsTUFBTSxFQUFFLGFBQWEsRUFBRSxHQUFHLE9BQU8sQ0FBQztJQUNsQyxJQUNJLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDeEQsT0FBTyxDQUFDLFFBQVEsRUFDbEI7UUFDRSxPQUFPLENBQ0gsQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUI7WUFDckQseUJBQXlCLENBQzVCLENBQUM7S0FDTDtJQUVELElBQUksQ0FBQyxtQkFBUyxJQUFJLE9BQU8sS0FBSyxLQUFLLFdBQVcsSUFBSSxLQUFLLEtBQUssSUFBSSxFQUFFO1FBQzlELE1BQU0sTUFBTSxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUV0RCxNQUFNLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUMvQyxJQUFJLFVBQWtCLENBQUM7WUFDdkIsSUFBSSxXQUE0QixDQUFDO1lBRWpDLElBQUksT0FBTyxPQUFPLENBQUMsVUFBVSxLQUFLLFFBQVEsRUFBRTtnQkFDeEMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7Z0JBRWhDLFlBQVk7Z0JBQ1osSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxFQUFFO29CQUNwRCxXQUFXLEdBQUcsR0FBRyxDQUFDO2lCQUNyQjthQUNKO1lBRUQsSUFDSSxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUM7Z0JBQ2pDLE9BQU8sR0FBRyxLQUFLLFFBQVE7aUJBQ3ZCLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxVQUFVLENBQUEsRUFDakI7Z0JBQ0UsVUFBVSxHQUFHLEdBQUcsQ0FBQyxVQUFVLENBQUM7Z0JBQzVCLFdBQVcsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO2FBQzNCO1lBRUQsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQ3ZDLFVBQVUsQ0FDYixDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUNoQixDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBQSx3QkFBZ0IsRUFBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLElBQUksQ0FDNUQsQ0FBQztZQUNGLElBQUksSUFBSSxDQUFDO1lBQ1QsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQzthQUN4RDtpQkFBTTtnQkFDSCxJQUFJLEdBQUcsVUFBVSxDQUFDO2FBQ3JCO1lBRUQsT0FBTyxDQUFDLElBQUEscUJBQVMsRUFBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLG9CQUFvQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDakMsT0FBTyxHQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsc0JBQXNCO2dCQUN4RCxrREFDSixJQUFJLG9CQUFvQjtpQkFDbkIsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxFQUFFO2dCQUNsQixPQUFPLEdBQUcsR0FBRyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztZQUMvQyxDQUFDLENBQUM7aUJBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFZLENBQUM7U0FDL0I7S0FDSjtJQUVELE9BQU8scUJBQXFCLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0FBQ2pELENBQUMsQ0FBQztBQXJFVyxRQUFBLFlBQVksZ0JBcUV2QjtBQUVLLE1BQU0sS0FBSyxHQUEyQyxDQUN6RCxLQUFLLEVBQ0wsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUUsRUFDL0MsRUFBRTtJQUNBLElBQUksT0FBTyxJQUFJLEtBQUssR0FBRyxPQUFPLEVBQUU7UUFDNUIsT0FBTyxHQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO1lBQy9ELDhCQUNKLElBQUksT0FBTyxJQUNQLENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO1lBQy9ELFNBQ0osRUFBRSxDQUFDO0tBQ047SUFFRCxJQUFJLE9BQU8sSUFBSSxLQUFLLEdBQUcsT0FBTyxFQUFFO1FBQzVCLE9BQU8sR0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztZQUM5RCxrQ0FDSixJQUFJLE9BQU8sSUFDUCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztZQUM5RCxTQUNKLEVBQUUsQ0FBQztLQUNOO0lBRUQsSUFBSSxDQUFDLEtBQUssSUFBSSxRQUFRLEVBQUU7UUFDcEIsT0FBTyxDQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsd0JBQXdCO1lBQzFELHVDQUF1QyxDQUMxQyxDQUFDO0tBQ0w7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNoQixDQUFDLENBQUM7QUFoQ1csUUFBQSxLQUFLLFNBZ0NoQjtBQUVLLE1BQU0sTUFBTSxHQUE0QyxDQUMzRCxLQUFLLEVBQ0wsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUUsRUFDL0MsRUFBRTtJQUNBLElBQ0ksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDcEIsS0FBSyxDQUFDLElBQUksQ0FDTixDQUFDLEtBQUssRUFBRSxFQUFFLENBQ04sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUNULENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FDUCxNQUFNLEtBQUssS0FBSztZQUNoQixDQUFDLE9BQU8sTUFBTSxLQUFLLFFBQVEsSUFBSSxDQUFBLE1BQU0sYUFBTixNQUFNLHVCQUFOLE1BQU0sQ0FBRSxLQUFLLE1BQUssS0FBSyxDQUFDLENBQzlELENBQ1IsRUFDSDtRQUNFLE9BQU8sQ0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQjtZQUN2RCxxQ0FBcUMsQ0FDeEMsQ0FBQztLQUNMO0lBRUQsSUFDSSxPQUFPLEtBQUssS0FBSyxRQUFRO1FBQ3pCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FDVCxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQ1AsTUFBTSxLQUFLLEtBQUs7WUFDaEIsQ0FBQyxPQUFPLE1BQU0sS0FBSyxRQUFRLElBQUksTUFBTSxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUMsQ0FDN0QsRUFDSDtRQUNFLE9BQU8sQ0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQjtZQUN2RCxxQ0FBcUMsQ0FDeEMsQ0FBQztLQUNMO0lBRUQsSUFDSSxRQUFRO1FBQ1IsQ0FBQyxPQUFPLEtBQUssS0FBSyxXQUFXO1lBQ3pCLEtBQUssS0FBSyxJQUFJO1lBQ2QsQ0FBQyxPQUFPLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQVksYUFBWixLQUFLLHVCQUFMLEtBQUssQ0FBUyxNQUFNLE1BQUssQ0FBQyxDQUFDLENBQUMsRUFDdkU7UUFDRSxPQUFPLENBQ0gsQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUI7WUFDckQseUJBQXlCLENBQzVCLENBQUM7S0FDTDtJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2hCLENBQUMsQ0FBQztBQWhEVyxRQUFBLE1BQU0sVUFnRGpCO0FBRUssTUFBTSxLQUFLLEdBQTJDLENBQ3pELEtBQUssRUFDTCxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLEVBQ3RDLEVBQUU7SUFDQSxNQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsSUFDSSxDQUFDLE9BQU8sS0FBSyxLQUFLLFdBQVcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMzQyxPQUFPLENBQUMsSUFBSSxDQUNSLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FDUCxNQUFNLENBQUMsT0FBTyxNQUFNLEtBQUssUUFBUSxLQUFJLE1BQU0sYUFBTixNQUFNLHVCQUFOLE1BQU0sQ0FBRSxLQUFLLENBQUEsQ0FBQztZQUNuRCxXQUFXLENBQ2xCO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsT0FBTyxDQUNILENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQU0sQ0FBQyxXQUFXLENBQUMsbUJBQW1CO1FBQ3JELHlCQUF5QixDQUM1QixDQUFDO0FBQ04sQ0FBQyxDQUFDO0FBbEJXLFFBQUEsS0FBSyxTQWtCaEI7QUFFSyxNQUFNLE1BQU0sR0FBMkMsQ0FDMUQsS0FBSyxFQUNMLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLEVBQy9DLEVBQUU7SUFDQSxJQUFJLE9BQU8sSUFBSSxLQUFLLEdBQUcsT0FBTyxFQUFFO1FBQzVCLE9BQU8sR0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztZQUMvRCw4QkFDSixJQUFJLE9BQU8sSUFDUCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztZQUMvRCxTQUNKLEVBQUUsQ0FBQztLQUNOO0lBRUQsSUFBSSxPQUFPLElBQUksS0FBSyxHQUFHLE9BQU8sRUFBRTtRQUM1QixPQUFPLEdBQ0gsQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUM7WUFDOUQsa0NBQ0osSUFBSSxPQUFPLElBQ1AsQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUM7WUFDOUQsU0FDSixFQUFFLENBQUM7S0FDTjtJQUVELElBQUksQ0FBQyxLQUFLLElBQUksUUFBUSxFQUFFO1FBQ3BCLE9BQU8sQ0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLHdCQUF3QjtZQUMxRCx1Q0FBdUMsQ0FDMUMsQ0FBQztLQUNMO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDaEIsQ0FBQyxDQUFDO0FBaENXLFFBQUEsTUFBTSxVQWdDakI7QUFFSyxNQUFNLEtBQUssR0FBMkMsQ0FDekQsUUFBNEMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQ3BELEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUM3QixFQUFFO0lBQ0EsTUFBTSxHQUFHLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pDLE1BQU0sR0FBRyxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN6QyxJQUNJLFFBQVE7UUFDUixDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNOLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDUixPQUFPLEdBQUcsS0FBSyxRQUFRO1lBQ3ZCLE9BQU8sR0FBRyxLQUFLLFFBQVEsQ0FBQztZQUN4QixNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztZQUNqQixNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztZQUNqQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUNuRDtRQUNFLE9BQU8sQ0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLDRCQUE0QjtZQUM5RCxpQ0FBaUMsQ0FDcEMsQ0FBQztLQUNMO0lBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1FBQ3BFLE9BQU8sQ0FDSCxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLHNCQUFzQjtZQUN4RCxpQ0FBaUMsQ0FDcEMsQ0FBQztLQUNMO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDaEIsQ0FBQyxDQUFDO0FBOUJXLFFBQUEsS0FBSyxTQThCaEI7QUFFRixrQkFBZTtJQUNYLE1BQU0sRUFBTixjQUFNO0lBQ04sSUFBSSxFQUFKLFlBQUk7SUFDSixRQUFRLEVBQVIsZ0JBQVE7SUFDUixLQUFLLEVBQUwsYUFBSztJQUNMLFFBQVEsRUFBUixnQkFBUTtJQUNSLElBQUksRUFBSixZQUFJO0lBQ0osUUFBUSxFQUFSLGdCQUFRO0lBQ1IsUUFBUSxFQUFSLGdCQUFRO0lBQ1IsSUFBSSxFQUFKLFlBQUk7SUFDSixNQUFNLEVBQU4sY0FBTTtJQUNOLFlBQVksRUFBWixvQkFBWTtJQUNaLEtBQUssRUFBTCxhQUFLO0lBQ0wsTUFBTSxFQUFOLGNBQU07SUFDTixLQUFLLEVBQUwsYUFBSztJQUNMLE1BQU0sRUFBTixjQUFNO0lBQ04sS0FBSyxFQUFMLGFBQUs7Q0FDUixDQUFDIn0=