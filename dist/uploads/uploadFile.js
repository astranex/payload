"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require(".."));
const file_type_1 = require("file-type");
const mkdirp_1 = __importDefault(require("mkdirp"));
const path_1 = __importDefault(require("path"));
const sanitize_filename_1 = __importDefault(require("sanitize-filename"));
const sharp_1 = __importDefault(require("sharp"));
const errors_1 = require("../errors");
const getImageSize_1 = __importDefault(require("./getImageSize"));
const getSafeFilename_1 = __importDefault(require("./getSafeFilename"));
const imageResizer_1 = __importDefault(require("./imageResizer"));
const saveBufferToFile_1 = __importDefault(require("./saveBufferToFile"));
const canResizeImage_1 = __importDefault(require("./canResizeImage"));
const uploadFile = async ({ config, collection: { config: collectionConfig, Model }, req, data, throwOnMissingFile, overwriteExistingFiles }) => {
    var _a;
    const adminUILocale = __1.default.config.admin.locale;
    let newData = data;
    if (collectionConfig.upload) {
        const fileData = {};
        const { staticDir, imageSizes, disableLocalStorage, resizeOptions, formatOptions } = collectionConfig.upload;
        const { file } = req.files || {};
        if (throwOnMissingFile && !file) {
            throw new errors_1.MissingFile(adminUILocale.errors.MissingFileLabel);
        }
        let staticPath = staticDir;
        if (staticDir.indexOf('/') !== 0) {
            staticPath = path_1.default.resolve(config.paths.configDir, staticDir);
        }
        if (!disableLocalStorage) {
            mkdirp_1.default.sync(staticPath);
        }
        if (file) {
            try {
                const shouldResize = (0, canResizeImage_1.default)(file.mimetype);
                let fsSafeName;
                let resized;
                let dimensions;
                if (shouldResize) {
                    if (resizeOptions) {
                        resized = (0, sharp_1.default)(file.data).resize(resizeOptions);
                    }
                    if (formatOptions) {
                        resized = (resized !== null && resized !== void 0 ? resized : (0, sharp_1.default)(file.data)).toFormat(formatOptions.format, formatOptions.options);
                    }
                    dimensions = await (0, getImageSize_1.default)(file);
                    fileData.width = dimensions.width;
                    fileData.height = dimensions.height;
                }
                const fileBuffer = resized
                    ? await resized.toBuffer()
                    : file.data;
                const { mime, ext } = (_a = (await (0, file_type_1.fromBuffer)(fileBuffer))) !== null && _a !== void 0 ? _a : {
                    mime: file.mimetype,
                    ext: file.name.split('.').pop()
                };
                const fileSize = fileBuffer.length;
                const baseFilename = (0, sanitize_filename_1.default)(file.name.substring(0, file.name.lastIndexOf('.')) ||
                    file.name);
                fsSafeName = `${baseFilename}.${ext}`;
                if (!overwriteExistingFiles) {
                    fsSafeName = await (0, getSafeFilename_1.default)(Model, staticPath, fsSafeName);
                }
                if (!disableLocalStorage) {
                    await (0, saveBufferToFile_1.default)(fileBuffer, `${staticPath}/${fsSafeName}`);
                }
                fileData.filename =
                    fsSafeName ||
                        (!overwriteExistingFiles
                            ? await (0, getSafeFilename_1.default)(Model, staticPath, file.name)
                            : file.name);
                fileData.filesize = fileSize || file.size;
                fileData.mimeType = mime || (await (0, file_type_1.fromBuffer)(file.data)).mime;
                if (Array.isArray(imageSizes) && shouldResize) {
                    req.payloadUploadSizes = {};
                    fileData.sizes = await (0, imageResizer_1.default)({
                        req,
                        file: file.data,
                        dimensions,
                        staticPath,
                        config: collectionConfig,
                        savedFilename: fsSafeName || file.name,
                        mimeType: fileData.mimeType
                    });
                }
            }
            catch (err) {
                console.error(err);
                throw new errors_1.FileUploadError(adminUILocale.errors.FileUploadErrorLabel);
            }
            newData = {
                ...newData,
                ...fileData
            };
        }
    }
    return newData;
};
exports.default = uploadFile;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkRmlsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91cGxvYWRzL3VwbG9hZEZpbGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSwyQ0FBeUI7QUFDekIseUNBQXVDO0FBQ3ZDLG9EQUE0QjtBQUM1QixnREFBd0I7QUFDeEIsMEVBQXlDO0FBQ3pDLGtEQUFxQztBQUdyQyxzQ0FBeUQ7QUFFekQsa0VBQStEO0FBQy9ELHdFQUFnRDtBQUNoRCxrRUFBMkM7QUFDM0MsMEVBQWtEO0FBRWxELHNFQUE4QztBQVc5QyxNQUFNLFVBQVUsR0FBRyxLQUFLLEVBQUUsRUFDdEIsTUFBTSxFQUNOLFVBQVUsRUFBRSxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUUsRUFDL0MsR0FBRyxFQUNILElBQUksRUFDSixrQkFBa0IsRUFDbEIsc0JBQXNCLEVBQ25CLEVBQW9DLEVBQUU7O0lBQ3pDLE1BQU0sYUFBYSxHQUFHLFdBQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUVsRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFFbkIsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7UUFDekIsTUFBTSxRQUFRLEdBQXNCLEVBQUUsQ0FBQztRQUV2QyxNQUFNLEVBQ0YsU0FBUyxFQUNULFVBQVUsRUFDVixtQkFBbUIsRUFDbkIsYUFBYSxFQUNiLGFBQWEsRUFDaEIsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUM7UUFFNUIsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLEdBQUcsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO1FBRWpDLElBQUksa0JBQWtCLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDN0IsTUFBTSxJQUFJLG9CQUFXLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ2hFO1FBRUQsSUFBSSxVQUFVLEdBQUcsU0FBUyxDQUFDO1FBRTNCLElBQUksU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDOUIsVUFBVSxHQUFHLGNBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7U0FDaEU7UUFFRCxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDdEIsZ0JBQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDM0I7UUFFRCxJQUFJLElBQUksRUFBRTtZQUNOLElBQUk7Z0JBQ0EsTUFBTSxZQUFZLEdBQUcsSUFBQSx3QkFBYyxFQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxVQUFrQixDQUFDO2dCQUN2QixJQUFJLE9BQTBCLENBQUM7Z0JBQy9CLElBQUksVUFBMkIsQ0FBQztnQkFDaEMsSUFBSSxZQUFZLEVBQUU7b0JBQ2QsSUFBSSxhQUFhLEVBQUU7d0JBQ2YsT0FBTyxHQUFHLElBQUEsZUFBSyxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7cUJBQ3BEO29CQUNELElBQUksYUFBYSxFQUFFO3dCQUNmLE9BQU8sR0FBRyxDQUFDLE9BQU8sYUFBUCxPQUFPLGNBQVAsT0FBTyxHQUFJLElBQUEsZUFBSyxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FDNUMsYUFBYSxDQUFDLE1BQU0sRUFDcEIsYUFBYSxDQUFDLE9BQU8sQ0FDeEIsQ0FBQztxQkFDTDtvQkFDRCxVQUFVLEdBQUcsTUFBTSxJQUFBLHNCQUFZLEVBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQztvQkFDbEMsUUFBUSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDO2lCQUN2QztnQkFFRCxNQUFNLFVBQVUsR0FBRyxPQUFPO29CQUN0QixDQUFDLENBQUMsTUFBTSxPQUFPLENBQUMsUUFBUSxFQUFFO29CQUMxQixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFFaEIsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsR0FBRyxNQUFBLENBQUMsTUFBTSxJQUFBLHNCQUFVLEVBQUMsVUFBVSxDQUFDLENBQUMsbUNBQUk7b0JBQ3BELElBQUksRUFBRSxJQUFJLENBQUMsUUFBUTtvQkFDbkIsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRTtpQkFDbEMsQ0FBQztnQkFDRixNQUFNLFFBQVEsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDO2dCQUNuQyxNQUFNLFlBQVksR0FBRyxJQUFBLDJCQUFRLEVBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDOUMsSUFBSSxDQUFDLElBQUksQ0FDaEIsQ0FBQztnQkFDRixVQUFVLEdBQUcsR0FBRyxZQUFZLElBQUksR0FBRyxFQUFFLENBQUM7Z0JBRXRDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtvQkFDekIsVUFBVSxHQUFHLE1BQU0sSUFBQSx5QkFBZSxFQUM5QixLQUFLLEVBQ0wsVUFBVSxFQUNWLFVBQVUsQ0FDYixDQUFDO2lCQUNMO2dCQUVELElBQUksQ0FBQyxtQkFBbUIsRUFBRTtvQkFDdEIsTUFBTSxJQUFBLDBCQUFnQixFQUNsQixVQUFVLEVBQ1YsR0FBRyxVQUFVLElBQUksVUFBVSxFQUFFLENBQ2hDLENBQUM7aUJBQ0w7Z0JBRUQsUUFBUSxDQUFDLFFBQVE7b0JBQ2IsVUFBVTt3QkFDVixDQUFDLENBQUMsc0JBQXNCOzRCQUNwQixDQUFDLENBQUMsTUFBTSxJQUFBLHlCQUFlLEVBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDOzRCQUNyRCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxHQUFHLFFBQVEsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUMxQyxRQUFRLENBQUMsUUFBUSxHQUFHLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBQSxzQkFBVSxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFFL0QsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLFlBQVksRUFBRTtvQkFDM0MsR0FBRyxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztvQkFDNUIsUUFBUSxDQUFDLEtBQUssR0FBRyxNQUFNLElBQUEsc0JBQWEsRUFBQzt3QkFDakMsR0FBRzt3QkFDSCxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7d0JBQ2YsVUFBVTt3QkFDVixVQUFVO3dCQUNWLE1BQU0sRUFBRSxnQkFBZ0I7d0JBQ3hCLGFBQWEsRUFBRSxVQUFVLElBQUksSUFBSSxDQUFDLElBQUk7d0JBQ3RDLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUTtxQkFDOUIsQ0FBQyxDQUFDO2lCQUNOO2FBQ0o7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNuQixNQUFNLElBQUksd0JBQWUsQ0FDckIsYUFBYSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FDNUMsQ0FBQzthQUNMO1lBRUQsT0FBTyxHQUFHO2dCQUNOLEdBQUcsT0FBTztnQkFDVixHQUFHLFFBQVE7YUFDZCxDQUFDO1NBQ0w7S0FDSjtJQUVELE9BQU8sT0FBTyxDQUFDO0FBQ25CLENBQUMsQ0FBQztBQUVGLGtCQUFlLFVBQVUsQ0FBQyJ9