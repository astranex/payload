"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Payload = void 0;
const local_1 = __importDefault(require("./collections/operations/local"));
const local_2 = __importDefault(require("./globals/operations/local"));
const crypto_1 = require("./auth/crypto");
const init_1 = require("./init");
require('isomorphic-fetch');
/**
 * @description Payload
 */
class Payload {
    constructor() {
        this.collections = {};
        this.versions = {};
        this.encrypt = crypto_1.encrypt;
        this.decrypt = crypto_1.decrypt;
        this.Query = {
            name: 'Query',
            fields: {}
        };
        this.Mutation = {
            name: 'Mutation',
            fields: {}
        };
        this.errorResponses = [];
        this.getAdminURL = () => `${this.config.serverURL}${this.config.routes.admin}`;
        this.getAPIURL = () => `${this.config.serverURL}${this.config.routes.api}`;
        /**
         * @description Performs create operation
         * @param options
         * @returns created document
         */
        this.create = async (options) => {
            const { create } = local_1.default;
            return create(this, options);
        };
        /**
         * @description Find documents with criteria
         * @param options
         * @returns documents satisfying query
         */
        this.find = async (options) => {
            const { find } = local_1.default;
            return find(this, options);
        };
        this.findGlobal = async (options) => {
            const { findOne } = local_2.default;
            return findOne(this, options);
        };
        this.updateGlobal = async (options) => {
            const { update } = local_2.default;
            return update(this, options);
        };
        /**
         * @description Find global versions with criteria
         * @param options
         * @returns versions satisfying query
         */
        this.findGlobalVersions = async (options) => {
            const { findVersions } = local_2.default;
            return findVersions(this, options);
        };
        /**
         * @description Find global version by ID
         * @param options
         * @returns global version with specified ID
         */
        this.findGlobalVersionByID = async (options) => {
            const { findVersionByID } = local_2.default;
            return findVersionByID(this, options);
        };
        /**
         * @description Restore global version by ID
         * @param options
         * @returns version with specified ID
         */
        this.restoreGlobalVersion = async (options) => {
            const { restoreVersion } = local_2.default;
            return restoreVersion(this, options);
        };
        /**
         * @description Find document by ID
         * @param options
         * @returns document with specified ID
         */
        this.findByID = async (options) => {
            const { findByID } = local_1.default;
            return findByID(this, options);
        };
        /**
         * @description Update document
         * @param options
         * @returns Updated document
         */
        this.update = async (options) => {
            const { update } = local_1.default;
            return update(this, options);
        };
        this.delete = async (options) => {
            const { localDelete } = local_1.default;
            return localDelete(this, options);
        };
        /**
         * @description Find versions with criteria
         * @param options
         * @returns versions satisfying query
         */
        this.findVersions = async (options) => {
            const { findVersions } = local_1.default;
            return findVersions(this, options);
        };
        /**
         * @description Find version by ID
         * @param options
         * @returns version with specified ID
         */
        this.findVersionByID = async (options) => {
            const { findVersionByID } = local_1.default;
            return findVersionByID(this, options);
        };
        /**
         * @description Restore version by ID
         * @param options
         * @returns version with specified ID
         */
        this.restoreVersion = async (options) => {
            const { restoreVersion } = local_1.default;
            return restoreVersion(this, options);
        };
        this.login = async (options) => {
            const { login } = local_1.default.auth;
            return login(this, options);
        };
        this.forgotPassword = async (options) => {
            const { forgotPassword } = local_1.default.auth;
            return forgotPassword(this, options);
        };
        this.resetPassword = async (options) => {
            const { resetPassword } = local_1.default.auth;
            return resetPassword(this, options);
        };
        this.unlock = async (options) => {
            const { unlock } = local_1.default.auth;
            return unlock(this, options);
        };
        this.verifyEmail = async (options) => {
            const { verifyEmail } = local_1.default.auth;
            return verifyEmail(this, options);
        };
    }
    /**
     * @description Initializes Payload
     * @param options
     */
    init(options) {
        (0, init_1.initSync)(this, options);
    }
    async initAsync(options) {
        await (0, init_1.initAsync)(this, options);
    }
}
exports.Payload = Payload;
const payload = new Payload();
exports.default = payload;
module.exports = payload;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBa0JBLDJFQUE2RDtBQUM3RCx1RUFBK0Q7QUFDL0QsMENBQWlEO0FBeUJqRCxpQ0FBNkM7QUFFN0MsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUM7QUFFNUI7O0dBRUc7QUFDSCxNQUFhLE9BQU87SUFBcEI7UUFHSSxnQkFBVyxHQUVQLEVBQUUsQ0FBQztRQUVQLGFBQVEsR0FFSixFQUFFLENBQUM7UUEwQlAsWUFBTyxHQUFHLGdCQUFPLENBQUM7UUFFbEIsWUFBTyxHQUFHLGdCQUFPLENBQUM7UUFhbEIsVUFBSyxHQUFxRDtZQUN0RCxJQUFJLEVBQUUsT0FBTztZQUNiLE1BQU0sRUFBRSxFQUFFO1NBQ2IsQ0FBQztRQUVGLGFBQVEsR0FBcUQ7WUFDekQsSUFBSSxFQUFFLFVBQVU7WUFDaEIsTUFBTSxFQUFFLEVBQUU7U0FDYixDQUFDO1FBVUYsbUJBQWMsR0FBNEIsRUFBRSxDQUFDO1FBZ0I3QyxnQkFBVyxHQUFHLEdBQVcsRUFBRSxDQUN2QixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRTFELGNBQVMsR0FBRyxHQUFXLEVBQUUsQ0FDckIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUV4RDs7OztXQUlHO1FBQ0gsV0FBTSxHQUFHLEtBQUssRUFBVyxPQUF5QixFQUFjLEVBQUU7WUFDOUQsTUFBTSxFQUFFLE1BQU0sRUFBRSxHQUFHLGVBQWUsQ0FBQztZQUNuQyxPQUFPLE1BQU0sQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDakMsQ0FBQyxDQUFDO1FBRUY7Ozs7V0FJRztRQUNILFNBQUksR0FBRyxLQUFLLEVBQ1IsT0FBb0IsRUFDSyxFQUFFO1lBQzNCLE1BQU0sRUFBRSxJQUFJLEVBQUUsR0FBRyxlQUFlLENBQUM7WUFDakMsT0FBTyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQztRQUVGLGVBQVUsR0FBRyxLQUFLLEVBQ2QsT0FBMEIsRUFDaEIsRUFBRTtZQUNaLE1BQU0sRUFBRSxPQUFPLEVBQUUsR0FBRyxlQUFxQixDQUFDO1lBQzFDLE9BQU8sT0FBTyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUM7UUFFRixpQkFBWSxHQUFHLEtBQUssRUFDaEIsT0FBNEIsRUFDbEIsRUFBRTtZQUNaLE1BQU0sRUFBRSxNQUFNLEVBQUUsR0FBRyxlQUFxQixDQUFDO1lBQ3pDLE9BQU8sTUFBTSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNqQyxDQUFDLENBQUM7UUFFRjs7OztXQUlHO1FBQ0gsdUJBQWtCLEdBQUcsS0FBSyxFQUN0QixPQUFrQyxFQUNULEVBQUU7WUFDM0IsTUFBTSxFQUFFLFlBQVksRUFBRSxHQUFHLGVBQXFCLENBQUM7WUFDL0MsT0FBTyxZQUFZLENBQUksSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQzFDLENBQUMsQ0FBQztRQUVGOzs7O1dBSUc7UUFDSCwwQkFBcUIsR0FBRyxLQUFLLEVBQ3pCLE9BQXFDLEVBQzNCLEVBQUU7WUFDWixNQUFNLEVBQUUsZUFBZSxFQUFFLEdBQUcsZUFBcUIsQ0FBQztZQUNsRCxPQUFPLGVBQWUsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDMUMsQ0FBQyxDQUFDO1FBRUY7Ozs7V0FJRztRQUNILHlCQUFvQixHQUFHLEtBQUssRUFDeEIsT0FBb0MsRUFDMUIsRUFBRTtZQUNaLE1BQU0sRUFBRSxjQUFjLEVBQUUsR0FBRyxlQUFxQixDQUFDO1lBQ2pELE9BQU8sY0FBYyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUM7UUFFRjs7OztXQUlHO1FBQ0gsYUFBUSxHQUFHLEtBQUssRUFDWixPQUF3QixFQUNkLEVBQUU7WUFDWixNQUFNLEVBQUUsUUFBUSxFQUFFLEdBQUcsZUFBZSxDQUFDO1lBQ3JDLE9BQU8sUUFBUSxDQUFJLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUM7UUFFRjs7OztXQUlHO1FBQ0gsV0FBTSxHQUFHLEtBQUssRUFBVyxPQUF5QixFQUFjLEVBQUU7WUFDOUQsTUFBTSxFQUFFLE1BQU0sRUFBRSxHQUFHLGVBQWUsQ0FBQztZQUNuQyxPQUFPLE1BQU0sQ0FBSSxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDcEMsQ0FBQyxDQUFDO1FBRUYsV0FBTSxHQUFHLEtBQUssRUFDVixPQUFzQixFQUNaLEVBQUU7WUFDWixNQUFNLEVBQUUsV0FBVyxFQUFFLEdBQUcsZUFBZSxDQUFDO1lBQ3hDLE9BQU8sV0FBVyxDQUFJLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUM7UUFFRjs7OztXQUlHO1FBQ0gsaUJBQVksR0FBRyxLQUFLLEVBQ2hCLE9BQTRCLEVBQ0gsRUFBRTtZQUMzQixNQUFNLEVBQUUsWUFBWSxFQUFFLEdBQUcsZUFBZSxDQUFDO1lBQ3pDLE9BQU8sWUFBWSxDQUFJLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUM7UUFFRjs7OztXQUlHO1FBQ0gsb0JBQWUsR0FBRyxLQUFLLEVBQ25CLE9BQStCLEVBQ3JCLEVBQUU7WUFDWixNQUFNLEVBQUUsZUFBZSxFQUFFLEdBQUcsZUFBZSxDQUFDO1lBQzVDLE9BQU8sZUFBZSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUM7UUFFRjs7OztXQUlHO1FBQ0gsbUJBQWMsR0FBRyxLQUFLLEVBQ2xCLE9BQThCLEVBQ3BCLEVBQUU7WUFDWixNQUFNLEVBQUUsY0FBYyxFQUFFLEdBQUcsZUFBZSxDQUFDO1lBQzNDLE9BQU8sY0FBYyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUM7UUFFRixVQUFLLEdBQUcsS0FBSyxFQUNULE9BQXFCLEVBQ2EsRUFBRTtZQUNwQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEdBQUcsZUFBZSxDQUFDLElBQUksQ0FBQztZQUN2QyxPQUFPLEtBQUssQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDO1FBRUYsbUJBQWMsR0FBRyxLQUFLLEVBQ2xCLE9BQThCLEVBQ0QsRUFBRTtZQUMvQixNQUFNLEVBQUUsY0FBYyxFQUFFLEdBQUcsZUFBZSxDQUFDLElBQUksQ0FBQztZQUNoRCxPQUFPLGNBQWMsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDO1FBRUYsa0JBQWEsR0FBRyxLQUFLLEVBQ2pCLE9BQTZCLEVBQ0QsRUFBRTtZQUM5QixNQUFNLEVBQUUsYUFBYSxFQUFFLEdBQUcsZUFBZSxDQUFDLElBQUksQ0FBQztZQUMvQyxPQUFPLGFBQWEsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDeEMsQ0FBQyxDQUFDO1FBRUYsV0FBTSxHQUFHLEtBQUssRUFBRSxPQUFzQixFQUFvQixFQUFFO1lBQ3hELE1BQU0sRUFBRSxNQUFNLEVBQUUsR0FBRyxlQUFlLENBQUMsSUFBSSxDQUFDO1lBQ3hDLE9BQU8sTUFBTSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNqQyxDQUFDLENBQUM7UUFFRixnQkFBVyxHQUFHLEtBQUssRUFBRSxPQUEyQixFQUFvQixFQUFFO1lBQ2xFLE1BQU0sRUFBRSxXQUFXLEVBQUUsR0FBRyxlQUFlLENBQUMsSUFBSSxDQUFDO1lBQzdDLE9BQU8sV0FBVyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUM7SUFDTixDQUFDO0lBekxHOzs7T0FHRztJQUNILElBQUksQ0FBQyxPQUFvQjtRQUNyQixJQUFBLGVBQVEsRUFBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUVELEtBQUssQ0FBQyxTQUFTLENBQUMsT0FBb0I7UUFDaEMsTUFBTSxJQUFBLGdCQUFTLEVBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Q0ErS0o7QUFqUUQsMEJBaVFDO0FBRUQsTUFBTSxPQUFPLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztBQUU5QixrQkFBZSxPQUFPLENBQUM7QUFDdkIsTUFBTSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMifQ==