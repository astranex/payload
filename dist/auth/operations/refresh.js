"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require("../.."));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const errors_1 = require("../../errors");
const getCookieExpiration_1 = __importDefault(require("../../utilities/getCookieExpiration"));
async function refresh(incomingArgs) {
    let args = incomingArgs;
    // /////////////////////////////////////
    // beforeOperation - Collection
    // /////////////////////////////////////
    await args.collection.config.hooks.beforeOperation.reduce(async (priorHook, hook) => {
        await priorHook;
        args =
            (await hook({
                args,
                operation: 'refresh'
            })) || args;
    }, Promise.resolve());
    // /////////////////////////////////////
    // Refresh
    // /////////////////////////////////////
    const adminUILocale = __1.default.config.admin.locale;
    const { collection: { config: collectionConfig }, req: { payload: { secret, config } } } = args;
    const opts = {
        expiresIn: args.collection.config.auth.tokenExpiration
    };
    if (typeof args.token !== 'string')
        throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
    const payloadResult = jsonwebtoken_1.default.verify(args.token, secret, {});
    delete payloadResult.iat;
    delete payloadResult.exp;
    const refreshedToken = jsonwebtoken_1.default.sign(payloadResult, secret, opts);
    const exp = jsonwebtoken_1.default.decode(refreshedToken)
        .exp;
    if (args.res) {
        const cookieOptions = {
            path: '/',
            httpOnly: true,
            expires: (0, getCookieExpiration_1.default)(collectionConfig.auth.tokenExpiration),
            secure: collectionConfig.auth.cookies.secure,
            sameSite: collectionConfig.auth.cookies.sameSite,
            domain: undefined
        };
        if (collectionConfig.auth.cookies.domain)
            cookieOptions.domain = collectionConfig.auth.cookies.domain;
        args.res.cookie(`${config.cookiePrefix}-token`, refreshedToken, cookieOptions);
    }
    // /////////////////////////////////////
    // After Refresh - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterRefresh.reduce(async (priorHook, hook) => {
        await priorHook;
        args =
            (await hook({
                req: args.req,
                res: args.res,
                exp,
                token: refreshedToken
            })) || args;
    }, Promise.resolve());
    // /////////////////////////////////////
    // Return results
    // /////////////////////////////////////
    return {
        refreshedToken,
        exp,
        user: payloadResult
    };
}
exports.default = refresh;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmcmVzaC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9hdXRoL29wZXJhdGlvbnMvcmVmcmVzaC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDhDQUE0QjtBQUM1QixnRUFBK0I7QUFNL0IseUNBQXlDO0FBQ3pDLDhGQUFzRTtBQWlCdEUsS0FBSyxVQUFVLE9BQU8sQ0FBQyxZQUF1QjtJQUMxQyxJQUFJLElBQUksR0FBRyxZQUFZLENBQUM7SUFFeEIsd0NBQXdDO0lBQ3hDLCtCQUErQjtJQUMvQix3Q0FBd0M7SUFFeEMsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FDckQsS0FBSyxFQUNELFNBQThDLEVBQzlDLElBQXlCLEVBQzNCLEVBQUU7UUFDQSxNQUFNLFNBQVMsQ0FBQztRQUVoQixJQUFJO1lBQ0EsQ0FBQyxNQUFNLElBQUksQ0FBQztnQkFDUixJQUFJO2dCQUNKLFNBQVMsRUFBRSxTQUFTO2FBQ3ZCLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQztJQUNwQixDQUFDLEVBQ0QsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUNwQixDQUFDO0lBRUYsd0NBQXdDO0lBQ3hDLFVBQVU7SUFDVix3Q0FBd0M7SUFFeEMsTUFBTSxhQUFhLEdBQUcsV0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWxELE1BQU0sRUFDRixVQUFVLEVBQUUsRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLEVBQUUsRUFDeEMsR0FBRyxFQUFFLEVBQ0QsT0FBTyxFQUFFLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxFQUM5QixFQUNKLEdBQUcsSUFBSSxDQUFDO0lBRVQsTUFBTSxJQUFJLEdBQUc7UUFDVCxTQUFTLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWU7S0FDekQsQ0FBQztJQUVGLElBQUksT0FBTyxJQUFJLENBQUMsS0FBSyxLQUFLLFFBQVE7UUFBRSxNQUFNLElBQUksa0JBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBRTdGLE1BQU0sYUFBYSxHQUFHLHNCQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEVBQUUsQ0FHdEQsQ0FBQztJQUNGLE9BQU8sYUFBYSxDQUFDLEdBQUcsQ0FBQztJQUN6QixPQUFPLGFBQWEsQ0FBQyxHQUFHLENBQUM7SUFDekIsTUFBTSxjQUFjLEdBQUcsc0JBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM3RCxNQUFNLEdBQUcsR0FBSSxzQkFBRyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQTZCO1NBQzlELEdBQWEsQ0FBQztJQUVuQixJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUU7UUFDVixNQUFNLGFBQWEsR0FBRztZQUNsQixJQUFJLEVBQUUsR0FBRztZQUNULFFBQVEsRUFBRSxJQUFJO1lBQ2QsT0FBTyxFQUFFLElBQUEsNkJBQW1CLEVBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUNuRSxNQUFNLEVBQUUsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNO1lBQzVDLFFBQVEsRUFBRSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVE7WUFDaEQsTUFBTSxFQUFFLFNBQVM7U0FDcEIsQ0FBQztRQUVGLElBQUksZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNO1lBQ3BDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFFaEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQ1gsR0FBRyxNQUFNLENBQUMsWUFBWSxRQUFRLEVBQzlCLGNBQWMsRUFDZCxhQUFhLENBQ2hCLENBQUM7S0FDTDtJQUVELHdDQUF3QztJQUN4Qyw2QkFBNkI7SUFDN0Isd0NBQXdDO0lBRXhDLE1BQU0sZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQzVDLEtBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDdEIsTUFBTSxTQUFTLENBQUM7UUFFaEIsSUFBSTtZQUNBLENBQUMsTUFBTSxJQUFJLENBQUM7Z0JBQ1IsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHO2dCQUNiLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztnQkFDYixHQUFHO2dCQUNILEtBQUssRUFBRSxjQUFjO2FBQ3hCLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQztJQUNwQixDQUFDLEVBQ0QsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUNwQixDQUFDO0lBRUYsd0NBQXdDO0lBQ3hDLGlCQUFpQjtJQUNqQix3Q0FBd0M7SUFFeEMsT0FBTztRQUNILGNBQWM7UUFDZCxHQUFHO1FBQ0gsSUFBSSxFQUFFLGFBQWE7S0FDdEIsQ0FBQztBQUNOLENBQUM7QUFFRCxrQkFBZSxPQUFPLENBQUMifQ==