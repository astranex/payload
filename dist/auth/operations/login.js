"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require("../.."));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const errors_1 = require("../../errors");
const getCookieExpiration_1 = __importDefault(require("../../utilities/getCookieExpiration"));
const isLocked_1 = __importDefault(require("../isLocked"));
const sanitizeInternalFields_1 = __importDefault(require("../../utilities/sanitizeInternalFields"));
const types_1 = require("../../fields/config/types");
const afterRead_1 = require("../../fields/hooks/afterRead");
const unlock_1 = __importDefault(require("./unlock"));
async function login(incomingArgs) {
    let args = incomingArgs;
    // /////////////////////////////////////
    // beforeOperation - Collection
    // /////////////////////////////////////
    await args.collection.config.hooks.beforeOperation.reduce(async (priorHook, hook) => {
        await priorHook;
        args =
            (await hook({
                args,
                operation: 'login'
            })) || args;
    }, Promise.resolve());
    const adminUILocale = __1.default.config.admin.locale;
    const { collection: { Model, config: collectionConfig }, data, req: { payload: { secret, config } }, req, depth, overrideAccess, showHiddenFields } = args;
    // /////////////////////////////////////
    // Login
    // /////////////////////////////////////
    const { email: unsanitizedEmail, password } = data;
    const email = unsanitizedEmail
        ? unsanitizedEmail.toLowerCase().trim()
        : null;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore Improper typing in library, additional args should be optional
    const userDoc = await Model.findByUsername(email);
    if (!userDoc ||
        (args.collection.config.auth.verify && userDoc._verified === false)) {
        throw new errors_1.AuthenticationError(adminUILocale.errors.AuthenticationErrorLabel);
    }
    if (userDoc && (0, isLocked_1.default)(userDoc.lockUntil)) {
        throw new errors_1.LockedAuth(adminUILocale.errors.LockedAuthLabel);
    }
    const authResult = await userDoc.authenticate(password);
    const maxLoginAttemptsEnabled = args.collection.config.auth.maxLoginAttempts > 0;
    if (!authResult.user) {
        if (maxLoginAttemptsEnabled)
            await userDoc.incLoginAttempts();
        throw new errors_1.AuthenticationError(adminUILocale.errors.AuthenticationErrorLabel);
    }
    if (maxLoginAttemptsEnabled) {
        await (0, unlock_1.default)({
            collection: {
                Model,
                config: collectionConfig
            },
            req,
            data,
            overrideAccess: true
        });
    }
    let user = userDoc.toJSON({ virtuals: true });
    user = JSON.parse(JSON.stringify(user));
    user = (0, sanitizeInternalFields_1.default)(user);
    const fieldsToSign = collectionConfig.fields.reduce((signedFields, field) => {
        const result = {
            ...signedFields
        };
        if (!(0, types_1.fieldAffectsData)(field) && (0, types_1.fieldHasSubFields)(field)) {
            field.fields.forEach((subField) => {
                if ((0, types_1.fieldAffectsData)(subField) && subField.saveToJWT) {
                    result[subField.name] = user[subField.name];
                }
            });
        }
        if ((0, types_1.fieldAffectsData)(field) && field.saveToJWT) {
            result[field.name] = user[field.name];
        }
        return result;
    }, {
        email,
        id: user.id,
        collection: collectionConfig.slug
    });
    const token = jsonwebtoken_1.default.sign(fieldsToSign, secret, {
        expiresIn: collectionConfig.auth.tokenExpiration
    });
    if (args.res) {
        const cookieOptions = {
            path: '/',
            httpOnly: true,
            expires: (0, getCookieExpiration_1.default)(collectionConfig.auth.tokenExpiration),
            secure: collectionConfig.auth.cookies.secure,
            sameSite: collectionConfig.auth.cookies.sameSite,
            domain: undefined
        };
        if (collectionConfig.auth.cookies.domain)
            cookieOptions.domain = collectionConfig.auth.cookies.domain;
        args.res.cookie(`${config.cookiePrefix}-token`, token, cookieOptions);
    }
    req.user = user;
    // /////////////////////////////////////
    // afterLogin - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterLogin.reduce(async (priorHook, hook) => {
        await priorHook;
        user =
            (await hook({
                doc: user,
                req: args.req,
                token
            })) || user;
    }, Promise.resolve());
    // /////////////////////////////////////
    // afterRead - Fields
    // /////////////////////////////////////
    user = await (0, afterRead_1.afterRead)({
        depth,
        doc: user,
        entityConfig: collectionConfig,
        overrideAccess,
        req,
        showHiddenFields
    });
    // /////////////////////////////////////
    // afterRead - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterRead.reduce(async (priorHook, hook) => {
        await priorHook;
        user =
            (await hook({
                req,
                doc: user
            })) || user;
    }, Promise.resolve());
    // /////////////////////////////////////
    // Return results
    // /////////////////////////////////////
    return {
        token,
        user,
        exp: jsonwebtoken_1.default.decode(token).exp
    };
}
exports.default = login;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvYXV0aC9vcGVyYXRpb25zL2xvZ2luLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsOENBQTRCO0FBQzVCLGdFQUErQjtBQUUvQix5Q0FBK0Q7QUFFL0QsOEZBQXNFO0FBQ3RFLDJEQUFtQztBQUNuQyxvR0FBNEU7QUFDNUUscURBSW1DO0FBR25DLDREQUF5RDtBQUN6RCxzREFBOEI7QUFxQjlCLEtBQUssVUFBVSxLQUFLLENBQ2hCLFlBQXVCO0lBRXZCLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQztJQUV4Qix3Q0FBd0M7SUFDeEMsK0JBQStCO0lBQy9CLHdDQUF3QztJQUV4QyxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUNyRCxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFO1FBQ3RCLE1BQU0sU0FBUyxDQUFDO1FBRWhCLElBQUk7WUFDQSxDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUNSLElBQUk7Z0JBQ0osU0FBUyxFQUFFLE9BQU87YUFDckIsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDO0lBQ3BCLENBQUMsRUFDRCxPQUFPLENBQUMsT0FBTyxFQUFFLENBQ3BCLENBQUM7SUFFRixNQUFNLGFBQWEsR0FBRyxXQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFFbEQsTUFBTSxFQUNGLFVBQVUsRUFBRSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLEVBQUUsRUFDL0MsSUFBSSxFQUNKLEdBQUcsRUFBRSxFQUNELE9BQU8sRUFBRSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsRUFDOUIsRUFDRCxHQUFHLEVBQ0gsS0FBSyxFQUNMLGNBQWMsRUFDZCxnQkFBZ0IsRUFDbkIsR0FBRyxJQUFJLENBQUM7SUFFVCx3Q0FBd0M7SUFDeEMsUUFBUTtJQUNSLHdDQUF3QztJQUV4QyxNQUFNLEVBQUUsS0FBSyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsRUFBRSxHQUFHLElBQUksQ0FBQztJQUVuRCxNQUFNLEtBQUssR0FBRyxnQkFBZ0I7UUFDMUIsQ0FBQyxDQUFFLGdCQUEyQixDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksRUFBRTtRQUNuRCxDQUFDLENBQUMsSUFBSSxDQUFDO0lBRVgsNkRBQTZEO0lBQzdELDRFQUE0RTtJQUM1RSxNQUFNLE9BQU8sR0FBRyxNQUFNLEtBQUssQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFbEQsSUFDSSxDQUFDLE9BQU87UUFDUixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksT0FBTyxDQUFDLFNBQVMsS0FBSyxLQUFLLENBQUMsRUFDckU7UUFDRSxNQUFNLElBQUksNEJBQW1CLENBQ3pCLGFBQWEsQ0FBQyxNQUFNLENBQUMsd0JBQXdCLENBQ2hELENBQUM7S0FDTDtJQUVELElBQUksT0FBTyxJQUFJLElBQUEsa0JBQVEsRUFBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7UUFDeEMsTUFBTSxJQUFJLG1CQUFVLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztLQUM5RDtJQUVELE1BQU0sVUFBVSxHQUFHLE1BQU0sT0FBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUV4RCxNQUFNLHVCQUF1QixHQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO0lBRXJELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFO1FBQ2xCLElBQUksdUJBQXVCO1lBQUUsTUFBTSxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM5RCxNQUFNLElBQUksNEJBQW1CLENBQ3pCLGFBQWEsQ0FBQyxNQUFNLENBQUMsd0JBQXdCLENBQ2hELENBQUM7S0FDTDtJQUVELElBQUksdUJBQXVCLEVBQUU7UUFDekIsTUFBTSxJQUFBLGdCQUFNLEVBQUM7WUFDVCxVQUFVLEVBQUU7Z0JBQ1IsS0FBSztnQkFDTCxNQUFNLEVBQUUsZ0JBQWdCO2FBQzNCO1lBQ0QsR0FBRztZQUNILElBQUk7WUFDSixjQUFjLEVBQUUsSUFBSTtTQUN2QixDQUFDLENBQUM7S0FDTjtJQUVELElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUM5QyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDeEMsSUFBSSxHQUFHLElBQUEsZ0NBQXNCLEVBQUMsSUFBSSxDQUFDLENBQUM7SUFFcEMsTUFBTSxZQUFZLEdBQUcsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FDL0MsQ0FBQyxZQUFZLEVBQUUsS0FBWSxFQUFFLEVBQUU7UUFDM0IsTUFBTSxNQUFNLEdBQUc7WUFDWCxHQUFHLFlBQVk7U0FDbEIsQ0FBQztRQUVGLElBQUksQ0FBQyxJQUFBLHdCQUFnQixFQUFDLEtBQUssQ0FBQyxJQUFJLElBQUEseUJBQWlCLEVBQUMsS0FBSyxDQUFDLEVBQUU7WUFDdEQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtnQkFDOUIsSUFBSSxJQUFBLHdCQUFnQixFQUFDLFFBQVEsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxTQUFTLEVBQUU7b0JBQ2xELE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDL0M7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxJQUFBLHdCQUFnQixFQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDNUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3pDO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQyxFQUNEO1FBQ0ksS0FBSztRQUNMLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTtRQUNYLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQyxJQUFJO0tBQ3BDLENBQ0osQ0FBQztJQUVGLE1BQU0sS0FBSyxHQUFHLHNCQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUU7UUFDekMsU0FBUyxFQUFFLGdCQUFnQixDQUFDLElBQUksQ0FBQyxlQUFlO0tBQ25ELENBQUMsQ0FBQztJQUVILElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRTtRQUNWLE1BQU0sYUFBYSxHQUFrQjtZQUNqQyxJQUFJLEVBQUUsR0FBRztZQUNULFFBQVEsRUFBRSxJQUFJO1lBQ2QsT0FBTyxFQUFFLElBQUEsNkJBQW1CLEVBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUNuRSxNQUFNLEVBQUUsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNO1lBQzVDLFFBQVEsRUFBRSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVE7WUFDaEQsTUFBTSxFQUFFLFNBQVM7U0FDcEIsQ0FBQztRQUVGLElBQUksZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNO1lBQ3BDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFFaEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxNQUFNLENBQUMsWUFBWSxRQUFRLEVBQUUsS0FBSyxFQUFFLGFBQWEsQ0FBQyxDQUFDO0tBQ3pFO0lBRUQsR0FBRyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7SUFFaEIsd0NBQXdDO0lBQ3hDLDBCQUEwQjtJQUMxQix3Q0FBd0M7SUFFeEMsTUFBTSxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFO1FBQ3JFLE1BQU0sU0FBUyxDQUFDO1FBRWhCLElBQUk7WUFDQSxDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUNSLEdBQUcsRUFBRSxJQUFJO2dCQUNULEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztnQkFDYixLQUFLO2FBQ1IsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDO0lBQ3BCLENBQUMsRUFBRSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV0Qix3Q0FBd0M7SUFDeEMscUJBQXFCO0lBQ3JCLHdDQUF3QztJQUV4QyxJQUFJLEdBQUcsTUFBTSxJQUFBLHFCQUFTLEVBQUM7UUFDbkIsS0FBSztRQUNMLEdBQUcsRUFBRSxJQUFJO1FBQ1QsWUFBWSxFQUFFLGdCQUFnQjtRQUM5QixjQUFjO1FBQ2QsR0FBRztRQUNILGdCQUFnQjtLQUNuQixDQUFDLENBQUM7SUFFSCx3Q0FBd0M7SUFDeEMseUJBQXlCO0lBQ3pCLHdDQUF3QztJQUV4QyxNQUFNLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDcEUsTUFBTSxTQUFTLENBQUM7UUFFaEIsSUFBSTtZQUNBLENBQUMsTUFBTSxJQUFJLENBQUM7Z0JBQ1IsR0FBRztnQkFDSCxHQUFHLEVBQUUsSUFBSTthQUNaLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQztJQUNwQixDQUFDLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFFdEIsd0NBQXdDO0lBQ3hDLGlCQUFpQjtJQUNqQix3Q0FBd0M7SUFFeEMsT0FBTztRQUNILEtBQUs7UUFDTCxJQUFJO1FBQ0osR0FBRyxFQUFHLHNCQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBb0IsQ0FBQyxHQUFHO0tBQ2pELENBQUM7QUFDTixDQUFDO0FBRUQsa0JBQWUsS0FBSyxDQUFDIn0=