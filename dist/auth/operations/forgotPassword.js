"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require("../.."));
const crypto_1 = __importDefault(require("crypto"));
const errors_1 = require("../../errors");
async function forgotPassword(incomingArgs) {
    const adminUILocale = __1.default.config.admin.locale;
    if (!Object.prototype.hasOwnProperty.call(incomingArgs.data, 'email')) {
        throw new errors_1.APIError(adminUILocale.operations.ForgotPassword.MissingEmailLabel, 400);
    }
    let args = incomingArgs;
    // /////////////////////////////////////
    // beforeOperation - Collection
    // /////////////////////////////////////
    await args.collection.config.hooks.beforeOperation.reduce(async (priorHook, hook) => {
        await priorHook;
        args =
            (await hook({
                args,
                operation: 'forgotPassword'
            })) || args;
    }, Promise.resolve());
    const { collection: { Model, config: collectionConfig }, data, disableEmail, expiration, req: { payload: { config, sendEmail: email, emailOptions } }, req } = args;
    // /////////////////////////////////////
    // Forget password
    // /////////////////////////////////////
    let token = crypto_1.default.randomBytes(20);
    token = token.toString('hex');
    const user = await Model.findOne({
        email: data.email.toLowerCase()
    });
    if (!user)
        return null;
    user.resetPasswordToken = token;
    user.resetPasswordExpiration = expiration || Date.now() + 3600000; // 1 hour
    await user.save();
    const userJSON = user.toJSON({ virtuals: true });
    if (!disableEmail) {
        let html = `${adminUILocale.operations.ForgotPassword.HTMLLabels[0]}
    <a href="${config.serverURL}${config.routes.admin}/reset/${token}">
     ${config.serverURL}${config.routes.admin}/reset/${token}
    </a>
    ${adminUILocale.operations.ForgotPassword.HTMLLabels[1]}`;
        if (typeof collectionConfig.auth.forgotPassword.generateEmailHTML ===
            'function') {
            html = await collectionConfig.auth.forgotPassword.generateEmailHTML({
                req,
                token,
                user: userJSON
            });
        }
        let subject = adminUILocale.operations.ForgotPassword.ResetPasswordLabel;
        if (typeof collectionConfig.auth.forgotPassword.generateEmailSubject ===
            'function') {
            subject =
                await collectionConfig.auth.forgotPassword.generateEmailSubject({
                    req,
                    token,
                    user: userJSON
                });
        }
        email({
            from: `"${emailOptions.fromName}" <${emailOptions.fromAddress}>`,
            to: data.email,
            subject,
            html
        });
    }
    // /////////////////////////////////////
    // afterForgotPassword - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterForgotPassword.reduce(async (priorHook, hook) => {
        await priorHook;
        await hook({ args });
    }, Promise.resolve());
    return token;
}
exports.default = forgotPassword;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9yZ290UGFzc3dvcmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvYXV0aC9vcGVyYXRpb25zL2ZvcmdvdFBhc3N3b3JkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsOENBQTRCO0FBQzVCLG9EQUE0QjtBQUU1Qix5Q0FBd0M7QUFpQnhDLEtBQUssVUFBVSxjQUFjLENBQUMsWUFBdUI7SUFDakQsTUFBTSxhQUFhLEdBQUcsV0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWxELElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsRUFBRTtRQUNuRSxNQUFNLElBQUksaUJBQVEsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLENBQUMsQ0FBQztLQUN0RjtJQUVELElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQztJQUV4Qix3Q0FBd0M7SUFDeEMsK0JBQStCO0lBQy9CLHdDQUF3QztJQUV4QyxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUNyRCxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFO1FBQ3RCLE1BQU0sU0FBUyxDQUFDO1FBRWhCLElBQUk7WUFDQSxDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUNSLElBQUk7Z0JBQ0osU0FBUyxFQUFFLGdCQUFnQjthQUM5QixDQUFDLENBQUMsSUFBSSxJQUFJLENBQUM7SUFDcEIsQ0FBQyxFQUNELE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FDcEIsQ0FBQztJQUVGLE1BQU0sRUFDRixVQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLGdCQUFnQixFQUFFLEVBQy9DLElBQUksRUFDSixZQUFZLEVBQ1osVUFBVSxFQUNWLEdBQUcsRUFBRSxFQUNELE9BQU8sRUFBRSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxFQUN0RCxFQUNELEdBQUcsRUFDTixHQUFHLElBQUksQ0FBQztJQUVULHdDQUF3QztJQUN4QyxrQkFBa0I7SUFDbEIsd0NBQXdDO0lBRXhDLElBQUksS0FBSyxHQUFvQixnQkFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNwRCxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQU05QixNQUFNLElBQUksR0FBWSxNQUFNLEtBQUssQ0FBQyxPQUFPLENBQUM7UUFDdEMsS0FBSyxFQUFHLElBQUksQ0FBQyxLQUFnQixDQUFDLFdBQVcsRUFBRTtLQUM5QyxDQUFDLENBQUM7SUFFSCxJQUFJLENBQUMsSUFBSTtRQUFFLE9BQU8sSUFBSSxDQUFDO0lBRXZCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7SUFDaEMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLFVBQVUsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsT0FBTyxDQUFDLENBQUMsU0FBUztJQUU1RSxNQUFNLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUVsQixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFFakQsSUFBSSxDQUFDLFlBQVksRUFBRTtRQUNmLElBQUksSUFBSSxHQUFHLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztlQUM1RCxNQUFNLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxVQUFVLEtBQUs7T0FDN0QsTUFBTSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssVUFBVSxLQUFLOztNQUV0RCxhQUFhLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUV0RCxJQUNJLE9BQU8sZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUI7WUFDN0QsVUFBVSxFQUNaO1lBQ0UsSUFBSSxHQUFHLE1BQU0sZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FDL0Q7Z0JBQ0ksR0FBRztnQkFDSCxLQUFLO2dCQUNMLElBQUksRUFBRSxRQUFRO2FBQ2pCLENBQ0osQ0FBQztTQUNMO1FBRUQsSUFBSSxPQUFPLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUM7UUFFekUsSUFDSSxPQUFPLGdCQUFnQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CO1lBQ2hFLFVBQVUsRUFDWjtZQUNFLE9BQU87Z0JBQ0gsTUFBTSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUMzRDtvQkFDSSxHQUFHO29CQUNILEtBQUs7b0JBQ0wsSUFBSSxFQUFFLFFBQVE7aUJBQ2pCLENBQ0osQ0FBQztTQUNUO1FBRUQsS0FBSyxDQUFDO1lBQ0YsSUFBSSxFQUFFLElBQUksWUFBWSxDQUFDLFFBQVEsTUFBTSxZQUFZLENBQUMsV0FBVyxHQUFHO1lBQ2hFLEVBQUUsRUFBRSxJQUFJLENBQUMsS0FBSztZQUNkLE9BQU87WUFDUCxJQUFJO1NBQ1AsQ0FBQyxDQUFDO0tBQ047SUFFRCx3Q0FBd0M7SUFDeEMsbUNBQW1DO0lBQ25DLHdDQUF3QztJQUV4QyxNQUFNLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQ25ELEtBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDdEIsTUFBTSxTQUFTLENBQUM7UUFDaEIsTUFBTSxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3pCLENBQUMsRUFDRCxPQUFPLENBQUMsT0FBTyxFQUFFLENBQ3BCLENBQUM7SUFFRixPQUFPLEtBQUssQ0FBQztBQUNqQixDQUFDO0FBRUQsa0JBQWUsY0FBYyxDQUFDIn0=