"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../../errors");
async function registerFirstUser(args) {
    const { collection: { Model, config: { slug, auth: { verify } } }, req: { payload }, req, data } = args;
    const adminUILocale = payload.config.admin.locale;
    const count = await Model.countDocuments({});
    if (count >= 1)
        throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
    // /////////////////////////////////////
    // Register first user
    // /////////////////////////////////////
    const result = await payload.create({
        req,
        collection: slug,
        data,
        overrideAccess: true
    });
    // auto-verify (if applicable)
    if (verify) {
        await payload.update({
            id: result.id,
            collection: slug,
            data: {
                _verified: true
            }
        });
    }
    // /////////////////////////////////////
    // Log in new user
    // /////////////////////////////////////
    const { token } = await payload.login({
        ...args,
        collection: slug
    });
    const resultToReturn = {
        ...result,
        token
    };
    return {
        message: adminUILocale.operations.RegisterFirstUser.RegisteredSuccessfullyLabel,
        user: resultToReturn
    };
}
exports.default = registerFirstUser;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaXN0ZXJGaXJzdFVzZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvYXV0aC9vcGVyYXRpb25zL3JlZ2lzdGVyRmlyc3RVc2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0EseUNBQXlDO0FBbUJ6QyxLQUFLLFVBQVUsaUJBQWlCLENBQUMsSUFBZTtJQUM1QyxNQUFNLEVBQ0YsVUFBVSxFQUFFLEVBQ1IsS0FBSyxFQUNMLE1BQU0sRUFBRSxFQUNKLElBQUksRUFDSixJQUFJLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFDbkIsRUFDSixFQUNELEdBQUcsRUFBRSxFQUFFLE9BQU8sRUFBRSxFQUNoQixHQUFHLEVBQ0gsSUFBSSxFQUNQLEdBQUcsSUFBSSxDQUFDO0lBRVQsTUFBTSxhQUFhLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWxELE1BQU0sS0FBSyxHQUFHLE1BQU0sS0FBSyxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUU3QyxJQUFJLEtBQUssSUFBSSxDQUFDO1FBQUUsTUFBTSxJQUFJLGtCQUFTLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUV6RSx3Q0FBd0M7SUFDeEMsc0JBQXNCO0lBQ3RCLHdDQUF3QztJQUV4QyxNQUFNLE1BQU0sR0FBRyxNQUFNLE9BQU8sQ0FBQyxNQUFNLENBQWE7UUFDNUMsR0FBRztRQUNILFVBQVUsRUFBRSxJQUFJO1FBQ2hCLElBQUk7UUFDSixjQUFjLEVBQUUsSUFBSTtLQUN2QixDQUFDLENBQUM7SUFFSCw4QkFBOEI7SUFDOUIsSUFBSSxNQUFNLEVBQUU7UUFDUixNQUFNLE9BQU8sQ0FBQyxNQUFNLENBQUM7WUFDakIsRUFBRSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ2IsVUFBVSxFQUFFLElBQUk7WUFDaEIsSUFBSSxFQUFFO2dCQUNGLFNBQVMsRUFBRSxJQUFJO2FBQ2xCO1NBQ0osQ0FBQyxDQUFDO0tBQ047SUFFRCx3Q0FBd0M7SUFDeEMsa0JBQWtCO0lBQ2xCLHdDQUF3QztJQUV4QyxNQUFNLEVBQUUsS0FBSyxFQUFFLEdBQUcsTUFBTSxPQUFPLENBQUMsS0FBSyxDQUFDO1FBQ2xDLEdBQUcsSUFBSTtRQUNQLFVBQVUsRUFBRSxJQUFJO0tBQ25CLENBQUMsQ0FBQztJQUVILE1BQU0sY0FBYyxHQUFHO1FBQ25CLEdBQUcsTUFBTTtRQUNULEtBQUs7S0FDUixDQUFDO0lBRUYsT0FBTztRQUNILE9BQU8sRUFBRSxhQUFhLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLDJCQUEyQjtRQUMvRSxJQUFJLEVBQUUsY0FBYztLQUN2QixDQUFDO0FBQ04sQ0FBQztBQUVELGtCQUFlLGlCQUFpQixDQUFDIn0=