"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require(".."));
const errors_1 = require("../errors");
const executeAccess = async (operation, access) => {
    const adminUILocale = __1.default.config.admin.locale;
    if (access) {
        const result = await access(operation);
        if (!result) {
            if (!operation.disableErrors)
                throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
        }
        return result;
    }
    if (operation.req.user) {
        return true;
    }
    if (!operation.disableErrors)
        throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
    return false;
};
exports.default = executeAccess;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhlY3V0ZUFjY2Vzcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hdXRoL2V4ZWN1dGVBY2Nlc3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSwyQ0FBeUI7QUFDekIsc0NBQXNDO0FBR3RDLE1BQU0sYUFBYSxHQUFHLEtBQUssRUFDdkIsU0FBUyxFQUNULE1BQWMsRUFDTyxFQUFFO0lBQ3ZCLE1BQU0sYUFBYSxHQUFHLFdBQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUVsRCxJQUFJLE1BQU0sRUFBRTtRQUNSLE1BQU0sTUFBTSxHQUFHLE1BQU0sTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXZDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDVCxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWE7Z0JBQ3hCLE1BQU0sSUFBSSxrQkFBUyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDaEU7UUFFRCxPQUFPLE1BQU0sQ0FBQztLQUNqQjtJQUVELElBQUksU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDcEIsT0FBTyxJQUFJLENBQUM7S0FDZjtJQUVELElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYTtRQUN4QixNQUFNLElBQUksa0JBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQzdELE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUMsQ0FBQztBQUVGLGtCQUFlLGFBQWEsQ0FBQyJ9