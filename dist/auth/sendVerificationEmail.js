"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
async function sendVerificationEmail(args) {
    // Verify token from e-mail
    const { config, emailOptions, sendEmail, collection: { config: collectionConfig }, user, disableEmail, req, token } = args;
    const adminUILocale = config.admin.locale;
    if (!disableEmail) {
        const defaultVerificationURL = `${config.serverURL}${config.routes.admin}/${collectionConfig.slug}/verify/${token}`;
        let html = `${adminUILocale.operations.SendVerificationEmail.HTMLLabels[0]}${config.serverURL}${adminUILocale.operations.SendVerificationEmail.HTMLLabels[1]}${config.serverURL}${adminUILocale.operations.SendVerificationEmail.HTMLLabels[2]}${defaultVerificationURL}${adminUILocale.operations.SendVerificationEmail.HTMLLabels[3]}${defaultVerificationURL}${adminUILocale.operations.SendVerificationEmail.HTMLLabels[4]}`;
        const verify = collectionConfig.auth.verify;
        // Allow config to override email content
        if (typeof verify.generateEmailHTML === 'function') {
            html = await verify.generateEmailHTML({
                req,
                token,
                user
            });
        }
        let subject = adminUILocale.operations.SendVerificationEmail.SubjectLabel;
        // Allow config to override email subject
        if (typeof verify.generateEmailSubject === 'function') {
            subject = await verify.generateEmailSubject({
                req,
                token,
                user
            });
        }
        sendEmail({
            from: `"${emailOptions.fromName}" <${emailOptions.fromAddress}>`,
            to: user.email,
            subject,
            html
        });
    }
}
exports.default = sendVerificationEmail;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VuZFZlcmlmaWNhdGlvbkVtYWlsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2F1dGgvc2VuZFZlcmlmaWNhdGlvbkVtYWlsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBaUJBLEtBQUssVUFBVSxxQkFBcUIsQ0FBQyxJQUFVO0lBQzNDLDJCQUEyQjtJQUMzQixNQUFNLEVBQ0YsTUFBTSxFQUNOLFlBQVksRUFDWixTQUFTLEVBQ1QsVUFBVSxFQUFFLEVBQUUsTUFBTSxFQUFFLGdCQUFnQixFQUFFLEVBQ3hDLElBQUksRUFDSixZQUFZLEVBQ1osR0FBRyxFQUNILEtBQUssRUFDUixHQUFHLElBQUksQ0FBQztJQUVULE1BQU0sYUFBYSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRTFDLElBQUksQ0FBQyxZQUFZLEVBQUU7UUFDZixNQUFNLHNCQUFzQixHQUFHLEdBQUcsTUFBTSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLFdBQVcsS0FBSyxFQUFFLENBQUM7UUFFcEgsSUFBSSxJQUFJLEdBQUcsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsU0FBUyxHQUFHLGFBQWEsQ0FBQyxVQUFVLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxTQUFTLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsc0JBQXNCLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsc0JBQXNCLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUVqYSxNQUFNLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsTUFBc0IsQ0FBQztRQUU1RCx5Q0FBeUM7UUFDekMsSUFBSSxPQUFPLE1BQU0sQ0FBQyxpQkFBaUIsS0FBSyxVQUFVLEVBQUU7WUFDaEQsSUFBSSxHQUFHLE1BQU0sTUFBTSxDQUFDLGlCQUFpQixDQUFDO2dCQUNsQyxHQUFHO2dCQUNILEtBQUs7Z0JBQ0wsSUFBSTthQUNQLENBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxPQUFPLEdBQ1AsYUFBYSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUM7UUFFaEUseUNBQXlDO1FBQ3pDLElBQUksT0FBTyxNQUFNLENBQUMsb0JBQW9CLEtBQUssVUFBVSxFQUFFO1lBQ25ELE9BQU8sR0FBRyxNQUFNLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQztnQkFDeEMsR0FBRztnQkFDSCxLQUFLO2dCQUNMLElBQUk7YUFDUCxDQUFDLENBQUM7U0FDTjtRQUVELFNBQVMsQ0FBQztZQUNOLElBQUksRUFBRSxJQUFJLFlBQVksQ0FBQyxRQUFRLE1BQU0sWUFBWSxDQUFDLFdBQVcsR0FBRztZQUNoRSxFQUFFLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDZCxPQUFPO1lBQ1AsSUFBSTtTQUNQLENBQUMsQ0FBQztLQUNOO0FBQ0wsQ0FBQztBQUVELGtCQUFlLHFCQUFxQixDQUFDIn0=