"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require(".."));
const executeAccess_1 = __importDefault(require("./executeAccess"));
const errors_1 = require("../errors");
const getExecuteStaticAccess = ({ config, Model }) => async (req, res, next) => {
    const adminUILocale = __1.default.config.admin.locale;
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    try {
        if (req.path) {
            const accessResult = await (0, executeAccess_1.default)({ req, isReadingStaticFile: true }, config.access.read);
            if (typeof accessResult === 'object') {
                const filename = decodeURI(req.path).replace(/^\/|\/$/g, '');
                const queryToBuild = {
                    where: {
                        and: [
                            {
                                or: [
                                    {
                                        filename: {
                                            equals: filename
                                        }
                                    }
                                ]
                            },
                            accessResult
                        ]
                    }
                };
                if (config.upload.imageSizes) {
                    config.upload.imageSizes.forEach(({ name }) => {
                        queryToBuild.where.and[0].or.push({
                            [`sizes.${name}.filename`]: {
                                equals: filename
                            }
                        });
                    });
                }
                const query = await Model.buildQuery(queryToBuild, req.locale);
                const doc = await Model.findOne(query);
                if (!doc) {
                    throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
                }
            }
        }
        return next();
    }
    catch (error) {
        return next(error);
    }
};
exports.default = getExecuteStaticAccess;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0RXhlY3V0ZVN0YXRpY0FjY2Vzcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hdXRoL2dldEV4ZWN1dGVTdGF0aWNBY2Nlc3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSwyQ0FBeUI7QUFHekIsb0VBQTRDO0FBQzVDLHNDQUFzQztBQUd0QyxNQUFNLHNCQUFzQixHQUN4QixDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsQ0FDdEIsS0FBSyxFQUFFLEdBQW1CLEVBQUUsR0FBYSxFQUFFLElBQWtCLEVBQUUsRUFBRTtJQUM3RCxNQUFNLGFBQWEsR0FBRyxXQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFFbEQsSUFBSSxHQUFHLENBQUMsTUFBTSxLQUFLLFNBQVMsRUFBRTtRQUMxQixPQUFPLEdBQUcsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7S0FDOUI7SUFFRCxJQUFJO1FBQ0EsSUFBSSxHQUFHLENBQUMsSUFBSSxFQUFFO1lBQ1YsTUFBTSxZQUFZLEdBQUcsTUFBTSxJQUFBLHVCQUFhLEVBQ3BDLEVBQUUsR0FBRyxFQUFFLG1CQUFtQixFQUFFLElBQUksRUFBRSxFQUNsQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FDckIsQ0FBQztZQUVGLElBQUksT0FBTyxZQUFZLEtBQUssUUFBUSxFQUFFO2dCQUNsQyxNQUFNLFFBQVEsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FDeEMsVUFBVSxFQUNWLEVBQUUsQ0FDTCxDQUFDO2dCQUVGLE1BQU0sWUFBWSxHQUFxQjtvQkFDbkMsS0FBSyxFQUFFO3dCQUNILEdBQUcsRUFBRTs0QkFDRDtnQ0FDSSxFQUFFLEVBQUU7b0NBQ0E7d0NBQ0ksUUFBUSxFQUFFOzRDQUNOLE1BQU0sRUFBRSxRQUFRO3lDQUNuQjtxQ0FDSjtpQ0FDSjs2QkFDSjs0QkFDRCxZQUFZO3lCQUNmO3FCQUNKO2lCQUNKLENBQUM7Z0JBRUYsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRTtvQkFDMUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFO3dCQUMxQyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzRCQUM5QixDQUFDLFNBQVMsSUFBSSxXQUFXLENBQUMsRUFBRTtnQ0FDeEIsTUFBTSxFQUFFLFFBQVE7NkJBQ25CO3lCQUNKLENBQUMsQ0FBQztvQkFDUCxDQUFDLENBQUMsQ0FBQztpQkFDTjtnQkFFRCxNQUFNLEtBQUssR0FBRyxNQUFNLEtBQUssQ0FBQyxVQUFVLENBQ2hDLFlBQVksRUFDWixHQUFHLENBQUMsTUFBTSxDQUNiLENBQUM7Z0JBQ0YsTUFBTSxHQUFHLEdBQUcsTUFBTSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUV2QyxJQUFJLENBQUMsR0FBRyxFQUFFO29CQUNOLE1BQU0sSUFBSSxrQkFBUyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7aUJBQzVEO2FBQ0o7U0FDSjtRQUVELE9BQU8sSUFBSSxFQUFFLENBQUM7S0FDakI7SUFBQyxPQUFPLEtBQUssRUFBRTtRQUNaLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3RCO0FBQ0wsQ0FBQyxDQUFDO0FBRU4sa0JBQWUsc0JBQXNCLENBQUMifQ==