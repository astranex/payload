"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.en = void 0;
const en = {
    elements: {
        ArrayAction: {
            MoveUpLabel: 'Move Up',
            MoveDownLabel: 'Move Down',
            AddBelowLabel: 'Add Below',
            DuplicateLabel: 'Duplicate',
            RemoveLabel: 'Remove'
        },
        Autosave: {
            SavingLabel: 'Saving...',
            SavingError: 'There was a problem while autosaving this document.',
            LastSavedLabels: ['Last saved', 'Ago']
        },
        Collapsible: {
            ToggleBlockLabel: 'Toggle block'
        },
        CopyToClipboard: {
            CopyLabel: 'copy',
            CopiedLabel: 'copied'
        },
        DatePicker: {
            TimeCaptionLabel: 'Time',
            PlaceholderLabel: ''
        },
        DeleteDocument: {
            DeletingError: [
                'There was an error while deleting',
                'Please check your connection and try again.'
            ],
            SuccessfullyDeletedLabel: 'successfully deleted.',
            DeletingLabel: 'Deleting...',
            ConfirmLabel: 'Confirm',
            DeleteLabel: 'Delete',
            ConfirmDeletionLabels: [
                'Confirm deletion',
                'You are about to delete the',
                'Are you sure?'
            ],
            CancelLabel: 'Cancel'
        },
        DuplicateDocument: {
            SuccessfullyDuplicatedLabel: 'successfully duplicated.',
            DuplicateLabel: 'Duplicate',
            ConfirmDuplicateLabels: [
                'Confirm duplicate',
                'You have unsaved changes. Would you like to continue to duplicate?'
            ],
            CancelLabel: 'Cancel',
            DuplicateWithoutSavingLabel: 'Duplicate without saving changes'
        },
        FilesDetails: {
            CopyURLLabel: 'Copy URL',
            MoreInfoLabel: 'More info',
            LessInfoLabel: 'Less info'
        },
        GenerateConfirmation: {
            GenerateAPIKeySuccess: 'New API Key Generated.',
            GenerateAPIKeyLabel: 'string',
            ConfirmGenerationLabels: [
                'Confirm Generation',
                'Generating a new API key will',
                'invalidate',
                'the previous key. Are you sure you wish to continue?'
            ],
            CancelLabel: 'Cancel',
            GenerateLabel: 'Generate'
        },
        ListControls: {
            ColumnsLabel: 'Columns',
            FiltersLabel: 'Filters',
            SortLabel: 'Sort'
        },
        NavGroup: {
            Collections: 'Collections',
            Globals: 'Globals'
        },
        PerPage: {
            PerPageLabel: 'Per Page:'
        },
        PreviewButton: {
            PreviewLabel: 'Preview'
        },
        Publish: {
            PublishChangesLabel: 'Publish changes'
        },
        ReactSelect: {
            DefaultPlaceholderLabel: 'Select...',
            NoOptionsMessageLabel: 'No options'
        },
        RenderTitle: {
            UntitledLabel: '[Untitled]'
        },
        SaveDraft: {
            SaveDraftLabel: 'Save draft'
        },
        SearchFilter: {
            SearchLabel: 'Search by'
        },
        SortComplex: {
            AscendingLabel: 'Ascending',
            DescendingLabel: 'Descending',
            ColumnLabel: 'Column to Sort',
            OrderLabel: 'Order'
        },
        Status: {
            StatusChangedLabel: 'Changed',
            StatusDraftLabel: 'Draft',
            StatusPublishedLabel: 'Published',
            UnpublishingError: 'There was a problem while un-publishing this document.',
            UnpublishLabel: 'Unpublish',
            ConfirmUnpublishLabels: [
                'Confirm unpublish',
                'You are about to unpublish this document. Are you sure?'
            ],
            CancelLabel: 'Cancel',
            UnpublishingLabel: 'Unpublishing...',
            ConfirmLabel: 'Confirm',
            RevertLabel: 'Revert to published',
            ConfirmRevertLabels: [
                'Confirm revert to saved',
                "You are about to revert this document's changes to its published state. Are you sure?"
            ],
            RevertingLabel: 'Reverting...'
        },
        StepNav: {
            DashboardLabel: 'Dashboard'
        },
        VersionsCount: {
            NoVersionsFoundLabel: 'No versions found',
            VersionsFoundLabel: 'Versions found:'
        },
        WhereBuilder: {
            fieldTypes: {
                equals: 'equals',
                not_equals: 'is not equal to',
                in: 'is in',
                not_in: 'is not in',
                exists: 'exists',
                greater_than: 'is greater than',
                less_than: 'is less than',
                less_than_equal: 'is less than or equal to',
                greater_than_equals: 'is greater than or equal to',
                near: 'near',
                like: 'is like',
                contains: 'contains'
            },
            RelationshipLoadingError: 'An error has occurred.',
            RelationshipIDLoadingError: 'There was a problem loading the document with ID of',
            RelationshipSelectLabel: 'Select a value',
            RelationshipNoneLabel: 'None',
            TextInputLabel: 'Enter a value',
            FilterWhereLabels: ['Filter', 'where'],
            OrLabel: 'Or',
            AndLabel: 'And',
            NoFiltersLabel: 'No filters set',
            AddFilterLabel: 'Add filter'
        }
    },
    forms: {
        Error: {
            ErrorMessageLabel: 'Please complete this field.'
        },
        Form: {
            SubmitError: 'Please correct invalid fields.',
            SubmitSuccess: 'Submission successful.',
            UnknownError: 'An unknown error occurred.'
        },
        RenderFields: {
            NoMatchedFieldLabel: 'No matched field found for'
        },
        FieldTypes: {
            Array: {
                CollapseAllLabel: 'Collapse All',
                ShowAllLabel: 'Show All',
                FieldRequiersLabel: 'This field requires at least',
                FieldHasNoLabel: 'This field has no',
                AddLabel: 'Add'
            },
            Blocks: {
                FieldRequiersLabel: 'This field requires at least',
                FieldHasNoLabel: 'This field has no',
                AddLabel: 'Add',
                SearchBlockLabel: 'Search for a block',
                UntitledLabel: 'Untitled'
            },
            ConfirmPassword: {
                RequiredFieldLabel: 'This field is required',
                PasswordsNotMatchLabel: 'Passwords do not match.',
                ConfirmPasswordLabel: 'Confirm Password'
            },
            Point: {
                LongitudeLabel: 'Longitude',
                LatitudeLabel: 'Latitude'
            },
            Relationship: {
                NoneLabel: 'None',
                ErrorLabel: 'An error has occurred.',
                UntitledLabel: 'Untitled'
            },
            Upload: {
                UploadNewLabel: 'Upload new',
                ChooseFromExistingLabel: 'Choose from existing',
                NewLabel: 'New',
                SaveLabel: 'Save',
                SelectExistingLabel: 'Select existing',
                OfLabel: 'of'
            },
            RichText: {
                EditLinkLabel: 'Edit Link',
                ConfirmLabel: 'Confirm',
                TextToDisplayLabel: 'Text to display',
                LinkTypeLabel: 'Link Type',
                LinkTypeDescriptionLabel: 'Choose between entering a custom text URL or linking to another document.',
                CustomURLLabel: 'Custom URL',
                InternalLinkLabel: 'Internal Link',
                EnterURLLabel: 'Enter a URL',
                ChooseDocumentLabel: 'Choose a document to link to',
                OpenInNewTabLabel: 'Open in new tab',
                LinkedToLabel: 'Linked to',
                LinkEditTooltipLabel: 'Edit',
                LinkRemoveTooltipLabel: 'Remove',
                RelationshipLabel: 'Relationship',
                AddRelationshipLabel: 'Add Relationship',
                AddRelationshipButtonLabel: 'Add relationship',
                RelationToLabel: 'Relation To',
                RelatedDocumentLabel: 'Related Document',
                UploadAddLabel: 'Add',
                UploadEditTooltipLabel: 'Edit',
                UploadSwapTooltipLabel: 'Swap Upload',
                UploadRemoveTooltipLabel: 'Remove Upload',
                UploadSwapLabel: 'Choose',
                SelectLabel: 'Select a Collection to Browse',
                OfLabel: 'of'
            }
        }
    },
    modals: {
        LeaveWithoutSaving: {
            LeaveWithoutSavingLabels: [
                'Leave without saving',
                'Your changes have not been saved. If you leave now, you will lose your changes.'
            ],
            StayOnThisPageLabel: 'Stay on this page',
            LeaveAnywayLabel: 'Leave anyway'
        },
        StayLoggedIn: {
            StayLoggedInLabels: [
                'Stay logged in',
                "You haven't been active in a little while and will shortly be automatically logged out for your own security. Would you like to stay logged in?"
            ],
            LogOutLabel: 'Log out',
            StayLoggedInLabel: 'Stay logged in'
        }
    },
    templates: {
        Default: {
            MetaTitleLabel: 'Dashboard',
            MetaDescriptionLabel: 'Dashboard for Payload CMS',
            MetaKeywordsLabel: 'Dashboard, Payload, CMS'
        }
    },
    views: {
        Account: {
            RadioGroupInputLabel: 'Admin Theme',
            AutomaticLabel: 'Automatic',
            LightLabel: 'Light',
            DarkLabel: 'Dark',
            AccountTitleLabel: 'Account',
            AccountDescriptionLabel: 'Account of current user',
            AccountKeywordsLabel: 'Account, Dashboard, Payload, CMS',
            AccountLabel: 'Account',
            UntitledLabel: '[Untitled]',
            PayloadSettingsLabel: 'Payload Settings',
            CreateNewLabel: 'Create New',
            SaveLabel: 'Save',
            APIURLLabel: 'API URL',
            IDLabel: 'ID',
            LastModifiedLabel: 'Last Modified',
            CreatedLabel: 'Created'
        },
        collections: {
            Edit: {
                Auth: {
                    APIKeyLabel: 'API Key',
                    UnlockSuccess: 'Successfully unlocked',
                    UnlockError: 'Successfully unlocked',
                    EmailLabel: 'Email',
                    NewPasswordLabel: 'New Password',
                    CancelLabel: 'Cancel',
                    ChangePasswordLabel: 'Change Password',
                    ForceUnlockLabel: 'Force Unlock',
                    EnableAPIKeyLabel: 'Enable API Key',
                    VerifiedLabel: 'Verified'
                },
                Upload: {
                    RequiredFileLabel: 'A file is required.',
                    SelectFileLabel: 'Select a file',
                    DragAndDropHereLabel: 'or drag and drop a file here',
                    EditingLabel: 'Editing',
                    CreatingLabel: 'Creating',
                    UntitledLabel: '[Untitled]',
                    CreateNewLabel: 'Create New',
                    SaveLabel: 'Save',
                    APIURLLabel: 'API URL',
                    VersionsLabel: 'Versions',
                    LastModifiedLabel: 'Last Modified',
                    CreatedLabel: 'Created'
                }
            },
            List: {
                Cell: {
                    NoFieldLabels: ['<No ', '>'],
                    fieldTypes: {
                        Array: {
                            RowsLabel: 'Rows'
                        },
                        Blocks: {
                            MoreLabels: ['and', 'more']
                        },
                        Relationship: {
                            UntitledLabel: 'Untitled',
                            LoadingLabel: 'Loading...',
                            MoreLabels: ['and', 'more'],
                            NoFieldLabels: ['No <', '>']
                        }
                    }
                },
                fieldTypes: {
                    updatedAt: 'Updated At',
                    createdAt: 'Created At',
                    filename: 'Filename'
                },
                CreateNewLabel: 'Create New',
                NoFoundLabels: [
                    'No',
                    'found. Either no',
                    "exist yet or none match the filters you've specified above."
                ],
                CreateNewButtonLabel: 'Create new',
                OfLabel: 'of'
            }
        },
        CreateFirstUser: {
            EmailAddressLabel: 'Email Address',
            PasswordLabel: 'Password',
            ConfirmPasswordLabel: 'Confirm Password',
            WelcomeLabel: 'Welcome',
            CreateFirstUserLabel: 'To begin, create your first user.',
            CreateLabel: 'Create',
            MetaTitleLabel: 'Create First User',
            MetaDescriptionLabel: 'Create first user',
            MetaKeywordsLabel: 'Create, Payload, CMS'
        },
        Dashboard: {
            Collections: 'Collections',
            Globals: 'Globals'
        },
        ForgotPassword: {
            ErrorLabel: 'The email provided is not valid.',
            MetaTitleLabel: 'Forgot Password',
            MetaDescriptionLabel: 'Forgot password',
            MetaKeywordsLabel: 'Forgot, Password, Payload, CMS',
            ForgotPasswordLabels: [
                "You're already logged in",
                'To change your password, go to your',
                'account',
                'and edit your password there.'
            ],
            BackToDashboardLabel: 'Back to Dashboard',
            EmailSentLabel: 'Email sent',
            CheckEmailLabel: 'Check your email for a link that will allow you to securely reset your password.',
            ForgotPasswordLabel: 'Forgot Password',
            EnterEmailLabel: 'Please enter your email below. You will receive an email message with instructions on how to reset your password.',
            EmailAddressLabel: 'Email Address',
            SubmitLabel: 'Submit',
            BackToLogin: 'Back to login'
        },
        Global: {
            EditLabel: 'Edit ',
            SaveLabel: 'Save',
            VersionsLabel: 'Versions',
            APIURLLabel: 'API URL',
            LastModifiedLabel: 'Last Modified'
        },
        Login: {
            MetaTitleLabel: 'Login',
            MetaDescriptionLabel: 'Login user',
            MetaKeywordsLabel: 'Login, Payload, CMS',
            AlreadyLoggedInLabels: [
                'Already logged in',
                'To log in with another user, you should',
                'log out',
                'first.'
            ],
            BackToDashboardLabel: 'Back to Dashboard',
            EmailAddressLabel: 'Email Address',
            PasswordLabel: 'Password',
            ForgotPasswordLabel: 'Forgot password?',
            LoginLabel: 'Login'
        },
        Logout: {
            MetaTitleLabel: 'Logout',
            MetaDescriptionLabel: 'Logout user',
            MetaKeywordsLabel: 'Logout, Payload, CMS',
            LoggedOutInactivityLabel: 'You have been logged out due to inactivity.',
            LoggedOutSuccessfullyLabel: 'You have been logged out successfully.',
            LogBackInLabel: 'Log back in'
        },
        NotFound: {
            NotFoundLabel: 'Not Found',
            MetaTitleLabel: 'Not Found',
            MetaDescriptionLabel: 'Page not found',
            MetaKeywordsLabel: '404, Not found, Payload, CMS',
            NothingFoundLabels: [
                'Nothing found',
                'Sorry–there is nothing to correspond with your request.'
            ],
            BackToDashboardLabel: 'Back to Dashboard'
        },
        ResetPassword: {
            MetaTitleLabel: 'Reset Password',
            MetaDescriptionLabel: 'Reset password',
            MetaKeywordsLabel: 'Reset Password, Payload, CMS',
            AlreadyLoggedInLabels: [
                'Already logged in',
                'To log in with another user, you should',
                'log out',
                'first.'
            ],
            BackToDashboardLabel: 'Back to Dashboard',
            ResetPasswordLabel: 'Reset Password',
            NewPasswordLabel: 'New Password',
            ResetPasswordButtonLabel: 'Reset Password'
        },
        Unauthorized: {
            MetaTitleLabel: 'Unauthorized',
            MetaDescriptionLabel: 'Unauthorized',
            MetaKeywordsLabel: 'Unauthorized, Payload, CMS',
            UnauthorizedLabel: 'Unauthorized',
            NotAllowedLabel: 'You are not allowed to access this page.',
            LogOutLabel: 'Log out'
        },
        Verify: {
            VerifiedSuccessfullyLabel: 'Verified Successfully',
            AlreadyActivatedLabel: 'Already Activated',
            UnableToVerifyLabel: 'Unable To Verify',
            MetaTitleLabel: 'Verify',
            MetaDescriptionLabel: 'Verify user',
            MetaKeywordsLabel: 'Verify, Payload, CMS',
            LoginLabel: 'Login'
        },
        Version: {
            LoadingError: 'An error has occurred.',
            CompareVersionLabel: 'Compare version against:',
            SelectVersionLabel: 'Select a version to compare'
        },
        Versions: {
            UpdatedAtLabel: 'Updated At',
            VersionIDLabel: 'Version ID',
            TypeLabel: 'Type',
            AutosaveLabel: 'Autosave',
            PublishedLabel: 'Published',
            DraftLabel: 'Draft',
            UntitledLabel: '[Untitled]',
            VersionsLabel: 'Versions',
            ViewingVersionsCollectionsLabel: 'Viewing versions for the',
            ViewingVersionsGlobalsLabel: 'Viewing versions for the global',
            ShowingVersionsLabel: 'Showing versions for:',
            CurrentStatusLabels: ['Current', 'document -'],
            EditLabel: 'Edit',
            OfLabel: 'of',
            NoVersionsFoundLabel: 'No further versions found'
        }
    },
    errors: {
        AuthenticationErrorLabel: 'The email or password provided is incorrect.',
        ErrorDeletingFileLabel: 'There was an error deleting file.',
        FileUploadErrorLabel: 'There was a problem while uploading the file.',
        ForbiddenLabel: 'You are not allowed to perform this action.',
        NotFoundLabel: 'There was an error deleting file.',
        LockedAuthLabel: 'This user is locked due to having too many failed login attempts.',
        MissingFileLabel: 'No files were uploaded.',
        UnauthorizedErrorLabel: 'Unauthorized, you must be logged in to make this request.'
    },
    operations: {
        ForgotPassword: {
            MissingEmailLabel: 'Missing email.',
            HTMLLabels: [
                'You are receiving this because you (or someone else) have requested the reset of the password for your account. Please click on the following link, or paste this into your browser to complete the process:',
                'If you did not request this, please ignore this email and your password will remain unchanged.'
            ],
            ResetPasswordLabel: 'Reset your password'
        },
        RegisterFirstUser: {
            RegisteredSuccessfullyLabel: 'Registered and logged in successfully. Welcome!'
        },
        SendVerificationEmail: {
            HTMLLabels: [
                'A new account has just been created for you to access <a href="',
                '">',
                '</a>. Please click on the following link or paste the URL below into your browser to verify your email: <a href="',
                '">',
                '</a><br>After verifying your email, you will be able to log in successfully.'
            ],
            SubjectLabel: 'Verify your email'
        },
        FindVersionByID: {
            MissingIDLabel: 'Missing ID of version.'
        },
        RestoreVersion: {
            MissingIDLabel: 'Missing ID of version to restore.'
        },
        Update: {
            MissingIDLabel: 'Missing ID of document to update.',
            ValueMustBeUniqueLabel: 'Value must be unique'
        }
    },
    requestHandlers: {
        Create: {
            SuccessfullyCreatedLabel: 'successfully created.'
        },
        Update: {
            SavedSuccessfullyLabel: 'Saved successfully.',
            UpdatedSuccessfullyLabel: 'Updated successfully.',
            DraftSavedSuccessfullyLabel: 'Draft saved successfully.',
            AutosavedSuccessfullyLabel: 'Autosaved successfully.'
        }
    },
    fields: {
        validations: {
            DefaultMessageLabel: 'This field is required.',
            EnterValidNumberLabel: 'Please enter a valid number.',
            NumberGreaterLabel: 'is greater than the max allowed value of',
            NumberLessLabel: 'is less than the min allowed value of',
            TextShorterLabels: [
                'This value must be shorter than the max length of',
                'characters.'
            ],
            TextLongerLabels: [
                'This value must be longer than the minimum length of',
                'characters.'
            ],
            PasswordShorterLabels: [
                'This value must be shorter than the max length of',
                'characters.'
            ],
            PasswordLongerLabels: [
                'This value must be longer than the minimum length of',
                'characters.'
            ],
            EnterValidEmailLabel: 'Please enter a valid email address.',
            TextareaShorterLabels: [
                'This value must be shorter than the max length of',
                'characters.'
            ],
            TextareaLongerLabels: [
                'This value must be longer than the minimum length of',
                'characters.'
            ],
            InvalidCheckboxLabel: 'This field can only be equal to true or false.',
            InvalidDateLabel: 'is not a valid date.',
            InvalidSelectionsLabel: 'This field has the following invalid selections:',
            InvalidUploadLabel: 'This field is not a valid upload ID',
            ArrayRequiresAtLeastLabels: [
                'This field requires at least',
                'row(s).'
            ],
            ArrayRequiresNoMoreLabels: [
                'This field requires no more than',
                'row(s).'
            ],
            ArrayRequiresOneRowLabel: 'This field requires at least one row.',
            InvalidSelectionLabel: 'This field has an invalid selection',
            BlockRequiresAtLeastLabels: [
                'This field requires at least',
                'row(s).'
            ],
            BlockRequiresNoMoreLabels: [
                'This field requires no more than',
                'row(s).'
            ],
            BlockRequiresOneRowLabel: 'This field requires at least one row.',
            PointRequiresTwoNumbersLabel: 'This field requires two numbers',
            PointInvalidInputLabel: 'This field has an invalid input'
        }
    }
};
exports.en = en;
exports.default = en;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW4uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY29uZmlnL2xvY2FsZS9lbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFFQSxNQUFNLEVBQUUsR0FBa0I7SUFDdEIsUUFBUSxFQUFFO1FBQ04sV0FBVyxFQUFFO1lBQ1QsV0FBVyxFQUFFLFNBQVM7WUFDdEIsYUFBYSxFQUFFLFdBQVc7WUFDMUIsYUFBYSxFQUFFLFdBQVc7WUFDMUIsY0FBYyxFQUFFLFdBQVc7WUFDM0IsV0FBVyxFQUFFLFFBQVE7U0FDeEI7UUFDRCxRQUFRLEVBQUU7WUFDTixXQUFXLEVBQUUsV0FBVztZQUN4QixXQUFXLEVBQUUscURBQXFEO1lBQ2xFLGVBQWUsRUFBRSxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUM7U0FDekM7UUFDRCxXQUFXLEVBQUU7WUFDVCxnQkFBZ0IsRUFBRSxjQUFjO1NBQ25DO1FBQ0QsZUFBZSxFQUFFO1lBQ2IsU0FBUyxFQUFFLE1BQU07WUFDakIsV0FBVyxFQUFFLFFBQVE7U0FDeEI7UUFDRCxVQUFVLEVBQUU7WUFDUixnQkFBZ0IsRUFBRSxNQUFNO1lBQ3hCLGdCQUFnQixFQUFFLEVBQUU7U0FDdkI7UUFDRCxjQUFjLEVBQUU7WUFDWixhQUFhLEVBQUU7Z0JBQ1gsbUNBQW1DO2dCQUNuQyw2Q0FBNkM7YUFDaEQ7WUFDRCx3QkFBd0IsRUFBRSx1QkFBdUI7WUFDakQsYUFBYSxFQUFFLGFBQWE7WUFDNUIsWUFBWSxFQUFFLFNBQVM7WUFDdkIsV0FBVyxFQUFFLFFBQVE7WUFDckIscUJBQXFCLEVBQUU7Z0JBQ25CLGtCQUFrQjtnQkFDbEIsNkJBQTZCO2dCQUM3QixlQUFlO2FBQ2xCO1lBQ0QsV0FBVyxFQUFFLFFBQVE7U0FDeEI7UUFDRCxpQkFBaUIsRUFBRTtZQUNmLDJCQUEyQixFQUFFLDBCQUEwQjtZQUN2RCxjQUFjLEVBQUUsV0FBVztZQUMzQixzQkFBc0IsRUFBRTtnQkFDcEIsbUJBQW1CO2dCQUNuQixvRUFBb0U7YUFDdkU7WUFDRCxXQUFXLEVBQUUsUUFBUTtZQUNyQiwyQkFBMkIsRUFBRSxrQ0FBa0M7U0FDbEU7UUFDRCxZQUFZLEVBQUU7WUFDVixZQUFZLEVBQUUsVUFBVTtZQUN4QixhQUFhLEVBQUUsV0FBVztZQUMxQixhQUFhLEVBQUUsV0FBVztTQUM3QjtRQUNELG9CQUFvQixFQUFFO1lBQ2xCLHFCQUFxQixFQUFFLHdCQUF3QjtZQUMvQyxtQkFBbUIsRUFBRSxRQUFRO1lBQzdCLHVCQUF1QixFQUFFO2dCQUNyQixvQkFBb0I7Z0JBQ3BCLCtCQUErQjtnQkFDL0IsWUFBWTtnQkFDWixzREFBc0Q7YUFDekQ7WUFDRCxXQUFXLEVBQUUsUUFBUTtZQUNyQixhQUFhLEVBQUUsVUFBVTtTQUM1QjtRQUNELFlBQVksRUFBRTtZQUNWLFlBQVksRUFBRSxTQUFTO1lBQ3ZCLFlBQVksRUFBRSxTQUFTO1lBQ3ZCLFNBQVMsRUFBRSxNQUFNO1NBQ3BCO1FBQ0QsUUFBUSxFQUFFO1lBQ04sV0FBVyxFQUFFLGFBQWE7WUFDMUIsT0FBTyxFQUFFLFNBQVM7U0FDckI7UUFDRCxPQUFPLEVBQUU7WUFDTCxZQUFZLEVBQUUsV0FBVztTQUM1QjtRQUNELGFBQWEsRUFBRTtZQUNYLFlBQVksRUFBRSxTQUFTO1NBQzFCO1FBQ0QsT0FBTyxFQUFFO1lBQ0wsbUJBQW1CLEVBQUUsaUJBQWlCO1NBQ3pDO1FBQ0QsV0FBVyxFQUFFO1lBQ1QsdUJBQXVCLEVBQUUsV0FBVztZQUNwQyxxQkFBcUIsRUFBRSxZQUFZO1NBQ3RDO1FBQ0QsV0FBVyxFQUFFO1lBQ1QsYUFBYSxFQUFFLFlBQVk7U0FDOUI7UUFDRCxTQUFTLEVBQUU7WUFDUCxjQUFjLEVBQUUsWUFBWTtTQUMvQjtRQUNELFlBQVksRUFBRTtZQUNWLFdBQVcsRUFBRSxXQUFXO1NBQzNCO1FBQ0QsV0FBVyxFQUFFO1lBQ1QsY0FBYyxFQUFFLFdBQVc7WUFDM0IsZUFBZSxFQUFFLFlBQVk7WUFDN0IsV0FBVyxFQUFFLGdCQUFnQjtZQUM3QixVQUFVLEVBQUUsT0FBTztTQUN0QjtRQUNELE1BQU0sRUFBRTtZQUNKLGtCQUFrQixFQUFFLFNBQVM7WUFDN0IsZ0JBQWdCLEVBQUUsT0FBTztZQUN6QixvQkFBb0IsRUFBRSxXQUFXO1lBQ2pDLGlCQUFpQixFQUNiLHdEQUF3RDtZQUM1RCxjQUFjLEVBQUUsV0FBVztZQUMzQixzQkFBc0IsRUFBRTtnQkFDcEIsbUJBQW1CO2dCQUNuQix5REFBeUQ7YUFDNUQ7WUFDRCxXQUFXLEVBQUUsUUFBUTtZQUNyQixpQkFBaUIsRUFBRSxpQkFBaUI7WUFDcEMsWUFBWSxFQUFFLFNBQVM7WUFDdkIsV0FBVyxFQUFFLHFCQUFxQjtZQUNsQyxtQkFBbUIsRUFBRTtnQkFDakIseUJBQXlCO2dCQUN6Qix1RkFBdUY7YUFDMUY7WUFDRCxjQUFjLEVBQUUsY0FBYztTQUNqQztRQUNELE9BQU8sRUFBRTtZQUNMLGNBQWMsRUFBRSxXQUFXO1NBQzlCO1FBQ0QsYUFBYSxFQUFFO1lBQ1gsb0JBQW9CLEVBQUUsbUJBQW1CO1lBQ3pDLGtCQUFrQixFQUFFLGlCQUFpQjtTQUN4QztRQUNELFlBQVksRUFBRTtZQUNWLFVBQVUsRUFBRTtnQkFDUixNQUFNLEVBQUUsUUFBUTtnQkFDaEIsVUFBVSxFQUFFLGlCQUFpQjtnQkFDN0IsRUFBRSxFQUFFLE9BQU87Z0JBQ1gsTUFBTSxFQUFFLFdBQVc7Z0JBQ25CLE1BQU0sRUFBRSxRQUFRO2dCQUNoQixZQUFZLEVBQUUsaUJBQWlCO2dCQUMvQixTQUFTLEVBQUUsY0FBYztnQkFDekIsZUFBZSxFQUFFLDBCQUEwQjtnQkFDM0MsbUJBQW1CLEVBQUUsNkJBQTZCO2dCQUNsRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixJQUFJLEVBQUUsU0FBUztnQkFDZixRQUFRLEVBQUUsVUFBVTthQUN2QjtZQUVELHdCQUF3QixFQUFFLHdCQUF3QjtZQUNsRCwwQkFBMEIsRUFDdEIscURBQXFEO1lBQ3pELHVCQUF1QixFQUFFLGdCQUFnQjtZQUN6QyxxQkFBcUIsRUFBRSxNQUFNO1lBRTdCLGNBQWMsRUFBRSxlQUFlO1lBRS9CLGlCQUFpQixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQztZQUN0QyxPQUFPLEVBQUUsSUFBSTtZQUNiLFFBQVEsRUFBRSxLQUFLO1lBQ2YsY0FBYyxFQUFFLGdCQUFnQjtZQUNoQyxjQUFjLEVBQUUsWUFBWTtTQUMvQjtLQUNKO0lBQ0QsS0FBSyxFQUFFO1FBQ0gsS0FBSyxFQUFFO1lBQ0gsaUJBQWlCLEVBQUUsNkJBQTZCO1NBQ25EO1FBQ0QsSUFBSSxFQUFFO1lBQ0YsV0FBVyxFQUFFLGdDQUFnQztZQUM3QyxhQUFhLEVBQUUsd0JBQXdCO1lBQ3ZDLFlBQVksRUFBRSw0QkFBNEI7U0FDN0M7UUFDRCxZQUFZLEVBQUU7WUFDVixtQkFBbUIsRUFBRSw0QkFBNEI7U0FDcEQ7UUFDRCxVQUFVLEVBQUU7WUFDUixLQUFLLEVBQUU7Z0JBQ0gsZ0JBQWdCLEVBQUUsY0FBYztnQkFDaEMsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLGtCQUFrQixFQUFFLDhCQUE4QjtnQkFDbEQsZUFBZSxFQUFFLG1CQUFtQjtnQkFDcEMsUUFBUSxFQUFFLEtBQUs7YUFDbEI7WUFDRCxNQUFNLEVBQUU7Z0JBQ0osa0JBQWtCLEVBQUUsOEJBQThCO2dCQUNsRCxlQUFlLEVBQUUsbUJBQW1CO2dCQUNwQyxRQUFRLEVBQUUsS0FBSztnQkFFZixnQkFBZ0IsRUFBRSxvQkFBb0I7Z0JBRXRDLGFBQWEsRUFBRSxVQUFVO2FBQzVCO1lBQ0QsZUFBZSxFQUFFO2dCQUNiLGtCQUFrQixFQUFFLHdCQUF3QjtnQkFDNUMsc0JBQXNCLEVBQUUseUJBQXlCO2dCQUNqRCxvQkFBb0IsRUFBRSxrQkFBa0I7YUFDM0M7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsY0FBYyxFQUFFLFdBQVc7Z0JBQzNCLGFBQWEsRUFBRSxVQUFVO2FBQzVCO1lBQ0QsWUFBWSxFQUFFO2dCQUNWLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixVQUFVLEVBQUUsd0JBQXdCO2dCQUNwQyxhQUFhLEVBQUUsVUFBVTthQUM1QjtZQUNELE1BQU0sRUFBRTtnQkFDSixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsdUJBQXVCLEVBQUUsc0JBQXNCO2dCQUUvQyxRQUFRLEVBQUUsS0FBSztnQkFDZixTQUFTLEVBQUUsTUFBTTtnQkFFakIsbUJBQW1CLEVBQUUsaUJBQWlCO2dCQUN0QyxPQUFPLEVBQUUsSUFBSTthQUNoQjtZQUNELFFBQVEsRUFBRTtnQkFDTixhQUFhLEVBQUUsV0FBVztnQkFDMUIsWUFBWSxFQUFFLFNBQVM7Z0JBRXZCLGtCQUFrQixFQUFFLGlCQUFpQjtnQkFDckMsYUFBYSxFQUFFLFdBQVc7Z0JBQzFCLHdCQUF3QixFQUNwQiwyRUFBMkU7Z0JBQy9FLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixpQkFBaUIsRUFBRSxlQUFlO2dCQUNsQyxhQUFhLEVBQUUsYUFBYTtnQkFDNUIsbUJBQW1CLEVBQUUsOEJBQThCO2dCQUNuRCxpQkFBaUIsRUFBRSxpQkFBaUI7Z0JBRXBDLGFBQWEsRUFBRSxXQUFXO2dCQUMxQixvQkFBb0IsRUFBRSxNQUFNO2dCQUM1QixzQkFBc0IsRUFBRSxRQUFRO2dCQUVoQyxpQkFBaUIsRUFBRSxjQUFjO2dCQUNqQyxvQkFBb0IsRUFBRSxrQkFBa0I7Z0JBQ3hDLDBCQUEwQixFQUFFLGtCQUFrQjtnQkFDOUMsZUFBZSxFQUFFLGFBQWE7Z0JBQzlCLG9CQUFvQixFQUFFLGtCQUFrQjtnQkFFeEMsY0FBYyxFQUFFLEtBQUs7Z0JBRXJCLHNCQUFzQixFQUFFLE1BQU07Z0JBQzlCLHNCQUFzQixFQUFFLGFBQWE7Z0JBQ3JDLHdCQUF3QixFQUFFLGVBQWU7Z0JBRXpDLGVBQWUsRUFBRSxRQUFRO2dCQUV6QixXQUFXLEVBQUUsK0JBQStCO2dCQUM1QyxPQUFPLEVBQUUsSUFBSTthQUNoQjtTQUNKO0tBQ0o7SUFDRCxNQUFNLEVBQUU7UUFDSixrQkFBa0IsRUFBRTtZQUNoQix3QkFBd0IsRUFBRTtnQkFDdEIsc0JBQXNCO2dCQUN0QixpRkFBaUY7YUFDcEY7WUFDRCxtQkFBbUIsRUFBRSxtQkFBbUI7WUFDeEMsZ0JBQWdCLEVBQUUsY0FBYztTQUNuQztRQUNELFlBQVksRUFBRTtZQUNWLGtCQUFrQixFQUFFO2dCQUNoQixnQkFBZ0I7Z0JBQ2hCLGlKQUFpSjthQUNwSjtZQUNELFdBQVcsRUFBRSxTQUFTO1lBQ3RCLGlCQUFpQixFQUFFLGdCQUFnQjtTQUN0QztLQUNKO0lBQ0QsU0FBUyxFQUFFO1FBQ1AsT0FBTyxFQUFFO1lBQ0wsY0FBYyxFQUFFLFdBQVc7WUFDM0Isb0JBQW9CLEVBQUUsMkJBQTJCO1lBQ2pELGlCQUFpQixFQUFFLHlCQUF5QjtTQUMvQztLQUNKO0lBQ0QsS0FBSyxFQUFFO1FBQ0gsT0FBTyxFQUFFO1lBQ0wsb0JBQW9CLEVBQUUsYUFBYTtZQUNuQyxjQUFjLEVBQUUsV0FBVztZQUMzQixVQUFVLEVBQUUsT0FBTztZQUNuQixTQUFTLEVBQUUsTUFBTTtZQUVqQixpQkFBaUIsRUFBRSxTQUFTO1lBQzVCLHVCQUF1QixFQUFFLHlCQUF5QjtZQUNsRCxvQkFBb0IsRUFBRSxrQ0FBa0M7WUFDeEQsWUFBWSxFQUFFLFNBQVM7WUFDdkIsYUFBYSxFQUFFLFlBQVk7WUFFM0Isb0JBQW9CLEVBQUUsa0JBQWtCO1lBQ3hDLGNBQWMsRUFBRSxZQUFZO1lBQzVCLFNBQVMsRUFBRSxNQUFNO1lBQ2pCLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLE9BQU8sRUFBRSxJQUFJO1lBQ2IsaUJBQWlCLEVBQUUsZUFBZTtZQUNsQyxZQUFZLEVBQUUsU0FBUztTQUMxQjtRQUNELFdBQVcsRUFBRTtZQUNULElBQUksRUFBRTtnQkFDRixJQUFJLEVBQUU7b0JBQ0YsV0FBVyxFQUFFLFNBQVM7b0JBQ3RCLGFBQWEsRUFBRSx1QkFBdUI7b0JBQ3RDLFdBQVcsRUFBRSx1QkFBdUI7b0JBQ3BDLFVBQVUsRUFBRSxPQUFPO29CQUNuQixnQkFBZ0IsRUFBRSxjQUFjO29CQUNoQyxXQUFXLEVBQUUsUUFBUTtvQkFDckIsbUJBQW1CLEVBQUUsaUJBQWlCO29CQUN0QyxnQkFBZ0IsRUFBRSxjQUFjO29CQUNoQyxpQkFBaUIsRUFBRSxnQkFBZ0I7b0JBQ25DLGFBQWEsRUFBRSxVQUFVO2lCQUM1QjtnQkFDRCxNQUFNLEVBQUU7b0JBQ0osaUJBQWlCLEVBQUUscUJBQXFCO29CQUN4QyxlQUFlLEVBQUUsZUFBZTtvQkFDaEMsb0JBQW9CLEVBQUUsOEJBQThCO29CQUVwRCxZQUFZLEVBQUUsU0FBUztvQkFDdkIsYUFBYSxFQUFFLFVBQVU7b0JBQ3pCLGFBQWEsRUFBRSxZQUFZO29CQUMzQixjQUFjLEVBQUUsWUFBWTtvQkFDNUIsU0FBUyxFQUFFLE1BQU07b0JBQ2pCLFdBQVcsRUFBRSxTQUFTO29CQUN0QixhQUFhLEVBQUUsVUFBVTtvQkFDekIsaUJBQWlCLEVBQUUsZUFBZTtvQkFDbEMsWUFBWSxFQUFFLFNBQVM7aUJBQzFCO2FBQ0o7WUFDRCxJQUFJLEVBQUU7Z0JBQ0YsSUFBSSxFQUFFO29CQUNGLGFBQWEsRUFBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUM7b0JBRTVCLFVBQVUsRUFBRTt3QkFDUixLQUFLLEVBQUU7NEJBQ0gsU0FBUyxFQUFFLE1BQU07eUJBQ3BCO3dCQUNELE1BQU0sRUFBRTs0QkFDSixVQUFVLEVBQUUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDO3lCQUM5Qjt3QkFDRCxZQUFZLEVBQUU7NEJBQ1YsYUFBYSxFQUFFLFVBQVU7NEJBQ3pCLFlBQVksRUFBRSxZQUFZOzRCQUMxQixVQUFVLEVBQUUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDOzRCQUMzQixhQUFhLEVBQUUsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDO3lCQUMvQjtxQkFDSjtpQkFDSjtnQkFFRCxVQUFVLEVBQUU7b0JBQ1IsU0FBUyxFQUFFLFlBQVk7b0JBQ3ZCLFNBQVMsRUFBRSxZQUFZO29CQUN2QixRQUFRLEVBQUUsVUFBVTtpQkFDdkI7Z0JBRUQsY0FBYyxFQUFFLFlBQVk7Z0JBQzVCLGFBQWEsRUFBRTtvQkFDWCxJQUFJO29CQUNKLGtCQUFrQjtvQkFDbEIsNkRBQTZEO2lCQUNoRTtnQkFDRCxvQkFBb0IsRUFBRSxZQUFZO2dCQUNsQyxPQUFPLEVBQUUsSUFBSTthQUNoQjtTQUNKO1FBQ0QsZUFBZSxFQUFFO1lBQ2IsaUJBQWlCLEVBQUUsZUFBZTtZQUNsQyxhQUFhLEVBQUUsVUFBVTtZQUN6QixvQkFBb0IsRUFBRSxrQkFBa0I7WUFDeEMsWUFBWSxFQUFFLFNBQVM7WUFDdkIsb0JBQW9CLEVBQUUsbUNBQW1DO1lBQ3pELFdBQVcsRUFBRSxRQUFRO1lBRXJCLGNBQWMsRUFBRSxtQkFBbUI7WUFDbkMsb0JBQW9CLEVBQUUsbUJBQW1CO1lBQ3pDLGlCQUFpQixFQUFFLHNCQUFzQjtTQUM1QztRQUNELFNBQVMsRUFBRTtZQUNQLFdBQVcsRUFBRSxhQUFhO1lBQzFCLE9BQU8sRUFBRSxTQUFTO1NBQ3JCO1FBQ0QsY0FBYyxFQUFFO1lBQ1osVUFBVSxFQUFFLGtDQUFrQztZQUU5QyxjQUFjLEVBQUUsaUJBQWlCO1lBQ2pDLG9CQUFvQixFQUFFLGlCQUFpQjtZQUN2QyxpQkFBaUIsRUFBRSxnQ0FBZ0M7WUFFbkQsb0JBQW9CLEVBQUU7Z0JBQ2xCLDBCQUEwQjtnQkFDMUIscUNBQXFDO2dCQUNyQyxTQUFTO2dCQUNULCtCQUErQjthQUNsQztZQUNELG9CQUFvQixFQUFFLG1CQUFtQjtZQUV6QyxjQUFjLEVBQUUsWUFBWTtZQUM1QixlQUFlLEVBQ1gsa0ZBQWtGO1lBRXRGLG1CQUFtQixFQUFFLGlCQUFpQjtZQUN0QyxlQUFlLEVBQ1gsbUhBQW1IO1lBQ3ZILGlCQUFpQixFQUFFLGVBQWU7WUFDbEMsV0FBVyxFQUFFLFFBQVE7WUFDckIsV0FBVyxFQUFFLGVBQWU7U0FDL0I7UUFDRCxNQUFNLEVBQUU7WUFDSixTQUFTLEVBQUUsT0FBTztZQUNsQixTQUFTLEVBQUUsTUFBTTtZQUNqQixhQUFhLEVBQUUsVUFBVTtZQUN6QixXQUFXLEVBQUUsU0FBUztZQUN0QixpQkFBaUIsRUFBRSxlQUFlO1NBQ3JDO1FBQ0QsS0FBSyxFQUFFO1lBQ0gsY0FBYyxFQUFFLE9BQU87WUFDdkIsb0JBQW9CLEVBQUUsWUFBWTtZQUNsQyxpQkFBaUIsRUFBRSxxQkFBcUI7WUFFeEMscUJBQXFCLEVBQUU7Z0JBQ25CLG1CQUFtQjtnQkFDbkIseUNBQXlDO2dCQUN6QyxTQUFTO2dCQUNULFFBQVE7YUFDWDtZQUVELG9CQUFvQixFQUFFLG1CQUFtQjtZQUV6QyxpQkFBaUIsRUFBRSxlQUFlO1lBQ2xDLGFBQWEsRUFBRSxVQUFVO1lBQ3pCLG1CQUFtQixFQUFFLGtCQUFrQjtZQUN2QyxVQUFVLEVBQUUsT0FBTztTQUN0QjtRQUNELE1BQU0sRUFBRTtZQUNKLGNBQWMsRUFBRSxRQUFRO1lBQ3hCLG9CQUFvQixFQUFFLGFBQWE7WUFDbkMsaUJBQWlCLEVBQUUsc0JBQXNCO1lBRXpDLHdCQUF3QixFQUNwQiw2Q0FBNkM7WUFDakQsMEJBQTBCLEVBQ3RCLHdDQUF3QztZQUM1QyxjQUFjLEVBQUUsYUFBYTtTQUNoQztRQUNELFFBQVEsRUFBRTtZQUNOLGFBQWEsRUFBRSxXQUFXO1lBRTFCLGNBQWMsRUFBRSxXQUFXO1lBQzNCLG9CQUFvQixFQUFFLGdCQUFnQjtZQUN0QyxpQkFBaUIsRUFBRSw4QkFBOEI7WUFFakQsa0JBQWtCLEVBQUU7Z0JBQ2hCLGVBQWU7Z0JBQ2YseURBQXlEO2FBQzVEO1lBQ0Qsb0JBQW9CLEVBQUUsbUJBQW1CO1NBQzVDO1FBQ0QsYUFBYSxFQUFFO1lBQ1gsY0FBYyxFQUFFLGdCQUFnQjtZQUNoQyxvQkFBb0IsRUFBRSxnQkFBZ0I7WUFDdEMsaUJBQWlCLEVBQUUsOEJBQThCO1lBRWpELHFCQUFxQixFQUFFO2dCQUNuQixtQkFBbUI7Z0JBQ25CLHlDQUF5QztnQkFDekMsU0FBUztnQkFDVCxRQUFRO2FBQ1g7WUFFRCxvQkFBb0IsRUFBRSxtQkFBbUI7WUFFekMsa0JBQWtCLEVBQUUsZ0JBQWdCO1lBQ3BDLGdCQUFnQixFQUFFLGNBQWM7WUFDaEMsd0JBQXdCLEVBQUUsZ0JBQWdCO1NBQzdDO1FBQ0QsWUFBWSxFQUFFO1lBQ1YsY0FBYyxFQUFFLGNBQWM7WUFDOUIsb0JBQW9CLEVBQUUsY0FBYztZQUNwQyxpQkFBaUIsRUFBRSw0QkFBNEI7WUFFL0MsaUJBQWlCLEVBQUUsY0FBYztZQUNqQyxlQUFlLEVBQUUsMENBQTBDO1lBQzNELFdBQVcsRUFBRSxTQUFTO1NBQ3pCO1FBQ0QsTUFBTSxFQUFFO1lBQ0oseUJBQXlCLEVBQUUsdUJBQXVCO1lBQ2xELHFCQUFxQixFQUFFLG1CQUFtQjtZQUMxQyxtQkFBbUIsRUFBRSxrQkFBa0I7WUFFdkMsY0FBYyxFQUFFLFFBQVE7WUFDeEIsb0JBQW9CLEVBQUUsYUFBYTtZQUNuQyxpQkFBaUIsRUFBRSxzQkFBc0I7WUFFekMsVUFBVSxFQUFFLE9BQU87U0FDdEI7UUFDRCxPQUFPLEVBQUU7WUFDTCxZQUFZLEVBQUUsd0JBQXdCO1lBQ3RDLG1CQUFtQixFQUFFLDBCQUEwQjtZQUMvQyxrQkFBa0IsRUFBRSw2QkFBNkI7U0FDcEQ7UUFDRCxRQUFRLEVBQUU7WUFDTixjQUFjLEVBQUUsWUFBWTtZQUM1QixjQUFjLEVBQUUsWUFBWTtZQUM1QixTQUFTLEVBQUUsTUFBTTtZQUNqQixhQUFhLEVBQUUsVUFBVTtZQUN6QixjQUFjLEVBQUUsV0FBVztZQUMzQixVQUFVLEVBQUUsT0FBTztZQUVuQixhQUFhLEVBQUUsWUFBWTtZQUMzQixhQUFhLEVBQUUsVUFBVTtZQUV6QiwrQkFBK0IsRUFBRSwwQkFBMEI7WUFDM0QsMkJBQTJCLEVBQUUsaUNBQWlDO1lBRTlELG9CQUFvQixFQUFFLHVCQUF1QjtZQUM3QyxtQkFBbUIsRUFBRSxDQUFDLFNBQVMsRUFBRSxZQUFZLENBQUM7WUFDOUMsU0FBUyxFQUFFLE1BQU07WUFDakIsT0FBTyxFQUFFLElBQUk7WUFDYixvQkFBb0IsRUFBRSwyQkFBMkI7U0FDcEQ7S0FDSjtJQUNELE1BQU0sRUFBRTtRQUNKLHdCQUF3QixFQUNwQiw4Q0FBOEM7UUFDbEQsc0JBQXNCLEVBQUUsbUNBQW1DO1FBQzNELG9CQUFvQixFQUFFLCtDQUErQztRQUNyRSxjQUFjLEVBQUUsNkNBQTZDO1FBQzdELGFBQWEsRUFBRSxtQ0FBbUM7UUFDbEQsZUFBZSxFQUNYLG1FQUFtRTtRQUN2RSxnQkFBZ0IsRUFBRSx5QkFBeUI7UUFDM0Msc0JBQXNCLEVBQ2xCLDJEQUEyRDtLQUNsRTtJQUNELFVBQVUsRUFBRTtRQUNSLGNBQWMsRUFBRTtZQUNaLGlCQUFpQixFQUFFLGdCQUFnQjtZQUNuQyxVQUFVLEVBQUU7Z0JBQ1IsOE1BQThNO2dCQUM5TSxnR0FBZ0c7YUFDbkc7WUFDRCxrQkFBa0IsRUFBRSxxQkFBcUI7U0FDNUM7UUFDRCxpQkFBaUIsRUFBRTtZQUNmLDJCQUEyQixFQUN2QixpREFBaUQ7U0FDeEQ7UUFDRCxxQkFBcUIsRUFBRTtZQUNuQixVQUFVLEVBQUU7Z0JBQ1IsaUVBQWlFO2dCQUNqRSxJQUFJO2dCQUNKLG1IQUFtSDtnQkFDbkgsSUFBSTtnQkFDSiw4RUFBOEU7YUFDakY7WUFDRCxZQUFZLEVBQUUsbUJBQW1CO1NBQ3BDO1FBQ0QsZUFBZSxFQUFFO1lBQ2IsY0FBYyxFQUFFLHdCQUF3QjtTQUMzQztRQUNELGNBQWMsRUFBRTtZQUNaLGNBQWMsRUFBRSxtQ0FBbUM7U0FDdEQ7UUFDRCxNQUFNLEVBQUU7WUFDSixjQUFjLEVBQUUsbUNBQW1DO1lBQ25ELHNCQUFzQixFQUFFLHNCQUFzQjtTQUNqRDtLQUNKO0lBQ0QsZUFBZSxFQUFFO1FBQ2IsTUFBTSxFQUFFO1lBQ0osd0JBQXdCLEVBQUUsdUJBQXVCO1NBQ3BEO1FBQ0QsTUFBTSxFQUFFO1lBQ0osc0JBQXNCLEVBQUUscUJBQXFCO1lBQzdDLHdCQUF3QixFQUFFLHVCQUF1QjtZQUNqRCwyQkFBMkIsRUFBRSwyQkFBMkI7WUFDeEQsMEJBQTBCLEVBQUUseUJBQXlCO1NBQ3hEO0tBQ0o7SUFDRCxNQUFNLEVBQUU7UUFDQSxXQUFXLEVBQUU7WUFDVCxtQkFBbUIsRUFBRSx5QkFBeUI7WUFFOUMscUJBQXFCLEVBQUUsOEJBQThCO1lBQ3JELGtCQUFrQixFQUFFLDBDQUEwQztZQUM5RCxlQUFlLEVBQUUsdUNBQXVDO1lBRXhELGlCQUFpQixFQUFFO2dCQUNmLG1EQUFtRDtnQkFDbkQsYUFBYTthQUNoQjtZQUNELGdCQUFnQixFQUFFO2dCQUNkLHNEQUFzRDtnQkFDdEQsYUFBYTthQUNoQjtZQUVELHFCQUFxQixFQUFFO2dCQUNuQixtREFBbUQ7Z0JBQ25ELGFBQWE7YUFDaEI7WUFDRCxvQkFBb0IsRUFBRTtnQkFDbEIsc0RBQXNEO2dCQUN0RCxhQUFhO2FBQ2hCO1lBRUQsb0JBQW9CLEVBQUUscUNBQXFDO1lBRTNELHFCQUFxQixFQUFFO2dCQUNuQixtREFBbUQ7Z0JBQ25ELGFBQWE7YUFDaEI7WUFDRCxvQkFBb0IsRUFBRTtnQkFDbEIsc0RBQXNEO2dCQUN0RCxhQUFhO2FBQ2hCO1lBRUQsb0JBQW9CLEVBQUUsZ0RBQWdEO1lBRXRFLGdCQUFnQixFQUFFLHNCQUFzQjtZQUV4QyxzQkFBc0IsRUFBRSxrREFBa0Q7WUFFMUUsa0JBQWtCLEVBQUUscUNBQXFDO1lBRXpELDBCQUEwQixFQUFFO2dCQUN4Qiw4QkFBOEI7Z0JBQzlCLFNBQVM7YUFDWjtZQUNELHlCQUF5QixFQUFFO2dCQUN2QixrQ0FBa0M7Z0JBQ2xDLFNBQVM7YUFDWjtZQUNELHdCQUF3QixFQUFFLHVDQUF1QztZQUVqRSxxQkFBcUIsRUFBRSxxQ0FBcUM7WUFFNUQsMEJBQTBCLEVBQUU7Z0JBQ3hCLDhCQUE4QjtnQkFDOUIsU0FBUzthQUNaO1lBQ0QseUJBQXlCLEVBQUU7Z0JBQ3ZCLGtDQUFrQztnQkFDbEMsU0FBUzthQUNaO1lBQ0Qsd0JBQXdCLEVBQUUsdUNBQXVDO1lBRWpFLDRCQUE0QixFQUFFLGlDQUFpQztZQUMvRCxzQkFBc0IsRUFBRSxpQ0FBaUM7U0FDNUQ7S0FDUjtDQUNKLENBQUM7QUFFTyxnQkFBRTtBQUNYLGtCQUFlLEVBQUUsQ0FBQyJ9