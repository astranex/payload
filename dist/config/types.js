"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hasTransportOptions = exports.hasTransport = void 0;
/**
 * type guard for EmailOptions
 * @param emailConfig
 */
function hasTransport(emailConfig) {
    return emailConfig.transport !== undefined;
}
exports.hasTransport = hasTransport;
/**
 * type guard for EmailOptions
 * @param emailConfig
 */
function hasTransportOptions(emailConfig) {
    return (emailConfig.transportOptions !== undefined);
}
exports.hasTransportOptions = hasTransportOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvY29uZmlnL3R5cGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQXNEQTs7O0dBR0c7QUFDSCxTQUFnQixZQUFZLENBQ3hCLFdBQXlCO0lBRXpCLE9BQVEsV0FBOEIsQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDO0FBQ25FLENBQUM7QUFKRCxvQ0FJQztBQUVEOzs7R0FHRztBQUNILFNBQWdCLG1CQUFtQixDQUMvQixXQUF5QjtJQUV6QixPQUFPLENBQ0YsV0FBcUMsQ0FBQyxnQkFBZ0IsS0FBSyxTQUFTLENBQ3hFLENBQUM7QUFDTixDQUFDO0FBTkQsa0RBTUMifQ==