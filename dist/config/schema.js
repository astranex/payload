"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.endpointsSchema = void 0;
const joi_1 = __importDefault(require("joi"));
const component = joi_1.default.alternatives().try(joi_1.default.object().unknown(), joi_1.default.func());
exports.endpointsSchema = joi_1.default.array().items(joi_1.default.object({
    path: joi_1.default.string(),
    method: joi_1.default
        .string()
        .valid('get', 'head', 'post', 'put', 'patch', 'delete', 'connect', 'options'),
    root: joi_1.default.bool(),
    handler: joi_1.default
        .alternatives()
        .try(joi_1.default.array().items(joi_1.default.func()), joi_1.default.func())
}));
exports.default = joi_1.default.object({
    serverURL: joi_1.default
        .string()
        .uri()
        .allow('')
        .custom((value, helper) => {
        const urlWithoutProtocol = value.split('//')[1];
        if (!urlWithoutProtocol) {
            return helper.message({
                custom: 'You need to include either "https://" or "http://" in your serverURL.'
            });
        }
        if (urlWithoutProtocol.indexOf('/') > -1) {
            return helper.message({
                custom: 'Your serverURL cannot have a path. It can only contain a protocol, a domain, and an optional port.'
            });
        }
        return value;
    }),
    cookiePrefix: joi_1.default.string(),
    routes: joi_1.default.object({
        admin: joi_1.default.string(),
        api: joi_1.default.string(),
        graphQL: joi_1.default.string(),
        graphQLPlayground: joi_1.default.string()
    }),
    typescript: joi_1.default.object({
        outputFile: joi_1.default.string()
    }),
    collections: joi_1.default.array(),
    endpoints: exports.endpointsSchema,
    globals: joi_1.default.array(),
    admin: joi_1.default.object({
        user: joi_1.default.string(),
        meta: joi_1.default.object().keys({
            titleSuffix: joi_1.default.string(),
            ogImage: joi_1.default.string(),
            favicon: joi_1.default.string()
        }),
        disable: joi_1.default.bool(),
        indexHTML: joi_1.default.string(),
        css: joi_1.default.string(),
        dateFormat: joi_1.default.string(),
        avatar: joi_1.default.alternatives().try(joi_1.default.string(), component),
        components: joi_1.default.object().keys({
            routes: joi_1.default.array().items(joi_1.default.object().keys({
                Component: component.required(),
                path: joi_1.default.string().required(),
                exact: joi_1.default.bool(),
                strict: joi_1.default.bool(),
                sensitive: joi_1.default.bool()
            })),
            providers: joi_1.default.array().items(component),
            beforeDashboard: joi_1.default.array().items(component),
            afterDashboard: joi_1.default.array().items(component),
            beforeLogin: joi_1.default.array().items(component),
            afterLogin: joi_1.default.array().items(component),
            beforeNavLinks: joi_1.default.array().items(component),
            afterNavLinks: joi_1.default.array().items(component),
            Nav: component,
            views: joi_1.default.object({
                Dashboard: component,
                Account: component
            }),
            graphics: joi_1.default.object({
                Icon: component,
                Logo: component
            })
        }),
        webpack: joi_1.default.func(),
        locale: joi_1.default.object(),
        dateLocale: joi_1.default.object(),
        datePickerFormat: joi_1.default.object()
    }),
    defaultDepth: joi_1.default.number().min(0).max(30),
    maxDepth: joi_1.default.number().min(0).max(100),
    csrf: joi_1.default.array().items(joi_1.default.string().allow('')).sparse(),
    cors: [joi_1.default.string().valid('*'), joi_1.default.array().items(joi_1.default.string())],
    express: joi_1.default.object().keys({
        json: joi_1.default.object(),
        compression: joi_1.default.object(),
        middleware: joi_1.default.array().items(joi_1.default.func()),
        preMiddleware: joi_1.default.array().items(joi_1.default.func()),
        postMiddleware: joi_1.default.array().items(joi_1.default.func())
    }),
    local: joi_1.default.boolean(),
    upload: joi_1.default.object(),
    indexSortableFields: joi_1.default.boolean(),
    rateLimit: joi_1.default.object().keys({
        window: joi_1.default.number(),
        max: joi_1.default.number(),
        trustProxy: joi_1.default.boolean(),
        skip: joi_1.default.func()
    }),
    graphQL: joi_1.default.object().keys({
        mutations: joi_1.default.function(),
        queries: joi_1.default.function(),
        maxComplexity: joi_1.default.number(),
        disablePlaygroundInProduction: joi_1.default.boolean(),
        disable: joi_1.default.boolean(),
        schemaOutputFile: joi_1.default.string()
    }),
    localization: joi_1.default.alternatives().try(joi_1.default.object().keys({
        locales: joi_1.default.array().items(joi_1.default.string()),
        defaultLocale: joi_1.default.string(),
        fallback: joi_1.default.boolean()
    }), joi_1.default.boolean()),
    hooks: joi_1.default.object().keys({
        afterError: joi_1.default.func()
    }),
    telemetry: joi_1.default.boolean(),
    plugins: joi_1.default.array().items(joi_1.default.func()),
    onInit: joi_1.default.func(),
    debug: joi_1.default.boolean()
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NoZW1hLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbmZpZy9zY2hlbWEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsOENBQXNCO0FBRXRCLE1BQU0sU0FBUyxHQUFHLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0FBRWhFLFFBQUEsZUFBZSxHQUFHLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQzVDLGFBQUcsQ0FBQyxNQUFNLENBQUM7SUFDUCxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtJQUNsQixNQUFNLEVBQUUsYUFBRztTQUNOLE1BQU0sRUFBRTtTQUNSLEtBQUssQ0FDRixLQUFLLEVBQ0wsTUFBTSxFQUNOLE1BQU0sRUFDTixLQUFLLEVBQ0wsT0FBTyxFQUNQLFFBQVEsRUFDUixTQUFTLEVBQ1QsU0FBUyxDQUNaO0lBQ0wsSUFBSSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7SUFDaEIsT0FBTyxFQUFFLGFBQUc7U0FDUCxZQUFZLEVBQUU7U0FDZCxHQUFHLENBQUMsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7Q0FDdEQsQ0FBQyxDQUNMLENBQUM7QUFFRixrQkFBZSxhQUFHLENBQUMsTUFBTSxDQUFDO0lBQ3RCLFNBQVMsRUFBRSxhQUFHO1NBQ1QsTUFBTSxFQUFFO1NBQ1IsR0FBRyxFQUFFO1NBQ0wsS0FBSyxDQUFDLEVBQUUsQ0FBQztTQUNULE1BQU0sQ0FBQyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsRUFBRTtRQUN0QixNQUFNLGtCQUFrQixHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFaEQsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3JCLE9BQU8sTUFBTSxDQUFDLE9BQU8sQ0FBQztnQkFDbEIsTUFBTSxFQUFFLHVFQUF1RTthQUNsRixDQUFDLENBQUM7U0FDTjtRQUVELElBQUksa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ3RDLE9BQU8sTUFBTSxDQUFDLE9BQU8sQ0FBQztnQkFDbEIsTUFBTSxFQUFFLG9HQUFvRzthQUMvRyxDQUFDLENBQUM7U0FDTjtRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUMsQ0FBQztJQUNOLFlBQVksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQzFCLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO1FBQ2YsS0FBSyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDbkIsR0FBRyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDakIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDckIsaUJBQWlCLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtLQUNsQyxDQUFDO0lBQ0YsVUFBVSxFQUFFLGFBQUcsQ0FBQyxNQUFNLENBQUM7UUFDbkIsVUFBVSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7S0FDM0IsQ0FBQztJQUNGLFdBQVcsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFO0lBQ3hCLFNBQVMsRUFBRSx1QkFBZTtJQUMxQixPQUFPLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRTtJQUNwQixLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sQ0FBQztRQUNkLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ2xCLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDO1lBQ3BCLFdBQVcsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1lBQ3pCLE9BQU8sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1lBQ3JCLE9BQU8sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1NBQ3hCLENBQUM7UUFDRixPQUFPLEVBQUUsYUFBRyxDQUFDLElBQUksRUFBRTtRQUNuQixTQUFTLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUN2QixHQUFHLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUNqQixVQUFVLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUN4QixNQUFNLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FBQyxhQUFHLENBQUMsTUFBTSxFQUFFLEVBQUUsU0FBUyxDQUFDO1FBQ3ZELFVBQVUsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDO1lBQzFCLE1BQU0sRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUNyQixhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDO2dCQUNkLFNBQVMsRUFBRSxTQUFTLENBQUMsUUFBUSxFQUFFO2dCQUMvQixJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtnQkFDN0IsS0FBSyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7Z0JBQ2pCLE1BQU0sRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO2dCQUNsQixTQUFTLEVBQUUsYUFBRyxDQUFDLElBQUksRUFBRTthQUN4QixDQUFDLENBQ0w7WUFDRCxTQUFTLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7WUFDdkMsZUFBZSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO1lBQzdDLGNBQWMsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUM1QyxXQUFXLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7WUFDekMsVUFBVSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO1lBQ3hDLGNBQWMsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUM1QyxhQUFhLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7WUFDM0MsR0FBRyxFQUFFLFNBQVM7WUFDZCxLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sQ0FBQztnQkFDZCxTQUFTLEVBQUUsU0FBUztnQkFDcEIsT0FBTyxFQUFFLFNBQVM7YUFDckIsQ0FBQztZQUNGLFFBQVEsRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO2dCQUNqQixJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsU0FBUzthQUNsQixDQUFDO1NBQ0wsQ0FBQztRQUNGLE9BQU8sRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO1FBQ25CLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ3BCLFVBQVUsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ3hCLGdCQUFnQixFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7S0FDakMsQ0FBQztJQUNGLFlBQVksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7SUFDekMsUUFBUSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQztJQUN0QyxJQUFJLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFO0lBQ3hELElBQUksRUFBRSxDQUFDLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUNoRSxPQUFPLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztRQUN2QixJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUNsQixXQUFXLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUN6QixVQUFVLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDekMsYUFBYSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzVDLGNBQWMsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztLQUNoRCxDQUFDO0lBQ0YsS0FBSyxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7SUFDcEIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7SUFDcEIsbUJBQW1CLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRTtJQUNsQyxTQUFTLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztRQUN6QixNQUFNLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUNwQixHQUFHLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUNqQixVQUFVLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRTtRQUN6QixJQUFJLEVBQUUsYUFBRyxDQUFDLElBQUksRUFBRTtLQUNuQixDQUFDO0lBQ0YsT0FBTyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7UUFDdkIsU0FBUyxFQUFFLGFBQUcsQ0FBQyxRQUFRLEVBQUU7UUFDekIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxRQUFRLEVBQUU7UUFDdkIsYUFBYSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDM0IsNkJBQTZCLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRTtRQUM1QyxPQUFPLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRTtRQUN0QixnQkFBZ0IsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0tBQ2pDLENBQUM7SUFDRixZQUFZLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDaEMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztRQUNkLE9BQU8sRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN4QyxhQUFhLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUMzQixRQUFRLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRTtLQUMxQixDQUFDLEVBQ0YsYUFBRyxDQUFDLE9BQU8sRUFBRSxDQUNoQjtJQUNELEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDO1FBQ3JCLFVBQVUsRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO0tBQ3pCLENBQUM7SUFDRixTQUFTLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRTtJQUN4QixPQUFPLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdEMsTUFBTSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7SUFDbEIsS0FBSyxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7Q0FDdkIsQ0FBQyxDQUFDIn0=