"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaults = void 0;
const path_1 = __importDefault(require("path"));
const en_1 = require("./locale/en");
const en_US_1 = __importDefault(require("date-fns/locale/en-US"));
exports.defaults = {
    serverURL: '',
    defaultDepth: 2,
    maxDepth: 10,
    collections: [],
    globals: [],
    endpoints: [],
    cookiePrefix: 'payload',
    csrf: [],
    cors: [],
    admin: {
        locale: en_1.en,
        meta: {
            titleSuffix: '- Payload'
        },
        disable: false,
        indexHTML: path_1.default.resolve(__dirname, '../admin/index.html'),
        avatar: 'default',
        components: {},
        css: path_1.default.resolve(__dirname, '../admin/scss/custom.css'),
        dateLocale: en_US_1.default,
        dateFormat: 'MMMM do yyyy, h:mm a',
        datePickerFormat: {
            defaultTimeFormat: 'h:mm aa',
            timeOnlyFormat: 'h:mm a',
            monthOnlyFormat: 'MM/yyyy',
            elseFormat: 'MMM d, yyy'
        }
    },
    typescript: {
        outputFile: `${typeof (process === null || process === void 0 ? void 0 : process.cwd) === 'function' ? process.cwd() : ''}/payload-types.ts`
    },
    upload: {},
    graphQL: {
        maxComplexity: 1000,
        disablePlaygroundInProduction: true,
        schemaOutputFile: `${typeof (process === null || process === void 0 ? void 0 : process.cwd) === 'function' ? process.cwd() : ''}/schema.graphql`
    },
    routes: {
        admin: '/admin',
        api: '/api',
        graphQL: '/graphql',
        graphQLPlayground: '/graphql-playground'
    },
    rateLimit: {
        window: 15 * 60 * 100,
        max: 500
    },
    express: {
        json: {},
        compression: {},
        middleware: [],
        preMiddleware: [],
        postMiddleware: []
    },
    hooks: {},
    localization: false,
    telemetry: true
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmYXVsdHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvY29uZmlnL2RlZmF1bHRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLGdEQUF3QjtBQUN4QixvQ0FBaUM7QUFDakMsa0VBQWlEO0FBR3BDLFFBQUEsUUFBUSxHQUFXO0lBQzVCLFNBQVMsRUFBRSxFQUFFO0lBQ2IsWUFBWSxFQUFFLENBQUM7SUFDZixRQUFRLEVBQUUsRUFBRTtJQUNaLFdBQVcsRUFBRSxFQUFFO0lBQ2YsT0FBTyxFQUFFLEVBQUU7SUFDWCxTQUFTLEVBQUUsRUFBRTtJQUNiLFlBQVksRUFBRSxTQUFTO0lBQ3ZCLElBQUksRUFBRSxFQUFFO0lBQ1IsSUFBSSxFQUFFLEVBQUU7SUFDUixLQUFLLEVBQUU7UUFDSCxNQUFNLEVBQUUsT0FBRTtRQUNWLElBQUksRUFBRTtZQUNGLFdBQVcsRUFBRSxXQUFXO1NBQzNCO1FBQ0QsT0FBTyxFQUFFLEtBQUs7UUFDZCxTQUFTLEVBQUUsY0FBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUscUJBQXFCLENBQUM7UUFDekQsTUFBTSxFQUFFLFNBQVM7UUFDakIsVUFBVSxFQUFFLEVBQUU7UUFDZCxHQUFHLEVBQUUsY0FBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsMEJBQTBCLENBQUM7UUFDeEQsVUFBVSxFQUFFLGVBQVk7UUFDeEIsVUFBVSxFQUFFLHNCQUFzQjtRQUNsQyxnQkFBZ0IsRUFBRTtZQUNkLGlCQUFpQixFQUFFLFNBQVM7WUFDNUIsY0FBYyxFQUFFLFFBQVE7WUFDeEIsZUFBZSxFQUFFLFNBQVM7WUFDMUIsVUFBVSxFQUFFLFlBQVk7U0FDM0I7S0FDSjtJQUNELFVBQVUsRUFBRTtRQUNSLFVBQVUsRUFBRSxHQUNSLE9BQU8sQ0FBQSxPQUFPLGFBQVAsT0FBTyx1QkFBUCxPQUFPLENBQUUsR0FBRyxDQUFBLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQ3pELG1CQUFtQjtLQUN0QjtJQUNELE1BQU0sRUFBRSxFQUFFO0lBQ1YsT0FBTyxFQUFFO1FBQ0wsYUFBYSxFQUFFLElBQUk7UUFDbkIsNkJBQTZCLEVBQUUsSUFBSTtRQUNuQyxnQkFBZ0IsRUFBRSxHQUNkLE9BQU8sQ0FBQSxPQUFPLGFBQVAsT0FBTyx1QkFBUCxPQUFPLENBQUUsR0FBRyxDQUFBLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQ3pELGlCQUFpQjtLQUNwQjtJQUNELE1BQU0sRUFBRTtRQUNKLEtBQUssRUFBRSxRQUFRO1FBQ2YsR0FBRyxFQUFFLE1BQU07UUFDWCxPQUFPLEVBQUUsVUFBVTtRQUNuQixpQkFBaUIsRUFBRSxxQkFBcUI7S0FDM0M7SUFDRCxTQUFTLEVBQUU7UUFDUCxNQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxHQUFHO1FBQ3JCLEdBQUcsRUFBRSxHQUFHO0tBQ1g7SUFDRCxPQUFPLEVBQUU7UUFDTCxJQUFJLEVBQUUsRUFBRTtRQUNSLFdBQVcsRUFBRSxFQUFFO1FBQ2YsVUFBVSxFQUFFLEVBQUU7UUFDZCxhQUFhLEVBQUUsRUFBRTtRQUNqQixjQUFjLEVBQUUsRUFBRTtLQUNyQjtJQUNELEtBQUssRUFBRSxFQUFFO0lBQ1QsWUFBWSxFQUFFLEtBQUs7SUFDbkIsU0FBUyxFQUFFLElBQUk7Q0FDbEIsQ0FBQyJ9