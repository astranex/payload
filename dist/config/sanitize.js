"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const deepmerge_1 = __importDefault(require("deepmerge"));
const is_plain_object_1 = require("is-plain-object");
const defaultUser_1 = __importDefault(require("../auth/defaultUser"));
const sanitize_1 = __importDefault(require("../collections/config/sanitize"));
const errors_1 = require("../errors");
const sanitize_2 = __importDefault(require("../globals/config/sanitize"));
const checkDuplicateCollections_1 = __importDefault(require("../utilities/checkDuplicateCollections"));
const defaults_1 = require("./defaults");
const sanitizeConfig = (config) => {
    const sanitizedConfig = (0, deepmerge_1.default)(defaults_1.defaults, config, {
        isMergeableObject: is_plain_object_1.isPlainObject
    });
    if (!sanitizedConfig.admin.user) {
        const firstCollectionWithAuth = sanitizedConfig.collections.find((c) => c.auth);
        if (firstCollectionWithAuth) {
            sanitizedConfig.admin.user = firstCollectionWithAuth.slug;
        }
        else {
            sanitizedConfig.admin.user = 'users';
            const sanitizedDefaultUser = (0, sanitize_1.default)(sanitizedConfig, defaultUser_1.default);
            sanitizedConfig.collections.push(sanitizedDefaultUser);
        }
    }
    else if (!sanitizedConfig.collections.find((c) => c.slug === sanitizedConfig.admin.user)) {
        throw new errors_1.InvalidConfiguration(`${sanitizedConfig.admin.user} is not a valid admin user collection`);
    }
    sanitizedConfig.collections = sanitizedConfig.collections.map((collection) => (0, sanitize_1.default)(sanitizedConfig, collection));
    (0, checkDuplicateCollections_1.default)(sanitizedConfig.collections);
    if (sanitizedConfig.globals.length > 0) {
        sanitizedConfig.globals = (0, sanitize_2.default)(sanitizedConfig.collections, sanitizedConfig.globals);
    }
    if (typeof sanitizedConfig.serverURL === 'undefined') {
        sanitizedConfig.serverURL = '';
    }
    if (sanitizedConfig.serverURL !== '') {
        sanitizedConfig.csrf.push(sanitizedConfig.serverURL);
    }
    return sanitizedConfig;
};
exports.default = sanitizeConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FuaXRpemUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvY29uZmlnL3Nhbml0aXplLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsMERBQThCO0FBQzlCLHFEQUFnRDtBQUVoRCxzRUFBOEM7QUFDOUMsOEVBQWdFO0FBQ2hFLHNDQUFpRDtBQUNqRCwwRUFBeUQ7QUFDekQsdUdBQStFO0FBQy9FLHlDQUFzQztBQUV0QyxNQUFNLGNBQWMsR0FBRyxDQUFDLE1BQWMsRUFBbUIsRUFBRTtJQUN2RCxNQUFNLGVBQWUsR0FBRyxJQUFBLG1CQUFLLEVBQUMsbUJBQVEsRUFBRSxNQUFNLEVBQUU7UUFDNUMsaUJBQWlCLEVBQUUsK0JBQWE7S0FDbkMsQ0FBVyxDQUFDO0lBRWIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFO1FBQzdCLE1BQU0sdUJBQXVCLEdBQUcsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQzVELENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUNoQixDQUFDO1FBQ0YsSUFBSSx1QkFBdUIsRUFBRTtZQUN6QixlQUFlLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyx1QkFBdUIsQ0FBQyxJQUFJLENBQUM7U0FDN0Q7YUFBTTtZQUNILGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQztZQUNyQyxNQUFNLG9CQUFvQixHQUFHLElBQUEsa0JBQWtCLEVBQzNDLGVBQWUsRUFDZixxQkFBVyxDQUNkLENBQUM7WUFDRixlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1NBQzFEO0tBQ0o7U0FBTSxJQUNILENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQzdCLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUMvQyxFQUNIO1FBQ0UsTUFBTSxJQUFJLDZCQUFvQixDQUMxQixHQUFHLGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSSx1Q0FBdUMsQ0FDdkUsQ0FBQztLQUNMO0lBRUQsZUFBZSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FDekQsQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLElBQUEsa0JBQWtCLEVBQUMsZUFBZSxFQUFFLFVBQVUsQ0FBQyxDQUNsRSxDQUFDO0lBQ0YsSUFBQSxtQ0FBeUIsRUFBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7SUFFdkQsSUFBSSxlQUFlLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7UUFDcEMsZUFBZSxDQUFDLE9BQU8sR0FBRyxJQUFBLGtCQUFlLEVBQ3JDLGVBQWUsQ0FBQyxXQUFXLEVBQzNCLGVBQWUsQ0FBQyxPQUFPLENBQzFCLENBQUM7S0FDTDtJQUVELElBQUksT0FBTyxlQUFlLENBQUMsU0FBUyxLQUFLLFdBQVcsRUFBRTtRQUNsRCxlQUFlLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztLQUNsQztJQUVELElBQUksZUFBZSxDQUFDLFNBQVMsS0FBSyxFQUFFLEVBQUU7UUFDbEMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0tBQ3hEO0lBRUQsT0FBTyxlQUFrQyxDQUFDO0FBQzlDLENBQUMsQ0FBQztBQUVGLGtCQUFlLGNBQWMsQ0FBQyJ9