import { Permissions } from '../../auth';
import { SanitizedCollectionConfig } from '../../collections/config/types';
import { SanitizedGlobalConfig } from '../../globals/config/types';
export declare enum EntityType {
    collection = "Collections",
    global = "Globals"
}
export declare type EntityToGroup = {
    type: EntityType.collection;
    entity: SanitizedCollectionConfig;
} | {
    type: EntityType.global;
    entity: SanitizedGlobalConfig;
};
export declare type Group = {
    label: string;
    entities: EntityToGroup[];
};
export declare function groupNavItems(entities: EntityToGroup[], permissions: Permissions): Group[];
