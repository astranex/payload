export var EntityType;
(function (EntityType) {
    EntityType["collection"] = "Collections";
    EntityType["global"] = "Globals";
})(EntityType || (EntityType = {}));
export function groupNavItems(entities, permissions) {
    const result = entities.reduce((groups, entityToGroup) => {
        var _a, _b;
        if ((_b = (_a = permissions === null || permissions === void 0 ? void 0 : permissions[entityToGroup.type.toLowerCase()]) === null || _a === void 0 ? void 0 : _a[entityToGroup.entity.slug]) === null || _b === void 0 ? void 0 : _b.read.permission) {
            if (entityToGroup.entity.admin.group) {
                const existingGroup = groups.find((group) => group.label === entityToGroup.entity.admin.group);
                let matchedGroup = existingGroup;
                if (!existingGroup) {
                    matchedGroup = {
                        label: entityToGroup.entity.admin.group,
                        entities: []
                    };
                    groups.push(matchedGroup);
                }
                matchedGroup.entities.push(entityToGroup);
            }
            else {
                const defaultGroup = groups.find((group) => group.label === entityToGroup.type);
                defaultGroup.entities.push(entityToGroup);
            }
        }
        return groups;
    }, [
        {
            label: 'Collections',
            entities: []
        },
        {
            label: 'Globals',
            entities: []
        }
    ]);
    return result.filter((group) => group.entities.length > 0);
}
