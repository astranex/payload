import React, { useState, useCallback } from 'react';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { Modal, useModal } from '@faceless-ui/modal';
import { useConfig } from '../../utilities/Config';
import Button from '../Button';
import MinimalTemplate from '../../templates/Minimal';
import { useForm } from '../../forms/Form/context';
import useTitle from '../../../hooks/useTitle';
import { requests } from '../../../api';
import './index.scss';
const baseClass = 'delete-document';
const DeleteDocument = (props) => {
    const { title: titleFromProps, id, buttonId, collection: { admin: { useAsTitle }, slug, labels: { singular } = {} } = {} } = props;
    const { serverURL, routes: { api, admin }, admin: { locale: adminUILocale } } = useConfig();
    const { setModified } = useForm();
    const [deleting, setDeleting] = useState(false);
    const { toggleModal } = useModal();
    const history = useHistory();
    const title = useTitle(useAsTitle) || id;
    const titleToRender = titleFromProps || title;
    const modalSlug = `delete-${id}`;
    const addDefaultError = useCallback(() => {
        toast.error(`${adminUILocale.elements.DeleteDocument.DeletingError[0]} ${title}. ${adminUILocale.elements.DeleteDocument.DeletingError[1]}`);
    }, [title]);
    const handleDelete = useCallback(() => {
        setDeleting(true);
        setModified(false);
        requests
            .delete(`${serverURL}${api}/${slug}/${id}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(async (res) => {
            try {
                const json = await res.json();
                if (res.status < 400) {
                    toggleModal(modalSlug);
                    toast.success(`${singular} "${title}" ${adminUILocale.elements.DeleteDocument.SuccessfullyDeletedLabel}`);
                    return history.push(`${admin}/collections/${slug}`);
                }
                toggleModal(modalSlug);
                if (json.errors) {
                    json.errors.forEach((error) => toast.error(error.message));
                }
                else {
                    addDefaultError();
                }
                return false;
            }
            catch (e) {
                return addDefaultError();
            }
        });
    }, [
        addDefaultError,
        toggleModal,
        modalSlug,
        history,
        id,
        singular,
        slug,
        title,
        admin,
        api,
        serverURL,
        setModified
    ]);
    if (id) {
        return (React.createElement(React.Fragment, null,
            React.createElement("button", { type: "button", id: buttonId, className: `${baseClass}__toggle`, onClick: (e) => {
                    e.preventDefault();
                    setDeleting(false);
                    toggleModal(modalSlug);
                } }, adminUILocale.elements.DeleteDocument.DeleteLabel),
            React.createElement(Modal, { slug: modalSlug, className: baseClass },
                React.createElement(MinimalTemplate, { className: `${baseClass}__template` },
                    React.createElement("h1", null, adminUILocale.elements.DeleteDocument
                        .ConfirmDeletionLabels[0]),
                    React.createElement("p", null,
                        adminUILocale.elements.DeleteDocument
                            .ConfirmDeletionLabels[1],
                        ' ',
                        singular,
                        " \"",
                        React.createElement("strong", null, titleToRender),
                        "\".",
                        ' ',
                        adminUILocale.elements.DeleteDocument
                            .ConfirmDeletionLabels[2]),
                    React.createElement(Button, { id: "confirm-cancel", buttonStyle: "secondary", type: "button", onClick: deleting
                            ? undefined
                            : () => toggleModal(modalSlug) }, adminUILocale.elements.DeleteDocument.CancelLabel),
                    React.createElement(Button, { onClick: deleting ? undefined : handleDelete, id: "confirm-delete" }, deleting
                        ? adminUILocale.elements.DeleteDocument
                            .DeletingLabel
                        : adminUILocale.elements.DeleteDocument
                            .ConfirmLabel)))));
    }
    return null;
};
export default DeleteDocument;
