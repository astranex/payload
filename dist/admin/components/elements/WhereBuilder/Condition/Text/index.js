import React from 'react';
import './index.scss';
import { useConfig } from '../../../../utilities/Config';
const baseClass = 'condition-value-text';
const Text = ({ onChange, value }) => {
    const { admin } = useConfig();
    const { locale: adminUILocale } = admin;
    return (React.createElement("input", { placeholder: adminUILocale.elements.WhereBuilder.TextInputLabel, className: baseClass, type: "text", onChange: (e) => onChange(e.target.value), value: value || '' }));
};
export default Text;
