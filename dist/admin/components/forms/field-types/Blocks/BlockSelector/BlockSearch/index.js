import React from 'react';
import SearchIcon from '../../../../../graphics/Search';
import { useConfig } from '../../../../../utilities/Config';
import './index.scss';
const baseClass = 'block-search';
const BlockSearch = (props) => {
    const { admin: { locale: adminUILocale } } = useConfig();
    const { setSearchTerm } = props;
    const handleChange = (e) => {
        setSearchTerm(e.target.value);
    };
    return (React.createElement("div", { className: baseClass },
        React.createElement("input", { className: `${baseClass}__input`, placeholder: adminUILocale.forms.FieldTypes.Blocks.SearchBlockLabel, onChange: handleChange }),
        React.createElement(SearchIcon, null)));
};
export default BlockSearch;
