import React from 'react';
import useField from '../../../useField';
import './index.scss';
import { useConfig } from '../../../../utilities/Config';
const baseClass = 'section-title';
const SectionTitle = (props) => {
    const { admin: { locale: adminUILocale } } = useConfig();
    const { path, readOnly } = props;
    const { value, setValue } = useField({ path });
    const classes = [baseClass].filter(Boolean).join(' ');
    return (React.createElement("div", { className: classes, "data-value": value },
        React.createElement("input", { className: `${baseClass}__input`, id: path, value: value || '', placeholder: adminUILocale.forms.FieldTypes.Blocks.UntitledLabel, type: "text", name: path, onChange: (e) => {
                e.stopPropagation();
                e.preventDefault();
                setValue(e.target.value);
            }, readOnly: readOnly })));
};
export default SectionTitle;
