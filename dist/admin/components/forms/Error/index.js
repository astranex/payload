import React from 'react';
import Tooltip from '../../elements/Tooltip';
import './index.scss';
import { useConfig } from '../../utilities/Config';
const baseClass = 'field-error';
const Error = (props) => {
    const { admin } = useConfig();
    const { locale: adminUILocale } = admin;
    const { showError = false, message = adminUILocale.forms.Error.ErrorMessageLabel } = props;
    if (showError) {
        return React.createElement(Tooltip, { className: baseClass }, message);
    }
    return null;
};
export default Error;
