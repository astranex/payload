import React from 'react';
import { useHistory } from 'react-router-dom';
import { useModal, Modal } from '@faceless-ui/modal';
import { useConfig } from '../../utilities/Config';
import MinimalTemplate from '../../templates/Minimal';
import Button from '../../elements/Button';
import './index.scss';
const baseClass = 'stay-logged-in';
const modalSlug = 'stay-logged-in';
const StayLoggedInModal = (props) => {
    const { refreshCookie } = props;
    const history = useHistory();
    const { routes: { admin }, admin: { locale: adminUILocale } } = useConfig();
    const { toggleModal } = useModal();
    return (React.createElement(Modal, { className: baseClass, slug: "stay-logged-in" },
        React.createElement(MinimalTemplate, { className: `${baseClass}__template` },
            React.createElement("h1", null, adminUILocale.modals.StayLoggedIn.StayLoggedInLabels[0]),
            React.createElement("p", null, adminUILocale.modals.StayLoggedIn.StayLoggedInLabels[1]),
            React.createElement("div", { className: `${baseClass}__actions` },
                React.createElement(Button, { buttonStyle: "secondary", onClick: () => {
                        toggleModal(modalSlug);
                        history.push(`${admin}/logout`);
                    } }, adminUILocale.modals.StayLoggedIn.LogOutLabel),
                React.createElement(Button, { onClick: () => {
                        refreshCookie();
                        toggleModal(modalSlug);
                    } }, adminUILocale.modals.StayLoggedIn.StayLoggedInLabel)))));
};
export default StayLoggedInModal;
