import React from 'react';
import NavigationPrompt from 'react-router-navigation-prompt';
import { useAuth } from '../../utilities/Auth';
import { useFormModified } from '../../forms/Form/context';
import MinimalTemplate from '../../templates/Minimal';
import Button from '../../elements/Button';
import './index.scss';
import { useConfig } from '../../utilities/Config';
const modalSlug = 'leave-without-saving';
const LeaveWithoutSaving = () => {
    const { admin: { locale: adminUILocale } } = useConfig();
    const modified = useFormModified();
    const { user } = useAuth();
    return (React.createElement(NavigationPrompt, { when: Boolean(modified && user) }, ({ onConfirm, onCancel }) => (React.createElement("div", { className: modalSlug },
        React.createElement(MinimalTemplate, { className: `${modalSlug}__template` },
            React.createElement("h1", null, adminUILocale.modals.LeaveWithoutSaving
                .LeaveWithoutSavingLabels[0]),
            React.createElement("p", null, adminUILocale.modals.LeaveWithoutSaving
                .LeaveWithoutSavingLabels[1]),
            React.createElement(Button, { onClick: onCancel, buttonStyle: "secondary" }, adminUILocale.modals.LeaveWithoutSaving
                .StayOnThisPageLabel),
            React.createElement(Button, { onClick: onConfirm }, adminUILocale.modals.LeaveWithoutSaving
                .LeaveAnywayLabel))))));
};
export default LeaveWithoutSaving;
