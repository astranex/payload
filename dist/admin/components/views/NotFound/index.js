import React, { useEffect } from 'react';
import { useConfig } from '../../utilities/Config';
import Eyebrow from '../../elements/Eyebrow';
import { useStepNav } from '../../elements/StepNav';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';
import { Gutter } from '../../elements/Gutter';
const baseClass = 'not-found';
const NotFound = () => {
    const { setStepNav } = useStepNav();
    const { routes: { admin }, admin: { locale: adminUILocale } } = useConfig();
    useEffect(() => {
        setStepNav([
            {
                label: adminUILocale.views.NotFound.NotFoundLabel
            }
        ]);
    }, [setStepNav]);
    return (React.createElement("div", { className: baseClass },
        React.createElement(Meta, { title: adminUILocale.views.NotFound.MetaTitleLabel, description: adminUILocale.views.NotFound.MetaDescriptionLabel, keywords: adminUILocale.views.NotFound.MetaKeywordsLabel }),
        React.createElement(Eyebrow, null),
        React.createElement(Gutter, { className: `${baseClass}__wrap` },
            React.createElement("h1", null, adminUILocale.views.NotFound.NothingFoundLabels[0]),
            React.createElement("p", null, adminUILocale.views.NotFound.NothingFoundLabels[1]),
            React.createElement(Button, { el: "link", to: `${admin}` }, adminUILocale.views.NotFound.BackToDashboardLabel))));
};
export default NotFound;
