import React from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useConfig } from '../../utilities/Config';
import { useAuth } from '../../utilities/Auth';
import MinimalTemplate from '../../templates/Minimal';
import Form from '../../forms/Form';
import Password from '../../forms/field-types/Password';
import ConfirmPassword from '../../forms/field-types/ConfirmPassword';
import FormSubmit from '../../forms/Submit';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';
import './index.scss';
import HiddenInput from '../../forms/field-types/HiddenInput';
const baseClass = 'reset-password';
const ResetPassword = () => {
    const { admin: { user: userSlug, locale: adminUILocale }, serverURL, routes: { admin, api } } = useConfig();
    const { token } = useParams();
    const history = useHistory();
    const { user, setToken } = useAuth();
    const onSuccess = (data) => {
        if (data.token) {
            setToken(data.token);
            history.push(`${admin}`);
        }
    };
    if (user) {
        return (React.createElement(MinimalTemplate, { className: baseClass },
            React.createElement(Meta, { title: adminUILocale.views.ResetPassword.MetaTitleLabel, description: adminUILocale.views.ResetPassword.MetaDescriptionLabel, keywords: adminUILocale.views.ResetPassword.MetaKeywordsLabel }),
            React.createElement("div", { className: `${baseClass}__wrap` },
                React.createElement("h1", null, adminUILocale.views.ResetPassword
                    .AlreadyLoggedInLabels[0]),
                React.createElement("p", null,
                    adminUILocale.views.ResetPassword
                        .AlreadyLoggedInLabels[1],
                    ' ',
                    React.createElement(Link, { to: `${admin}/logout` }, adminUILocale.views.ResetPassword
                        .AlreadyLoggedInLabels[2]),
                    ' ',
                    adminUILocale.views.ResetPassword
                        .AlreadyLoggedInLabels[3]),
                React.createElement("br", null),
                React.createElement(Button, { el: "link", buttonStyle: "secondary", to: admin }, adminUILocale.views.ResetPassword.BackToDashboardLabel))));
    }
    return (React.createElement(MinimalTemplate, { className: baseClass },
        React.createElement("div", { className: `${baseClass}__wrap` },
            React.createElement("h1", null, adminUILocale.views.ResetPassword.ResetPasswordLabel),
            React.createElement(Form, { onSuccess: onSuccess, method: "post", action: `${serverURL}${api}/${userSlug}/reset-password`, redirect: admin },
                React.createElement(Password, { label: adminUILocale.views.ResetPassword.NewPasswordLabel, name: "password", autoComplete: "off", required: true }),
                React.createElement(ConfirmPassword, null),
                React.createElement(HiddenInput, { name: "token", value: token }),
                React.createElement(FormSubmit, null, adminUILocale.views.ResetPassword
                    .ResetPasswordButtonLabel)))));
};
export default ResetPassword;
