import React from 'react';
import { useConfig } from '../../../../../../utilities/Config';
const ArrayCell = ({ data, field }) => {
    var _a;
    const { admin: { locale: adminUILocale } } = useConfig();
    const arrayFields = data !== null && data !== void 0 ? data : [];
    const label = `${arrayFields.length} ${((_a = field === null || field === void 0 ? void 0 : field.labels) === null || _a === void 0 ? void 0 : _a.plural) || adminUILocale.views.collections.List.Cell.fieldTypes.Array.RowsLabel}`;
    return React.createElement("span", null, label);
};
export default ArrayCell;
