import React from 'react';
import { useConfig } from '../../utilities/Config';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';
import MinimalTemplate from '../../templates/Minimal';
const Unauthorized = () => {
    const { routes: { admin }, admin: { locale: adminUILocale } } = useConfig();
    return (React.createElement(MinimalTemplate, { className: "unauthorized" },
        React.createElement(Meta, { title: adminUILocale.views.Unauthorized.MetaTitleLabel, description: adminUILocale.views.Unauthorized.MetaDescriptionLabel, keywords: adminUILocale.views.Unauthorized.MetaKeywordsLabel }),
        React.createElement("h2", null, adminUILocale.views.Unauthorized.UnauthorizedLabel),
        React.createElement("p", null, adminUILocale.views.Unauthorized.NotAllowedLabel),
        React.createElement("br", null),
        React.createElement(Button, { el: "link", to: `${admin}/logout` }, adminUILocale.views.Unauthorized.LogOutLabel)));
};
export default Unauthorized;
