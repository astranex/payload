import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useConfig } from '../../utilities/Config';
import { useAuth } from '../../utilities/Auth';
import MinimalTemplate from '../../templates/Minimal';
import Form from '../../forms/Form';
import Email from '../../forms/field-types/Email';
import FormSubmit from '../../forms/Submit';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';
import './index.scss';
const baseClass = 'forgot-password';
const ForgotPassword = () => {
    const { admin: { locale: adminUILocale } } = useConfig();
    const [hasSubmitted, setHasSubmitted] = useState(false);
    const { user } = useAuth();
    const { admin: { user: userSlug }, serverURL, routes: { admin, api } } = useConfig();
    const handleResponse = (res) => {
        res.json().then(() => {
            setHasSubmitted(true);
        }, () => {
            toast.error(adminUILocale.views.ForgotPassword.ErrorLabel);
        });
    };
    if (user) {
        return (React.createElement(MinimalTemplate, { className: baseClass },
            React.createElement(Meta, { title: adminUILocale.views.ForgotPassword.MetaTitleLabel, description: adminUILocale.views.ForgotPassword.MetaDescriptionLabel, keywords: adminUILocale.views.ForgotPassword.MetaKeywordsLabel }),
            React.createElement("h1", null, adminUILocale.views.ForgotPassword.ForgotPasswordLabels[0]),
            React.createElement("p", null,
                adminUILocale.views.ForgotPassword.ForgotPasswordLabels[1],
                ' ',
                React.createElement(Link, { to: `${admin}/account` }, adminUILocale.views.ForgotPassword
                    .ForgotPasswordLabels[2]),
                ' ',
                adminUILocale.views.ForgotPassword.ForgotPasswordLabels[3]),
            React.createElement("br", null),
            React.createElement(Button, { el: "link", buttonStyle: "secondary", to: admin }, adminUILocale.views.ForgotPassword.BackToDashboardLabel)));
    }
    if (hasSubmitted) {
        return (React.createElement(MinimalTemplate, { className: baseClass },
            React.createElement("h1", null, adminUILocale.views.ForgotPassword.EmailSentLabel),
            React.createElement("p", null, adminUILocale.views.ForgotPassword.CheckEmailLabel)));
    }
    return (React.createElement(MinimalTemplate, { className: baseClass },
        React.createElement(Form, { handleResponse: handleResponse, method: "post", action: `${serverURL}${api}/${userSlug}/forgot-password` },
            React.createElement("h1", null, adminUILocale.views.ForgotPassword.ForgotPasswordLabel),
            React.createElement("p", null, adminUILocale.views.ForgotPassword.EnterEmailLabel),
            React.createElement(Email, { label: adminUILocale.views.ForgotPassword.EmailAddressLabel, name: "email", admin: { autoComplete: 'email' }, required: true }),
            React.createElement(FormSubmit, null, adminUILocale.views.ForgotPassword.SubmitLabel)),
        React.createElement(Link, { to: `${admin}/login` }, adminUILocale.views.ForgotPassword.BackToLogin)));
};
export default ForgotPassword;
