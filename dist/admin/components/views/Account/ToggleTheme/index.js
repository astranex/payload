import React, { useCallback } from 'react';
import RadioGroupInput from '../../../forms/field-types/RadioGroup/Input';
import { useConfig } from '../../../utilities/Config';
import { useTheme } from '../../../utilities/Theme';
export const ToggleTheme = () => {
    const { admin: { locale: adminUILocale } } = useConfig();
    const { theme, setTheme, autoMode } = useTheme();
    const onChange = useCallback((newTheme) => {
        setTheme(newTheme);
    }, [setTheme]);
    return (React.createElement(RadioGroupInput, { name: "theme", label: adminUILocale.views.Account.RadioGroupInputLabel, value: autoMode ? 'auto' : theme, onChange: onChange, options: [
            {
                label: adminUILocale.views.Account.AutomaticLabel,
                value: 'auto'
            },
            {
                label: adminUILocale.views.Account.LightLabel,
                value: 'light'
            },
            {
                label: adminUILocale.views.Account.DarkLabel,
                value: 'dark'
            }
        ] }));
};
