import React, { useEffect } from 'react';
import { useConfig } from '../../utilities/Config';
import { useAuth } from '../../utilities/Auth';
import Minimal from '../../templates/Minimal';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';
import './index.scss';
const baseClass = 'logout';
const Logout = (props) => {
    const { inactivity } = props;
    const { logOut } = useAuth();
    const { routes: { admin }, admin: { locale: adminUILocale } } = useConfig();
    useEffect(() => {
        logOut();
    }, [logOut]);
    return (React.createElement(Minimal, { className: baseClass },
        React.createElement(Meta, { title: adminUILocale.views.Logout.MetaTitleLabel, description: adminUILocale.views.Logout.MetaDescriptionLabel, keywords: adminUILocale.views.Logout.MetaKeywordsLabel }),
        React.createElement("div", { className: `${baseClass}__wrap` },
            inactivity && (React.createElement("h2", null, adminUILocale.views.Logout.LoggedOutInactivityLabel)),
            !inactivity && (React.createElement("h2", null, adminUILocale.views.Logout.LoggedOutSuccessfullyLabel)),
            React.createElement("br", null),
            React.createElement(Button, { el: "anchor", buttonStyle: "secondary", url: `${admin}/login` }, adminUILocale.views.Logout.LogBackInLabel))));
};
export default Logout;
