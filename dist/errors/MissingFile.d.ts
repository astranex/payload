import APIError from './APIError';
declare class MissingFile extends APIError {
    constructor(message?: string);
}
export default MissingFile;
