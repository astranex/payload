import APIError from './APIError';
declare class NotFound extends APIError {
    constructor(message?: string);
}
export default NotFound;
