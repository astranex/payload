import APIError from './APIError';
declare class FileUploadError extends APIError {
    constructor(message?: string);
}
export default FileUploadError;
