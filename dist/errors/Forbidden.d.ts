import APIError from './APIError';
declare class Forbidden extends APIError {
    constructor(message?: string);
}
export default Forbidden;
