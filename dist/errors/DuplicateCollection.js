"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const APIError_1 = __importDefault(require("./APIError"));
class DuplicateCollection extends APIError_1.default {
    constructor(propertyName, duplicates, message = ['Collection', 'already in use:']) {
        super(`${message[0]} ${propertyName} ${message[1]} "${duplicates.join(', ')}"`);
    }
}
exports.default = DuplicateCollection;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRHVwbGljYXRlQ29sbGVjdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9lcnJvcnMvRHVwbGljYXRlQ29sbGVjdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDBEQUFrQztBQUVsQyxNQUFNLG1CQUFvQixTQUFRLGtCQUFRO0lBQ3RDLFlBQ0ksWUFBb0IsRUFDcEIsVUFBb0IsRUFDcEIsVUFBb0IsQ0FBQyxZQUFZLEVBQUUsaUJBQWlCLENBQUM7UUFFckQsS0FBSyxDQUNELEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLFlBQVksSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssVUFBVSxDQUFDLElBQUksQ0FDM0QsSUFBSSxDQUNQLEdBQUcsQ0FDUCxDQUFDO0lBQ04sQ0FBQztDQUNKO0FBRUQsa0JBQWUsbUJBQW1CLENBQUMifQ==