import APIError from './APIError';
declare class UnauthorizedError extends APIError {
    constructor(message?: string);
}
export default UnauthorizedError;
