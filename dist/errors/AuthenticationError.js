"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = __importDefault(require("http-status"));
const APIError_1 = __importDefault(require("./APIError"));
class AuthenticationError extends APIError_1.default {
    constructor(message = 'The email or password provided is incorrect.') {
        super(message, http_status_1.default.UNAUTHORIZED);
    }
}
exports.default = AuthenticationError;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0aGVudGljYXRpb25FcnJvci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9lcnJvcnMvQXV0aGVudGljYXRpb25FcnJvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDhEQUFxQztBQUNyQywwREFBa0M7QUFDbEMsTUFBTSxtQkFBb0IsU0FBUSxrQkFBUTtJQUN0QyxZQUFZLE9BQU8sR0FBRyw4Q0FBOEM7UUFDaEUsS0FBSyxDQUFDLE9BQU8sRUFBRSxxQkFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzVDLENBQUM7Q0FDSjtBQUVELGtCQUFlLG1CQUFtQixDQUFDIn0=