import APIError from './APIError';
declare class AuthenticationError extends APIError {
    constructor(message?: string);
}
export default AuthenticationError;
