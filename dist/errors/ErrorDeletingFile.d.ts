import APIError from './APIError';
declare class ErrorDeletingFile extends APIError {
    constructor(message?: string);
}
export default ErrorDeletingFile;
