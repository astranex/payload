import APIError from './APIError';
declare class LockedAuth extends APIError {
    constructor(message?: string);
}
export default LockedAuth;
