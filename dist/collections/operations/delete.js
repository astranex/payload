"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require("../.."));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const sanitizeInternalFields_1 = __importDefault(require("../../utilities/sanitizeInternalFields"));
const errors_1 = require("../../errors");
const executeAccess_1 = __importDefault(require("../../auth/executeAccess"));
const types_1 = require("../../auth/types");
const fileExists_1 = __importDefault(require("../../uploads/fileExists"));
const afterRead_1 = require("../../fields/hooks/afterRead");
async function deleteOperation(incomingArgs) {
    let args = incomingArgs;
    // /////////////////////////////////////
    // beforeOperation - Collection
    // /////////////////////////////////////
    await args.collection.config.hooks.beforeOperation.reduce(async (priorHook, hook) => {
        await priorHook;
        args =
            (await hook({
                args,
                operation: 'delete'
            })) || args;
    }, Promise.resolve());
    const adminUILocale = __1.default.config.admin.locale;
    const { depth, collection: { Model, config: collectionConfig }, id, req, req: { locale, payload: { config, preferences } }, overrideAccess, showHiddenFields } = args;
    // /////////////////////////////////////
    // Access
    // /////////////////////////////////////
    const accessResults = !overrideAccess
        ? await (0, executeAccess_1.default)({ req, id }, collectionConfig.access.delete)
        : true;
    const hasWhereAccess = (0, types_1.hasWhereAccessResult)(accessResults);
    // /////////////////////////////////////
    // beforeDelete - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.beforeDelete.reduce(async (priorHook, hook) => {
        await priorHook;
        return hook({
            req,
            id
        });
    }, Promise.resolve());
    // /////////////////////////////////////
    // Retrieve document
    // /////////////////////////////////////
    const queryToBuild = {
        where: {
            and: [
                {
                    id: {
                        equals: id
                    }
                }
            ]
        }
    };
    if ((0, types_1.hasWhereAccessResult)(accessResults)) {
        queryToBuild.where.and.push(accessResults);
    }
    const query = await Model.buildQuery(queryToBuild, locale);
    const docToDelete = await Model.findOne(query);
    if (!docToDelete && !hasWhereAccess)
        throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
    if (!docToDelete && hasWhereAccess)
        throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
    const resultToDelete = docToDelete.toJSON({ virtuals: true });
    // /////////////////////////////////////
    // Delete any associated files
    // /////////////////////////////////////
    if (collectionConfig.upload) {
        const { staticDir } = collectionConfig.upload;
        const staticPath = path_1.default.resolve(config.paths.configDir, staticDir);
        const fileToDelete = `${staticPath}/${resultToDelete.filename}`;
        if (await (0, fileExists_1.default)(fileToDelete)) {
            fs_1.default.unlink(fileToDelete, (err) => {
                if (err) {
                    throw new errors_1.ErrorDeletingFile(adminUILocale.errors.ErrorDeletingFileLabel);
                }
            });
        }
        if (resultToDelete.sizes) {
            Object.values(resultToDelete.sizes).forEach(async (size) => {
                const sizeToDelete = `${staticPath}/${size.filename}`;
                if (await (0, fileExists_1.default)(sizeToDelete)) {
                    fs_1.default.unlink(sizeToDelete, (err) => {
                        if (err) {
                            throw new errors_1.ErrorDeletingFile(adminUILocale.errors.ErrorDeletingFileLabel);
                        }
                    });
                }
            });
        }
    }
    // /////////////////////////////////////
    // Delete document
    // /////////////////////////////////////
    const doc = await Model.findOneAndDelete({ _id: id });
    let result = doc.toJSON({ virtuals: true });
    // custom id type reset
    result.id = result._id;
    result = JSON.stringify(result);
    result = JSON.parse(result);
    result = (0, sanitizeInternalFields_1.default)(result);
    // /////////////////////////////////////
    // Delete Preferences
    // /////////////////////////////////////
    if (collectionConfig.auth) {
        await preferences.Model.deleteMany({
            user: id,
            userCollection: collectionConfig.slug
        });
    }
    await preferences.Model.deleteMany({
        key: `collection-${collectionConfig.slug}-${id}`
    });
    // /////////////////////////////////////
    // afterDelete - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterDelete.reduce(async (priorHook, hook) => {
        await priorHook;
        result = (await hook({ req, id, doc: result })) || result;
    }, Promise.resolve());
    // /////////////////////////////////////
    // afterRead - Fields
    // /////////////////////////////////////
    result = await (0, afterRead_1.afterRead)({
        depth,
        doc: result,
        entityConfig: collectionConfig,
        overrideAccess,
        req,
        showHiddenFields
    });
    // /////////////////////////////////////
    // afterRead - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result =
            (await hook({
                req,
                doc: result
            })) || result;
    }, Promise.resolve());
    // /////////////////////////////////////
    // 8. Return results
    // /////////////////////////////////////
    return result;
}
exports.default = deleteOperation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsZXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbGxlY3Rpb25zL29wZXJhdGlvbnMvZGVsZXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsOENBQTRCO0FBQzVCLDRDQUFvQjtBQUNwQixnREFBd0I7QUFHeEIsb0dBQTRFO0FBQzVFLHlDQUFzRTtBQUN0RSw2RUFBcUQ7QUFHckQsNENBQXdEO0FBRXhELDBFQUFrRDtBQUNsRCw0REFBeUQ7QUFXekQsS0FBSyxVQUFVLGVBQWUsQ0FBQyxZQUF1QjtJQUNsRCxJQUFJLElBQUksR0FBRyxZQUFZLENBQUM7SUFFeEIsd0NBQXdDO0lBQ3hDLCtCQUErQjtJQUMvQix3Q0FBd0M7SUFFeEMsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FDckQsS0FBSyxFQUNELFNBQThDLEVBQzlDLElBQXlCLEVBQzNCLEVBQUU7UUFDQSxNQUFNLFNBQVMsQ0FBQztRQUVoQixJQUFJO1lBQ0EsQ0FBQyxNQUFNLElBQUksQ0FBQztnQkFDUixJQUFJO2dCQUNKLFNBQVMsRUFBRSxRQUFRO2FBQ3RCLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQztJQUNwQixDQUFDLEVBQ0QsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUNwQixDQUFDO0lBRUYsTUFBTSxhQUFhLEdBQUcsV0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWxELE1BQU0sRUFDRixLQUFLLEVBQ0wsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxFQUMvQyxFQUFFLEVBQ0YsR0FBRyxFQUNILEdBQUcsRUFBRSxFQUNELE1BQU0sRUFDTixPQUFPLEVBQUUsRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLEVBQ25DLEVBQ0QsY0FBYyxFQUNkLGdCQUFnQixFQUNuQixHQUFHLElBQUksQ0FBQztJQUVULHdDQUF3QztJQUN4QyxTQUFTO0lBQ1Qsd0NBQXdDO0lBRXhDLE1BQU0sYUFBYSxHQUFHLENBQUMsY0FBYztRQUNqQyxDQUFDLENBQUMsTUFBTSxJQUFBLHVCQUFhLEVBQUMsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUNsRSxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ1gsTUFBTSxjQUFjLEdBQUcsSUFBQSw0QkFBb0IsRUFBQyxhQUFhLENBQUMsQ0FBQztJQUUzRCx3Q0FBd0M7SUFDeEMsNEJBQTRCO0lBQzVCLHdDQUF3QztJQUV4QyxNQUFNLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUM1QyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFO1FBQ3RCLE1BQU0sU0FBUyxDQUFDO1FBRWhCLE9BQU8sSUFBSSxDQUFDO1lBQ1IsR0FBRztZQUNILEVBQUU7U0FDTCxDQUFDLENBQUM7SUFDUCxDQUFDLEVBQ0QsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUNwQixDQUFDO0lBRUYsd0NBQXdDO0lBQ3hDLG9CQUFvQjtJQUNwQix3Q0FBd0M7SUFFeEMsTUFBTSxZQUFZLEdBRWQ7UUFDQSxLQUFLLEVBQUU7WUFDSCxHQUFHLEVBQUU7Z0JBQ0Q7b0JBQ0ksRUFBRSxFQUFFO3dCQUNBLE1BQU0sRUFBRSxFQUFFO3FCQUNiO2lCQUNKO2FBQ0o7U0FDSjtLQUNKLENBQUM7SUFFRixJQUFJLElBQUEsNEJBQW9CLEVBQUMsYUFBYSxDQUFDLEVBQUU7UUFDcEMsWUFBWSxDQUFDLEtBQUssQ0FBQyxHQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0tBQzNEO0lBRUQsTUFBTSxLQUFLLEdBQUcsTUFBTSxLQUFLLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUUzRCxNQUFNLFdBQVcsR0FBRyxNQUFNLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFL0MsSUFBSSxDQUFDLFdBQVcsSUFBSSxDQUFDLGNBQWM7UUFBRSxNQUFNLElBQUksaUJBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzVGLElBQUksQ0FBQyxXQUFXLElBQUksY0FBYztRQUFFLE1BQU0sSUFBSSxrQkFBUyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7SUFFN0YsTUFBTSxjQUFjLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBRTlELHdDQUF3QztJQUN4Qyw4QkFBOEI7SUFDOUIsd0NBQXdDO0lBRXhDLElBQUksZ0JBQWdCLENBQUMsTUFBTSxFQUFFO1FBQ3pCLE1BQU0sRUFBRSxTQUFTLEVBQUUsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUM7UUFFOUMsTUFBTSxVQUFVLEdBQUcsY0FBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUVuRSxNQUFNLFlBQVksR0FBRyxHQUFHLFVBQVUsSUFBSSxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEUsSUFBSSxNQUFNLElBQUEsb0JBQVUsRUFBQyxZQUFZLENBQUMsRUFBRTtZQUNoQyxZQUFFLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUM1QixJQUFJLEdBQUcsRUFBRTtvQkFDTCxNQUFNLElBQUksMEJBQWlCLENBQ3ZCLGFBQWEsQ0FBQyxNQUFNLENBQUMsc0JBQXNCLENBQzlDLENBQUM7aUJBQ0w7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxjQUFjLENBQUMsS0FBSyxFQUFFO1lBQ3RCLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FDdkMsS0FBSyxFQUFFLElBQWMsRUFBRSxFQUFFO2dCQUNyQixNQUFNLFlBQVksR0FBRyxHQUFHLFVBQVUsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RELElBQUksTUFBTSxJQUFBLG9CQUFVLEVBQUMsWUFBWSxDQUFDLEVBQUU7b0JBQ2hDLFlBQUUsQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUU7d0JBQzVCLElBQUksR0FBRyxFQUFFOzRCQUNMLE1BQU0sSUFBSSwwQkFBaUIsQ0FDdkIsYUFBYSxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsQ0FDOUMsQ0FBQzt5QkFDTDtvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjtZQUNMLENBQUMsQ0FDSixDQUFDO1NBQ0w7S0FDSjtJQUVELHdDQUF3QztJQUN4QyxrQkFBa0I7SUFDbEIsd0NBQXdDO0lBRXhDLE1BQU0sR0FBRyxHQUFHLE1BQU0sS0FBSyxDQUFDLGdCQUFnQixDQUFDLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFFdEQsSUFBSSxNQUFNLEdBQWEsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBRXRELHVCQUF1QjtJQUN2QixNQUFNLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7SUFDdkIsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDNUIsTUFBTSxHQUFHLElBQUEsZ0NBQXNCLEVBQUMsTUFBTSxDQUFDLENBQUM7SUFFeEMsd0NBQXdDO0lBQ3hDLHFCQUFxQjtJQUNyQix3Q0FBd0M7SUFFeEMsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUU7UUFDdkIsTUFBTSxXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztZQUMvQixJQUFJLEVBQUUsRUFBRTtZQUNSLGNBQWMsRUFBRSxnQkFBZ0IsQ0FBQyxJQUFJO1NBQ3hDLENBQUMsQ0FBQztLQUNOO0lBQ0QsTUFBTSxXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUMvQixHQUFHLEVBQUUsY0FBYyxnQkFBZ0IsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFO0tBQ25ELENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4QywyQkFBMkI7SUFDM0Isd0NBQXdDO0lBRXhDLE1BQU0sZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUN0RSxNQUFNLFNBQVMsQ0FBQztRQUVoQixNQUFNLEdBQUcsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUM7SUFDOUQsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBRXRCLHdDQUF3QztJQUN4QyxxQkFBcUI7SUFDckIsd0NBQXdDO0lBRXhDLE1BQU0sR0FBRyxNQUFNLElBQUEscUJBQVMsRUFBQztRQUNyQixLQUFLO1FBQ0wsR0FBRyxFQUFFLE1BQU07UUFDWCxZQUFZLEVBQUUsZ0JBQWdCO1FBQzlCLGNBQWM7UUFDZCxHQUFHO1FBQ0gsZ0JBQWdCO0tBQ25CLENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4Qyx5QkFBeUI7SUFDekIsd0NBQXdDO0lBRXhDLE1BQU0sZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUNwRSxNQUFNLFNBQVMsQ0FBQztRQUVoQixNQUFNO1lBQ0YsQ0FBQyxNQUFNLElBQUksQ0FBQztnQkFDUixHQUFHO2dCQUNILEdBQUcsRUFBRSxNQUFNO2FBQ2QsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDO0lBQ3RCLENBQUMsRUFBRSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV0Qix3Q0FBd0M7SUFDeEMsb0JBQW9CO0lBQ3BCLHdDQUF3QztJQUV4QyxPQUFPLE1BQU0sQ0FBQztBQUNsQixDQUFDO0FBRUQsa0JBQWUsZUFBZSxDQUFDIn0=