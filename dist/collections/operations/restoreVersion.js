"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-underscore-dangle */
const http_status_1 = __importDefault(require("http-status"));
const errors_1 = require("../../errors");
const executeAccess_1 = __importDefault(require("../../auth/executeAccess"));
const types_1 = require("../../auth/types");
const sanitizeInternalFields_1 = __importDefault(require("../../utilities/sanitizeInternalFields"));
const afterChange_1 = require("../../fields/hooks/afterChange");
const afterRead_1 = require("../../fields/hooks/afterRead");
async function restoreVersion(args) {
    const { collection: { Model, config: collectionConfig }, id, overrideAccess = false, showHiddenFields, depth, req: { locale, payload }, req } = args;
    const adminUILocale = payload.config.admin.locale;
    if (!id) {
        throw new errors_1.APIError(adminUILocale.operations.RestoreVersion.MissingIDLabel, http_status_1.default.BAD_REQUEST);
    }
    // /////////////////////////////////////
    // Retrieve original raw version
    // /////////////////////////////////////
    const VersionModel = payload.versions[collectionConfig.slug];
    let rawVersion = await VersionModel.findOne({
        _id: id
    });
    if (!rawVersion) {
        throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
    }
    rawVersion = rawVersion.toJSON({ virtuals: true });
    const parentDocID = rawVersion.parent;
    // /////////////////////////////////////
    // Access
    // /////////////////////////////////////
    const accessResults = !overrideAccess
        ? await (0, executeAccess_1.default)({ req, id: parentDocID }, collectionConfig.access.update)
        : true;
    const hasWherePolicy = (0, types_1.hasWhereAccessResult)(accessResults);
    // /////////////////////////////////////
    // Retrieve document
    // /////////////////////////////////////
    const queryToBuild = {
        where: {
            and: [
                {
                    id: {
                        equals: parentDocID
                    }
                }
            ]
        }
    };
    if ((0, types_1.hasWhereAccessResult)(accessResults)) {
        queryToBuild.where.and.push(accessResults);
    }
    const query = await Model.buildQuery(queryToBuild, locale);
    const doc = await Model.findOne(query);
    if (!doc && !hasWherePolicy)
        throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
    if (!doc && hasWherePolicy)
        throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
    // /////////////////////////////////////
    // fetch previousDoc
    // /////////////////////////////////////
    const previousDoc = await payload.findByID({
        collection: collectionConfig.slug,
        id: parentDocID,
        depth
    });
    // /////////////////////////////////////
    // Update
    // /////////////////////////////////////
    let result = await Model.findByIdAndUpdate({ _id: parentDocID }, rawVersion.version, { new: true });
    result = result.toJSON({ virtuals: true });
    // custom id type reset
    result.id = result._id;
    result = JSON.stringify(result);
    result = JSON.parse(result);
    result = (0, sanitizeInternalFields_1.default)(result);
    // /////////////////////////////////////
    // afterRead - Fields
    // /////////////////////////////////////
    result = await (0, afterRead_1.afterRead)({
        depth,
        doc: result,
        entityConfig: collectionConfig,
        req,
        overrideAccess,
        showHiddenFields
    });
    // /////////////////////////////////////
    // afterRead - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result =
            (await hook({
                req,
                doc: result
            })) || result;
    }, Promise.resolve());
    // /////////////////////////////////////
    // afterChange - Fields
    // /////////////////////////////////////
    result = await (0, afterChange_1.afterChange)({
        data: result,
        doc: result,
        previousDoc,
        entityConfig: collectionConfig,
        operation: 'update',
        req
    });
    // /////////////////////////////////////
    // afterChange - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterChange.reduce(async (priorHook, hook) => {
        await priorHook;
        result =
            (await hook({
                doc: result,
                req,
                previousDoc,
                operation: 'update'
            })) || result;
    }, Promise.resolve());
    return result;
}
exports.default = restoreVersion;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzdG9yZVZlcnNpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY29sbGVjdGlvbnMvb3BlcmF0aW9ucy9yZXN0b3JlVmVyc2lvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLHlDQUF5QztBQUN6Qyw4REFBcUM7QUFHckMseUNBQTZEO0FBQzdELDZFQUFxRDtBQUNyRCw0Q0FBd0Q7QUFFeEQsb0dBQTRFO0FBQzVFLGdFQUE2RDtBQUM3RCw0REFBeUQ7QUFhekQsS0FBSyxVQUFVLGNBQWMsQ0FDekIsSUFBZTtJQUVmLE1BQU0sRUFDRixVQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLGdCQUFnQixFQUFFLEVBQy9DLEVBQUUsRUFDRixjQUFjLEdBQUcsS0FBSyxFQUN0QixnQkFBZ0IsRUFDaEIsS0FBSyxFQUNMLEdBQUcsRUFBRSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsRUFDeEIsR0FBRyxFQUNOLEdBQUcsSUFBSSxDQUFDO0lBRVQsTUFBTSxhQUFhLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWxELElBQUksQ0FBQyxFQUFFLEVBQUU7UUFDTCxNQUFNLElBQUksaUJBQVEsQ0FDZCxhQUFhLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxjQUFjLEVBQ3RELHFCQUFVLENBQUMsV0FBVyxDQUN6QixDQUFDO0tBQ0w7SUFFRCx3Q0FBd0M7SUFDeEMsZ0NBQWdDO0lBQ2hDLHdDQUF3QztJQUV4QyxNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBRTdELElBQUksVUFBVSxHQUFHLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQztRQUN4QyxHQUFHLEVBQUUsRUFBRTtLQUNWLENBQUMsQ0FBQztJQUVILElBQUksQ0FBQyxVQUFVLEVBQUU7UUFDYixNQUFNLElBQUksaUJBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0tBQzFEO0lBRUQsVUFBVSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUVuRCxNQUFNLFdBQVcsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDO0lBRXRDLHdDQUF3QztJQUN4QyxTQUFTO0lBQ1Qsd0NBQXdDO0lBRXhDLE1BQU0sYUFBYSxHQUFHLENBQUMsY0FBYztRQUNqQyxDQUFDLENBQUMsTUFBTSxJQUFBLHVCQUFhLEVBQ2YsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLFdBQVcsRUFBRSxFQUN4QixnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUNqQztRQUNILENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDWCxNQUFNLGNBQWMsR0FBRyxJQUFBLDRCQUFvQixFQUFDLGFBQWEsQ0FBQyxDQUFDO0lBRTNELHdDQUF3QztJQUN4QyxvQkFBb0I7SUFDcEIsd0NBQXdDO0lBRXhDLE1BQU0sWUFBWSxHQUFxQjtRQUNuQyxLQUFLLEVBQUU7WUFDSCxHQUFHLEVBQUU7Z0JBQ0Q7b0JBQ0ksRUFBRSxFQUFFO3dCQUNBLE1BQU0sRUFBRSxXQUFXO3FCQUN0QjtpQkFDSjthQUNKO1NBQ0o7S0FDSixDQUFDO0lBRUYsSUFBSSxJQUFBLDRCQUFvQixFQUFDLGFBQWEsQ0FBQyxFQUFFO1FBQ3BDLFlBQVksQ0FBQyxLQUFLLENBQUMsR0FBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztLQUMzRDtJQUVELE1BQU0sS0FBSyxHQUFHLE1BQU0sS0FBSyxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFFM0QsTUFBTSxHQUFHLEdBQUcsTUFBTSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBRXZDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjO1FBQ3ZCLE1BQU0sSUFBSSxpQkFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDM0QsSUFBSSxDQUFDLEdBQUcsSUFBSSxjQUFjO1FBQ3RCLE1BQU0sSUFBSSxrQkFBUyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7SUFFN0Qsd0NBQXdDO0lBQ3hDLG9CQUFvQjtJQUNwQix3Q0FBd0M7SUFFeEMsTUFBTSxXQUFXLEdBQUcsTUFBTSxPQUFPLENBQUMsUUFBUSxDQUFDO1FBQ3ZDLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQyxJQUFJO1FBQ2pDLEVBQUUsRUFBRSxXQUFXO1FBQ2YsS0FBSztLQUNSLENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4QyxTQUFTO0lBQ1Qsd0NBQXdDO0lBRXhDLElBQUksTUFBTSxHQUFHLE1BQU0sS0FBSyxDQUFDLGlCQUFpQixDQUN0QyxFQUFFLEdBQUcsRUFBRSxXQUFXLEVBQUUsRUFDcEIsVUFBVSxDQUFDLE9BQU8sRUFDbEIsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQ2hCLENBQUM7SUFFRixNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBRTNDLHVCQUF1QjtJQUN2QixNQUFNLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7SUFDdkIsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDNUIsTUFBTSxHQUFHLElBQUEsZ0NBQXNCLEVBQUMsTUFBTSxDQUFDLENBQUM7SUFFeEMsd0NBQXdDO0lBQ3hDLHFCQUFxQjtJQUNyQix3Q0FBd0M7SUFFeEMsTUFBTSxHQUFHLE1BQU0sSUFBQSxxQkFBUyxFQUFDO1FBQ3JCLEtBQUs7UUFDTCxHQUFHLEVBQUUsTUFBTTtRQUNYLFlBQVksRUFBRSxnQkFBZ0I7UUFDOUIsR0FBRztRQUNILGNBQWM7UUFDZCxnQkFBZ0I7S0FDbkIsQ0FBQyxDQUFDO0lBRUgsd0NBQXdDO0lBQ3hDLHlCQUF5QjtJQUN6Qix3Q0FBd0M7SUFFeEMsTUFBTSxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFO1FBQ3BFLE1BQU0sU0FBUyxDQUFDO1FBRWhCLE1BQU07WUFDRixDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUNSLEdBQUc7Z0JBQ0gsR0FBRyxFQUFFLE1BQU07YUFDZCxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUM7SUFDdEIsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBRXRCLHdDQUF3QztJQUN4Qyx1QkFBdUI7SUFDdkIsd0NBQXdDO0lBRXhDLE1BQU0sR0FBRyxNQUFNLElBQUEseUJBQVcsRUFBQztRQUN2QixJQUFJLEVBQUUsTUFBTTtRQUNaLEdBQUcsRUFBRSxNQUFNO1FBQ1gsV0FBVztRQUNYLFlBQVksRUFBRSxnQkFBZ0I7UUFDOUIsU0FBUyxFQUFFLFFBQVE7UUFDbkIsR0FBRztLQUNOLENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4QywyQkFBMkI7SUFDM0Isd0NBQXdDO0lBRXhDLE1BQU0sZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUN0RSxNQUFNLFNBQVMsQ0FBQztRQUVoQixNQUFNO1lBQ0YsQ0FBQyxNQUFNLElBQUksQ0FBQztnQkFDUixHQUFHLEVBQUUsTUFBTTtnQkFDWCxHQUFHO2dCQUNILFdBQVc7Z0JBQ1gsU0FBUyxFQUFFLFFBQVE7YUFDdEIsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDO0lBQ3RCLENBQUMsRUFBRSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV0QixPQUFPLE1BQU0sQ0FBQztBQUNsQixDQUFDO0FBRUQsa0JBQWUsY0FBYyxDQUFDIn0=