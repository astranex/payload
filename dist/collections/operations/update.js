"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = __importDefault(require("http-status"));
const sanitizeInternalFields_1 = __importDefault(require("../../utilities/sanitizeInternalFields"));
const executeAccess_1 = __importDefault(require("../../auth/executeAccess"));
const errors_1 = require("../../errors");
const types_1 = require("../../auth/types");
const saveCollectionDraft_1 = require("../../versions/drafts/saveCollectionDraft");
const saveCollectionVersion_1 = require("../../versions/saveCollectionVersion");
const uploadFile_1 = __importDefault(require("../../uploads/uploadFile"));
const cleanUpFailedVersion_1 = __importDefault(require("../../versions/cleanUpFailedVersion"));
const ensurePublishedCollectionVersion_1 = require("../../versions/ensurePublishedCollectionVersion");
const beforeChange_1 = require("../../fields/hooks/beforeChange");
const beforeValidate_1 = require("../../fields/hooks/beforeValidate");
const afterChange_1 = require("../../fields/hooks/afterChange");
const afterRead_1 = require("../../fields/hooks/afterRead");
async function update(incomingArgs) {
    let args = incomingArgs;
    // /////////////////////////////////////
    // beforeOperation - Collection
    // /////////////////////////////////////
    await args.collection.config.hooks.beforeOperation.reduce(async (priorHook, hook) => {
        await priorHook;
        args = (await hook({
            args,
            operation: 'update',
        })) || args;
    }, Promise.resolve());
    const { depth, collection, collection: { Model, config: collectionConfig, }, id, req, req: { locale, payload, payload: { config, }, }, overrideAccess, showHiddenFields, overwriteExistingFiles = false, draft: draftArg = false, autosave = false, } = args;
    const adminUILocale = payload.config.admin.locale;
    let { data } = args;
    if (!id) {
        throw new errors_1.APIError(adminUILocale.operations.Update.MissingIDLabel, http_status_1.default.BAD_REQUEST);
    }
    const shouldSaveDraft = Boolean(draftArg && collectionConfig.versions.drafts);
    // /////////////////////////////////////
    // Access
    // /////////////////////////////////////
    const accessResults = !overrideAccess ? await (0, executeAccess_1.default)({ req, id, data }, collectionConfig.access.update) : true;
    const hasWherePolicy = (0, types_1.hasWhereAccessResult)(accessResults);
    // /////////////////////////////////////
    // Retrieve document
    // /////////////////////////////////////
    const queryToBuild = {
        where: {
            and: [
                {
                    id: {
                        equals: id,
                    },
                },
            ],
        },
    };
    if ((0, types_1.hasWhereAccessResult)(accessResults)) {
        queryToBuild.where.and.push(accessResults);
    }
    const query = await Model.buildQuery(queryToBuild, locale);
    const doc = await Model.findOne(query);
    if (!doc && !hasWherePolicy)
        throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
    if (!doc && hasWherePolicy)
        throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
    let docWithLocales = doc.toJSON({ virtuals: true });
    docWithLocales = JSON.stringify(docWithLocales);
    docWithLocales = JSON.parse(docWithLocales);
    const originalDoc = await (0, afterRead_1.afterRead)({
        depth: 0,
        doc: docWithLocales,
        entityConfig: collectionConfig,
        req,
        overrideAccess: true,
        showHiddenFields,
    });
    // /////////////////////////////////////
    // Upload and resize potential files
    // /////////////////////////////////////
    data = await (0, uploadFile_1.default)({
        config,
        collection,
        req,
        data,
        throwOnMissingFile: false,
        overwriteExistingFiles,
    });
    // /////////////////////////////////////
    // beforeValidate - Fields
    // /////////////////////////////////////
    data = await (0, beforeValidate_1.beforeValidate)({
        data,
        doc: originalDoc,
        entityConfig: collectionConfig,
        id,
        operation: 'update',
        overrideAccess,
        req,
    });
    // // /////////////////////////////////////
    // // beforeValidate - Collection
    // // /////////////////////////////////////
    await collectionConfig.hooks.beforeValidate.reduce(async (priorHook, hook) => {
        await priorHook;
        data = (await hook({
            data,
            req,
            operation: 'update',
            originalDoc,
        })) || data;
    }, Promise.resolve());
    // /////////////////////////////////////
    // beforeChange - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.beforeChange.reduce(async (priorHook, hook) => {
        await priorHook;
        data = (await hook({
            data,
            req,
            originalDoc,
            operation: 'update',
        })) || data;
    }, Promise.resolve());
    // /////////////////////////////////////
    // beforeChange - Fields
    // /////////////////////////////////////
    let result = await (0, beforeChange_1.beforeChange)({
        data,
        doc: originalDoc,
        docWithLocales,
        entityConfig: collectionConfig,
        id,
        operation: 'update',
        req,
        skipValidation: shouldSaveDraft,
    });
    // /////////////////////////////////////
    // Handle potential password update
    // /////////////////////////////////////
    const { password } = data;
    if (password && collectionConfig.auth && !shouldSaveDraft) {
        await doc.setPassword(password);
        await doc.save();
        delete data.password;
        delete result.password;
    }
    // /////////////////////////////////////
    // Create version from existing doc
    // /////////////////////////////////////
    let createdVersion;
    if (collectionConfig.versions && !shouldSaveDraft) {
        createdVersion = await (0, saveCollectionVersion_1.saveCollectionVersion)({
            payload,
            config: collectionConfig,
            req,
            docWithLocales,
            id,
        });
    }
    // /////////////////////////////////////
    // Update
    // /////////////////////////////////////
    if (shouldSaveDraft) {
        await (0, ensurePublishedCollectionVersion_1.ensurePublishedCollectionVersion)({
            payload,
            config: collectionConfig,
            req,
            docWithLocales,
            id,
        });
        result = await (0, saveCollectionDraft_1.saveCollectionDraft)({
            payload,
            config: collectionConfig,
            req,
            data: result,
            id,
            autosave,
        });
    }
    else {
        try {
            result = await Model.findByIdAndUpdate({ _id: id }, result, { new: true });
        }
        catch (error) {
            (0, cleanUpFailedVersion_1.default)({
                payload,
                entityConfig: collectionConfig,
                version: createdVersion,
            });
            // Handle uniqueness error from MongoDB
            throw error.code === 11000 && error.keyValue
                ? new errors_1.ValidationError([{ message: adminUILocale.operations.Update.ValueMustBeUniqueLabel, field: Object.keys(error.keyValue)[0] }])
                : error;
        }
        const resultString = JSON.stringify(result);
        result = JSON.parse(resultString);
        // custom id type reset
        result.id = result._id;
    }
    result = (0, sanitizeInternalFields_1.default)(result);
    // /////////////////////////////////////
    // afterRead - Fields
    // /////////////////////////////////////
    result = await (0, afterRead_1.afterRead)({
        depth,
        doc: result,
        entityConfig: collectionConfig,
        req,
        overrideAccess,
        showHiddenFields,
    });
    // /////////////////////////////////////
    // afterRead - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result = await hook({
            req,
            doc: result,
        }) || result;
    }, Promise.resolve());
    // /////////////////////////////////////
    // afterChange - Fields
    // /////////////////////////////////////
    result = await (0, afterChange_1.afterChange)({
        data,
        doc: result,
        previousDoc: originalDoc,
        entityConfig: collectionConfig,
        operation: 'update',
        req,
    });
    // /////////////////////////////////////
    // afterChange - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterChange.reduce(async (priorHook, hook) => {
        await priorHook;
        result = await hook({
            doc: result,
            previousDoc: originalDoc,
            req,
            operation: 'update',
        }) || result;
    }, Promise.resolve());
    // /////////////////////////////////////
    // Return results
    // /////////////////////////////////////
    return result;
}
exports.default = update;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbGxlY3Rpb25zL29wZXJhdGlvbnMvdXBkYXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsOERBQXFDO0FBR3JDLG9HQUE0RTtBQUM1RSw2RUFBcUQ7QUFDckQseUNBQThFO0FBRTlFLDRDQUFzRTtBQUN0RSxtRkFBZ0Y7QUFDaEYsZ0ZBQTZFO0FBQzdFLDBFQUFrRDtBQUNsRCwrRkFBdUU7QUFDdkUsc0dBQW1HO0FBQ25HLGtFQUErRDtBQUMvRCxzRUFBbUU7QUFDbkUsZ0VBQTZEO0FBQzdELDREQUF5RDtBQWdCekQsS0FBSyxVQUFVLE1BQU0sQ0FBQyxZQUF1QjtJQUMzQyxJQUFJLElBQUksR0FBRyxZQUFZLENBQUM7SUFFeEIsd0NBQXdDO0lBQ3hDLCtCQUErQjtJQUMvQix3Q0FBd0M7SUFFeEMsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFO1FBQ2xGLE1BQU0sU0FBUyxDQUFDO1FBRWhCLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDO1lBQ2pCLElBQUk7WUFDSixTQUFTLEVBQUUsUUFBUTtTQUNwQixDQUFDLENBQUMsSUFBSSxJQUFJLENBQUM7SUFDZCxDQUFDLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFFdEIsTUFBTSxFQUNKLEtBQUssRUFDTCxVQUFVLEVBQ1YsVUFBVSxFQUFFLEVBQ1YsS0FBSyxFQUNMLE1BQU0sRUFBRSxnQkFBZ0IsR0FDekIsRUFDRCxFQUFFLEVBQ0YsR0FBRyxFQUNILEdBQUcsRUFBRSxFQUNILE1BQU0sRUFDTixPQUFPLEVBQ1AsT0FBTyxFQUFFLEVBQ1AsTUFBTSxHQUNQLEdBQ0YsRUFDRCxjQUFjLEVBQ2QsZ0JBQWdCLEVBQ2hCLHNCQUFzQixHQUFHLEtBQUssRUFDOUIsS0FBSyxFQUFFLFFBQVEsR0FBRyxLQUFLLEVBQ3ZCLFFBQVEsR0FBRyxLQUFLLEdBQ2pCLEdBQUcsSUFBSSxDQUFDO0lBRVQsTUFBTSxhQUFhLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWxELElBQUksRUFBRSxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUM7SUFFcEIsSUFBSSxDQUFDLEVBQUUsRUFBRTtRQUNQLE1BQU0sSUFBSSxpQkFBUSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxxQkFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQzVGO0lBRUQsTUFBTSxlQUFlLEdBQUcsT0FBTyxDQUFDLFFBQVEsSUFBSSxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFFOUUsd0NBQXdDO0lBQ3hDLFNBQVM7SUFDVCx3Q0FBd0M7SUFFeEMsTUFBTSxhQUFhLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBQSx1QkFBYSxFQUFDLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN0SCxNQUFNLGNBQWMsR0FBRyxJQUFBLDRCQUFvQixFQUFDLGFBQWEsQ0FBQyxDQUFDO0lBRTNELHdDQUF3QztJQUN4QyxvQkFBb0I7SUFDcEIsd0NBQXdDO0lBRXhDLE1BQU0sWUFBWSxHQUFxQjtRQUNyQyxLQUFLLEVBQUU7WUFDTCxHQUFHLEVBQUU7Z0JBQ0g7b0JBQ0UsRUFBRSxFQUFFO3dCQUNGLE1BQU0sRUFBRSxFQUFFO3FCQUNYO2lCQUNGO2FBQ0Y7U0FDRjtLQUNGLENBQUM7SUFFRixJQUFJLElBQUEsNEJBQW9CLEVBQUMsYUFBYSxDQUFDLEVBQUU7UUFDdEMsWUFBWSxDQUFDLEtBQUssQ0FBQyxHQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0tBQ3pEO0lBRUQsTUFBTSxLQUFLLEdBQUcsTUFBTSxLQUFLLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUUzRCxNQUFNLEdBQUcsR0FBRyxNQUFNLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFpQixDQUFDO0lBRXZELElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjO1FBQUUsTUFBTSxJQUFJLGlCQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNwRixJQUFJLENBQUMsR0FBRyxJQUFJLGNBQWM7UUFBRSxNQUFNLElBQUksa0JBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBRXJGLElBQUksY0FBYyxHQUFhLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUM5RCxjQUFjLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUNoRCxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUU1QyxNQUFNLFdBQVcsR0FBRyxNQUFNLElBQUEscUJBQVMsRUFBQztRQUNsQyxLQUFLLEVBQUUsQ0FBQztRQUNSLEdBQUcsRUFBRSxjQUFjO1FBQ25CLFlBQVksRUFBRSxnQkFBZ0I7UUFDOUIsR0FBRztRQUNILGNBQWMsRUFBRSxJQUFJO1FBQ3BCLGdCQUFnQjtLQUNqQixDQUFDLENBQUM7SUFFSCx3Q0FBd0M7SUFDeEMsb0NBQW9DO0lBQ3BDLHdDQUF3QztJQUV4QyxJQUFJLEdBQUcsTUFBTSxJQUFBLG9CQUFVLEVBQUM7UUFDdEIsTUFBTTtRQUNOLFVBQVU7UUFDVixHQUFHO1FBQ0gsSUFBSTtRQUNKLGtCQUFrQixFQUFFLEtBQUs7UUFDekIsc0JBQXNCO0tBQ3ZCLENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4QywwQkFBMEI7SUFDMUIsd0NBQXdDO0lBRXhDLElBQUksR0FBRyxNQUFNLElBQUEsK0JBQWMsRUFBQztRQUMxQixJQUFJO1FBQ0osR0FBRyxFQUFFLFdBQVc7UUFDaEIsWUFBWSxFQUFFLGdCQUFnQjtRQUM5QixFQUFFO1FBQ0YsU0FBUyxFQUFFLFFBQVE7UUFDbkIsY0FBYztRQUNkLEdBQUc7S0FDSixDQUFDLENBQUM7SUFFSCwyQ0FBMkM7SUFDM0MsaUNBQWlDO0lBQ2pDLDJDQUEyQztJQUUzQyxNQUFNLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDM0UsTUFBTSxTQUFTLENBQUM7UUFFaEIsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLENBQUM7WUFDakIsSUFBSTtZQUNKLEdBQUc7WUFDSCxTQUFTLEVBQUUsUUFBUTtZQUNuQixXQUFXO1NBQ1osQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDO0lBQ2QsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBRXRCLHdDQUF3QztJQUN4Qyw0QkFBNEI7SUFDNUIsd0NBQXdDO0lBRXhDLE1BQU0sZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUN6RSxNQUFNLFNBQVMsQ0FBQztRQUVoQixJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksQ0FBQztZQUNqQixJQUFJO1lBQ0osR0FBRztZQUNILFdBQVc7WUFDWCxTQUFTLEVBQUUsUUFBUTtTQUNwQixDQUFDLENBQUMsSUFBSSxJQUFJLENBQUM7SUFDZCxDQUFDLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFFdEIsd0NBQXdDO0lBQ3hDLHdCQUF3QjtJQUN4Qix3Q0FBd0M7SUFFeEMsSUFBSSxNQUFNLEdBQUcsTUFBTSxJQUFBLDJCQUFZLEVBQUM7UUFDOUIsSUFBSTtRQUNKLEdBQUcsRUFBRSxXQUFXO1FBQ2hCLGNBQWM7UUFDZCxZQUFZLEVBQUUsZ0JBQWdCO1FBQzlCLEVBQUU7UUFDRixTQUFTLEVBQUUsUUFBUTtRQUNuQixHQUFHO1FBQ0gsY0FBYyxFQUFFLGVBQWU7S0FDaEMsQ0FBQyxDQUFDO0lBRUgsd0NBQXdDO0lBQ3hDLG1DQUFtQztJQUNuQyx3Q0FBd0M7SUFFeEMsTUFBTSxFQUFFLFFBQVEsRUFBRSxHQUFHLElBQUksQ0FBQztJQUUxQixJQUFJLFFBQVEsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7UUFDekQsTUFBTSxHQUFHLENBQUMsV0FBVyxDQUFDLFFBQWtCLENBQUMsQ0FBQztRQUMxQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNqQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDckIsT0FBTyxNQUFNLENBQUMsUUFBUSxDQUFDO0tBQ3hCO0lBRUQsd0NBQXdDO0lBQ3hDLG1DQUFtQztJQUNuQyx3Q0FBd0M7SUFFeEMsSUFBSSxjQUFjLENBQUM7SUFFbkIsSUFBSSxnQkFBZ0IsQ0FBQyxRQUFRLElBQUksQ0FBQyxlQUFlLEVBQUU7UUFDakQsY0FBYyxHQUFHLE1BQU0sSUFBQSw2Q0FBcUIsRUFBQztZQUMzQyxPQUFPO1lBQ1AsTUFBTSxFQUFFLGdCQUFnQjtZQUN4QixHQUFHO1lBQ0gsY0FBYztZQUNkLEVBQUU7U0FDSCxDQUFDLENBQUM7S0FDSjtJQUVELHdDQUF3QztJQUN4QyxTQUFTO0lBQ1Qsd0NBQXdDO0lBRXhDLElBQUksZUFBZSxFQUFFO1FBQ25CLE1BQU0sSUFBQSxtRUFBZ0MsRUFBQztZQUNyQyxPQUFPO1lBQ1AsTUFBTSxFQUFFLGdCQUFnQjtZQUN4QixHQUFHO1lBQ0gsY0FBYztZQUNkLEVBQUU7U0FDSCxDQUFDLENBQUM7UUFFSCxNQUFNLEdBQUcsTUFBTSxJQUFBLHlDQUFtQixFQUFDO1lBQ2pDLE9BQU87WUFDUCxNQUFNLEVBQUUsZ0JBQWdCO1lBQ3hCLEdBQUc7WUFDSCxJQUFJLEVBQUUsTUFBTTtZQUNaLEVBQUU7WUFDRixRQUFRO1NBQ1QsQ0FBQyxDQUFDO0tBQ0o7U0FBTTtRQUNMLElBQUk7WUFDRixNQUFNLEdBQUcsTUFBTSxLQUFLLENBQUMsaUJBQWlCLENBQ3BDLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxFQUNYLE1BQU0sRUFDTixFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FDZCxDQUFDO1NBQ0g7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNkLElBQUEsOEJBQW9CLEVBQUM7Z0JBQ25CLE9BQU87Z0JBQ1AsWUFBWSxFQUFFLGdCQUFnQjtnQkFDOUIsT0FBTyxFQUFFLGNBQWM7YUFDeEIsQ0FBQyxDQUFDO1lBRUgsdUNBQXVDO1lBQ3ZDLE1BQU0sS0FBSyxDQUFDLElBQUksS0FBSyxLQUFLLElBQUksS0FBSyxDQUFDLFFBQVE7Z0JBQzFDLENBQUMsQ0FBQyxJQUFJLHdCQUFlLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxhQUFhLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNuSSxDQUFDLENBQUMsS0FBSyxDQUFDO1NBQ1g7UUFFRCxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzVDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRWxDLHVCQUF1QjtRQUN2QixNQUFNLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7S0FDeEI7SUFFRCxNQUFNLEdBQUcsSUFBQSxnQ0FBc0IsRUFBQyxNQUFNLENBQUMsQ0FBQztJQUV4Qyx3Q0FBd0M7SUFDeEMscUJBQXFCO0lBQ3JCLHdDQUF3QztJQUV4QyxNQUFNLEdBQUcsTUFBTSxJQUFBLHFCQUFTLEVBQUM7UUFDdkIsS0FBSztRQUNMLEdBQUcsRUFBRSxNQUFNO1FBQ1gsWUFBWSxFQUFFLGdCQUFnQjtRQUM5QixHQUFHO1FBQ0gsY0FBYztRQUNkLGdCQUFnQjtLQUNqQixDQUFDLENBQUM7SUFFSCx3Q0FBd0M7SUFDeEMseUJBQXlCO0lBQ3pCLHdDQUF3QztJQUV4QyxNQUFNLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDdEUsTUFBTSxTQUFTLENBQUM7UUFFaEIsTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDO1lBQ2xCLEdBQUc7WUFDSCxHQUFHLEVBQUUsTUFBTTtTQUNaLENBQUMsSUFBSSxNQUFNLENBQUM7SUFDZixDQUFDLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFFdEIsd0NBQXdDO0lBQ3hDLHVCQUF1QjtJQUN2Qix3Q0FBd0M7SUFFeEMsTUFBTSxHQUFHLE1BQU0sSUFBQSx5QkFBVyxFQUFDO1FBQ3pCLElBQUk7UUFDSixHQUFHLEVBQUUsTUFBTTtRQUNYLFdBQVcsRUFBRSxXQUFXO1FBQ3hCLFlBQVksRUFBRSxnQkFBZ0I7UUFDOUIsU0FBUyxFQUFFLFFBQVE7UUFDbkIsR0FBRztLQUNKLENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4QywyQkFBMkI7SUFDM0Isd0NBQXdDO0lBRXhDLE1BQU0sZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUN4RSxNQUFNLFNBQVMsQ0FBQztRQUVoQixNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUM7WUFDbEIsR0FBRyxFQUFFLE1BQU07WUFDWCxXQUFXLEVBQUUsV0FBVztZQUN4QixHQUFHO1lBQ0gsU0FBUyxFQUFFLFFBQVE7U0FDcEIsQ0FBQyxJQUFJLE1BQU0sQ0FBQztJQUNmLENBQUMsRUFBRSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV0Qix3Q0FBd0M7SUFDeEMsaUJBQWlCO0lBQ2pCLHdDQUF3QztJQUV4QyxPQUFPLE1BQU0sQ0FBQztBQUNoQixDQUFDO0FBRUQsa0JBQWUsTUFBTSxDQUFDIn0=