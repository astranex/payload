"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-underscore-dangle */
const http_status_1 = __importDefault(require("http-status"));
const sanitizeInternalFields_1 = __importDefault(require("../../utilities/sanitizeInternalFields"));
const errors_1 = require("../../errors");
const executeAccess_1 = __importDefault(require("../../auth/executeAccess"));
const types_1 = require("../../auth/types");
const afterRead_1 = require("../../fields/hooks/afterRead");
async function findVersionByID(args) {
    const { depth, collection: { config: collectionConfig }, id, req, req: { locale, payload }, disableErrors, currentDepth, overrideAccess, showHiddenFields } = args;
    const adminUILocale = payload.config.admin.locale;
    if (!id) {
        throw new errors_1.APIError(adminUILocale.operations.FindVersionByID.MissingIDLabel, http_status_1.default.BAD_REQUEST);
    }
    const VersionsModel = payload.versions[collectionConfig.slug];
    // /////////////////////////////////////
    // Access
    // /////////////////////////////////////
    const accessResults = !overrideAccess
        ? await (0, executeAccess_1.default)({ req, disableErrors, id }, collectionConfig.access.readVersions)
        : true;
    // If errors are disabled, and access returns false, return null
    if (accessResults === false)
        return null;
    const hasWhereAccess = typeof accessResults === 'object';
    const queryToBuild = {
        where: {
            and: [
                {
                    _id: {
                        equals: id
                    }
                }
            ]
        }
    };
    if ((0, types_1.hasWhereAccessResult)(accessResults)) {
        queryToBuild.where.and.push(accessResults);
    }
    const query = await VersionsModel.buildQuery(queryToBuild, locale);
    // /////////////////////////////////////
    // Find by ID
    // /////////////////////////////////////
    if (!query.$and[0]._id)
        throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
    let result = await VersionsModel.findOne(query, {}).lean();
    if (!result) {
        if (!disableErrors) {
            if (!hasWhereAccess)
                throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
            if (hasWhereAccess)
                throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
        }
        return null;
    }
    // Clone the result - it may have come back memoized
    result = JSON.parse(JSON.stringify(result));
    result = (0, sanitizeInternalFields_1.default)(result);
    // /////////////////////////////////////
    // beforeRead - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.beforeRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result.version =
            (await hook({
                req,
                query,
                doc: result.version
            })) || result.version;
    }, Promise.resolve());
    // /////////////////////////////////////
    // afterRead - Fields
    // /////////////////////////////////////
    result.version = await (0, afterRead_1.afterRead)({
        currentDepth,
        depth,
        doc: result.version,
        entityConfig: collectionConfig,
        overrideAccess,
        req,
        showHiddenFields
    });
    // /////////////////////////////////////
    // afterRead - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result.version =
            (await hook({
                req,
                query,
                doc: result.version
            })) || result.version;
    }, Promise.resolve());
    // /////////////////////////////////////
    // Return results
    // /////////////////////////////////////
    return result;
}
exports.default = findVersionByID;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmluZFZlcnNpb25CeUlELmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbGxlY3Rpb25zL29wZXJhdGlvbnMvZmluZFZlcnNpb25CeUlELnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEseUNBQXlDO0FBQ3pDLDhEQUFxQztBQUdyQyxvR0FBNEU7QUFDNUUseUNBQTZEO0FBQzdELDZFQUFxRDtBQUVyRCw0Q0FBd0Q7QUFFeEQsNERBQXlEO0FBYXpELEtBQUssVUFBVSxlQUFlLENBQzFCLElBQWU7SUFFZixNQUFNLEVBQ0YsS0FBSyxFQUNMLFVBQVUsRUFBRSxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxFQUN4QyxFQUFFLEVBQ0YsR0FBRyxFQUNILEdBQUcsRUFBRSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsRUFDeEIsYUFBYSxFQUNiLFlBQVksRUFDWixjQUFjLEVBQ2QsZ0JBQWdCLEVBQ25CLEdBQUcsSUFBSSxDQUFDO0lBRVQsTUFBTSxhQUFhLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWxELElBQUksQ0FBQyxFQUFFLEVBQUU7UUFDTCxNQUFNLElBQUksaUJBQVEsQ0FDZCxhQUFhLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxjQUFjLEVBQ3ZELHFCQUFVLENBQUMsV0FBVyxDQUN6QixDQUFDO0tBQ0w7SUFFRCxNQUFNLGFBQWEsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUNsQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQ0wsQ0FBQztJQUVyQix3Q0FBd0M7SUFDeEMsU0FBUztJQUNULHdDQUF3QztJQUV4QyxNQUFNLGFBQWEsR0FBRyxDQUFDLGNBQWM7UUFDakMsQ0FBQyxDQUFDLE1BQU0sSUFBQSx1QkFBYSxFQUNmLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxFQUFFLEVBQUUsRUFDMUIsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FDdkM7UUFDSCxDQUFDLENBQUMsSUFBSSxDQUFDO0lBRVgsZ0VBQWdFO0lBQ2hFLElBQUksYUFBYSxLQUFLLEtBQUs7UUFBRSxPQUFPLElBQUksQ0FBQztJQUV6QyxNQUFNLGNBQWMsR0FBRyxPQUFPLGFBQWEsS0FBSyxRQUFRLENBQUM7SUFFekQsTUFBTSxZQUFZLEdBQXFCO1FBQ25DLEtBQUssRUFBRTtZQUNILEdBQUcsRUFBRTtnQkFDRDtvQkFDSSxHQUFHLEVBQUU7d0JBQ0QsTUFBTSxFQUFFLEVBQUU7cUJBQ2I7aUJBQ0o7YUFDSjtTQUNKO0tBQ0osQ0FBQztJQUVGLElBQUksSUFBQSw0QkFBb0IsRUFBQyxhQUFhLENBQUMsRUFBRTtRQUNwQyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7S0FDM0Q7SUFFRCxNQUFNLEtBQUssR0FBRyxNQUFNLGFBQWEsQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBRW5FLHdDQUF3QztJQUN4QyxhQUFhO0lBQ2Isd0NBQXdDO0lBRXhDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUc7UUFDbEIsTUFBTSxJQUFJLGlCQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUUzRCxJQUFJLE1BQU0sR0FBRyxNQUFNLGFBQWEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBRTNELElBQUksQ0FBQyxNQUFNLEVBQUU7UUFDVCxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxjQUFjO2dCQUNmLE1BQU0sSUFBSSxpQkFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDM0QsSUFBSSxjQUFjO2dCQUNkLE1BQU0sSUFBSSxrQkFBUyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDaEU7UUFFRCxPQUFPLElBQUksQ0FBQztLQUNmO0lBRUQsb0RBQW9EO0lBQ3BELE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUU1QyxNQUFNLEdBQUcsSUFBQSxnQ0FBc0IsRUFBQyxNQUFNLENBQUMsQ0FBQztJQUV4Qyx3Q0FBd0M7SUFDeEMsMEJBQTBCO0lBQzFCLHdDQUF3QztJQUV4QyxNQUFNLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDckUsTUFBTSxTQUFTLENBQUM7UUFFaEIsTUFBTSxDQUFDLE9BQU87WUFDVixDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUNSLEdBQUc7Z0JBQ0gsS0FBSztnQkFDTCxHQUFHLEVBQUUsTUFBTSxDQUFDLE9BQU87YUFDdEIsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUM5QixDQUFDLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFFdEIsd0NBQXdDO0lBQ3hDLHFCQUFxQjtJQUNyQix3Q0FBd0M7SUFFeEMsTUFBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLElBQUEscUJBQVMsRUFBQztRQUM3QixZQUFZO1FBQ1osS0FBSztRQUNMLEdBQUcsRUFBRSxNQUFNLENBQUMsT0FBTztRQUNuQixZQUFZLEVBQUUsZ0JBQWdCO1FBQzlCLGNBQWM7UUFDZCxHQUFHO1FBQ0gsZ0JBQWdCO0tBQ25CLENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4Qyx5QkFBeUI7SUFDekIsd0NBQXdDO0lBRXhDLE1BQU0sZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUNwRSxNQUFNLFNBQVMsQ0FBQztRQUVoQixNQUFNLENBQUMsT0FBTztZQUNWLENBQUMsTUFBTSxJQUFJLENBQUM7Z0JBQ1IsR0FBRztnQkFDSCxLQUFLO2dCQUNMLEdBQUcsRUFBRSxNQUFNLENBQUMsT0FBTzthQUN0QixDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQzlCLENBQUMsRUFBRSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV0Qix3Q0FBd0M7SUFDeEMsaUJBQWlCO0lBQ2pCLHdDQUF3QztJQUV4QyxPQUFPLE1BQU0sQ0FBQztBQUNsQixDQUFDO0FBRUQsa0JBQWUsZUFBZSxDQUFDIn0=