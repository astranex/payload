"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-underscore-dangle */
const micro_memoize_1 = __importDefault(require("micro-memoize"));
const sanitizeInternalFields_1 = __importDefault(require("../../utilities/sanitizeInternalFields"));
const errors_1 = require("../../errors");
const executeAccess_1 = __importDefault(require("../../auth/executeAccess"));
const types_1 = require("../../auth/types");
const replaceWithDraftIfAvailable_1 = __importDefault(require("../../versions/drafts/replaceWithDraftIfAvailable"));
const afterRead_1 = require("../../fields/hooks/afterRead");
// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function findByID(incomingArgs) {
    var _a;
    let args = incomingArgs;
    // /////////////////////////////////////
    // beforeOperation - Collection
    // /////////////////////////////////////
    await args.collection.config.hooks.beforeOperation.reduce(async (priorHook, hook) => {
        await priorHook;
        args =
            (await hook({
                args,
                operation: 'read'
            })) || args;
    }, Promise.resolve());
    const { depth, collection: { Model, config: collectionConfig }, id, req, req: { locale, payload }, disableErrors, currentDepth, overrideAccess = false, showHiddenFields, draft: draftEnabled = false } = args;
    const adminUILocale = payload.config.admin.locale;
    // /////////////////////////////////////
    // Access
    // /////////////////////////////////////
    const accessResult = !overrideAccess
        ? await (0, executeAccess_1.default)({ req, disableErrors, id }, collectionConfig.access.read)
        : true;
    // If errors are disabled, and access returns false, return null
    if (accessResult === false)
        return null;
    const queryToBuild = {
        where: {
            and: [
                {
                    _id: {
                        equals: id
                    }
                }
            ]
        }
    };
    if ((0, types_1.hasWhereAccessResult)(accessResult)) {
        queryToBuild.where.and.push(accessResult);
    }
    const query = await Model.buildQuery(queryToBuild, locale);
    // /////////////////////////////////////
    // Find by ID
    // /////////////////////////////////////
    if (!query.$and[0]._id)
        throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
    if (!req.findByID)
        req.findByID = {};
    if (!req.findByID[collectionConfig.slug]) {
        const nonMemoizedFindByID = async (q) => Model.findOne(q, {}).lean();
        req.findByID[collectionConfig.slug] = (0, micro_memoize_1.default)(nonMemoizedFindByID, {
            isPromise: true,
            maxSize: 100,
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore This is straight from their docs, bad typings
            transformKey: JSON.stringify
        });
    }
    let result = await req.findByID[collectionConfig.slug](query);
    if (!result) {
        if (!disableErrors) {
            throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
        }
        return null;
    }
    // Clone the result - it may have come back memoized
    result = JSON.parse(JSON.stringify(result));
    result = (0, sanitizeInternalFields_1.default)(result);
    // /////////////////////////////////////
    // Replace document with draft if available
    // /////////////////////////////////////
    if (((_a = collectionConfig.versions) === null || _a === void 0 ? void 0 : _a.drafts) && draftEnabled) {
        result = await (0, replaceWithDraftIfAvailable_1.default)({
            payload,
            entity: collectionConfig,
            doc: result,
            accessResult,
            locale
        });
    }
    // /////////////////////////////////////
    // beforeRead - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.beforeRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result =
            (await hook({
                req,
                query,
                doc: result
            })) || result;
    }, Promise.resolve());
    // /////////////////////////////////////
    // afterRead - Fields
    // /////////////////////////////////////
    result = await (0, afterRead_1.afterRead)({
        currentDepth,
        doc: result,
        depth,
        entityConfig: collectionConfig,
        overrideAccess,
        req,
        showHiddenFields
    });
    // /////////////////////////////////////
    // afterRead - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result =
            (await hook({
                req,
                query,
                doc: result
            })) || result;
    }, Promise.resolve());
    // /////////////////////////////////////
    // Return results
    // /////////////////////////////////////
    return result;
}
exports.default = findByID;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmluZEJ5SUQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY29sbGVjdGlvbnMvb3BlcmF0aW9ucy9maW5kQnlJRC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLHlDQUF5QztBQUN6QyxrRUFBb0M7QUFHcEMsb0dBQTRFO0FBQzVFLHlDQUF3QztBQUN4Qyw2RUFBcUQ7QUFFckQsNENBQXdEO0FBQ3hELG9IQUE0RjtBQUM1Riw0REFBeUQ7QUFjekQsOERBQThEO0FBQzlELEtBQUssVUFBVSxRQUFRLENBQ25CLFlBQXVCOztJQUV2QixJQUFJLElBQUksR0FBRyxZQUFZLENBQUM7SUFFeEIsd0NBQXdDO0lBQ3hDLCtCQUErQjtJQUMvQix3Q0FBd0M7SUFFeEMsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FDckQsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUN0QixNQUFNLFNBQVMsQ0FBQztRQUVoQixJQUFJO1lBQ0EsQ0FBQyxNQUFNLElBQUksQ0FBQztnQkFDUixJQUFJO2dCQUNKLFNBQVMsRUFBRSxNQUFNO2FBQ3BCLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQztJQUNwQixDQUFDLEVBQ0QsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUNwQixDQUFDO0lBRUYsTUFBTSxFQUNGLEtBQUssRUFDTCxVQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLGdCQUFnQixFQUFFLEVBQy9DLEVBQUUsRUFDRixHQUFHLEVBQ0gsR0FBRyxFQUFFLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxFQUN4QixhQUFhLEVBQ2IsWUFBWSxFQUNaLGNBQWMsR0FBRyxLQUFLLEVBQ3RCLGdCQUFnQixFQUNoQixLQUFLLEVBQUUsWUFBWSxHQUFHLEtBQUssRUFDOUIsR0FBRyxJQUFJLENBQUM7SUFFVCxNQUFNLGFBQWEsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFFbEQsd0NBQXdDO0lBQ3hDLFNBQVM7SUFDVCx3Q0FBd0M7SUFFeEMsTUFBTSxZQUFZLEdBQUcsQ0FBQyxjQUFjO1FBQ2hDLENBQUMsQ0FBQyxNQUFNLElBQUEsdUJBQWEsRUFDZixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsRUFBRSxFQUFFLEVBQzFCLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQy9CO1FBQ0gsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUVYLGdFQUFnRTtJQUNoRSxJQUFJLFlBQVksS0FBSyxLQUFLO1FBQUUsT0FBTyxJQUFJLENBQUM7SUFFeEMsTUFBTSxZQUFZLEdBQXFCO1FBQ25DLEtBQUssRUFBRTtZQUNILEdBQUcsRUFBRTtnQkFDRDtvQkFDSSxHQUFHLEVBQUU7d0JBQ0QsTUFBTSxFQUFFLEVBQUU7cUJBQ2I7aUJBQ0o7YUFDSjtTQUNKO0tBQ0osQ0FBQztJQUVGLElBQUksSUFBQSw0QkFBb0IsRUFBQyxZQUFZLENBQUMsRUFBRTtRQUNwQyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7S0FDN0M7SUFFRCxNQUFNLEtBQUssR0FBRyxNQUFNLEtBQUssQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBRTNELHdDQUF3QztJQUN4QyxhQUFhO0lBQ2Isd0NBQXdDO0lBRXhDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUc7UUFDbEIsTUFBTSxJQUFJLGlCQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUUzRCxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVE7UUFBRSxHQUFHLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUVyQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFBRTtRQUN0QyxNQUFNLG1CQUFtQixHQUFHLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3JFLEdBQUcsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBQSx1QkFBTyxFQUFDLG1CQUFtQixFQUFFO1lBQy9ELFNBQVMsRUFBRSxJQUFJO1lBQ2YsT0FBTyxFQUFFLEdBQUc7WUFDWiw2REFBNkQ7WUFDN0QsMkRBQTJEO1lBQzNELFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUztTQUMvQixDQUFDLENBQUM7S0FDTjtJQUVELElBQUksTUFBTSxHQUFHLE1BQU0sR0FBRyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUU5RCxJQUFJLENBQUMsTUFBTSxFQUFFO1FBQ1QsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNoQixNQUFNLElBQUksaUJBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzFEO1FBRUQsT0FBTyxJQUFJLENBQUM7S0FDZjtJQUVELG9EQUFvRDtJQUNwRCxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFFNUMsTUFBTSxHQUFHLElBQUEsZ0NBQXNCLEVBQUMsTUFBTSxDQUFDLENBQUM7SUFFeEMsd0NBQXdDO0lBQ3hDLDJDQUEyQztJQUMzQyx3Q0FBd0M7SUFFeEMsSUFBSSxDQUFBLE1BQUEsZ0JBQWdCLENBQUMsUUFBUSwwQ0FBRSxNQUFNLEtBQUksWUFBWSxFQUFFO1FBQ25ELE1BQU0sR0FBRyxNQUFNLElBQUEscUNBQTJCLEVBQUM7WUFDdkMsT0FBTztZQUNQLE1BQU0sRUFBRSxnQkFBZ0I7WUFDeEIsR0FBRyxFQUFFLE1BQU07WUFDWCxZQUFZO1lBQ1osTUFBTTtTQUNULENBQUMsQ0FBQztLQUNOO0lBRUQsd0NBQXdDO0lBQ3hDLDBCQUEwQjtJQUMxQix3Q0FBd0M7SUFFeEMsTUFBTSxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFO1FBQ3JFLE1BQU0sU0FBUyxDQUFDO1FBRWhCLE1BQU07WUFDRixDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUNSLEdBQUc7Z0JBQ0gsS0FBSztnQkFDTCxHQUFHLEVBQUUsTUFBTTthQUNkLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQztJQUN0QixDQUFDLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFFdEIsd0NBQXdDO0lBQ3hDLHFCQUFxQjtJQUNyQix3Q0FBd0M7SUFFeEMsTUFBTSxHQUFHLE1BQU0sSUFBQSxxQkFBUyxFQUFDO1FBQ3JCLFlBQVk7UUFDWixHQUFHLEVBQUUsTUFBTTtRQUNYLEtBQUs7UUFDTCxZQUFZLEVBQUUsZ0JBQWdCO1FBQzlCLGNBQWM7UUFDZCxHQUFHO1FBQ0gsZ0JBQWdCO0tBQ25CLENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4Qyx5QkFBeUI7SUFDekIsd0NBQXdDO0lBRXhDLE1BQU0sZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUNwRSxNQUFNLFNBQVMsQ0FBQztRQUVoQixNQUFNO1lBQ0YsQ0FBQyxNQUFNLElBQUksQ0FBQztnQkFDUixHQUFHO2dCQUNILEtBQUs7Z0JBQ0wsR0FBRyxFQUFFLE1BQU07YUFDZCxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUM7SUFDdEIsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBRXRCLHdDQUF3QztJQUN4QyxpQkFBaUI7SUFDakIsd0NBQXdDO0lBRXhDLE9BQU8sTUFBTSxDQUFDO0FBQ2xCLENBQUM7QUFFRCxrQkFBZSxRQUFRLENBQUMifQ==