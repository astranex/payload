"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-param-reassign */
const graphql_scalars_1 = require("graphql-scalars");
const graphql_1 = require("graphql");
const formatName_1 = __importDefault(require("../../graphql/utilities/formatName"));
const buildPaginatedListType_1 = __importDefault(require("../../graphql/schema/buildPaginatedListType"));
const buildMutationInputType_1 = __importStar(require("../../graphql/schema/buildMutationInputType"));
const buildCollectionFields_1 = require("../../versions/buildCollectionFields");
const create_1 = __importDefault(require("./resolvers/create"));
const update_1 = __importDefault(require("./resolvers/update"));
const find_1 = __importDefault(require("./resolvers/find"));
const findByID_1 = __importDefault(require("./resolvers/findByID"));
const findVersionByID_1 = __importDefault(require("./resolvers/findVersionByID"));
const findVersions_1 = __importDefault(require("./resolvers/findVersions"));
const restoreVersion_1 = __importDefault(require("./resolvers/restoreVersion"));
const me_1 = __importDefault(require("../../auth/graphql/resolvers/me"));
const init_1 = __importDefault(require("../../auth/graphql/resolvers/init"));
const login_1 = __importDefault(require("../../auth/graphql/resolvers/login"));
const logout_1 = __importDefault(require("../../auth/graphql/resolvers/logout"));
const forgotPassword_1 = __importDefault(require("../../auth/graphql/resolvers/forgotPassword"));
const resetPassword_1 = __importDefault(require("../../auth/graphql/resolvers/resetPassword"));
const verifyEmail_1 = __importDefault(require("../../auth/graphql/resolvers/verifyEmail"));
const unlock_1 = __importDefault(require("../../auth/graphql/resolvers/unlock"));
const refresh_1 = __importDefault(require("../../auth/graphql/resolvers/refresh"));
const types_1 = require("../../fields/config/types");
const buildObjectType_1 = __importDefault(require("../../graphql/schema/buildObjectType"));
const buildWhereInputType_1 = __importDefault(require("../../graphql/schema/buildWhereInputType"));
const delete_1 = __importDefault(require("./resolvers/delete"));
function initCollectionsGraphQL(payload) {
    Object.keys(payload.collections).forEach((slug) => {
        const collection = payload.collections[slug];
        const { config: { 
        // labels: { singular, plural },
        slugs: { singular, plural }, fields, timestamps, versions } } = collection;
        const singularLabel = (0, formatName_1.default)(singular);
        let pluralLabel = (0, formatName_1.default)(plural);
        // For collections named 'Media' or similar,
        // there is a possibility that the singular name
        // will equal the plural name. Append `all` to the beginning
        // of potential conflicts
        if (singularLabel === pluralLabel) {
            pluralLabel = `all${singularLabel}`;
        }
        collection.graphQL = {};
        const idField = fields.find((field) => (0, types_1.fieldAffectsData)(field) && field.name === 'id');
        const idType = (0, buildMutationInputType_1.getCollectionIDType)(collection.config);
        const baseFields = {};
        const whereInputFields = [
            ...fields,
        ];
        if (!idField) {
            baseFields.id = { type: idType };
            whereInputFields.push({
                name: 'id',
                type: 'text',
            });
        }
        if (timestamps) {
            baseFields.createdAt = {
                type: new graphql_1.GraphQLNonNull(graphql_scalars_1.DateTimeResolver),
            };
            baseFields.updatedAt = {
                type: new graphql_1.GraphQLNonNull(graphql_scalars_1.DateTimeResolver),
            };
            whereInputFields.push({
                name: 'createdAt',
                label: 'Created At',
                type: 'date',
            });
            whereInputFields.push({
                name: 'updatedAt',
                label: 'Updated At',
                type: 'date',
            });
        }
        const forceNullableObjectType = Boolean(versions === null || versions === void 0 ? void 0 : versions.drafts);
        collection.graphQL.type = (0, buildObjectType_1.default)({
            payload,
            name: singularLabel,
            parentName: singularLabel,
            fields,
            baseFields,
            forceNullable: forceNullableObjectType,
        });
        collection.graphQL.whereInputType = (0, buildWhereInputType_1.default)(singularLabel, whereInputFields, singularLabel);
        if (collection.config.auth && !collection.config.auth.disableLocalStrategy) {
            fields.push({
                name: 'password',
                label: 'Password',
                type: 'text',
                required: true,
            });
        }
        collection.graphQL.mutationInputType = new graphql_1.GraphQLNonNull((0, buildMutationInputType_1.default)(payload, singularLabel, fields, singularLabel));
        collection.graphQL.updateMutationInputType = new graphql_1.GraphQLNonNull((0, buildMutationInputType_1.default)(payload, `${singularLabel}Update`, fields.filter((field) => !((0, types_1.fieldAffectsData)(field) && field.name === 'id')), `${singularLabel}Update`, true));
        payload.Query.fields[singularLabel] = {
            type: collection.graphQL.type,
            args: {
                id: { type: new graphql_1.GraphQLNonNull(idType) },
                draft: { type: graphql_1.GraphQLBoolean },
                ...(payload.config.localization ? {
                    locale: { type: payload.types.localeInputType },
                    fallbackLocale: { type: payload.types.fallbackLocaleInputType },
                } : {}),
            },
            resolve: (0, findByID_1.default)(collection),
        };
        payload.Query.fields[pluralLabel] = {
            type: (0, buildPaginatedListType_1.default)(pluralLabel, collection.graphQL.type),
            args: {
                where: { type: collection.graphQL.whereInputType },
                draft: { type: graphql_1.GraphQLBoolean },
                ...(payload.config.localization ? {
                    locale: { type: payload.types.localeInputType },
                    fallbackLocale: { type: payload.types.fallbackLocaleInputType },
                } : {}),
                page: { type: graphql_1.GraphQLInt },
                limit: { type: graphql_1.GraphQLInt },
                sort: { type: graphql_1.GraphQLString },
            },
            resolve: (0, find_1.default)(collection),
        };
        payload.Mutation.fields[`create${singularLabel}`] = {
            type: collection.graphQL.type,
            args: {
                data: { type: collection.graphQL.mutationInputType },
                draft: { type: graphql_1.GraphQLBoolean },
                ...(payload.config.localization ? {
                    locale: { type: payload.types.localeInputType },
                } : {}),
            },
            resolve: (0, create_1.default)(collection),
        };
        payload.Mutation.fields[`update${singularLabel}`] = {
            type: collection.graphQL.type,
            args: {
                id: { type: new graphql_1.GraphQLNonNull(idType) },
                data: { type: collection.graphQL.updateMutationInputType },
                draft: { type: graphql_1.GraphQLBoolean },
                autosave: { type: graphql_1.GraphQLBoolean },
                ...(payload.config.localization ? {
                    locale: { type: payload.types.localeInputType },
                } : {}),
            },
            resolve: (0, update_1.default)(collection),
        };
        payload.Mutation.fields[`delete${singularLabel}`] = {
            type: collection.graphQL.type,
            args: {
                id: { type: new graphql_1.GraphQLNonNull(idType) },
            },
            resolve: (0, delete_1.default)(collection),
        };
        if (collection.config.versions) {
            const versionCollectionFields = [
                ...(0, buildCollectionFields_1.buildVersionCollectionFields)(collection.config),
                {
                    name: 'id',
                    type: 'text',
                },
                {
                    name: 'createdAt',
                    label: 'Created At',
                    type: 'date',
                },
                {
                    name: 'updatedAt',
                    label: 'Updated At',
                    type: 'date',
                },
            ];
            collection.graphQL.versionType = (0, buildObjectType_1.default)({
                payload,
                name: `${singularLabel}Version`,
                fields: versionCollectionFields,
                parentName: `${singularLabel}Version`,
                forceNullable: forceNullableObjectType,
            });
            payload.Query.fields[`version${(0, formatName_1.default)(singularLabel)}`] = {
                type: collection.graphQL.versionType,
                args: {
                    id: { type: graphql_1.GraphQLString },
                    ...(payload.config.localization ? {
                        locale: { type: payload.types.localeInputType },
                        fallbackLocale: { type: payload.types.fallbackLocaleInputType },
                    } : {}),
                },
                resolve: (0, findVersionByID_1.default)(collection),
            };
            payload.Query.fields[`versions${pluralLabel}`] = {
                type: (0, buildPaginatedListType_1.default)(`versions${(0, formatName_1.default)(pluralLabel)}`, collection.graphQL.versionType),
                args: {
                    where: {
                        type: (0, buildWhereInputType_1.default)(`versions${singularLabel}`, versionCollectionFields, `versions${singularLabel}`),
                    },
                    ...(payload.config.localization ? {
                        locale: { type: payload.types.localeInputType },
                        fallbackLocale: { type: payload.types.fallbackLocaleInputType },
                    } : {}),
                    page: { type: graphql_1.GraphQLInt },
                    limit: { type: graphql_1.GraphQLInt },
                    sort: { type: graphql_1.GraphQLString },
                },
                resolve: (0, findVersions_1.default)(collection),
            };
            payload.Mutation.fields[`restoreVersion${(0, formatName_1.default)(singularLabel)}`] = {
                type: collection.graphQL.type,
                args: {
                    id: { type: graphql_1.GraphQLString },
                },
                resolve: (0, restoreVersion_1.default)(collection),
            };
        }
        if (collection.config.auth) {
            const authFields = collection.config.auth.disableLocalStrategy ? [] : [{
                    name: 'email',
                    type: 'email',
                    required: true,
                }];
            collection.graphQL.JWT = (0, buildObjectType_1.default)({
                payload,
                name: (0, formatName_1.default)(`${slug}JWT`),
                fields: [
                    ...collection.config.fields.filter((field) => (0, types_1.fieldAffectsData)(field) && field.saveToJWT),
                    ...authFields,
                    {
                        name: 'collection',
                        type: 'text',
                        required: true,
                    }
                ],
                parentName: (0, formatName_1.default)(`${slug}JWT`),
            });
            payload.Query.fields[`me${singularLabel}`] = {
                type: new graphql_1.GraphQLObjectType({
                    name: (0, formatName_1.default)(`${slug}Me`),
                    fields: {
                        token: {
                            type: graphql_1.GraphQLString,
                        },
                        user: {
                            type: collection.graphQL.type,
                        },
                        exp: {
                            type: graphql_1.GraphQLInt,
                        },
                        collection: {
                            type: graphql_1.GraphQLString,
                        },
                    },
                }),
                resolve: (0, me_1.default)(collection),
            };
            payload.Query.fields[`initialized${singularLabel}`] = {
                type: graphql_1.GraphQLBoolean,
                resolve: (0, init_1.default)(collection),
            };
            payload.Mutation.fields[`refreshToken${singularLabel}`] = {
                type: new graphql_1.GraphQLObjectType({
                    name: (0, formatName_1.default)(`${slug}Refreshed${singularLabel}`),
                    fields: {
                        user: {
                            type: collection.graphQL.JWT,
                        },
                        refreshedToken: {
                            type: graphql_1.GraphQLString,
                        },
                        exp: {
                            type: graphql_1.GraphQLInt,
                        },
                    },
                }),
                args: {
                    token: { type: graphql_1.GraphQLString },
                },
                resolve: (0, refresh_1.default)(collection),
            };
            payload.Mutation.fields[`logout${singularLabel}`] = {
                type: graphql_1.GraphQLString,
                resolve: (0, logout_1.default)(collection),
            };
            if (!collection.config.auth.disableLocalStrategy) {
                if (collection.config.auth.maxLoginAttempts > 0) {
                    payload.Mutation.fields[`unlock${singularLabel}`] = {
                        type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLBoolean),
                        args: {
                            email: { type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLString) },
                        },
                        resolve: (0, unlock_1.default)(collection),
                    };
                }
                payload.Mutation.fields[`login${singularLabel}`] = {
                    type: new graphql_1.GraphQLObjectType({
                        name: (0, formatName_1.default)(`${slug}LoginResult`),
                        fields: {
                            token: {
                                type: graphql_1.GraphQLString,
                            },
                            user: {
                                type: collection.graphQL.type,
                            },
                            exp: {
                                type: graphql_1.GraphQLInt,
                            },
                        },
                    }),
                    args: {
                        email: { type: graphql_1.GraphQLString },
                        password: { type: graphql_1.GraphQLString },
                    },
                    resolve: (0, login_1.default)(collection),
                };
                payload.Mutation.fields[`forgotPassword${singularLabel}`] = {
                    type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLBoolean),
                    args: {
                        email: { type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLString) },
                        disableEmail: { type: graphql_1.GraphQLBoolean },
                        expiration: { type: graphql_1.GraphQLInt },
                    },
                    resolve: (0, forgotPassword_1.default)(collection),
                };
                payload.Mutation.fields[`resetPassword${singularLabel}`] = {
                    type: new graphql_1.GraphQLObjectType({
                        name: (0, formatName_1.default)(`${slug}ResetPassword`),
                        fields: {
                            token: { type: graphql_1.GraphQLString },
                            user: { type: collection.graphQL.type },
                        },
                    }),
                    args: {
                        token: { type: graphql_1.GraphQLString },
                        password: { type: graphql_1.GraphQLString },
                    },
                    resolve: (0, resetPassword_1.default)(collection),
                };
                payload.Mutation.fields[`verifyEmail${singularLabel}`] = {
                    type: graphql_1.GraphQLBoolean,
                    args: {
                        token: { type: graphql_1.GraphQLString },
                    },
                    resolve: (0, verifyEmail_1.default)(collection),
                };
            }
        }
    });
}
exports.default = initCollectionsGraphQL;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb2xsZWN0aW9ucy9ncmFwaHFsL2luaXQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHNDQUFzQztBQUN0QyxxREFBbUQ7QUFDbkQscUNBTWlCO0FBRWpCLG9GQUE0RDtBQUM1RCx5R0FBaUY7QUFDakYsc0dBQTBHO0FBQzFHLGdGQUFvRjtBQUNwRixnRUFBZ0Q7QUFDaEQsZ0VBQWdEO0FBQ2hELDREQUE0QztBQUM1QyxvRUFBb0Q7QUFDcEQsa0ZBQWtFO0FBQ2xFLDRFQUE0RDtBQUM1RCxnRkFBZ0U7QUFDaEUseUVBQWlEO0FBQ2pELDZFQUFxRDtBQUNyRCwrRUFBdUQ7QUFDdkQsaUZBQXlEO0FBQ3pELGlHQUF5RTtBQUN6RSwrRkFBdUU7QUFDdkUsMkZBQW1FO0FBQ25FLGlGQUF5RDtBQUN6RCxtRkFBMkQ7QUFFM0QscURBQW9FO0FBQ3BFLDJGQUF5RjtBQUN6RixtR0FBMkU7QUFDM0UsZ0VBQW1EO0FBRW5ELFNBQVMsc0JBQXNCLENBQUMsT0FBZ0I7SUFDOUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7UUFDaEQsTUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QyxNQUFNLEVBQ0YsTUFBTSxFQUFFO1FBQ0osZ0NBQWdDO1FBQ2hDLEtBQUssRUFBRSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsRUFDM0IsTUFBTSxFQUNOLFVBQVUsRUFDVixRQUFRLEVBQ1gsRUFDSixHQUFHLFVBQVUsQ0FBQztRQUVmLE1BQU0sYUFBYSxHQUFHLElBQUEsb0JBQVUsRUFBQyxRQUFRLENBQUMsQ0FBQztRQUMzQyxJQUFJLFdBQVcsR0FBRyxJQUFBLG9CQUFVLEVBQUMsTUFBTSxDQUFDLENBQUM7UUFFckMsNENBQTRDO1FBQzVDLGdEQUFnRDtRQUNoRCw0REFBNEQ7UUFDNUQseUJBQXlCO1FBRXpCLElBQUksYUFBYSxLQUFLLFdBQVcsRUFBRTtZQUNqQyxXQUFXLEdBQUcsTUFBTSxhQUFhLEVBQUUsQ0FBQztTQUNyQztRQUVELFVBQVUsQ0FBQyxPQUFPLEdBQUcsRUFBUyxDQUFDO1FBRS9CLE1BQU0sT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUEsd0JBQWdCLEVBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztRQUN2RixNQUFNLE1BQU0sR0FBRyxJQUFBLDRDQUFtQixFQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV0RCxNQUFNLFVBQVUsR0FBcUIsRUFBRSxDQUFDO1FBRXhDLE1BQU0sZ0JBQWdCLEdBQUc7WUFDdkIsR0FBRyxNQUFNO1NBQ1YsQ0FBQztRQUVGLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixVQUFVLENBQUMsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDO1lBQ2pDLGdCQUFnQixDQUFDLElBQUksQ0FBQztnQkFDcEIsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsSUFBSSxFQUFFLE1BQU07YUFDYixDQUFDLENBQUM7U0FDSjtRQUVELElBQUksVUFBVSxFQUFFO1lBQ2QsVUFBVSxDQUFDLFNBQVMsR0FBRztnQkFDckIsSUFBSSxFQUFFLElBQUksd0JBQWMsQ0FBQyxrQ0FBZ0IsQ0FBQzthQUMzQyxDQUFDO1lBRUYsVUFBVSxDQUFDLFNBQVMsR0FBRztnQkFDckIsSUFBSSxFQUFFLElBQUksd0JBQWMsQ0FBQyxrQ0FBZ0IsQ0FBQzthQUMzQyxDQUFDO1lBRUYsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO2dCQUNwQixJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLFlBQVk7Z0JBQ25CLElBQUksRUFBRSxNQUFNO2FBQ2IsQ0FBQyxDQUFDO1lBRUgsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO2dCQUNwQixJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLFlBQVk7Z0JBQ25CLElBQUksRUFBRSxNQUFNO2FBQ2IsQ0FBQyxDQUFDO1NBQ0o7UUFFRCxNQUFNLHVCQUF1QixHQUFHLE9BQU8sQ0FBQyxRQUFRLGFBQVIsUUFBUSx1QkFBUixRQUFRLENBQUUsTUFBTSxDQUFDLENBQUM7UUFFMUQsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBQSx5QkFBZSxFQUFDO1lBQ3hDLE9BQU87WUFDUCxJQUFJLEVBQUUsYUFBYTtZQUNuQixVQUFVLEVBQUUsYUFBYTtZQUN6QixNQUFNO1lBQ04sVUFBVTtZQUNWLGFBQWEsRUFBRSx1QkFBdUI7U0FDdkMsQ0FBQyxDQUFDO1FBRUgsVUFBVSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEdBQUcsSUFBQSw2QkFBbUIsRUFDckQsYUFBYSxFQUNiLGdCQUFnQixFQUNoQixhQUFhLENBQ2QsQ0FBQztRQUVGLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUMxRSxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNWLElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsVUFBVTtnQkFDakIsSUFBSSxFQUFFLE1BQU07Z0JBQ1osUUFBUSxFQUFFLElBQUk7YUFDZixDQUFDLENBQUM7U0FDSjtRQUVELFVBQVUsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEdBQUcsSUFBSSx3QkFBYyxDQUFDLElBQUEsZ0NBQXNCLEVBQzlFLE9BQU8sRUFDUCxhQUFhLEVBQ2IsTUFBTSxFQUNOLGFBQWEsQ0FDZCxDQUFDLENBQUM7UUFFSCxVQUFVLENBQUMsT0FBTyxDQUFDLHVCQUF1QixHQUFHLElBQUksd0JBQWMsQ0FBQyxJQUFBLGdDQUFzQixFQUNwRixPQUFPLEVBQ1AsR0FBRyxhQUFhLFFBQVEsRUFDeEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUEsd0JBQWdCLEVBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxFQUMzRSxHQUFHLGFBQWEsUUFBUSxFQUN4QixJQUFJLENBQ0wsQ0FBQyxDQUFDO1FBRUgsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEdBQUc7WUFDcEMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM3QixJQUFJLEVBQUU7Z0JBQ0osRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksd0JBQWMsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDeEMsS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLHdCQUFjLEVBQUU7Z0JBQy9CLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7b0JBQ2hDLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLGVBQWUsRUFBRTtvQkFDL0MsY0FBYyxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUJBQXVCLEVBQUU7aUJBQ2hFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzthQUNSO1lBQ0QsT0FBTyxFQUFFLElBQUEsa0JBQWdCLEVBQUMsVUFBVSxDQUFDO1NBQ3RDLENBQUM7UUFFRixPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRztZQUNsQyxJQUFJLEVBQUUsSUFBQSxnQ0FBc0IsRUFBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7WUFDbEUsSUFBSSxFQUFFO2dCQUNKLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRTtnQkFDbEQsS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLHdCQUFjLEVBQUU7Z0JBQy9CLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7b0JBQ2hDLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLGVBQWUsRUFBRTtvQkFDL0MsY0FBYyxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUJBQXVCLEVBQUU7aUJBQ2hFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDUCxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsb0JBQVUsRUFBRTtnQkFDMUIsS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLG9CQUFVLEVBQUU7Z0JBQzNCLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO2FBQzlCO1lBQ0QsT0FBTyxFQUFFLElBQUEsY0FBWSxFQUFDLFVBQVUsQ0FBQztTQUNsQyxDQUFDO1FBRUYsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxhQUFhLEVBQUUsQ0FBQyxHQUFHO1lBQ2xELElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDN0IsSUFBSSxFQUFFO2dCQUNKLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLGlCQUFpQixFQUFFO2dCQUNwRCxLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsd0JBQWMsRUFBRTtnQkFDL0IsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztvQkFDaEMsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFO2lCQUNoRCxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDUjtZQUNELE9BQU8sRUFBRSxJQUFBLGdCQUFjLEVBQUMsVUFBVSxDQUFDO1NBQ3BDLENBQUM7UUFFRixPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLGFBQWEsRUFBRSxDQUFDLEdBQUc7WUFDbEQsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM3QixJQUFJLEVBQUU7Z0JBQ0osRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksd0JBQWMsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDeEMsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLEVBQUU7Z0JBQzFELEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSx3QkFBYyxFQUFFO2dCQUMvQixRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsd0JBQWMsRUFBRTtnQkFDbEMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztvQkFDaEMsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFO2lCQUNoRCxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDUjtZQUNELE9BQU8sRUFBRSxJQUFBLGdCQUFjLEVBQUMsVUFBVSxDQUFDO1NBQ3BDLENBQUM7UUFFRixPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLGFBQWEsRUFBRSxDQUFDLEdBQUc7WUFDbEQsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSTtZQUM3QixJQUFJLEVBQUU7Z0JBQ0osRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksd0JBQWMsQ0FBQyxNQUFNLENBQUMsRUFBRTthQUN6QztZQUNELE9BQU8sRUFBRSxJQUFBLGdCQUFpQixFQUFDLFVBQVUsQ0FBQztTQUN2QyxDQUFDO1FBRUYsSUFBSSxVQUFVLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUM5QixNQUFNLHVCQUF1QixHQUFZO2dCQUN2QyxHQUFHLElBQUEsb0RBQTRCLEVBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQztnQkFDbEQ7b0JBQ0UsSUFBSSxFQUFFLElBQUk7b0JBQ1YsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0Q7b0JBQ0UsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLEtBQUssRUFBRSxZQUFZO29CQUNuQixJQUFJLEVBQUUsTUFBTTtpQkFDYjtnQkFDRDtvQkFDRSxJQUFJLEVBQUUsV0FBVztvQkFDakIsS0FBSyxFQUFFLFlBQVk7b0JBQ25CLElBQUksRUFBRSxNQUFNO2lCQUNiO2FBQ0YsQ0FBQztZQUVGLFVBQVUsQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLElBQUEseUJBQWUsRUFBQztnQkFDL0MsT0FBTztnQkFDUCxJQUFJLEVBQUUsR0FBRyxhQUFhLFNBQVM7Z0JBQy9CLE1BQU0sRUFBRSx1QkFBdUI7Z0JBQy9CLFVBQVUsRUFBRSxHQUFHLGFBQWEsU0FBUztnQkFDckMsYUFBYSxFQUFFLHVCQUF1QjthQUN2QyxDQUFDLENBQUM7WUFFSCxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUEsb0JBQVUsRUFBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEdBQUc7Z0JBQzVELElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLFdBQVc7Z0JBQ3BDLElBQUksRUFBRTtvQkFDSixFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtvQkFDM0IsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzt3QkFDaEMsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFO3dCQUMvQyxjQUFjLEVBQUUsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsRUFBRTtxQkFDaEUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2lCQUNSO2dCQUNELE9BQU8sRUFBRSxJQUFBLHlCQUF1QixFQUFDLFVBQVUsQ0FBQzthQUM3QyxDQUFDO1lBQ0YsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsV0FBVyxXQUFXLEVBQUUsQ0FBQyxHQUFHO2dCQUMvQyxJQUFJLEVBQUUsSUFBQSxnQ0FBc0IsRUFBQyxXQUFXLElBQUEsb0JBQVUsRUFBQyxXQUFXLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO2dCQUNsRyxJQUFJLEVBQUU7b0JBQ0osS0FBSyxFQUFFO3dCQUNMLElBQUksRUFBRSxJQUFBLDZCQUFtQixFQUN2QixXQUFXLGFBQWEsRUFBRSxFQUMxQix1QkFBdUIsRUFDdkIsV0FBVyxhQUFhLEVBQUUsQ0FDM0I7cUJBQ0Y7b0JBQ0QsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzt3QkFDaEMsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFO3dCQUMvQyxjQUFjLEVBQUUsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsRUFBRTtxQkFDaEUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUNQLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxvQkFBVSxFQUFFO29CQUMxQixLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsb0JBQVUsRUFBRTtvQkFDM0IsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7aUJBQzlCO2dCQUNELE9BQU8sRUFBRSxJQUFBLHNCQUFvQixFQUFDLFVBQVUsQ0FBQzthQUMxQyxDQUFDO1lBQ0YsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLElBQUEsb0JBQVUsRUFBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEdBQUc7Z0JBQ3RFLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUk7Z0JBQzdCLElBQUksRUFBRTtvQkFDSixFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtpQkFDNUI7Z0JBQ0QsT0FBTyxFQUFFLElBQUEsd0JBQXNCLEVBQUMsVUFBVSxDQUFDO2FBQzVDLENBQUM7U0FDSDtRQUVELElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUU7WUFDMUIsTUFBTSxVQUFVLEdBQVksVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDOUUsSUFBSSxFQUFFLE9BQU87b0JBQ2IsSUFBSSxFQUFFLE9BQU87b0JBQ2IsUUFBUSxFQUFFLElBQUk7aUJBQ2YsQ0FBQyxDQUFBO1lBQ0YsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsSUFBQSx5QkFBZSxFQUFDO2dCQUN2QyxPQUFPO2dCQUNQLElBQUksRUFBRSxJQUFBLG9CQUFVLEVBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQztnQkFDOUIsTUFBTSxFQUFFO29CQUNOLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFBLHdCQUFnQixFQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxTQUFTLENBQUM7b0JBQ3pGLEdBQUcsVUFBVTtvQkFDYjt3QkFDRSxJQUFJLEVBQUUsWUFBWTt3QkFDbEIsSUFBSSxFQUFFLE1BQU07d0JBQ1osUUFBUSxFQUFFLElBQUk7cUJBQ2Y7aUJBQ0Y7Z0JBQ0QsVUFBVSxFQUFFLElBQUEsb0JBQVUsRUFBQyxHQUFHLElBQUksS0FBSyxDQUFDO2FBQ3JDLENBQUMsQ0FBQztZQUVILE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssYUFBYSxFQUFFLENBQUMsR0FBRztnQkFDM0MsSUFBSSxFQUFFLElBQUksMkJBQWlCLENBQUM7b0JBQzFCLElBQUksRUFBRSxJQUFBLG9CQUFVLEVBQUMsR0FBRyxJQUFJLElBQUksQ0FBQztvQkFDN0IsTUFBTSxFQUFFO3dCQUNOLEtBQUssRUFBRTs0QkFDTCxJQUFJLEVBQUUsdUJBQWE7eUJBQ3BCO3dCQUNELElBQUksRUFBRTs0QkFDSixJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJO3lCQUM5Qjt3QkFDRCxHQUFHLEVBQUU7NEJBQ0gsSUFBSSxFQUFFLG9CQUFVO3lCQUNqQjt3QkFDRCxVQUFVLEVBQUU7NEJBQ1YsSUFBSSxFQUFFLHVCQUFhO3lCQUNwQjtxQkFDRjtpQkFDRixDQUFDO2dCQUNGLE9BQU8sRUFBRSxJQUFBLFlBQUUsRUFBQyxVQUFVLENBQUM7YUFDeEIsQ0FBQztZQUVGLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGNBQWMsYUFBYSxFQUFFLENBQUMsR0FBRztnQkFDcEQsSUFBSSxFQUFFLHdCQUFjO2dCQUNwQixPQUFPLEVBQUUsSUFBQSxjQUFJLEVBQUMsVUFBVSxDQUFDO2FBQzFCLENBQUM7WUFFRixPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxlQUFlLGFBQWEsRUFBRSxDQUFDLEdBQUc7Z0JBQ3hELElBQUksRUFBRSxJQUFJLDJCQUFpQixDQUFDO29CQUMxQixJQUFJLEVBQUUsSUFBQSxvQkFBVSxFQUFDLEdBQUcsSUFBSSxZQUFZLGFBQWEsRUFBRSxDQUFDO29CQUNwRCxNQUFNLEVBQUU7d0JBQ04sSUFBSSxFQUFFOzRCQUNKLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUc7eUJBQzdCO3dCQUNELGNBQWMsRUFBRTs0QkFDZCxJQUFJLEVBQUUsdUJBQWE7eUJBQ3BCO3dCQUNELEdBQUcsRUFBRTs0QkFDSCxJQUFJLEVBQUUsb0JBQVU7eUJBQ2pCO3FCQUNGO2lCQUNGLENBQUM7Z0JBQ0YsSUFBSSxFQUFFO29CQUNKLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO2lCQUMvQjtnQkFDRCxPQUFPLEVBQUUsSUFBQSxpQkFBTyxFQUFDLFVBQVUsQ0FBQzthQUM3QixDQUFDO1lBRUYsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxhQUFhLEVBQUUsQ0FBQyxHQUFHO2dCQUNsRCxJQUFJLEVBQUUsdUJBQWE7Z0JBQ25CLE9BQU8sRUFBRSxJQUFBLGdCQUFNLEVBQUMsVUFBVSxDQUFDO2FBQzVCLENBQUM7WUFFRixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ2hELElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxFQUFFO29CQUMvQyxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLGFBQWEsRUFBRSxDQUFDLEdBQUc7d0JBQ2xELElBQUksRUFBRSxJQUFJLHdCQUFjLENBQUMsd0JBQWMsQ0FBQzt3QkFDeEMsSUFBSSxFQUFFOzRCQUNKLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLHdCQUFjLENBQUMsdUJBQWEsQ0FBQyxFQUFFO3lCQUNuRDt3QkFDRCxPQUFPLEVBQUUsSUFBQSxnQkFBTSxFQUFDLFVBQVUsQ0FBQztxQkFDNUIsQ0FBQztpQkFDSDtnQkFFRCxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLGFBQWEsRUFBRSxDQUFDLEdBQUc7b0JBQ2pELElBQUksRUFBRSxJQUFJLDJCQUFpQixDQUFDO3dCQUMxQixJQUFJLEVBQUUsSUFBQSxvQkFBVSxFQUFDLEdBQUcsSUFBSSxhQUFhLENBQUM7d0JBQ3RDLE1BQU0sRUFBRTs0QkFDTixLQUFLLEVBQUU7Z0NBQ0wsSUFBSSxFQUFFLHVCQUFhOzZCQUNwQjs0QkFDRCxJQUFJLEVBQUU7Z0NBQ0osSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSTs2QkFDOUI7NEJBQ0QsR0FBRyxFQUFFO2dDQUNILElBQUksRUFBRSxvQkFBVTs2QkFDakI7eUJBQ0Y7cUJBQ0YsQ0FBQztvQkFDRixJQUFJLEVBQUU7d0JBQ0osS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7d0JBQzlCLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO3FCQUNsQztvQkFDRCxPQUFPLEVBQUUsSUFBQSxlQUFLLEVBQUMsVUFBVSxDQUFDO2lCQUMzQixDQUFDO2dCQUVGLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGlCQUFpQixhQUFhLEVBQUUsQ0FBQyxHQUFHO29CQUMxRCxJQUFJLEVBQUUsSUFBSSx3QkFBYyxDQUFDLHdCQUFjLENBQUM7b0JBQ3hDLElBQUksRUFBRTt3QkFDSixLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSx3QkFBYyxDQUFDLHVCQUFhLENBQUMsRUFBRTt3QkFDbEQsWUFBWSxFQUFFLEVBQUUsSUFBSSxFQUFFLHdCQUFjLEVBQUU7d0JBQ3RDLFVBQVUsRUFBRSxFQUFFLElBQUksRUFBRSxvQkFBVSxFQUFFO3FCQUNqQztvQkFDRCxPQUFPLEVBQUUsSUFBQSx3QkFBYyxFQUFDLFVBQVUsQ0FBQztpQkFDcEMsQ0FBQztnQkFFRixPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsYUFBYSxFQUFFLENBQUMsR0FBRztvQkFDekQsSUFBSSxFQUFFLElBQUksMkJBQWlCLENBQUM7d0JBQzFCLElBQUksRUFBRSxJQUFBLG9CQUFVLEVBQUMsR0FBRyxJQUFJLGVBQWUsQ0FBQzt3QkFDeEMsTUFBTSxFQUFFOzRCQUNOLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFOzRCQUM5QixJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUU7eUJBQ3hDO3FCQUNGLENBQUM7b0JBQ0YsSUFBSSxFQUFFO3dCQUNKLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO3dCQUM5QixRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtxQkFDbEM7b0JBQ0QsT0FBTyxFQUFFLElBQUEsdUJBQWEsRUFBQyxVQUFVLENBQUM7aUJBQ25DLENBQUM7Z0JBRUYsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsY0FBYyxhQUFhLEVBQUUsQ0FBQyxHQUFHO29CQUN2RCxJQUFJLEVBQUUsd0JBQWM7b0JBQ3BCLElBQUksRUFBRTt3QkFDSixLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtxQkFDL0I7b0JBQ0QsT0FBTyxFQUFFLElBQUEscUJBQVcsRUFBQyxVQUFVLENBQUM7aUJBQ2pDLENBQUM7YUFDSDtTQUNGO0lBQ0gsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDO0FBRUQsa0JBQWUsc0JBQXNCLENBQUMifQ==