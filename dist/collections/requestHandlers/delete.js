"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require("../.."));
const http_status_1 = __importDefault(require("http-status"));
const errors_1 = require("../../errors");
const delete_1 = __importDefault(require("../operations/delete"));
async function deleteHandler(req, res, next) {
    const adminUILocale = __1.default.config.admin.locale;
    try {
        const doc = await (0, delete_1.default)({
            req,
            collection: req.collection,
            id: req.params.id,
            depth: parseInt(String(req.query.depth), 10)
        });
        if (!doc) {
            return res
                .status(http_status_1.default.NOT_FOUND)
                .json(new errors_1.NotFound(adminUILocale.errors.NotFoundLabel));
        }
        return res.status(http_status_1.default.OK).send(doc);
    }
    catch (error) {
        return next(error);
    }
}
exports.default = deleteHandler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsZXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbGxlY3Rpb25zL3JlcXVlc3RIYW5kbGVycy9kZWxldGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSw4Q0FBNEI7QUFFNUIsOERBQXFDO0FBRXJDLHlDQUF3QztBQUV4QyxrRUFBbUQ7QUFPcEMsS0FBSyxVQUFVLGFBQWEsQ0FDdkMsR0FBbUIsRUFDbkIsR0FBYSxFQUNiLElBQWtCO0lBRWxCLE1BQU0sYUFBYSxHQUFHLFdBQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUVsRCxJQUFJO1FBQ0EsTUFBTSxHQUFHLEdBQUcsTUFBTSxJQUFBLGdCQUFlLEVBQUM7WUFDOUIsR0FBRztZQUNILFVBQVUsRUFBRSxHQUFHLENBQUMsVUFBVTtZQUMxQixFQUFFLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2pCLEtBQUssRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxDQUFDO1NBQy9DLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDTixPQUFPLEdBQUc7aUJBQ0wsTUFBTSxDQUFDLHFCQUFVLENBQUMsU0FBUyxDQUFDO2lCQUM1QixJQUFJLENBQUMsSUFBSSxpQkFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztTQUMvRDtRQUVELE9BQU8sR0FBRyxDQUFDLE1BQU0sQ0FBQyxxQkFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUM5QztJQUFDLE9BQU8sS0FBSyxFQUFFO1FBQ1osT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDdEI7QUFDTCxDQUFDO0FBekJELGdDQXlCQyJ9