"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require("../.."));
const http_status_1 = __importDefault(require("http-status"));
const formatSuccess_1 = __importDefault(require("../../express/responses/formatSuccess"));
const create_1 = __importDefault(require("../operations/create"));
async function createHandler(req, res, next) {
    const adminUILocale = __1.default.config.admin.locale;
    try {
        const doc = await (0, create_1.default)({
            req,
            collection: req.collection,
            data: req.body,
            depth: Number(req.query.depth),
            draft: req.query.draft === 'true'
        });
        return res.status(http_status_1.default.CREATED).json({
            ...(0, formatSuccess_1.default)(`${req.collection.config.labels.singular} ${adminUILocale.requestHandlers.Create.SuccessfullyCreatedLabel}.`, 'message'),
            doc
        });
    }
    catch (error) {
        return next(error);
    }
}
exports.default = createHandler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbGxlY3Rpb25zL3JlcXVlc3RIYW5kbGVycy9jcmVhdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSw4Q0FBNEI7QUFDNUIsOERBQXFDO0FBR3JDLDBGQUEwRTtBQUUxRSxrRUFBMEM7QUFPM0IsS0FBSyxVQUFVLGFBQWEsQ0FDdkMsR0FBbUIsRUFDbkIsR0FBYSxFQUNiLElBQWtCO0lBRWxCLE1BQU0sYUFBYSxHQUFHLFdBQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUVsRCxJQUFJO1FBQ0EsTUFBTSxHQUFHLEdBQUcsTUFBTSxJQUFBLGdCQUFNLEVBQUM7WUFDckIsR0FBRztZQUNILFVBQVUsRUFBRSxHQUFHLENBQUMsVUFBVTtZQUMxQixJQUFJLEVBQUUsR0FBRyxDQUFDLElBQUk7WUFDZCxLQUFLLEVBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQzlCLEtBQUssRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxNQUFNO1NBQ3BDLENBQUMsQ0FBQztRQUVILE9BQU8sR0FBRyxDQUFDLE1BQU0sQ0FBQyxxQkFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQztZQUN2QyxHQUFHLElBQUEsdUJBQXFCLEVBQ3BCLEdBQUcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxhQUFhLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyx3QkFBd0IsR0FBRyxFQUM1RyxTQUFTLENBQ1o7WUFDRCxHQUFHO1NBQ04sQ0FBQyxDQUFDO0tBQ047SUFBQyxPQUFPLEtBQUssRUFBRTtRQUNaLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3RCO0FBQ0wsQ0FBQztBQTFCRCxnQ0EwQkMifQ==