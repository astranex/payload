"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deprecatedUpdate = void 0;
const __1 = __importDefault(require("../.."));
const http_status_1 = __importDefault(require("http-status"));
const formatSuccess_1 = __importDefault(require("../../express/responses/formatSuccess"));
const update_1 = __importDefault(require("../operations/update"));
async function deprecatedUpdate(req, res, next) {
    req.payload.logger.warn('The PUT method is deprecated and will no longer be supported in a future release. Please use the PATCH method for update requests.');
    return updateHandler(req, res, next);
}
exports.deprecatedUpdate = deprecatedUpdate;
async function updateHandler(req, res, next) {
    const adminUILocale = __1.default.config.admin.locale;
    try {
        const draft = req.query.draft === 'true';
        const autosave = req.query.autosave === 'true';
        const doc = await (0, update_1.default)({
            req,
            collection: req.collection,
            id: req.params.id,
            data: req.body,
            depth: parseInt(String(req.query.depth), 10),
            draft,
            autosave
        });
        let message = adminUILocale.requestHandlers.Update.UpdatedSuccessfullyLabel;
        if (draft)
            message =
                adminUILocale.requestHandlers.Update
                    .DraftSavedSuccessfullyLabel;
        if (autosave)
            message =
                adminUILocale.requestHandlers.Update.AutosavedSuccessfullyLabel;
        return res.status(http_status_1.default.OK).json({
            ...(0, formatSuccess_1.default)(message, 'message'),
            doc
        });
    }
    catch (error) {
        return next(error);
    }
}
exports.default = updateHandler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbGxlY3Rpb25zL3JlcXVlc3RIYW5kbGVycy91cGRhdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsOENBQTRCO0FBRTVCLDhEQUFxQztBQUVyQywwRkFBMEU7QUFDMUUsa0VBQTBDO0FBT25DLEtBQUssVUFBVSxnQkFBZ0IsQ0FDbEMsR0FBbUIsRUFDbkIsR0FBYSxFQUNiLElBQWtCO0lBRWxCLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FDbkIsb0lBQW9JLENBQ3ZJLENBQUM7SUFFRixPQUFPLGFBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO0FBQ3pDLENBQUM7QUFWRCw0Q0FVQztBQUVjLEtBQUssVUFBVSxhQUFhLENBQ3ZDLEdBQW1CLEVBQ25CLEdBQWEsRUFDYixJQUFrQjtJQUVsQixNQUFNLGFBQWEsR0FBRyxXQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFFbEQsSUFBSTtRQUNBLE1BQU0sS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQztRQUN6QyxNQUFNLFFBQVEsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsS0FBSyxNQUFNLENBQUM7UUFFL0MsTUFBTSxHQUFHLEdBQUcsTUFBTSxJQUFBLGdCQUFNLEVBQUM7WUFDckIsR0FBRztZQUNILFVBQVUsRUFBRSxHQUFHLENBQUMsVUFBVTtZQUMxQixFQUFFLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2pCLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSTtZQUNkLEtBQUssRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzVDLEtBQUs7WUFDTCxRQUFRO1NBQ1gsQ0FBQyxDQUFDO1FBRUgsSUFBSSxPQUFPLEdBQ1AsYUFBYSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsd0JBQXdCLENBQUM7UUFFbEUsSUFBSSxLQUFLO1lBQ0wsT0FBTztnQkFDSCxhQUFhLENBQUMsZUFBZSxDQUFDLE1BQU07cUJBQy9CLDJCQUEyQixDQUFDO1FBQ3pDLElBQUksUUFBUTtZQUNSLE9BQU87Z0JBQ0gsYUFBYSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsMEJBQTBCLENBQUM7UUFFeEUsT0FBTyxHQUFHLENBQUMsTUFBTSxDQUFDLHFCQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ2xDLEdBQUcsSUFBQSx1QkFBcUIsRUFBQyxPQUFPLEVBQUUsU0FBUyxDQUFDO1lBQzVDLEdBQUc7U0FDTixDQUFDLENBQUM7S0FDTjtJQUFDLE9BQU8sS0FBSyxFQUFFO1FBQ1osT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDdEI7QUFDTCxDQUFDO0FBdkNELGdDQXVDQyJ9