"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const componentSchema_1 = require("../../utilities/componentSchema");
const schema_1 = require("../../config/schema");
const strategyBaseSchema = joi_1.default.object().keys({
    refresh: joi_1.default.boolean(),
    logout: joi_1.default.boolean(),
});
const collectionSchema = joi_1.default.object().keys({
    slug: joi_1.default.string().required(),
    labels: joi_1.default.object({
        singular: joi_1.default.string(),
        plural: joi_1.default.string()
    }),
    slugs: joi_1.default.object({
        singular: joi_1.default.string(),
        plural: joi_1.default.string()
    }),
    access: joi_1.default.object({
        create: joi_1.default.func(),
        read: joi_1.default.func(),
        readVersions: joi_1.default.func(),
        update: joi_1.default.func(),
        delete: joi_1.default.func(),
        unlock: joi_1.default.func(),
        admin: joi_1.default.func()
    }),
    timestamps: joi_1.default.boolean(),
    admin: joi_1.default.object({
        useAsTitle: joi_1.default.string(),
        defaultColumns: joi_1.default.array().items(joi_1.default.string()),
        listSearchableFields: joi_1.default.array().items(joi_1.default.string()),
        group: joi_1.default.string(),
        description: joi_1.default.alternatives().try(joi_1.default.string(), componentSchema_1.componentSchema),
        enableRichTextRelationship: joi_1.default.boolean(),
        components: joi_1.default.object({
            views: joi_1.default.object({
                List: componentSchema_1.componentSchema,
                Edit: componentSchema_1.componentSchema
            })
        }),
        pagination: joi_1.default.object({
            defaultLimit: joi_1.default.number(),
            limits: joi_1.default.array().items(joi_1.default.number())
        }),
        preview: joi_1.default.func(),
        disableDuplicate: joi_1.default.bool(),
        hideAPIURL: joi_1.default.bool()
    }),
    fields: joi_1.default.array(),
    hooks: joi_1.default.object({
        beforeOperation: joi_1.default.array().items(joi_1.default.func()),
        beforeValidate: joi_1.default.array().items(joi_1.default.func()),
        beforeChange: joi_1.default.array().items(joi_1.default.func()),
        afterChange: joi_1.default.array().items(joi_1.default.func()),
        beforeRead: joi_1.default.array().items(joi_1.default.func()),
        afterRead: joi_1.default.array().items(joi_1.default.func()),
        beforeDelete: joi_1.default.array().items(joi_1.default.func()),
        afterDelete: joi_1.default.array().items(joi_1.default.func()),
        beforeLogin: joi_1.default.array().items(joi_1.default.func()),
        afterLogin: joi_1.default.array().items(joi_1.default.func()),
        afterLogout: joi_1.default.array().items(joi_1.default.func()),
        afterMe: joi_1.default.array().items(joi_1.default.func()),
        afterRefresh: joi_1.default.array().items(joi_1.default.func()),
        afterForgotPassword: joi_1.default.array().items(joi_1.default.func())
    }),
    endpoints: schema_1.endpointsSchema,
    auth: joi_1.default.alternatives().try(joi_1.default.object({
        tokenExpiration: joi_1.default.number(),
        depth: joi_1.default.number(),
        verify: joi_1.default.alternatives().try(joi_1.default.boolean(), joi_1.default.object().keys({
            generateEmailHTML: joi_1.default.func(),
            generateEmailSubject: joi_1.default.func()
        })),
        lockTime: joi_1.default.number(),
        useAPIKey: joi_1.default.boolean(),
        cookies: joi_1.default.object().keys({
            secure: joi_1.default.boolean(),
            sameSite: joi_1.default.string(),
            domain: joi_1.default.string()
        }),
        forgotPassword: joi_1.default.object().keys({
            generateEmailHTML: joi_1.default.func(),
            generateEmailSubject: joi_1.default.func()
        }),
        maxLoginAttempts: joi_1.default.number(),
        disableLocalStrategy: joi_1.default.boolean().valid(true),
        strategies: joi_1.default.array().items(joi_1.default.alternatives().try(strategyBaseSchema.keys({
            name: joi_1.default.string().required(),
            strategy: joi_1.default.func().maxArity(1).required()
        }), strategyBaseSchema.keys({
            name: joi_1.default.string(),
            strategy: joi_1.default.object().required()
        })))
    }), joi_1.default.boolean()),
    versions: joi_1.default.alternatives().try(joi_1.default.object({
        maxPerDoc: joi_1.default.number(),
        retainDeleted: joi_1.default.boolean(),
        drafts: joi_1.default.alternatives().try(joi_1.default.object({
            autosave: joi_1.default.alternatives().try(joi_1.default.boolean(), joi_1.default.object({
                interval: joi_1.default.number()
            }))
        }), joi_1.default.boolean())
    }), joi_1.default.boolean()),
    upload: joi_1.default.alternatives().try(joi_1.default.object({
        staticURL: joi_1.default.string(),
        staticDir: joi_1.default.string(),
        disableLocalStorage: joi_1.default.bool(),
        adminThumbnail: joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.func()),
        imageSizes: joi_1.default.array().items(joi_1.default
            .object()
            .keys({
            name: joi_1.default.string(),
            width: joi_1.default.number().allow(null),
            height: joi_1.default.number().allow(null),
            crop: joi_1.default.string() // TODO: add further specificity with joi.xor
        })
            .unknown()),
        mimeTypes: joi_1.default.array().items(joi_1.default.string()),
        staticOptions: joi_1.default.object(),
        handlers: joi_1.default.array().items(joi_1.default.func()),
        resizeOptions: joi_1.default
            .object()
            .keys({
            width: joi_1.default.number().allow(null),
            height: joi_1.default.number().allow(null),
            fit: joi_1.default.string(),
            position: joi_1.default
                .alternatives()
                .try(joi_1.default.string(), joi_1.default.number()),
            background: joi_1.default.string(),
            kernel: joi_1.default.string(),
            withoutEnlargement: joi_1.default.bool(),
            fastShrinkOnLoad: joi_1.default.bool()
        })
            .allow(null),
        formatOptions: joi_1.default.object().keys({
            format: joi_1.default.string(),
            options: joi_1.default.object()
        })
    }), joi_1.default.boolean())
});
exports.default = collectionSchema;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NoZW1hLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbGxlY3Rpb25zL2NvbmZpZy9zY2hlbWEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSw4Q0FBc0I7QUFDdEIscUVBQWtFO0FBQ2xFLGdEQUFzRDtBQUV0RCxNQUFNLGtCQUFrQixHQUFHLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7SUFDM0MsT0FBTyxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7SUFDdEIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7Q0FDdEIsQ0FBQyxDQUFDO0FBRUgsTUFBTSxnQkFBZ0IsR0FBRyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDO0lBQ3ZDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO0lBQzdCLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO1FBQ2YsUUFBUSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDdEIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7S0FDdkIsQ0FBQztJQUNGLEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO1FBQ2QsUUFBUSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDdEIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7S0FDdkIsQ0FBQztJQUNGLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO1FBQ2YsTUFBTSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDbEIsSUFBSSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDaEIsWUFBWSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDeEIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDbEIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDbEIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDbEIsS0FBSyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7S0FDcEIsQ0FBQztJQUNGLFVBQVUsRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFO0lBQ3pCLEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO1FBQ2QsVUFBVSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDeEIsY0FBYyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQy9DLG9CQUFvQixFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3JELEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ25CLFdBQVcsRUFBRSxhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLGFBQUcsQ0FBQyxNQUFNLEVBQUUsRUFBRSxpQ0FBZSxDQUFDO1FBQ2xFLDBCQUEwQixFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7UUFDekMsVUFBVSxFQUFFLGFBQUcsQ0FBQyxNQUFNLENBQUM7WUFDbkIsS0FBSyxFQUFFLGFBQUcsQ0FBQyxNQUFNLENBQUM7Z0JBQ2QsSUFBSSxFQUFFLGlDQUFlO2dCQUNyQixJQUFJLEVBQUUsaUNBQWU7YUFDeEIsQ0FBQztTQUNMLENBQUM7UUFDRixVQUFVLEVBQUUsYUFBRyxDQUFDLE1BQU0sQ0FBQztZQUNuQixZQUFZLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtZQUMxQixNQUFNLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDMUMsQ0FBQztRQUNGLE9BQU8sRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO1FBQ25CLGdCQUFnQixFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDNUIsVUFBVSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7S0FDekIsQ0FBQztJQUNGLE1BQU0sRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFO0lBQ25CLEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO1FBQ2QsZUFBZSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzlDLGNBQWMsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUM3QyxZQUFZLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0MsV0FBVyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFDLFVBQVUsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN6QyxTQUFTLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEMsWUFBWSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNDLFdBQVcsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQyxXQUFXLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUMsVUFBVSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pDLFdBQVcsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQyxPQUFPLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEMsWUFBWSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNDLG1CQUFtQixFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDO0tBQ3JELENBQUM7SUFDRixTQUFTLEVBQUUsd0JBQWU7SUFDMUIsSUFBSSxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQ3hCLGFBQUcsQ0FBQyxNQUFNLENBQUM7UUFDUCxlQUFlLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUM3QixLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUNuQixNQUFNLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDMUIsYUFBRyxDQUFDLE9BQU8sRUFBRSxFQUNiLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7WUFDZCxpQkFBaUIsRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO1lBQzdCLG9CQUFvQixFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7U0FDbkMsQ0FBQyxDQUNMO1FBQ0QsUUFBUSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDdEIsU0FBUyxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7UUFDeEIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7WUFDdkIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7WUFDckIsUUFBUSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7WUFDdEIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7U0FDdkIsQ0FBQztRQUNGLGNBQWMsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDO1lBQzlCLGlCQUFpQixFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7WUFDN0Isb0JBQW9CLEVBQUUsYUFBRyxDQUFDLElBQUksRUFBRTtTQUNuQyxDQUFDO1FBQ0YsZ0JBQWdCLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUM5QixvQkFBb0IsRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUMvQyxVQUFVLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FDekIsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDbEIsa0JBQWtCLENBQUMsSUFBSSxDQUFDO1lBQ3BCLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO1lBQzdCLFFBQVEsRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtTQUM5QyxDQUFDLEVBQ0Ysa0JBQWtCLENBQUMsSUFBSSxDQUFDO1lBQ3BCLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1lBQ2xCLFFBQVEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO1NBQ3BDLENBQUMsQ0FDTCxDQUNKO0tBQ0osQ0FBQyxFQUNGLGFBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FDaEI7SUFDRCxRQUFRLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDNUIsYUFBRyxDQUFDLE1BQU0sQ0FBQztRQUNQLFNBQVMsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ3ZCLGFBQWEsRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFO1FBQzVCLE1BQU0sRUFBRSxhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUMxQixhQUFHLENBQUMsTUFBTSxDQUFDO1lBQ1AsUUFBUSxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQzVCLGFBQUcsQ0FBQyxPQUFPLEVBQUUsRUFDYixhQUFHLENBQUMsTUFBTSxDQUFDO2dCQUNQLFFBQVEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO2FBQ3pCLENBQUMsQ0FDTDtTQUNKLENBQUMsRUFDRixhQUFHLENBQUMsT0FBTyxFQUFFLENBQ2hCO0tBQ0osQ0FBQyxFQUNGLGFBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FDaEI7SUFDRCxNQUFNLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDMUIsYUFBRyxDQUFDLE1BQU0sQ0FBQztRQUNQLFNBQVMsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ3ZCLFNBQVMsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ3ZCLG1CQUFtQixFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDL0IsY0FBYyxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNoRSxVQUFVLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FDekIsYUFBRzthQUNFLE1BQU0sRUFBRTthQUNSLElBQUksQ0FBQztZQUNGLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1lBQ2xCLEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztZQUMvQixNQUFNLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7WUFDaEMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyw2Q0FBNkM7U0FDbkUsQ0FBQzthQUNELE9BQU8sRUFBRSxDQUNqQjtRQUNELFNBQVMsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUMxQyxhQUFhLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUMzQixRQUFRLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdkMsYUFBYSxFQUFFLGFBQUc7YUFDYixNQUFNLEVBQUU7YUFDUixJQUFJLENBQUM7WUFDRixLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7WUFDL0IsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBQ2hDLEdBQUcsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1lBQ2pCLFFBQVEsRUFBRSxhQUFHO2lCQUNSLFlBQVksRUFBRTtpQkFDZCxHQUFHLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNwQyxVQUFVLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtZQUN4QixNQUFNLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtZQUNwQixrQkFBa0IsRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO1lBQzlCLGdCQUFnQixFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7U0FDL0IsQ0FBQzthQUNELEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDaEIsYUFBYSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7WUFDN0IsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7WUFDcEIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7U0FDeEIsQ0FBQztLQUNMLENBQUMsRUFDRixhQUFHLENBQUMsT0FBTyxFQUFFLENBQ2hCO0NBQ0osQ0FBQyxDQUFDO0FBRUgsa0JBQWUsZ0JBQWdCLENBQUMifQ==