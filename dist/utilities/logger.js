"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pino_1 = __importDefault(require("pino"));
const micro_memoize_1 = __importDefault(require("micro-memoize"));
exports.default = (0, micro_memoize_1.default)((name = 'payload', options) => (0, pino_1.default)({
    name,
    enabled: process.env.DISABLE_LOGGING !== 'true',
    ...(options
        ? { options }
        : {
            prettyPrint: {
                ignore: 'pid,hostname',
                translateTime: 'HH:MM:ss',
            },
        }),
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nZ2VyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3V0aWxpdGllcy9sb2dnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxnREFBd0I7QUFDeEIsa0VBQW9DO0FBSXBDLGtCQUFlLElBQUEsdUJBQU8sRUFDcEIsQ0FBQyxJQUFJLEdBQUcsU0FBUyxFQUFFLE9BQTRCLEVBQUUsRUFBRSxDQUFDLElBQUEsY0FBSSxFQUFDO0lBQ3ZELElBQUk7SUFDSixPQUFPLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEtBQUssTUFBTTtJQUMvQyxHQUFHLENBQUMsT0FBTztRQUNULENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRTtRQUNiLENBQUMsQ0FBQztZQUNBLFdBQVcsRUFBRTtnQkFDWCxNQUFNLEVBQUUsY0FBYztnQkFDdEIsYUFBYSxFQUFFLFVBQVU7YUFDMUI7U0FDRixDQUFDO0NBQ0wsQ0FBa0IsQ0FDcEIsQ0FBQyJ9