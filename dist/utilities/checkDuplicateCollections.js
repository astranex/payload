"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore - need to do this because this file doesn't actually exist
// import config from 'payload-config';
// import payload from '..';
const errors_1 = require("../errors");
const getDuplicates = (arr) => arr.filter((item, index) => arr.indexOf(item) !== index);
const checkDuplicateCollections = (collections) => {
    const duplicateSlugs = getDuplicates(collections.map((c) => c.slug));
    if (duplicateSlugs.length > 0) {
        throw new errors_1.DuplicateCollection('slug', duplicateSlugs, ['Collection', 'already in use:']
        // payload?.config?.admin.locale.errors.DuplicateCollectionLabels
        );
    }
    const duplicateLabels = getDuplicates(collections.map((c) => c.labels.singular));
    if (duplicateLabels.length > 0) {
        throw new errors_1.DuplicateCollection('label', duplicateLabels, ['Collection', 'already in use:']
        // payload?.config?.admin.locale.errors.DuplicateCollectionLabels
        );
    }
};
exports.default = checkDuplicateCollections;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tEdXBsaWNhdGVDb2xsZWN0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlsaXRpZXMvY2hlY2tEdXBsaWNhdGVDb2xsZWN0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDZEQUE2RDtBQUM3RCx3RUFBd0U7QUFDeEUsdUNBQXVDO0FBQ3ZDLDRCQUE0QjtBQUM1QixzQ0FBZ0Q7QUFHaEQsTUFBTSxhQUFhLEdBQUcsQ0FBQyxHQUFhLEVBQUUsRUFBRSxDQUNwQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQztBQUU3RCxNQUFNLHlCQUF5QixHQUFHLENBQUMsV0FBK0IsRUFBUSxFQUFFO0lBQ3hFLE1BQU0sY0FBYyxHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNyRSxJQUFJLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1FBQzNCLE1BQU0sSUFBSSw0QkFBbUIsQ0FDekIsTUFBTSxFQUNOLGNBQWMsRUFDZCxDQUFDLFlBQVksRUFBRSxpQkFBaUIsQ0FBQztRQUNqQyxpRUFBaUU7U0FDcEUsQ0FBQztLQUNMO0lBQ0QsTUFBTSxlQUFlLEdBQUcsYUFBYSxDQUNqQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUM1QyxDQUFDO0lBQ0YsSUFBSSxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtRQUM1QixNQUFNLElBQUksNEJBQW1CLENBQ3pCLE9BQU8sRUFDUCxlQUFlLEVBQ2YsQ0FBQyxZQUFZLEVBQUUsaUJBQWlCLENBQUM7UUFDakMsaUVBQWlFO1NBQ3BFLENBQUM7S0FDTDtBQUNMLENBQUMsQ0FBQztBQUVGLGtCQUFlLHlCQUF5QixDQUFDIn0=