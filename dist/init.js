"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initSync = exports.initAsync = exports.init = void 0;
/* eslint-disable no-param-reassign */
const express_1 = __importDefault(require("express"));
const crypto_1 = __importDefault(require("crypto"));
const mongoose_1 = __importDefault(require("mongoose"));
const authenticate_1 = __importDefault(require("./express/middleware/authenticate"));
const connect_1 = __importDefault(require("./mongoose/connect"));
const middleware_1 = __importDefault(require("./express/middleware"));
const admin_1 = __importDefault(require("./express/admin"));
const init_1 = __importDefault(require("./auth/init"));
const access_1 = __importDefault(require("./auth/requestHandlers/access"));
const init_2 = __importDefault(require("./collections/init"));
const init_3 = __importDefault(require("./preferences/init"));
const init_4 = __importDefault(require("./globals/init"));
const initPlayground_1 = __importDefault(require("./graphql/initPlayground"));
const static_1 = __importDefault(require("./express/static"));
const registerSchema_1 = __importDefault(require("./graphql/registerSchema"));
const graphQLHandler_1 = __importDefault(require("./graphql/graphQLHandler"));
const build_1 = __importDefault(require("./email/build"));
const identifyAPI_1 = __importDefault(require("./express/middleware/identifyAPI"));
const errorHandler_1 = __importDefault(require("./express/middleware/errorHandler"));
const sendEmail_1 = __importDefault(require("./email/sendEmail"));
const serverInit_1 = require("./utilities/telemetry/events/serverInit");
const load_1 = __importDefault(require("./config/load"));
const logger_1 = __importDefault(require("./utilities/logger"));
const dataloader_1 = require("./collections/dataloader");
const mountEndpoints_1 = __importDefault(require("./express/mountEndpoints"));
const init = (payload, options) => {
    payload.logger.info('Starting Payload...');
    if (!options.secret) {
        throw new Error('Error: missing secret key. A secret key is needed to secure Payload.');
    }
    if (options.mongoURL !== false && typeof options.mongoURL !== 'string') {
        throw new Error('Error: missing MongoDB connection URL.');
    }
    payload.emailOptions = { ...options.email };
    payload.secret = crypto_1.default
        .createHash('sha256')
        .update(options.secret)
        .digest('hex')
        .slice(0, 32);
    payload.local = options.local;
    payload.config = (0, load_1.default)(payload.logger);
    // If not initializing locally, scaffold router
    if (!payload.local) {
        payload.router = express_1.default.Router();
        payload.router.use(...(0, middleware_1.default)(payload));
        (0, init_1.default)(payload);
    }
    // Configure email service
    payload.email = (0, build_1.default)(payload.emailOptions, payload.logger);
    payload.sendEmail = sendEmail_1.default.bind(payload);
    // Initialize collections & globals
    (0, init_2.default)(payload);
    (0, init_4.default)(payload);
    if (!payload.config.graphQL.disable) {
        (0, registerSchema_1.default)(payload);
    }
    // If not initializing locally, set up HTTP routing
    if (!payload.local) {
        options.express.use((req, res, next) => {
            req.payload = payload;
            next();
        });
        options.express.use((req, res, next) => {
            req.payloadDataLoader = (0, dataloader_1.getDataLoader)(req);
            return next();
        });
        payload.express = options.express;
        if (payload.config.rateLimit.trustProxy) {
            payload.express.set('trust proxy', 1);
        }
        (0, admin_1.default)(payload);
        (0, init_3.default)(payload);
        payload.router.get('/access', access_1.default);
        if (!payload.config.graphQL.disable) {
            payload.router.use(payload.config.routes.graphQL, (req, res, next) => {
                if (req.method === 'OPTIONS') {
                    res.sendStatus(204);
                }
                else {
                    next();
                }
            }, (0, identifyAPI_1.default)('GraphQL'), (req, res) => (0, graphQLHandler_1.default)(req, res)(req, res));
            (0, initPlayground_1.default)(payload);
        }
        (0, mountEndpoints_1.default)(options.express, payload.router, payload.config.endpoints);
        // Bind router to API
        payload.express.use(payload.config.routes.api, payload.router);
        // Enable static routes for all collections permitting upload
        (0, static_1.default)(payload);
        payload.errorHandler = (0, errorHandler_1.default)(payload.config, payload.logger);
        payload.router.use(payload.errorHandler);
        payload.authenticate = (0, authenticate_1.default)(payload.config);
    }
    (0, serverInit_1.serverInit)(payload);
};
exports.init = init;
const initAsync = async (payload, options) => {
    payload.logger = (0, logger_1.default)('payload', options.loggerOptions);
    payload.mongoURL = options.mongoURL;
    if (payload.mongoURL) {
        mongoose_1.default.set('strictQuery', false);
        payload.mongoMemoryServer = await (0, connect_1.default)(payload.mongoURL, options.mongoOptions, payload.logger);
    }
    (0, exports.init)(payload, options);
    if (typeof options.onInit === 'function')
        await options.onInit(payload);
    if (typeof payload.config.onInit === 'function')
        await payload.config.onInit(payload);
};
exports.initAsync = initAsync;
const initSync = (payload, options) => {
    payload.logger = (0, logger_1.default)('payload', options.loggerOptions);
    payload.mongoURL = options.mongoURL;
    if (payload.mongoURL) {
        mongoose_1.default.set('strictQuery', false);
        (0, connect_1.default)(payload.mongoURL, options.mongoOptions, payload.logger);
    }
    (0, exports.init)(payload, options);
    if (typeof options.onInit === 'function')
        options.onInit(payload);
    if (typeof payload.config.onInit === 'function')
        payload.config.onInit(payload);
};
exports.initSync = initSync;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9pbml0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLHNDQUFzQztBQUN0QyxzREFBMEQ7QUFDMUQsb0RBQTRCO0FBQzVCLHdEQUFnQztBQUdoQyxxRkFBNkQ7QUFDN0QsaUVBQWlEO0FBQ2pELHNFQUFxRDtBQUNyRCw0REFBd0M7QUFDeEMsdURBQW1DO0FBQ25DLDJFQUFtRDtBQUNuRCw4REFBaUQ7QUFDakQsOERBQWlEO0FBQ2pELDBEQUF5QztBQUN6Qyw4RUFBNkQ7QUFDN0QsOERBQTBDO0FBQzFDLDhFQUFzRDtBQUN0RCw4RUFBc0Q7QUFDdEQsMERBQXVDO0FBQ3ZDLG1GQUEyRDtBQUMzRCxxRkFBNkQ7QUFFN0Qsa0VBQTBDO0FBRTFDLHdFQUE0RjtBQUU1Rix5REFBdUM7QUFDdkMsZ0VBQXdDO0FBQ3hDLHlEQUF5RDtBQUN6RCw4RUFBc0Q7QUFFL0MsTUFBTSxJQUFJLEdBQUcsQ0FBQyxPQUFnQixFQUFFLE9BQW9CLEVBQVEsRUFBRTtJQUNqRSxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQzNDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO1FBQ2pCLE1BQU0sSUFBSSxLQUFLLENBQ1gsc0VBQXNFLENBQ3pFLENBQUM7S0FDTDtJQUVELElBQUksT0FBTyxDQUFDLFFBQVEsS0FBSyxLQUFLLElBQUksT0FBTyxPQUFPLENBQUMsUUFBUSxLQUFLLFFBQVEsRUFBRTtRQUNwRSxNQUFNLElBQUksS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7S0FDN0Q7SUFFRCxPQUFPLENBQUMsWUFBWSxHQUFHLEVBQUUsR0FBRyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDNUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxnQkFBTTtTQUNsQixVQUFVLENBQUMsUUFBUSxDQUFDO1NBQ3BCLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO1NBQ3RCLE1BQU0sQ0FBQyxLQUFLLENBQUM7U0FDYixLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRWxCLE9BQU8sQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztJQUU5QixPQUFPLENBQUMsTUFBTSxHQUFHLElBQUEsY0FBVSxFQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUU1QywrQ0FBK0M7SUFDL0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7UUFDaEIsT0FBTyxDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2xDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBQSxvQkFBaUIsRUFBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ2xELElBQUEsY0FBUSxFQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQ3JCO0lBRUQsMEJBQTBCO0lBQzFCLE9BQU8sQ0FBQyxLQUFLLEdBQUcsSUFBQSxlQUFVLEVBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakUsT0FBTyxDQUFDLFNBQVMsR0FBRyxtQkFBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUU1QyxtQ0FBbUM7SUFDbkMsSUFBQSxjQUFlLEVBQUMsT0FBTyxDQUFDLENBQUM7SUFDekIsSUFBQSxjQUFXLEVBQUMsT0FBTyxDQUFDLENBQUM7SUFFckIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTtRQUNqQyxJQUFBLHdCQUFjLEVBQUMsT0FBTyxDQUFDLENBQUM7S0FDM0I7SUFDRCxtREFBbUQ7SUFDbkQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7UUFDaEIsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFtQixFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsRUFBRTtZQUNuRCxHQUFHLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztZQUN0QixJQUFJLEVBQUUsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO1FBRUgsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQ2YsQ0FBQyxHQUFtQixFQUFFLEdBQWEsRUFBRSxJQUFrQixFQUFRLEVBQUU7WUFDN0QsR0FBRyxDQUFDLGlCQUFpQixHQUFHLElBQUEsMEJBQWEsRUFBQyxHQUFHLENBQUMsQ0FBQztZQUMzQyxPQUFPLElBQUksRUFBRSxDQUFDO1FBQ2xCLENBQUMsQ0FDSixDQUFDO1FBRUYsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBRWxDLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFO1lBQ3JDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN6QztRQUVELElBQUEsZUFBUyxFQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ25CLElBQUEsY0FBZSxFQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXpCLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxnQkFBTSxDQUFDLENBQUM7UUFFdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUNqQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FDZCxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQzdCLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQVEsRUFBRTtnQkFDckIsSUFBSSxHQUFHLENBQUMsTUFBTSxLQUFLLFNBQVMsRUFBRTtvQkFDMUIsR0FBRyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsSUFBSSxFQUFFLENBQUM7aUJBQ1Y7WUFDTCxDQUFDLEVBQ0QsSUFBQSxxQkFBVyxFQUFDLFNBQVMsQ0FBQyxFQUN0QixDQUFDLEdBQW1CLEVBQUUsR0FBYSxFQUFFLEVBQUUsQ0FDbkMsSUFBQSx3QkFBYyxFQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQ3pDLENBQUM7WUFDRixJQUFBLHdCQUFxQixFQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2xDO1FBRUQsSUFBQSx3QkFBYyxFQUNWLE9BQU8sQ0FBQyxPQUFPLEVBQ2YsT0FBTyxDQUFDLE1BQU0sRUFDZCxPQUFPLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FDM0IsQ0FBQztRQUVGLHFCQUFxQjtRQUNyQixPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRS9ELDZEQUE2RDtRQUM3RCxJQUFBLGdCQUFVLEVBQUMsT0FBTyxDQUFDLENBQUM7UUFFcEIsT0FBTyxDQUFDLFlBQVksR0FBRyxJQUFBLHNCQUFZLEVBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXpDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsSUFBQSxzQkFBWSxFQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztLQUN2RDtJQUVELElBQUEsdUJBQW1CLEVBQUMsT0FBTyxDQUFDLENBQUM7QUFDakMsQ0FBQyxDQUFDO0FBdEdXLFFBQUEsSUFBSSxRQXNHZjtBQUVLLE1BQU0sU0FBUyxHQUFHLEtBQUssRUFDMUIsT0FBZ0IsRUFDaEIsT0FBb0IsRUFDUCxFQUFFO0lBQ2YsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFBLGdCQUFNLEVBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUMxRCxPQUFPLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUM7SUFFcEMsSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO1FBQ2xCLGtCQUFRLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNuQyxPQUFPLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxJQUFBLGlCQUFlLEVBQzdDLE9BQU8sQ0FBQyxRQUFRLEVBQ2hCLE9BQU8sQ0FBQyxZQUFZLEVBQ3BCLE9BQU8sQ0FBQyxNQUFNLENBQ2pCLENBQUM7S0FDTDtJQUVELElBQUEsWUFBSSxFQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztJQUV2QixJQUFJLE9BQU8sT0FBTyxDQUFDLE1BQU0sS0FBSyxVQUFVO1FBQUUsTUFBTSxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3hFLElBQUksT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sS0FBSyxVQUFVO1FBQzNDLE1BQU0sT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDN0MsQ0FBQyxDQUFDO0FBckJXLFFBQUEsU0FBUyxhQXFCcEI7QUFFSyxNQUFNLFFBQVEsR0FBRyxDQUFDLE9BQWdCLEVBQUUsT0FBb0IsRUFBUSxFQUFFO0lBQ3JFLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBQSxnQkFBTSxFQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDMUQsT0FBTyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDO0lBRXBDLElBQUksT0FBTyxDQUFDLFFBQVEsRUFBRTtRQUNsQixrQkFBUSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbkMsSUFBQSxpQkFBZSxFQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7S0FDM0U7SUFFRCxJQUFBLFlBQUksRUFBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFFdkIsSUFBSSxPQUFPLE9BQU8sQ0FBQyxNQUFNLEtBQUssVUFBVTtRQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDbEUsSUFBSSxPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxLQUFLLFVBQVU7UUFDM0MsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDdkMsQ0FBQyxDQUFDO0FBZFcsUUFBQSxRQUFRLFlBY25CIn0=