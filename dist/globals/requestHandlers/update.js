"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = __importDefault(require("../.."));
const http_status_1 = __importDefault(require("http-status"));
const update_1 = __importDefault(require("../operations/update"));
function updateHandler(globalConfig) {
    const adminUILocale = __1.default.config.admin.locale;
    return async function handler(req, res, next) {
        try {
            const { slug } = globalConfig;
            const draft = req.query.draft === 'true';
            const autosave = req.query.autosave === 'true';
            const result = await (0, update_1.default)({
                req,
                globalConfig,
                slug,
                depth: Number(req.query.depth),
                data: req.body,
                draft,
                autosave
            });
            let message = adminUILocale.requestHandlers.Update.SavedSuccessfullyLabel;
            if (draft)
                message =
                    adminUILocale.requestHandlers.Update
                        .DraftSavedSuccessfullyLabel;
            if (autosave)
                message =
                    adminUILocale.requestHandlers.Update
                        .AutosavedSuccessfullyLabel;
            return res.status(http_status_1.default.OK).json({ message, result });
        }
        catch (error) {
            return next(error);
        }
    };
}
exports.default = updateHandler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2dsb2JhbHMvcmVxdWVzdEhhbmRsZXJzL3VwZGF0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDhDQUE0QjtBQUU1Qiw4REFBcUM7QUFJckMsa0VBQTBDO0FBUzFDLFNBQXdCLGFBQWEsQ0FDakMsWUFBbUM7SUFFbkMsTUFBTSxhQUFhLEdBQUcsV0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWxELE9BQU8sS0FBSyxVQUFVLE9BQU8sQ0FDekIsR0FBbUIsRUFDbkIsR0FBYSxFQUNiLElBQWtCO1FBRWxCLElBQUk7WUFDQSxNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsWUFBWSxDQUFDO1lBQzlCLE1BQU0sS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQztZQUN6QyxNQUFNLFFBQVEsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsS0FBSyxNQUFNLENBQUM7WUFFL0MsTUFBTSxNQUFNLEdBQUcsTUFBTSxJQUFBLGdCQUFNLEVBQUM7Z0JBQ3hCLEdBQUc7Z0JBQ0gsWUFBWTtnQkFDWixJQUFJO2dCQUNKLEtBQUssRUFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7Z0JBQzlCLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSTtnQkFDZCxLQUFLO2dCQUNMLFFBQVE7YUFDWCxDQUFDLENBQUM7WUFFSCxJQUFJLE9BQU8sR0FDUCxhQUFhLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQztZQUVoRSxJQUFJLEtBQUs7Z0JBQ0wsT0FBTztvQkFDSCxhQUFhLENBQUMsZUFBZSxDQUFDLE1BQU07eUJBQy9CLDJCQUEyQixDQUFDO1lBQ3pDLElBQUksUUFBUTtnQkFDUixPQUFPO29CQUNILGFBQWEsQ0FBQyxlQUFlLENBQUMsTUFBTTt5QkFDL0IsMEJBQTBCLENBQUM7WUFFeEMsT0FBTyxHQUFHLENBQUMsTUFBTSxDQUFDLHFCQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7U0FDOUQ7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNaLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3RCO0lBQ0wsQ0FBQyxDQUFDO0FBQ04sQ0FBQztBQTFDRCxnQ0EwQ0MifQ==