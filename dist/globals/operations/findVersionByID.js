"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sanitizeInternalFields_1 = __importDefault(require("../../utilities/sanitizeInternalFields"));
const errors_1 = require("../../errors");
const executeAccess_1 = __importDefault(require("../../auth/executeAccess"));
const types_1 = require("../../auth/types");
const afterRead_1 = require("../../fields/hooks/afterRead");
async function findVersionByID(args) {
    const { depth, globalConfig, id, req, req: { payload, locale }, disableErrors, currentDepth, overrideAccess, showHiddenFields } = args;
    const adminUILocale = payload.config.admin.locale;
    const VersionsModel = payload.versions[globalConfig.slug];
    // /////////////////////////////////////
    // Access
    // /////////////////////////////////////
    const accessResults = !overrideAccess
        ? await (0, executeAccess_1.default)({ req, disableErrors, id }, globalConfig.access.readVersions)
        : true;
    // If errors are disabled, and access returns false, return null
    if (accessResults === false)
        return null;
    const hasWhereAccess = typeof accessResults === 'object';
    const queryToBuild = {
        where: {
            and: [
                {
                    _id: {
                        equals: id
                    }
                }
            ]
        }
    };
    if ((0, types_1.hasWhereAccessResult)(accessResults)) {
        queryToBuild.where.and.push(accessResults);
    }
    const query = await VersionsModel.buildQuery(queryToBuild, locale);
    // /////////////////////////////////////
    // Find by ID
    // /////////////////////////////////////
    if (!query.$and[0]._id)
        throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
    let result = await VersionsModel.findOne(query, {}).lean();
    if (!result) {
        if (!disableErrors) {
            if (!hasWhereAccess)
                throw new errors_1.NotFound(adminUILocale.errors.NotFoundLabel);
            if (hasWhereAccess)
                throw new errors_1.Forbidden(adminUILocale.errors.ForbiddenLabel);
        }
        return null;
    }
    // Clone the result - it may have come back memoized
    result = JSON.parse(JSON.stringify(result));
    result = (0, sanitizeInternalFields_1.default)(result);
    // /////////////////////////////////////
    // beforeRead - Collection
    // /////////////////////////////////////
    await globalConfig.hooks.beforeRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result.version =
            (await hook({
                req,
                doc: result.version
            })) || result.version;
    }, Promise.resolve());
    // /////////////////////////////////////
    // afterRead - Fields
    // /////////////////////////////////////
    result.version = await (0, afterRead_1.afterRead)({
        currentDepth,
        depth,
        doc: result.version,
        entityConfig: globalConfig,
        req,
        overrideAccess,
        showHiddenFields
    });
    // /////////////////////////////////////
    // afterRead - Global
    // /////////////////////////////////////
    await globalConfig.hooks.afterRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result.version =
            (await hook({
                req,
                query,
                doc: result.version
            })) || result.version;
    }, Promise.resolve());
    // /////////////////////////////////////
    // Return results
    // /////////////////////////////////////
    return result;
}
exports.default = findVersionByID;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmluZFZlcnNpb25CeUlELmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2dsb2JhbHMvb3BlcmF0aW9ucy9maW5kVmVyc2lvbkJ5SUQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFFQSxvR0FBNEU7QUFDNUUseUNBQW1EO0FBQ25ELDZFQUFxRDtBQUVyRCw0Q0FBd0Q7QUFHeEQsNERBQXlEO0FBYXpELEtBQUssVUFBVSxlQUFlLENBQzFCLElBQWU7SUFFZixNQUFNLEVBQ0YsS0FBSyxFQUNMLFlBQVksRUFDWixFQUFFLEVBQ0YsR0FBRyxFQUNILEdBQUcsRUFBRSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFDeEIsYUFBYSxFQUNiLFlBQVksRUFDWixjQUFjLEVBQ2QsZ0JBQWdCLEVBQ25CLEdBQUcsSUFBSSxDQUFDO0lBRVQsTUFBTSxhQUFhLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWxELE1BQU0sYUFBYSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRTFELHdDQUF3QztJQUN4QyxTQUFTO0lBQ1Qsd0NBQXdDO0lBRXhDLE1BQU0sYUFBYSxHQUFHLENBQUMsY0FBYztRQUNqQyxDQUFDLENBQUMsTUFBTSxJQUFBLHVCQUFhLEVBQ2YsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLEVBQUUsRUFBRSxFQUMxQixZQUFZLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FDbkM7UUFDSCxDQUFDLENBQUMsSUFBSSxDQUFDO0lBRVgsZ0VBQWdFO0lBQ2hFLElBQUksYUFBYSxLQUFLLEtBQUs7UUFBRSxPQUFPLElBQUksQ0FBQztJQUV6QyxNQUFNLGNBQWMsR0FBRyxPQUFPLGFBQWEsS0FBSyxRQUFRLENBQUM7SUFFekQsTUFBTSxZQUFZLEdBQXFCO1FBQ25DLEtBQUssRUFBRTtZQUNILEdBQUcsRUFBRTtnQkFDRDtvQkFDSSxHQUFHLEVBQUU7d0JBQ0QsTUFBTSxFQUFFLEVBQUU7cUJBQ2I7aUJBQ0o7YUFDSjtTQUNKO0tBQ0osQ0FBQztJQUVGLElBQUksSUFBQSw0QkFBb0IsRUFBQyxhQUFhLENBQUMsRUFBRTtRQUNwQyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7S0FDM0Q7SUFFRCxNQUFNLEtBQUssR0FBRyxNQUFNLGFBQWEsQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBRW5FLHdDQUF3QztJQUN4QyxhQUFhO0lBQ2Isd0NBQXdDO0lBRXhDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUc7UUFDbEIsTUFBTSxJQUFJLGlCQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUUzRCxJQUFJLE1BQU0sR0FBRyxNQUFNLGFBQWEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBRTNELElBQUksQ0FBQyxNQUFNLEVBQUU7UUFDVCxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxjQUFjO2dCQUNmLE1BQU0sSUFBSSxpQkFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDM0QsSUFBSSxjQUFjO2dCQUNkLE1BQU0sSUFBSSxrQkFBUyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDaEU7UUFFRCxPQUFPLElBQUksQ0FBQztLQUNmO0lBRUQsb0RBQW9EO0lBQ3BELE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUU1QyxNQUFNLEdBQUcsSUFBQSxnQ0FBc0IsRUFBQyxNQUFNLENBQUMsQ0FBQztJQUV4Qyx3Q0FBd0M7SUFDeEMsMEJBQTBCO0lBQzFCLHdDQUF3QztJQUV4QyxNQUFNLFlBQVksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFO1FBQ2pFLE1BQU0sU0FBUyxDQUFDO1FBRWhCLE1BQU0sQ0FBQyxPQUFPO1lBQ1YsQ0FBQyxNQUFNLElBQUksQ0FBQztnQkFDUixHQUFHO2dCQUNILEdBQUcsRUFBRSxNQUFNLENBQUMsT0FBTzthQUN0QixDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQzlCLENBQUMsRUFBRSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV0Qix3Q0FBd0M7SUFDeEMscUJBQXFCO0lBQ3JCLHdDQUF3QztJQUV4QyxNQUFNLENBQUMsT0FBTyxHQUFHLE1BQU0sSUFBQSxxQkFBUyxFQUFDO1FBQzdCLFlBQVk7UUFDWixLQUFLO1FBQ0wsR0FBRyxFQUFFLE1BQU0sQ0FBQyxPQUFPO1FBQ25CLFlBQVksRUFBRSxZQUFZO1FBQzFCLEdBQUc7UUFDSCxjQUFjO1FBQ2QsZ0JBQWdCO0tBQ25CLENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4QyxxQkFBcUI7SUFDckIsd0NBQXdDO0lBRXhDLE1BQU0sWUFBWSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDaEUsTUFBTSxTQUFTLENBQUM7UUFFaEIsTUFBTSxDQUFDLE9BQU87WUFDVixDQUFDLE1BQU0sSUFBSSxDQUFDO2dCQUNSLEdBQUc7Z0JBQ0gsS0FBSztnQkFDTCxHQUFHLEVBQUUsTUFBTSxDQUFDLE9BQU87YUFDdEIsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUM5QixDQUFDLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFFdEIsd0NBQXdDO0lBQ3hDLGlCQUFpQjtJQUNqQix3Q0FBd0M7SUFFeEMsT0FBTyxNQUFNLENBQUM7QUFDbEIsQ0FBQztBQUVELGtCQUFlLGVBQWUsQ0FBQyJ9