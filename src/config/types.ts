import { Express, NextFunction, Response } from 'express';
import { DeepPartial, DeepRequired } from 'ts-essentials';
import { Transporter } from 'nodemailer';
import { Options } from 'express-fileupload';
import { Configuration } from 'webpack';
import SMTPConnection from 'nodemailer/lib/smtp-connection';
import GraphQL from 'graphql';
import { ConnectOptions } from 'mongoose';
import React from 'react';
import { LoggerOptions } from 'pino';
import { Payload } from '..';
import {
    AfterErrorHook,
    CollectionConfig,
    SanitizedCollectionConfig
} from '../collections/config/types';
import { GlobalConfig, SanitizedGlobalConfig } from '../globals/config/types';
import { PayloadRequest } from '../express/types';
import { Where } from '../types';
import { User } from '../auth/types';
import { Locale } from 'date-fns';
import { StringOptions } from 'sass';

type Email = {
    fromName: string;
    fromAddress: string;
    logMockCredentials?: boolean;
};

// eslint-disable-next-line no-use-before-define
export type Plugin = (config: Config) => Config;

type GeneratePreviewURLOptions = {
    locale: string;
    token: string;
};

export type GeneratePreviewURL = (
    doc: Record<string, unknown>,
    options: GeneratePreviewURLOptions
) => Promise<string> | string;

export type EmailTransport = Email & {
    transport: Transporter;
    transportOptions?: SMTPConnection.Options;
};

export type EmailTransportOptions = Email & {
    transport?: Transporter;
    transportOptions: SMTPConnection.Options;
};

export type EmailOptions = EmailTransport | EmailTransportOptions | Email;

/**
 * type guard for EmailOptions
 * @param emailConfig
 */
export function hasTransport(
    emailConfig: EmailOptions
): emailConfig is EmailTransport {
    return (emailConfig as EmailTransport).transport !== undefined;
}

/**
 * type guard for EmailOptions
 * @param emailConfig
 */
export function hasTransportOptions(
    emailConfig: EmailOptions
): emailConfig is EmailTransportOptions {
    return (
        (emailConfig as EmailTransportOptions).transportOptions !== undefined
    );
}

export type InitOptions = {
    express?: Express;
    mongoURL: string | false;
    mongoOptions?: ConnectOptions;
    secret: string;
    email?: EmailOptions;
    local?: boolean;
    onInit?: (payload: Payload) => Promise<void> | void;
    /** Pino LoggerOptions */
    loggerOptions?: LoggerOptions;
};

export type AccessResult = boolean | Where;

/**
 * Access function
 */
export type Access = (args?: any) => AccessResult | Promise<AccessResult>;

export interface PayloadHandler {
    (req: PayloadRequest, res: Response, next: NextFunction): void;
}

export type Endpoint = {
    path: string;
    method:
        | 'get'
        | 'head'
        | 'post'
        | 'put'
        | 'patch'
        | 'delete'
        | 'connect'
        | 'options'
        | string;
    handler: PayloadHandler | PayloadHandler[];
    root?: boolean;
};

export type AdminView = React.ComponentType<{
    user: User;
    canAccessAdmin: boolean;
}>;

export type AdminRoute = {
    Component: AdminView;
    path: string;
    exact?: boolean;
    strict?: boolean;
    sensitive?: boolean;
};

export type LocalizationConfig = {
    locales: string[];
    defaultLocale: string;
    fallback?: boolean;
};

export type AdminUILocale = {
    elements: {
        ArrayAction: {
            MoveUpLabel: string;
            MoveDownLabel: string;
            AddBelowLabel: string;
            DuplicateLabel: string;
            RemoveLabel: string;
        };
        Autosave: {
            SavingLabel: string;
            SavingError: string;
            LastSavedLabels: [description1: string, description2: string];
        };
        Collapsible: {
            ToggleBlockLabel: string;
        };
        CopyToClipboard: {
            CopyLabel: string;
            CopiedLabel: string;
        };
        DatePicker: {
            TimeCaptionLabel: string;
            PlaceholderLabel: string;
        };
        DeleteDocument: {
            DeletingError: [description1: string, description2: string];
            SuccessfullyDeletedLabel: string;
            DeletingLabel: string;
            ConfirmLabel: string;
            DeleteLabel: string;
            ConfirmDeletionLabels: [
                title: string,
                description1: string,
                description2: string
            ];
            CancelLabel: string;
        };
        DuplicateDocument: {
            SuccessfullyDuplicatedLabel: string;
            DuplicateLabel: string;
            ConfirmDuplicateLabels: [title: string, description: string];
            CancelLabel: string;
            DuplicateWithoutSavingLabel: string;
        };
        FilesDetails: {
            CopyURLLabel: string;
            MoreInfoLabel: string;
            LessInfoLabel: string;
        };
        GenerateConfirmation: {
            GenerateAPIKeySuccess: string;
            GenerateAPIKeyLabel: string;
            ConfirmGenerationLabels: [
                title: string,
                description1: string,
                description2: string,
                description3: string
            ];
            CancelLabel: string;
            GenerateLabel: string;
        };
        ListControls: {
            ColumnsLabel: string;
            FiltersLabel: string;
            SortLabel: string;
        };
        NavGroup: {
            Collections: string;
            Globals: string;
        };
        PerPage: {
            PerPageLabel: string;
        };
        PreviewButton: {
            PreviewLabel: string;
        };
        Publish: {
            PublishChangesLabel: string;
        };
        ReactSelect: {
            DefaultPlaceholderLabel: string;
            NoOptionsMessageLabel: string;
        };
        RenderTitle: {
            UntitledLabel: string;
        };
        SaveDraft: {
            SaveDraftLabel: string;
        };
        SearchFilter: {
            SearchLabel: string;
        };
        SortComplex: {
            AscendingLabel: string;
            DescendingLabel: string;
            ColumnLabel: string;
            OrderLabel: string;
        };
        Status: {
            StatusChangedLabel: string;
            StatusDraftLabel: string;
            StatusPublishedLabel: string;
            UnpublishingError: string;
            UnpublishLabel: string;
            ConfirmUnpublishLabels: [title: string, description: string];
            CancelLabel: string;
            UnpublishingLabel: string;
            ConfirmLabel: string;
            RevertLabel: string;
            ConfirmRevertLabels: [title: string, description: string];
            RevertingLabel: string;
        };
        StepNav: {
            DashboardLabel: string;
        };
        VersionsCount: {
            NoVersionsFoundLabel: string;
            VersionsFoundLabel: string;
        };
        WhereBuilder: {
            fieldTypes: {
                equals: string;
                not_equals: string;
                in: string;
                not_in: string;
                exists: string;
                greater_than: string;
                less_than: string;
                less_than_equal: string;
                greater_than_equals: string;
                near: string;
                like: string;
                contains: string;
            };
            RelationshipLoadingError: string;
            RelationshipIDLoadingError: string;
            RelationshipSelectLabel: string;
            RelationshipNoneLabel: string;

            TextInputLabel: string;

            FilterWhereLabels: [description1: string, description2: string];
            OrLabel: string;
            AndLabel: string;
            NoFiltersLabel: string;
            AddFilterLabel: string;
        };
    };
    forms: {
        Error: {
            ErrorMessageLabel: string;
        };
        Form: {
            SubmitError: string;
            SubmitSuccess: string;
            UnknownError: string;
        };
        RenderFields: {
            NoMatchedFieldLabel: string;
        };
        FieldTypes: {
            Array: {
                CollapseAllLabel: string;
                ShowAllLabel: string;
                FieldRequiersLabel: string;
                FieldHasNoLabel: string;
                AddLabel: string;
            };
            Blocks: {
                FieldRequiersLabel: string;
                FieldHasNoLabel: string;
                AddLabel: string;

                SearchBlockLabel: string;

                UntitledLabel: string;
            };
            ConfirmPassword: {
                RequiredFieldLabel: string;
                PasswordsNotMatchLabel: string;
                ConfirmPasswordLabel: string;
            };
            Point: {
                LongitudeLabel: string;
                LatitudeLabel: string;
            };
            Relationship: {
                NoneLabel: string;
                ErrorLabel: string;
                UntitledLabel: string;
            };
            Upload: {
                UploadNewLabel: string;
                ChooseFromExistingLabel: string;

                NewLabel: string;
                SaveLabel: string;

                SelectExistingLabel: string;
                OfLabel: string;
            };
            RichText: {
                EditLinkLabel: string;
                ConfirmLabel: string;

                TextToDisplayLabel: string;
                LinkTypeLabel: string;
                LinkTypeDescriptionLabel: string;
                CustomURLLabel: string;
                InternalLinkLabel: string;
                EnterURLLabel: string;
                ChooseDocumentLabel: string;
                OpenInNewTabLabel: string;

                LinkedToLabel: string;
                LinkEditTooltipLabel: string;
                LinkRemoveTooltipLabel: string;

                RelationshipLabel: string;
                AddRelationshipLabel: string;
                AddRelationshipButtonLabel: string;
                RelationToLabel: string;
                RelatedDocumentLabel: string;

                UploadAddLabel: string;

                UploadEditTooltipLabel: string;
                UploadSwapTooltipLabel: string;
                UploadRemoveTooltipLabel: string;

                UploadSwapLabel: string;

                SelectLabel: string;
                OfLabel: string;
            };
        };
    };
    modals: {
        LeaveWithoutSaving: {
            LeaveWithoutSavingLabels: [title: string, description: string];
            StayOnThisPageLabel: string;
            LeaveAnywayLabel: string;
        };
        StayLoggedIn: {
            StayLoggedInLabels: [title: string, description: string];
            LogOutLabel: string;
            StayLoggedInLabel: string;
        };
    };
    templates: {
        Default: {
            MetaTitleLabel: string;
            MetaDescriptionLabel: string;
            MetaKeywordsLabel: string;
        };
    };
    views: {
        Account: {
            RadioGroupInputLabel: string;
            AutomaticLabel: string;
            LightLabel: string;
            DarkLabel: string;

            AccountTitleLabel: string;
            AccountDescriptionLabel: string;
            AccountKeywordsLabel: string;
            AccountLabel: string;
            UntitledLabel: string;

            PayloadSettingsLabel: string;
            CreateNewLabel: string;
            SaveLabel: string;
            APIURLLabel: string;
            IDLabel: string;
            LastModifiedLabel: string;
            CreatedLabel: string;
        };
        collections: {
            Edit: {
                Auth: {
                    APIKeyLabel: string;
                    UnlockSuccess: string;
                    UnlockError: string;
                    EmailLabel: string;
                    NewPasswordLabel: string;
                    CancelLabel: string;
                    ChangePasswordLabel: string;
                    ForceUnlockLabel: string;
                    EnableAPIKeyLabel: string;
                    VerifiedLabel: string;
                };
                Upload: {
                    RequiredFileLabel: string;
                    SelectFileLabel: string;
                    DragAndDropHereLabel: string;

                    EditingLabel: string;
                    CreatingLabel: string;
                    UntitledLabel: string;
                    CreateNewLabel: string;
                    SaveLabel: string;
                    APIURLLabel: string;
                    VersionsLabel: string;
                    LastModifiedLabel: string;
                    CreatedLabel: string;
                };
            };
            List: {
                Cell: {
                    NoFieldLabels: [description1: string, description2: string];

                    fieldTypes: {
                        Array: {
                            RowsLabel: string;
                        };
                        Blocks: {
                            MoreLabels: [
                                description1: string,
                                description2: string
                            ];
                        };
                        Relationship: {
                            UntitledLabel: string;
                            LoadingLabel: string;
                            MoreLabels: [
                                description1: string,
                                description2: string
                            ];
                            NoFieldLabels: [
                                description1: string,
                                description2: string
                            ];
                        };
                    };
                };

                fieldTypes: {
                    updatedAt: string;
                    createdAt: string;
                    filename: string;
                };

                CreateNewLabel: string;
                NoFoundLabels: [
                    description1: string,
                    description2: string,
                    description3: string
                ];
                CreateNewButtonLabel: string;
                OfLabel: string;
            };
        };
        CreateFirstUser: {
            EmailAddressLabel: string;
            PasswordLabel: string;
            ConfirmPasswordLabel: string;
            WelcomeLabel: string;
            CreateFirstUserLabel: string;
            CreateLabel: string;

            MetaTitleLabel: string;
            MetaDescriptionLabel: string;
            MetaKeywordsLabel: string;
        };
        Dashboard: {
            Collections: string;
            Globals: string;
        };
        ForgotPassword: {
            ErrorLabel: string;

            MetaTitleLabel: string;
            MetaDescriptionLabel: string;
            MetaKeywordsLabel: string;

            ForgotPasswordLabels: [
                title: string,
                description1: string,
                description2: string,
                description3: string
            ];
            BackToDashboardLabel: string;

            EmailSentLabel: string;
            CheckEmailLabel: string;

            ForgotPasswordLabel: string;
            EnterEmailLabel: string;
            EmailAddressLabel: string;
            SubmitLabel: string;
            BackToLogin: string;
        };
        Global: {
            EditLabel: string;
            SaveLabel: string;
            VersionsLabel: string;
            APIURLLabel: string;
            LastModifiedLabel: string;
        };
        Login: {
            MetaTitleLabel: string;
            MetaDescriptionLabel: string;
            MetaKeywordsLabel: string;

            AlreadyLoggedInLabels: [
                title: string,
                description1: string,
                description2: string,
                description3: string
            ];

            BackToDashboardLabel: string;

            EmailAddressLabel: string;
            PasswordLabel: string;
            ForgotPasswordLabel: string;
            LoginLabel: string;
        };
        Logout: {
            MetaTitleLabel: string;
            MetaDescriptionLabel: string;
            MetaKeywordsLabel: string;

            LoggedOutInactivityLabel: string;
            LoggedOutSuccessfullyLabel: string;
            LogBackInLabel: string;
        };
        NotFound: {
            NotFoundLabel: string;

            MetaTitleLabel: string;
            MetaDescriptionLabel: string;
            MetaKeywordsLabel: string;

            NothingFoundLabels: [title: string, dsecription: string];
            BackToDashboardLabel: string;
        };
        ResetPassword: {
            MetaTitleLabel: string;
            MetaDescriptionLabel: string;
            MetaKeywordsLabel: string;

            AlreadyLoggedInLabels: [
                title: string,
                description1: string,
                description2: string,
                description3: string
            ];
            BackToDashboardLabel: string;

            ResetPasswordLabel: string;
            NewPasswordLabel: string;
            ResetPasswordButtonLabel: string;
        };
        Unauthorized: {
            MetaTitleLabel: string;
            MetaDescriptionLabel: string;
            MetaKeywordsLabel: string;

            UnauthorizedLabel: string;
            NotAllowedLabel: string;
            LogOutLabel: string;
        };
        Verify: {
            VerifiedSuccessfullyLabel: string;
            AlreadyActivatedLabel: string;
            UnableToVerifyLabel: string;

            MetaTitleLabel: string;
            MetaDescriptionLabel: string;
            MetaKeywordsLabel: string;

            LoginLabel: string;
        };
        Version: {
            LoadingError: string;
            CompareVersionLabel: string;
            SelectVersionLabel: string;
        };
        Versions: {
            UpdatedAtLabel: string;
            VersionIDLabel: string;
            TypeLabel: string;
            AutosaveLabel: string;
            PublishedLabel: string;
            DraftLabel: string;

            UntitledLabel: string;
            VersionsLabel: string;

            ViewingVersionsCollectionsLabel: string;
            ViewingVersionsGlobalsLabel: string;

            ShowingVersionsLabel: string;
            CurrentStatusLabels: [description1: string, description2: string];
            EditLabel: string;
            OfLabel: string;
            NoVersionsFoundLabel: string;
        };
    };
    errors: {
        AuthenticationErrorLabel: string;
        ErrorDeletingFileLabel: string;
        FileUploadErrorLabel: string;
        ForbiddenLabel: string;
        NotFoundLabel: string;
        LockedAuthLabel: string;
        MissingFileLabel: string;
        UnauthorizedErrorLabel: string;
    };
    operations: {
        ForgotPassword: {
            MissingEmailLabel: string;
            HTMLLabels: [description1: string, description2: string];
            ResetPasswordLabel: string;
        };
        RegisterFirstUser: {
            RegisteredSuccessfullyLabel: string;
        };
        SendVerificationEmail: {
            HTMLLabels: [
                description1: string,
                description2: string,
                description3: string,
                description4: string,
                description5: string
            ];
            SubjectLabel: string;
        };
        FindVersionByID: {
            MissingIDLabel: string;
        };
        RestoreVersion: {
            MissingIDLabel: string;
        };
        Update: {
            MissingIDLabel: string;
            ValueMustBeUniqueLabel: string;
        };
    };
    requestHandlers: {
        Create: {
            SuccessfullyCreatedLabel: string;
        };
        Update: {
            SavedSuccessfullyLabel: string;
            UpdatedSuccessfullyLabel: string;
            DraftSavedSuccessfullyLabel: string;
            AutosavedSuccessfullyLabel: string;
        };
    };
    fields: {
        validations: {
            DefaultMessageLabel: string;

            EnterValidNumberLabel: string;
            NumberGreaterLabel: string;
            NumberLessLabel: string;

            TextShorterLabels: [description1: string, description2: string];
            TextLongerLabels: [description1: string, description2: string];

            PasswordShorterLabels: [description1: string, description2: string];
            PasswordLongerLabels: [description1: string, description2: string];

            EnterValidEmailLabel: string;

            TextareaShorterLabels: [description1: string, description2: string];
            TextareaLongerLabels: [description1: string, description2: string];

            InvalidCheckboxLabel: string;

            InvalidDateLabel: string;

            InvalidSelectionsLabel: string;

            InvalidUploadLabel: string;

            ArrayRequiresAtLeastLabels: [
                description1: string,
                description2: string
            ];
            ArrayRequiresNoMoreLabels: [
                description1: string,
                description2: string
            ];
            ArrayRequiresOneRowLabel: string;

            InvalidSelectionLabel: string;

            BlockRequiresAtLeastLabels: [
                description1: string,
                description2: string
            ];
            BlockRequiresNoMoreLabels: [
                description1: string,
                description2: string
            ];
            BlockRequiresOneRowLabel: string;

            PointRequiresTwoNumbersLabel: string;
            PointInvalidInputLabel: string;
        };
    };
};

export type DatePickerFormat = {
    defaultTimeFormat: string;
    timeOnlyFormat: string;
    monthOnlyFormat: string;
    elseFormat: string;
};

export type Config = {
    admin?: {
        user?: string;
        meta?: {
            titleSuffix?: string;
            ogImage?: string;
            favicon?: string;
        };
        locale?: AdminUILocale;
        disable?: boolean;
        indexHTML?: string;
        css?: string;
        dateLocale?: Locale;
        dateFormat?: string;
        datePickerFormat: DatePickerFormat;
        avatar?: 'default' | 'gravatar' | React.ComponentType<any>;
        components?: {
            routes?: AdminRoute[];
            providers?: React.ComponentType<{ children: React.ReactNode }>[];
            beforeDashboard?: React.ComponentType<any>[];
            afterDashboard?: React.ComponentType<any>[];
            beforeLogin?: React.ComponentType<any>[];
            afterLogin?: React.ComponentType<any>[];
            beforeNavLinks?: React.ComponentType<any>[];
            afterNavLinks?: React.ComponentType<any>[];
            Nav?: React.ComponentType<any>;
            graphics?: {
                Icon?: React.ComponentType<any>;
                Logo?: React.ComponentType<any>;
            };
            views?: {
                Account?: React.ComponentType<any>;
                Dashboard?: React.ComponentType<any>;
            };
        };
        pagination?: {
            defaultLimit?: number;
            options?: number[];
        };
        webpack?: (config: Configuration) => Configuration;
    };
    collections?: CollectionConfig[];
    endpoints?: Endpoint[];
    globals?: GlobalConfig[];
    serverURL?: string;
    cookiePrefix?: string;
    csrf?: string[];
    cors?: string[] | '*';
    routes?: {
        api?: string;
        admin?: string;
        graphQL?: string;
        graphQLPlayground?: string;
    };
    typescript?: {
        outputFile?: string;
    };
    debug?: boolean;
    express?: {
        json?: {
            limit?: number;
        };
        compression?: {
            [key: string]: unknown;
        };
        /**
         * @deprecated express.middleware will be removed in a future version. Please migrate to express.postMiddleware.
         */
        middleware?: any[];
        preMiddleware?: any[];
        postMiddleware?: any[];
    };
    defaultDepth?: number;
    maxDepth?: number;
    indexSortableFields?: boolean;
    rateLimit?: {
        window?: number;
        max?: number;
        trustProxy?: boolean;
        skip?: (req: PayloadRequest) => boolean;
    };
    upload?: Options;
    localization?: LocalizationConfig | false;
    graphQL?: {
        mutations?: (
            graphQL: typeof GraphQL,
            payload: Payload
        ) => Record<string, unknown>;
        queries?: (
            graphQL: typeof GraphQL,
            payload: Payload
        ) => Record<string, unknown>;
        maxComplexity?: number;
        disablePlaygroundInProduction?: boolean;
        disable?: boolean;
        schemaOutputFile?: string;
    };
    components?: { [key: string]: JSX.Element | (() => JSX.Element) };
    hooks?: {
        afterError?: AfterErrorHook;
    };
    plugins?: Plugin[];
    telemetry?: boolean;
    onInit?: (payload: Payload) => Promise<void> | void;
};

export type SanitizedConfig = Omit<
    DeepRequired<Config>,
    'collections' | 'globals'
> & {
    collections: SanitizedCollectionConfig[];
    globals: SanitizedGlobalConfig[];
    paths: { [key: string]: string };
    admin: {
        locale: DeepPartial<AdminUILocale>;
        dateLocale: Locale;
        datePickerFormat: DatePickerFormat;
    };
};

export type EntityDescription =
    | string
    | (() => string)
    | React.ComponentType<any>;
