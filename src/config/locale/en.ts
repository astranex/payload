import { AdminUILocale } from 'payload/config';

const en: AdminUILocale = {
    elements: {
        ArrayAction: {
            MoveUpLabel: 'Move Up',
            MoveDownLabel: 'Move Down',
            AddBelowLabel: 'Add Below',
            DuplicateLabel: 'Duplicate',
            RemoveLabel: 'Remove'
        },
        Autosave: {
            SavingLabel: 'Saving...',
            SavingError: 'There was a problem while autosaving this document.',
            LastSavedLabels: ['Last saved', 'Ago']
        },
        Collapsible: {
            ToggleBlockLabel: 'Toggle block'
        },
        CopyToClipboard: {
            CopyLabel: 'copy',
            CopiedLabel: 'copied'
        },
        DatePicker: {
            TimeCaptionLabel: 'Time',
            PlaceholderLabel: ''
        },
        DeleteDocument: {
            DeletingError: [
                'There was an error while deleting',
                'Please check your connection and try again.'
            ],
            SuccessfullyDeletedLabel: 'successfully deleted.',
            DeletingLabel: 'Deleting...',
            ConfirmLabel: 'Confirm',
            DeleteLabel: 'Delete',
            ConfirmDeletionLabels: [
                'Confirm deletion',
                'You are about to delete the',
                'Are you sure?'
            ],
            CancelLabel: 'Cancel'
        },
        DuplicateDocument: {
            SuccessfullyDuplicatedLabel: 'successfully duplicated.',
            DuplicateLabel: 'Duplicate',
            ConfirmDuplicateLabels: [
                'Confirm duplicate',
                'You have unsaved changes. Would you like to continue to duplicate?'
            ],
            CancelLabel: 'Cancel',
            DuplicateWithoutSavingLabel: 'Duplicate without saving changes'
        },
        FilesDetails: {
            CopyURLLabel: 'Copy URL',
            MoreInfoLabel: 'More info',
            LessInfoLabel: 'Less info'
        },
        GenerateConfirmation: {
            GenerateAPIKeySuccess: 'New API Key Generated.',
            GenerateAPIKeyLabel: 'string',
            ConfirmGenerationLabels: [
                'Confirm Generation',
                'Generating a new API key will',
                'invalidate',
                'the previous key. Are you sure you wish to continue?'
            ],
            CancelLabel: 'Cancel',
            GenerateLabel: 'Generate'
        },
        ListControls: {
            ColumnsLabel: 'Columns',
            FiltersLabel: 'Filters',
            SortLabel: 'Sort'
        },
        NavGroup: {
            Collections: 'Collections',
            Globals: 'Globals'
        },
        PerPage: {
            PerPageLabel: 'Per Page:'
        },
        PreviewButton: {
            PreviewLabel: 'Preview'
        },
        Publish: {
            PublishChangesLabel: 'Publish changes'
        },
        ReactSelect: {
            DefaultPlaceholderLabel: 'Select...',
            NoOptionsMessageLabel: 'No options'
        },
        RenderTitle: {
            UntitledLabel: '[Untitled]'
        },
        SaveDraft: {
            SaveDraftLabel: 'Save draft'
        },
        SearchFilter: {
            SearchLabel: 'Search by'
        },
        SortComplex: {
            AscendingLabel: 'Ascending',
            DescendingLabel: 'Descending',
            ColumnLabel: 'Column to Sort',
            OrderLabel: 'Order'
        },
        Status: {
            StatusChangedLabel: 'Changed',
            StatusDraftLabel: 'Draft',
            StatusPublishedLabel: 'Published',
            UnpublishingError:
                'There was a problem while un-publishing this document.',
            UnpublishLabel: 'Unpublish',
            ConfirmUnpublishLabels: [
                'Confirm unpublish',
                'You are about to unpublish this document. Are you sure?'
            ],
            CancelLabel: 'Cancel',
            UnpublishingLabel: 'Unpublishing...',
            ConfirmLabel: 'Confirm',
            RevertLabel: 'Revert to published',
            ConfirmRevertLabels: [
                'Confirm revert to saved',
                "You are about to revert this document's changes to its published state. Are you sure?"
            ],
            RevertingLabel: 'Reverting...'
        },
        StepNav: {
            DashboardLabel: 'Dashboard'
        },
        VersionsCount: {
            NoVersionsFoundLabel: 'No versions found',
            VersionsFoundLabel: 'Versions found:'
        },
        WhereBuilder: {
            fieldTypes: {
                equals: 'equals',
                not_equals: 'is not equal to',
                in: 'is in',
                not_in: 'is not in',
                exists: 'exists',
                greater_than: 'is greater than',
                less_than: 'is less than',
                less_than_equal: 'is less than or equal to',
                greater_than_equals: 'is greater than or equal to',
                near: 'near',
                like: 'is like',
                contains: 'contains'
            },

            RelationshipLoadingError: 'An error has occurred.',
            RelationshipIDLoadingError:
                'There was a problem loading the document with ID of',
            RelationshipSelectLabel: 'Select a value',
            RelationshipNoneLabel: 'None',

            TextInputLabel: 'Enter a value',

            FilterWhereLabels: ['Filter', 'where'],
            OrLabel: 'Or',
            AndLabel: 'And',
            NoFiltersLabel: 'No filters set',
            AddFilterLabel: 'Add filter'
        }
    },
    forms: {
        Error: {
            ErrorMessageLabel: 'Please complete this field.'
        },
        Form: {
            SubmitError: 'Please correct invalid fields.',
            SubmitSuccess: 'Submission successful.',
            UnknownError: 'An unknown error occurred.'
        },
        RenderFields: {
            NoMatchedFieldLabel: 'No matched field found for'
        },
        FieldTypes: {
            Array: {
                CollapseAllLabel: 'Collapse All',
                ShowAllLabel: 'Show All',
                FieldRequiersLabel: 'This field requires at least',
                FieldHasNoLabel: 'This field has no',
                AddLabel: 'Add'
            },
            Blocks: {
                FieldRequiersLabel: 'This field requires at least',
                FieldHasNoLabel: 'This field has no',
                AddLabel: 'Add',

                SearchBlockLabel: 'Search for a block',

                UntitledLabel: 'Untitled'
            },
            ConfirmPassword: {
                RequiredFieldLabel: 'This field is required',
                PasswordsNotMatchLabel: 'Passwords do not match.',
                ConfirmPasswordLabel: 'Confirm Password'
            },
            Point: {
                LongitudeLabel: 'Longitude',
                LatitudeLabel: 'Latitude'
            },
            Relationship: {
                NoneLabel: 'None',
                ErrorLabel: 'An error has occurred.',
                UntitledLabel: 'Untitled'
            },
            Upload: {
                UploadNewLabel: 'Upload new',
                ChooseFromExistingLabel: 'Choose from existing',

                NewLabel: 'New',
                SaveLabel: 'Save',

                SelectExistingLabel: 'Select existing',
                OfLabel: 'of'
            },
            RichText: {
                EditLinkLabel: 'Edit Link',
                ConfirmLabel: 'Confirm',

                TextToDisplayLabel: 'Text to display',
                LinkTypeLabel: 'Link Type',
                LinkTypeDescriptionLabel:
                    'Choose between entering a custom text URL or linking to another document.',
                CustomURLLabel: 'Custom URL',
                InternalLinkLabel: 'Internal Link',
                EnterURLLabel: 'Enter a URL',
                ChooseDocumentLabel: 'Choose a document to link to',
                OpenInNewTabLabel: 'Open in new tab',

                LinkedToLabel: 'Linked to',
                LinkEditTooltipLabel: 'Edit',
                LinkRemoveTooltipLabel: 'Remove',

                RelationshipLabel: 'Relationship',
                AddRelationshipLabel: 'Add Relationship',
                AddRelationshipButtonLabel: 'Add relationship',
                RelationToLabel: 'Relation To',
                RelatedDocumentLabel: 'Related Document',

                UploadAddLabel: 'Add',

                UploadEditTooltipLabel: 'Edit',
                UploadSwapTooltipLabel: 'Swap Upload',
                UploadRemoveTooltipLabel: 'Remove Upload',

                UploadSwapLabel: 'Choose',

                SelectLabel: 'Select a Collection to Browse',
                OfLabel: 'of'
            }
        }
    },
    modals: {
        LeaveWithoutSaving: {
            LeaveWithoutSavingLabels: [
                'Leave without saving',
                'Your changes have not been saved. If you leave now, you will lose your changes.'
            ],
            StayOnThisPageLabel: 'Stay on this page',
            LeaveAnywayLabel: 'Leave anyway'
        },
        StayLoggedIn: {
            StayLoggedInLabels: [
                'Stay logged in',
                "You haven't been active in a little while and will shortly be automatically logged out for your own security. Would you like to stay logged in?"
            ],
            LogOutLabel: 'Log out',
            StayLoggedInLabel: 'Stay logged in'
        }
    },
    templates: {
        Default: {
            MetaTitleLabel: 'Dashboard',
            MetaDescriptionLabel: 'Dashboard for Payload CMS',
            MetaKeywordsLabel: 'Dashboard, Payload, CMS'
        }
    },
    views: {
        Account: {
            RadioGroupInputLabel: 'Admin Theme',
            AutomaticLabel: 'Automatic',
            LightLabel: 'Light',
            DarkLabel: 'Dark',

            AccountTitleLabel: 'Account',
            AccountDescriptionLabel: 'Account of current user',
            AccountKeywordsLabel: 'Account, Dashboard, Payload, CMS',
            AccountLabel: 'Account',
            UntitledLabel: '[Untitled]',

            PayloadSettingsLabel: 'Payload Settings',
            CreateNewLabel: 'Create New',
            SaveLabel: 'Save',
            APIURLLabel: 'API URL',
            IDLabel: 'ID',
            LastModifiedLabel: 'Last Modified',
            CreatedLabel: 'Created'
        },
        collections: {
            Edit: {
                Auth: {
                    APIKeyLabel: 'API Key',
                    UnlockSuccess: 'Successfully unlocked',
                    UnlockError: 'Successfully unlocked',
                    EmailLabel: 'Email',
                    NewPasswordLabel: 'New Password',
                    CancelLabel: 'Cancel',
                    ChangePasswordLabel: 'Change Password',
                    ForceUnlockLabel: 'Force Unlock',
                    EnableAPIKeyLabel: 'Enable API Key',
                    VerifiedLabel: 'Verified'
                },
                Upload: {
                    RequiredFileLabel: 'A file is required.',
                    SelectFileLabel: 'Select a file',
                    DragAndDropHereLabel: 'or drag and drop a file here',

                    EditingLabel: 'Editing',
                    CreatingLabel: 'Creating',
                    UntitledLabel: '[Untitled]',
                    CreateNewLabel: 'Create New',
                    SaveLabel: 'Save',
                    APIURLLabel: 'API URL',
                    VersionsLabel: 'Versions',
                    LastModifiedLabel: 'Last Modified',
                    CreatedLabel: 'Created'
                }
            },
            List: {
                Cell: {
                    NoFieldLabels: ['<No ', '>'],

                    fieldTypes: {
                        Array: {
                            RowsLabel: 'Rows'
                        },
                        Blocks: {
                            MoreLabels: ['and', 'more']
                        },
                        Relationship: {
                            UntitledLabel: 'Untitled',
                            LoadingLabel: 'Loading...',
                            MoreLabels: ['and', 'more'],
                            NoFieldLabels: ['No <', '>']
                        }
                    }
                },

                fieldTypes: {
                    updatedAt: 'Updated At',
                    createdAt: 'Created At',
                    filename: 'Filename'
                },

                CreateNewLabel: 'Create New',
                NoFoundLabels: [
                    'No',
                    'found. Either no',
                    "exist yet or none match the filters you've specified above."
                ],
                CreateNewButtonLabel: 'Create new',
                OfLabel: 'of'
            }
        },
        CreateFirstUser: {
            EmailAddressLabel: 'Email Address',
            PasswordLabel: 'Password',
            ConfirmPasswordLabel: 'Confirm Password',
            WelcomeLabel: 'Welcome',
            CreateFirstUserLabel: 'To begin, create your first user.',
            CreateLabel: 'Create',

            MetaTitleLabel: 'Create First User',
            MetaDescriptionLabel: 'Create first user',
            MetaKeywordsLabel: 'Create, Payload, CMS'
        },
        Dashboard: {
            Collections: 'Collections',
            Globals: 'Globals'
        },
        ForgotPassword: {
            ErrorLabel: 'The email provided is not valid.',

            MetaTitleLabel: 'Forgot Password',
            MetaDescriptionLabel: 'Forgot password',
            MetaKeywordsLabel: 'Forgot, Password, Payload, CMS',

            ForgotPasswordLabels: [
                "You're already logged in",
                'To change your password, go to your',
                'account',
                'and edit your password there.'
            ],
            BackToDashboardLabel: 'Back to Dashboard',

            EmailSentLabel: 'Email sent',
            CheckEmailLabel:
                'Check your email for a link that will allow you to securely reset your password.',

            ForgotPasswordLabel: 'Forgot Password',
            EnterEmailLabel:
                'Please enter your email below. You will receive an email message with instructions on how to reset your password.',
            EmailAddressLabel: 'Email Address',
            SubmitLabel: 'Submit',
            BackToLogin: 'Back to login'
        },
        Global: {
            EditLabel: 'Edit ',
            SaveLabel: 'Save',
            VersionsLabel: 'Versions',
            APIURLLabel: 'API URL',
            LastModifiedLabel: 'Last Modified'
        },
        Login: {
            MetaTitleLabel: 'Login',
            MetaDescriptionLabel: 'Login user',
            MetaKeywordsLabel: 'Login, Payload, CMS',

            AlreadyLoggedInLabels: [
                'Already logged in',
                'To log in with another user, you should',
                'log out',
                'first.'
            ],

            BackToDashboardLabel: 'Back to Dashboard',

            EmailAddressLabel: 'Email Address',
            PasswordLabel: 'Password',
            ForgotPasswordLabel: 'Forgot password?',
            LoginLabel: 'Login'
        },
        Logout: {
            MetaTitleLabel: 'Logout',
            MetaDescriptionLabel: 'Logout user',
            MetaKeywordsLabel: 'Logout, Payload, CMS',

            LoggedOutInactivityLabel:
                'You have been logged out due to inactivity.',
            LoggedOutSuccessfullyLabel:
                'You have been logged out successfully.',
            LogBackInLabel: 'Log back in'
        },
        NotFound: {
            NotFoundLabel: 'Not Found',

            MetaTitleLabel: 'Not Found',
            MetaDescriptionLabel: 'Page not found',
            MetaKeywordsLabel: '404, Not found, Payload, CMS',

            NothingFoundLabels: [
                'Nothing found',
                'Sorry–there is nothing to correspond with your request.'
            ],
            BackToDashboardLabel: 'Back to Dashboard'
        },
        ResetPassword: {
            MetaTitleLabel: 'Reset Password',
            MetaDescriptionLabel: 'Reset password',
            MetaKeywordsLabel: 'Reset Password, Payload, CMS',

            AlreadyLoggedInLabels: [
                'Already logged in',
                'To log in with another user, you should',
                'log out',
                'first.'
            ],

            BackToDashboardLabel: 'Back to Dashboard',

            ResetPasswordLabel: 'Reset Password',
            NewPasswordLabel: 'New Password',
            ResetPasswordButtonLabel: 'Reset Password'
        },
        Unauthorized: {
            MetaTitleLabel: 'Unauthorized',
            MetaDescriptionLabel: 'Unauthorized',
            MetaKeywordsLabel: 'Unauthorized, Payload, CMS',

            UnauthorizedLabel: 'Unauthorized',
            NotAllowedLabel: 'You are not allowed to access this page.',
            LogOutLabel: 'Log out'
        },
        Verify: {
            VerifiedSuccessfullyLabel: 'Verified Successfully',
            AlreadyActivatedLabel: 'Already Activated',
            UnableToVerifyLabel: 'Unable To Verify',

            MetaTitleLabel: 'Verify',
            MetaDescriptionLabel: 'Verify user',
            MetaKeywordsLabel: 'Verify, Payload, CMS',

            LoginLabel: 'Login'
        },
        Version: {
            LoadingError: 'An error has occurred.',
            CompareVersionLabel: 'Compare version against:',
            SelectVersionLabel: 'Select a version to compare'
        },
        Versions: {
            UpdatedAtLabel: 'Updated At',
            VersionIDLabel: 'Version ID',
            TypeLabel: 'Type',
            AutosaveLabel: 'Autosave',
            PublishedLabel: 'Published',
            DraftLabel: 'Draft',

            UntitledLabel: '[Untitled]',
            VersionsLabel: 'Versions',

            ViewingVersionsCollectionsLabel: 'Viewing versions for the',
            ViewingVersionsGlobalsLabel: 'Viewing versions for the global',

            ShowingVersionsLabel: 'Showing versions for:',
            CurrentStatusLabels: ['Current', 'document -'],
            EditLabel: 'Edit',
            OfLabel: 'of',
            NoVersionsFoundLabel: 'No further versions found'
        }
    },
    errors: {
        AuthenticationErrorLabel:
            'The email or password provided is incorrect.',
        ErrorDeletingFileLabel: 'There was an error deleting file.',
        FileUploadErrorLabel: 'There was a problem while uploading the file.',
        ForbiddenLabel: 'You are not allowed to perform this action.',
        NotFoundLabel: 'There was an error deleting file.',
        LockedAuthLabel:
            'This user is locked due to having too many failed login attempts.',
        MissingFileLabel: 'No files were uploaded.',
        UnauthorizedErrorLabel:
            'Unauthorized, you must be logged in to make this request.'
    },
    operations: {
        ForgotPassword: {
            MissingEmailLabel: 'Missing email.',
            HTMLLabels: [
                'You are receiving this because you (or someone else) have requested the reset of the password for your account. Please click on the following link, or paste this into your browser to complete the process:',
                'If you did not request this, please ignore this email and your password will remain unchanged.'
            ],
            ResetPasswordLabel: 'Reset your password'
        },
        RegisterFirstUser: {
            RegisteredSuccessfullyLabel:
                'Registered and logged in successfully. Welcome!'
        },
        SendVerificationEmail: {
            HTMLLabels: [
                'A new account has just been created for you to access <a href="',
                '">',
                '</a>. Please click on the following link or paste the URL below into your browser to verify your email: <a href="',
                '">',
                '</a><br>After verifying your email, you will be able to log in successfully.'
            ],
            SubjectLabel: 'Verify your email'
        },
        FindVersionByID: {
            MissingIDLabel: 'Missing ID of version.'
        },
        RestoreVersion: {
            MissingIDLabel: 'Missing ID of version to restore.'
        },
        Update: {
            MissingIDLabel: 'Missing ID of document to update.',
            ValueMustBeUniqueLabel: 'Value must be unique'
        }
    },
    requestHandlers: {
        Create: {
            SuccessfullyCreatedLabel: 'successfully created.'
        },
        Update: {
            SavedSuccessfullyLabel: 'Saved successfully.',
            UpdatedSuccessfullyLabel: 'Updated successfully.',
            DraftSavedSuccessfullyLabel: 'Draft saved successfully.',
            AutosavedSuccessfullyLabel: 'Autosaved successfully.'
        }
    },
    fields: {
            validations: {
                DefaultMessageLabel: 'This field is required.',

                EnterValidNumberLabel: 'Please enter a valid number.',
                NumberGreaterLabel: 'is greater than the max allowed value of',
                NumberLessLabel: 'is less than the min allowed value of',

                TextShorterLabels: [
                    'This value must be shorter than the max length of',
                    'characters.'
                ],
                TextLongerLabels: [
                    'This value must be longer than the minimum length of',
                    'characters.'
                ],

                PasswordShorterLabels: [
                    'This value must be shorter than the max length of',
                    'characters.'
                ],
                PasswordLongerLabels: [
                    'This value must be longer than the minimum length of',
                    'characters.'
                ],

                EnterValidEmailLabel: 'Please enter a valid email address.',

                TextareaShorterLabels: [
                    'This value must be shorter than the max length of',
                    'characters.'
                ],
                TextareaLongerLabels: [
                    'This value must be longer than the minimum length of',
                    'characters.'
                ],

                InvalidCheckboxLabel: 'This field can only be equal to true or false.',

                InvalidDateLabel: 'is not a valid date.',

                InvalidSelectionsLabel: 'This field has the following invalid selections:',

                InvalidUploadLabel: 'This field is not a valid upload ID',

                ArrayRequiresAtLeastLabels: [
                    'This field requires at least',
                    'row(s).'
                ],
                ArrayRequiresNoMoreLabels: [
                    'This field requires no more than',
                    'row(s).'
                ],
                ArrayRequiresOneRowLabel: 'This field requires at least one row.',

                InvalidSelectionLabel: 'This field has an invalid selection',

                BlockRequiresAtLeastLabels: [
                    'This field requires at least',
                    'row(s).'
                ],
                BlockRequiresNoMoreLabels: [
                    'This field requires no more than',
                    'row(s).'
                ],
                BlockRequiresOneRowLabel: 'This field requires at least one row.',

                PointRequiresTwoNumbersLabel: 'This field requires two numbers',
                PointInvalidInputLabel: 'This field has an invalid input'
            }
    }
};

export { en };
export default en;
