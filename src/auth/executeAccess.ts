import payload from '..';
import { Forbidden } from '../errors';
import { Access, AccessResult } from '../config/types';

const executeAccess = async (
    operation,
    access: Access
): Promise<AccessResult> => {
    const adminUILocale = payload.config.admin.locale;

    if (access) {
        const result = await access(operation);

        if (!result) {
            if (!operation.disableErrors)
                throw new Forbidden(adminUILocale.errors.ForbiddenLabel);
        }

        return result;
    }

    if (operation.req.user) {
        return true;
    }

    if (!operation.disableErrors)
        throw new Forbidden(adminUILocale.errors.ForbiddenLabel);
    return false;
};

export default executeAccess;
