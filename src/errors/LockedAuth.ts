import httpStatus from 'http-status';
import APIError from './APIError';

class LockedAuth extends APIError {
    constructor(
        message: string = 'This user is locked due to having too many failed login attempts.'
    ) {
        super(message, httpStatus.UNAUTHORIZED);
    }
}

export default LockedAuth;
