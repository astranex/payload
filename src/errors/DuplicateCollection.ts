import APIError from './APIError';

class DuplicateCollection extends APIError {
    constructor(
        propertyName: string,
        duplicates: string[],
        message: string[] = ['Collection', 'already in use:']
    ) {
        super(
            `${message[0]} ${propertyName} ${message[1]} "${duplicates.join(
                ', '
            )}"`
        );
    }
}

export default DuplicateCollection;
