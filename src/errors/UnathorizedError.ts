import httpStatus from 'http-status';
import APIError from './APIError';

class UnauthorizedError extends APIError {
    constructor(
        message: string = 'Unauthorized, you must be logged in to make this request.'
    ) {
        super(message, httpStatus.UNAUTHORIZED);
    }
}

export default UnauthorizedError;
