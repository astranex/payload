import httpStatus from 'http-status';
import APIError from './APIError';

class FileUploadError extends APIError {
    constructor(
        message: string = 'There was a problem while uploading the file.'
    ) {
        super(message, httpStatus.BAD_REQUEST);
    }
}

export default FileUploadError;
