import httpStatus from 'http-status';
import APIError from './APIError';

class ErrorDeletingFile extends APIError {
    constructor(message: string = 'There was an error deleting file.') {
        super(message, httpStatus.INTERNAL_SERVER_ERROR);
    }
}

export default ErrorDeletingFile;
