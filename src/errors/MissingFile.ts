import httpStatus from 'http-status';
import APIError from './APIError';

class MissingFile extends APIError {
    constructor(message: string = 'No files were uploaded.') {
        super(message, httpStatus.BAD_REQUEST);
    }
}

export default MissingFile;
