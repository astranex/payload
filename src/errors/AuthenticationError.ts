import httpStatus from 'http-status';
import APIError from './APIError';
class AuthenticationError extends APIError {
    constructor(message = 'The email or password provided is incorrect.') {
        super(message, httpStatus.UNAUTHORIZED);
    }
}

export default AuthenticationError;
