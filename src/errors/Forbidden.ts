import httpStatus from 'http-status';
import APIError from './APIError';

class Forbidden extends APIError {
    constructor(
        message: string = 'You are not allowed to perform this action.'
    ) {
        super(message, httpStatus.FORBIDDEN);
    }
}

export default Forbidden;
