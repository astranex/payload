import httpStatus from 'http-status';
import APIError from './APIError';

class NotFound extends APIError {
    constructor(message: string = 'The requested resource was not found.') {
        super(message, httpStatus.NOT_FOUND);
    }
}

export default NotFound;
