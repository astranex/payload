import React from 'react';
import Tooltip from '../../elements/Tooltip';
import { Props } from './types';

import './index.scss';
import { useConfig } from '../../utilities/Config';

const baseClass = 'field-error';

const Error: React.FC<Props> = (props) => {
    const { admin } = useConfig();
    const { locale: adminUILocale } = admin;

    const {
        showError = false,
        message = adminUILocale.forms.Error.ErrorMessageLabel
    } = props;

    if (showError) {
        return <Tooltip className={baseClass}>{message}</Tooltip>;
    }

    return null;
};

export default Error;
