import React, { useCallback } from 'react';
import useField from '../../useField';
import Label from '../../Label';
import Error from '../../Error';
import { useWatchForm } from '../../Form/context';

import './index.scss';
import { useConfig } from '../../../utilities/Config';

const ConfirmPassword: React.FC = () => {
    const {
        admin: { locale: adminUILocale }
    } = useConfig();

    const { getField } = useWatchForm();
    const password = getField('password');

    const validate = useCallback(
        (value) => {
            if (!value) {
                return adminUILocale.forms.FieldTypes.ConfirmPassword.RequiredFieldLabel;
            }

            if (value === password?.value) {
                return true;
            }

            return adminUILocale.forms.FieldTypes.ConfirmPassword.PasswordsNotMatchLabel;
        },
        [password]
    );

    const { value, showError, setValue, errorMessage } = useField({
        path: 'confirm-password',
        disableFormData: true,
        validate,
        enableDebouncedValue: true
    });

    const classes = ['field-type', 'confirm-password', showError && 'error']
        .filter(Boolean)
        .join(' ');

    return (
        <div className={classes}>
            <Error showError={showError} message={errorMessage} />
            <Label
                htmlFor="field-confirm-password"
                label={adminUILocale.forms.FieldTypes.ConfirmPassword.ConfirmPasswordLabel}
                required
            />
            <input
                value={(value as string) || ''}
                onChange={setValue}
                type="password"
                autoComplete="off"
                id="field-confirm-password"
                name="confirm-password"
            />
        </div>
    );
};

export default ConfirmPassword;
