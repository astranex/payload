import React from 'react';
import SearchIcon from '../../../../../graphics/Search';
import { useConfig } from '../../../../../utilities/Config';

import './index.scss';

const baseClass = 'block-search';

const BlockSearch: React.FC<{ setSearchTerm: (term: string) => void }> = (props) => {
  const { admin: {
    locale: adminUILocale
  }} = useConfig();

  const { setSearchTerm } = props;

  const handleChange = (e) => {
    setSearchTerm(e.target.value);
  };

  return (
      <div className={baseClass}>
          <input
              className={`${baseClass}__input`}
              placeholder={adminUILocale.forms.FieldTypes.Blocks.SearchBlockLabel}
              onChange={handleChange}
          />
          <SearchIcon />
      </div>
  );
};

export default BlockSearch;
