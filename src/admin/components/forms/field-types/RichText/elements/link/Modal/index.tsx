import { Modal } from '@faceless-ui/modal';
import React from 'react';
import { MinimalTemplate } from '../../../../../..';
import Button from '../../../../../../elements/Button';
import X from '../../../../../../icons/X';
import Form from '../../../../../Form';
import FormSubmit from '../../../../../Submit';
import { Props } from './types';
import fieldTypes from '../../../..';
import RenderFields from '../../../../../RenderFields';

import './index.scss';
import { useConfig } from '../../../../../../utilities/Config';

const baseClass = 'rich-text-link-edit-modal';

export const EditModal: React.FC<Props> = ({
    close,
    handleModalSubmit,
    initialState,
    fieldSchema,
    modalSlug
}) => {
    const {
        admin: { locale: adminUILocale }
    } = useConfig();

    return (
        <Modal slug={modalSlug} className={baseClass}>
            <MinimalTemplate className={`${baseClass}__template`}>
                <header className={`${baseClass}__header`}>
                    <h3>
                        {adminUILocale.forms.FieldTypes.RichText.EditLinkLabel}
                    </h3>
                    <Button buttonStyle="none" onClick={close}>
                        <X />
                    </Button>
                </header>
                <Form onSubmit={handleModalSubmit} initialState={initialState}>
                    <RenderFields
                        fieldTypes={fieldTypes}
                        readOnly={false}
                        fieldSchema={fieldSchema}
                        forceRender
                    />
                    <FormSubmit>
                        {adminUILocale.forms.FieldTypes.RichText.ConfirmLabel}
                    </FormSubmit>
                </Form>
            </MinimalTemplate>
        </Modal>
    );
};
