import { Config } from '../../../../../../../../config/types';
import { Field } from '../../../../../../../../fields/config/types';
import { useConfig } from '../../../../../../utilities/Config';

export const getBaseFields = (config: Config): Field[] => {
    const {
        admin: { locale: adminUILocale }
    } = useConfig();

    return [
        {
            name: 'text',
            label: adminUILocale.forms.FieldTypes.RichText.TextToDisplayLabel,
            type: 'text',
            required: true
        },
        {
            name: 'linkType',
            label: adminUILocale.forms.FieldTypes.RichText.LinkTypeLabel,
            type: 'radio',
            required: true,
            admin: {
                description:
                    adminUILocale.forms.FieldTypes.RichText
                        .LinkTypeDescriptionLabel
            },
            defaultValue: 'custom',
            options: [
                {
                    label: adminUILocale.forms.FieldTypes.RichText
                        .CustomURLLabel,
                    value: 'custom'
                },
                {
                    label: adminUILocale.forms.FieldTypes.RichText
                        .InternalLinkLabel,
                    value: 'internal'
                }
            ]
        },
        {
            name: 'url',
            label: adminUILocale.forms.FieldTypes.RichText.EnterURLLabel,
            type: 'text',
            required: true,
            admin: {
                condition: ({ linkType, url }) => {
                    return (
                        (typeof linkType === 'undefined' && url) ||
                        linkType === 'custom'
                    );
                }
            }
        },
        {
            name: 'doc',
            label: adminUILocale.forms.FieldTypes.RichText.ChooseDocumentLabel,
            type: 'relationship',
            required: true,
            relationTo: config.collections.map(({ slug }) => slug),
            admin: {
                condition: ({ linkType }) => {
                    return linkType === 'internal';
                }
            }
        },
        {
            name: 'newTab',
            label: adminUILocale.forms.FieldTypes.RichText.OpenInNewTabLabel,
            type: 'checkbox'
        }
    ];
};
