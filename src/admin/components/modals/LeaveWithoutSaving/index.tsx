import React from 'react';
import NavigationPrompt from 'react-router-navigation-prompt';
import { useAuth } from '../../utilities/Auth';
import { useFormModified } from '../../forms/Form/context';
import MinimalTemplate from '../../templates/Minimal';
import Button from '../../elements/Button';

import './index.scss';
import { useConfig } from '../../utilities/Config';

const modalSlug = 'leave-without-saving';

const LeaveWithoutSaving: React.FC = () => {
    const {
        admin: { locale: adminUILocale }
    } = useConfig();

    const modified = useFormModified();
    const { user } = useAuth();

    return (
        <NavigationPrompt when={Boolean(modified && user)}>
            {({ onConfirm, onCancel }) => (
                <div className={modalSlug}>
                    <MinimalTemplate className={`${modalSlug}__template`}>
                        <h1>
                            {
                                adminUILocale.modals.LeaveWithoutSaving
                                    .LeaveWithoutSavingLabels[0]
                            }
                        </h1>
                        <p>
                            {
                                adminUILocale.modals.LeaveWithoutSaving
                                    .LeaveWithoutSavingLabels[1]
                            }
                        </p>
                        <Button onClick={onCancel} buttonStyle="secondary">
                            {
                                adminUILocale.modals.LeaveWithoutSaving
                                    .StayOnThisPageLabel
                            }
                        </Button>
                        <Button onClick={onConfirm}>
                            {
                                adminUILocale.modals.LeaveWithoutSaving
                                    .LeaveAnywayLabel
                            }
                        </Button>
                    </MinimalTemplate>
                </div>
            )}
        </NavigationPrompt>
    );
};

export default LeaveWithoutSaving;
