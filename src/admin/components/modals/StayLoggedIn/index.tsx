import React from 'react';
import { useHistory } from 'react-router-dom';
import { useModal, Modal } from '@faceless-ui/modal';
import { useConfig } from '../../utilities/Config';
import MinimalTemplate from '../../templates/Minimal';
import Button from '../../elements/Button';
import { Props } from './types';

import './index.scss';

const baseClass = 'stay-logged-in';

const modalSlug = 'stay-logged-in';

const StayLoggedInModal: React.FC<Props> = (props) => {
    const { refreshCookie } = props;
    const history = useHistory();
    const {
        routes: { admin },
        admin: { locale: adminUILocale }
    } = useConfig();
    const { toggleModal } = useModal();

    return (
        <Modal className={baseClass} slug="stay-logged-in">
            <MinimalTemplate className={`${baseClass}__template`}>
                <h1>
                    {adminUILocale.modals.StayLoggedIn.StayLoggedInLabels[0]}
                </h1>
                <p>{adminUILocale.modals.StayLoggedIn.StayLoggedInLabels[1]}</p>
                <div className={`${baseClass}__actions`}>
                    <Button
                        buttonStyle="secondary"
                        onClick={() => {
                            toggleModal(modalSlug);
                            history.push(`${admin}/logout`);
                        }}
                    >
                        {adminUILocale.modals.StayLoggedIn.LogOutLabel}
                    </Button>
                    <Button
                        onClick={() => {
                            refreshCookie();
                            toggleModal(modalSlug);
                        }}
                    >
                        {adminUILocale.modals.StayLoggedIn.StayLoggedInLabel}
                    </Button>
                </div>
            </MinimalTemplate>
        </Modal>
    );
};

export default StayLoggedInModal;
