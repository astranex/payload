import React from 'react';
import { useConfig } from '../../utilities/Config';
import DefaultNav from '../../elements/Nav';
import RenderCustomComponent from '../../utilities/RenderCustomComponent';
import Meta from '../../utilities/Meta';
import { Props } from './types';

import './index.scss';

const baseClass = 'template-default';

const Default: React.FC<Props> = ({ children, className }) => {
  const {
    admin: {
      locale: adminUILocale,
      components: {
        Nav: CustomNav,
      } = {
        Nav: undefined,
      },
    } = {},
  } = useConfig();

  const classes = [
    baseClass,
    className,
  ].filter(Boolean).join(' ');

  return (
      <div className={classes}>
          <Meta
              title={adminUILocale.templates.Default.MetaTitleLabel}
              description={adminUILocale.templates.Default.MetaDescriptionLabel}
              keywords={adminUILocale.templates.Default.MetaKeywordsLabel}
          />
          <RenderCustomComponent
              DefaultComponent={DefaultNav}
              CustomComponent={CustomNav}
          />
          <div className={`${baseClass}__wrap`}>{children}</div>
      </div>
  );
};

export default Default;
