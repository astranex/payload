import React, { useCallback, useState } from 'react';
import { toast } from 'react-toastify';
import { Modal, useModal } from '@faceless-ui/modal';
import { useConfig } from '../../utilities/Config';
import { Props } from './types';
import { useDocumentInfo } from '../../utilities/DocumentInfo';
import Button from '../Button';
import { MinimalTemplate } from '../..';
import { requests } from '../../../api';
import { useForm } from '../../forms/Form/context';
import { Field } from '../../../../fields/config/types';
import { useLocale } from '../../utilities/Locale';

import './index.scss';

const baseClass = 'status';

const Status: React.FC<Props> = () => {
    const { admin } = useConfig();
    const { locale: adminUILocale } = admin;

    const {
        publishedDoc,
        unpublishedVersions,
        collection,
        global,
        id,
        getVersions
    } = useDocumentInfo();
    const { toggleModal } = useModal();
    const {
        serverURL,
        routes: { api }
    } = useConfig();
    const [processing, setProcessing] = useState(false);
    const { reset: resetForm } = useForm();
    const locale = useLocale();

    const unPublishModalSlug = `confirm-un-publish-${id}`;
    const revertModalSlug = `confirm-revert-${id}`;

    let statusToRender;

    if (unpublishedVersions?.docs?.length > 0 && publishedDoc) {
        statusToRender = adminUILocale.elements.Status.StatusChangedLabel;
    } else if (!publishedDoc) {
        statusToRender = adminUILocale.elements.Status.StatusDraftLabel;
    } else if (publishedDoc && unpublishedVersions?.docs?.length <= 1) {
        statusToRender = adminUILocale.elements.Status.StatusPublishedLabel;
    }

    const performAction = useCallback(
        async (action: 'revert' | 'unpublish') => {
            let url;
            let method;
            let body;

            setProcessing(true);

            if (action === 'unpublish') {
                body = {
                    _status: 'draft'
                };
            }

            if (action === 'revert') {
                body = publishedDoc;
            }

            if (collection) {
                url = `${serverURL}${api}/${collection.slug}/${id}?depth=0&locale=${locale}&fallback-locale=null`;
                method = 'patch';
            }
            if (global) {
                url = `${serverURL}${api}/globals/${global.slug}?depth=0&locale=${locale}&fallback-locale=null`;
                method = 'post';
            }

            const res = await requests[method](url, {
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            });

            if (res.status === 200) {
                let data;
                let fields: Field[];
                const json = await res.json();

                if (global) {
                    data = json.result;
                    fields = global.fields;
                }

                if (collection) {
                    data = json.doc;
                    fields = collection.fields;
                }

                resetForm(fields, data);
                toast.success(json.message);
                getVersions();
            } else {
                toast.error(adminUILocale.elements.Status.UnpublishingError);
            }

            setProcessing(false);
            if (action === 'revert') {
                toggleModal(revertModalSlug);
            }

            if (action === 'unpublish') {
                toggleModal(unPublishModalSlug);
            }
        },
        [
            collection,
            global,
            publishedDoc,
            serverURL,
            api,
            id,
            locale,
            resetForm,
            getVersions,
            toggleModal,
            revertModalSlug,
            unPublishModalSlug
        ]
    );

    if (statusToRender) {
        return (
            <div className={baseClass}>
                <div className={`${baseClass}__value-wrap`}>
                    <span className={`${baseClass}__value`}>
                        {statusToRender}
                    </span>
                    {statusToRender ===
                        adminUILocale.elements.Status.StatusPublishedLabel && (
                        <React.Fragment>
                            &nbsp;&mdash;&nbsp;
                            <Button
                                onClick={() => toggleModal(unPublishModalSlug)}
                                className={`${baseClass}__action`}
                                buttonStyle="none"
                            >
                                {adminUILocale.elements.Status.UnpublishLabel}
                            </Button>
                            <Modal
                                slug={unPublishModalSlug}
                                className={`${baseClass}__modal`}
                            >
                                <MinimalTemplate
                                    className={`${baseClass}__modal-template`}
                                >
                                    <h1>
                                        {
                                            adminUILocale.elements.Status
                                                .ConfirmUnpublishLabels[0]
                                        }
                                    </h1>
                                    <p>
                                        {
                                            adminUILocale.elements.Status
                                                .ConfirmUnpublishLabels[1]
                                        }
                                    </p>
                                    <Button
                                        buttonStyle="secondary"
                                        type="button"
                                        onClick={
                                            processing
                                                ? undefined
                                                : () =>
                                                      toggleModal(
                                                          unPublishModalSlug
                                                      )
                                        }
                                    >
                                        {
                                            adminUILocale.elements.Status
                                                .CancelLabel
                                        }
                                    </Button>
                                    <Button
                                        onClick={
                                            processing
                                                ? undefined
                                                : () =>
                                                      performAction('unpublish')
                                        }
                                    >
                                        {processing
                                            ? adminUILocale.elements.Status
                                                  .UnpublishingLabel
                                            : adminUILocale.elements.Status
                                                  .ConfirmLabel}
                                    </Button>
                                </MinimalTemplate>
                            </Modal>
                        </React.Fragment>
                    )}
                    {statusToRender ===
                        adminUILocale.elements.Status.StatusChangedLabel && (
                        <React.Fragment>
                            &nbsp;&mdash;&nbsp;
                            <Button
                                onClick={() => toggleModal(revertModalSlug)}
                                className={`${baseClass}__action`}
                                buttonStyle="none"
                            >
                                {adminUILocale.elements.Status.RevertLabel}
                            </Button>
                            <Modal
                                slug={revertModalSlug}
                                className={`${baseClass}__modal`}
                            >
                                <MinimalTemplate
                                    className={`${baseClass}__modal-template`}
                                >
                                    <h1>
                                        {
                                            adminUILocale.elements.Status
                                                .ConfirmRevertLabels[0]
                                        }
                                    </h1>
                                    <p>
                                        {
                                            adminUILocale.elements.Status
                                                .ConfirmRevertLabels[1]
                                        }
                                    </p>
                                    <Button
                                        buttonStyle="secondary"
                                        type="button"
                                        onClick={
                                            processing
                                                ? undefined
                                                : () =>
                                                      toggleModal(
                                                          revertModalSlug
                                                      )
                                        }
                                    >
                                        Cancel
                                    </Button>
                                    <Button
                                        onClick={
                                            processing
                                                ? undefined
                                                : () => performAction('revert')
                                        }
                                    >
                                        {processing
                                            ? adminUILocale.elements.Status
                                                  .RevertingLabel
                                            : adminUILocale.elements.Status
                                                  .ConfirmLabel}
                                    </Button>
                                </MinimalTemplate>
                            </Modal>
                        </React.Fragment>
                    )}
                </div>
            </div>
        );
    }

    return null;
};

export default Status;
