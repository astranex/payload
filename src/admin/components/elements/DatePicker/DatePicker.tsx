import React from 'react';
import DatePicker, { registerLocale, setDefaultLocale } from 'react-datepicker';
import CalendarIcon from '../../icons/Calendar';
import XIcon from '../../icons/X';
import { Props } from './types';

import 'react-datepicker/dist/react-datepicker.css';
import './index.scss';
import { useConfig } from '../../utilities/Config';

const baseClass = 'date-time-picker';

const DateTime: React.FC<Props> = (props) => {
    const {
        admin: {
            dateLocale,
            dateFormat,
            datePickerFormat,
            locale: adminUILocale
        }
    } = useConfig();

    const {
        value,
        onChange,
        displayFormat,
        pickerAppearance = 'dayAndTime',
        minDate,
        maxDate,
        monthsToShow = 1,
        minTime,
        maxTime,
        timeIntervals = 30,
        timeFormat = datePickerFormat.defaultTimeFormat,
        readOnly,
        placeholder: placeholderText = adminUILocale.elements.DatePicker
            .PlaceholderLabel
    } = props;

    let dateTimeFormat = displayFormat;

    if (dateTimeFormat === undefined) {
        if (pickerAppearance === 'dayAndTime') dateTimeFormat = dateFormat;
        else if (pickerAppearance === 'timeOnly')
            dateTimeFormat = datePickerFormat.timeOnlyFormat;
        else if (pickerAppearance === 'monthOnly')
            dateTimeFormat = datePickerFormat.monthOnlyFormat;
        else dateTimeFormat = datePickerFormat.elseFormat;
    }

    registerLocale('datePickerLocale', dateLocale);
    setDefaultLocale('datePickerLocale');

    const dateTimePickerProps = {
        minDate,
        maxDate,
        dateFormat: dateTimeFormat,
        dateLocale: dateLocale,
        monthsShown: Math.min(2, monthsToShow),
        showTimeSelect:
            pickerAppearance === 'dayAndTime' ||
            pickerAppearance === 'timeOnly',
        minTime,
        maxTime,
        timeIntervals,
        timeFormat,
        placeholderText,
        disabled: readOnly,
        onChange,
        showPopperArrow: false,
        selected: value && new Date(value),
        customInputRef: 'ref',
        showMonthYearPicker: pickerAppearance === 'monthOnly'
    };

    const classes = [baseClass, `${baseClass}__appearance--${pickerAppearance}`]
        .filter(Boolean)
        .join(' ');

    return (
        <div className={classes}>
            <div className={`${baseClass}__icon-wrap`}>
                {dateTimePickerProps.selected && (
                    <button
                        type="button"
                        className={`${baseClass}__clear-button`}
                        onClick={() => onChange(null)}
                    >
                        <XIcon />
                    </button>
                )}
                <CalendarIcon />
            </div>
            <div className={`${baseClass}__input-wrapper`}>
                <DatePicker
                    {...dateTimePickerProps}
                    timeCaption={
                        adminUILocale.elements.DatePicker.TimeCaptionLabel
                    }
                    popperModifiers={{
                        preventOverflow: {
                            enabled: true
                        }
                    }}
                />
            </div>
        </div>
    );
};

export default DateTime;
