import React from 'react';
import { Props } from './types';

import './index.scss';
import { useConfig } from '../../../../utilities/Config';

const baseClass = 'condition-value-text';

const Text: React.FC<Props> = ({ onChange, value }) => {
    const { admin } = useConfig();
    const { locale: adminUILocale } = admin;

    return (
        <input
            placeholder={adminUILocale.elements.WhereBuilder.TextInputLabel}
            className={baseClass}
            type="text"
            onChange={(e) => onChange(e.target.value)}
            value={value || ''}
        />
    );
};

export default Text;
