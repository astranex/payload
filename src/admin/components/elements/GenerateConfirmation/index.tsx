import React from 'react';
import { toast } from 'react-toastify';
import { Modal, useModal } from '@faceless-ui/modal';
import Button from '../Button';
import MinimalTemplate from '../../templates/Minimal';
import { Props } from './types';
import { useDocumentInfo } from '../../utilities/DocumentInfo';

import './index.scss';
import { useConfig } from '../../utilities/Config';

const baseClass = 'generate-confirmation';

// function description(strings) {
//   const str0 = strings[0]; // "That "
//   const str1 = strings[1]; // " is a "
//   const str2 = strings[2]; // "."
//   return ${str0}${str1}${str2};
// }

const GenerateConfirmation: React.FC<Props> = (props) => {
    const { admin } = useConfig();
    const { locale: adminUILocale } = admin;
    const { setKey, highlightField } = props;

    const { id } = useDocumentInfo();
    const { toggleModal } = useModal();

    const modalSlug = `generate-confirmation-${id}`;

    const handleGenerate = () => {
        setKey();
        toggleModal(modalSlug);
        toast.success(
            adminUILocale.elements.GenerateConfirmation.GenerateAPIKeySuccess,
            { autoClose: 3000 }
        );
        highlightField(true);
    };

    return (
        <React.Fragment>
            <Button
                size="small"
                buttonStyle="secondary"
                onClick={() => {
                    toggleModal(modalSlug);
                }}
            >
                {
                    adminUILocale.elements.GenerateConfirmation
                        .GenerateAPIKeyLabel
                }
            </Button>
            <Modal slug={modalSlug} className={baseClass}>
                <MinimalTemplate className={`${baseClass}__template`}>
                    <h1>
                        {
                            adminUILocale.elements.GenerateConfirmation
                                .ConfirmGenerationLabels[0]
                        }
                    </h1>
                    <p>
                        {
                            adminUILocale.elements.GenerateConfirmation
                                .ConfirmGenerationLabels[1]
                        }{' '}
                        <strong>
                            {
                                adminUILocale.elements.GenerateConfirmation
                                    .ConfirmGenerationLabels[2]
                            }
                        </strong>{' '}
                        {
                            adminUILocale.elements.GenerateConfirmation
                                .ConfirmGenerationLabels[3]
                        }
                        {}
                    </p>

                    <Button
                        buttonStyle="secondary"
                        type="button"
                        onClick={() => {
                            toggleModal(modalSlug);
                        }}
                    >
                        {
                            adminUILocale.elements.GenerateConfirmation
                                .CancelLabel
                        }
                    </Button>
                    <Button onClick={handleGenerate}>
                        {
                            adminUILocale.elements.GenerateConfirmation
                                .GenerateLabel
                        }
                    </Button>
                </MinimalTemplate>
            </Modal>
        </React.Fragment>
    );
};

export default GenerateConfirmation;
