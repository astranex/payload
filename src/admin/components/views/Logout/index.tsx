import React, { useEffect } from 'react';
import { useConfig } from '../../utilities/Config';
import { useAuth } from '../../utilities/Auth';
import Minimal from '../../templates/Minimal';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';

import './index.scss';

const baseClass = 'logout';

const Logout: React.FC<{ inactivity?: boolean }> = (props) => {
    const { inactivity } = props;

    const { logOut } = useAuth();
    const {
        routes: { admin },
        admin: { locale: adminUILocale }
    } = useConfig();

    useEffect(() => {
        logOut();
    }, [logOut]);

    return (
        <Minimal className={baseClass}>
            <Meta
                title={adminUILocale.views.Logout.MetaTitleLabel}
                description={adminUILocale.views.Logout.MetaDescriptionLabel}
                keywords={adminUILocale.views.Logout.MetaKeywordsLabel}
            />
            <div className={`${baseClass}__wrap`}>
                {inactivity && (
                    <h2>
                        {adminUILocale.views.Logout.LoggedOutInactivityLabel}
                    </h2>
                )}
                {!inactivity && (
                    <h2>
                        {adminUILocale.views.Logout.LoggedOutSuccessfullyLabel}
                    </h2>
                )}
                <br />
                <Button
                    el="anchor"
                    buttonStyle="secondary"
                    url={`${admin}/login`}
                >
                    {adminUILocale.views.Logout.LogBackInLabel}
                </Button>
            </div>
        </Minimal>
    );
};

export default Logout;
