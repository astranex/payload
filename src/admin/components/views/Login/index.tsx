import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useConfig } from '../../utilities/Config';
import { useAuth } from '../../utilities/Auth';
import Logo from '../../graphics/Logo';
import MinimalTemplate from '../../templates/Minimal';
import Form from '../../forms/Form';
import Email from '../../forms/field-types/Email';
import Password from '../../forms/field-types/Password';
import FormSubmit from '../../forms/Submit';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';

import './index.scss';

const baseClass = 'login';

const Login: React.FC = () => {
    const {
        admin: { locale: adminUILocale }
    } = useConfig();

    const history = useHistory();
    const { user, setToken } = useAuth();
    const {
        admin: { user: userSlug, components: { beforeLogin, afterLogin } = {} },
        serverURL,
        routes: { admin, api },
        collections
    } = useConfig();

    const collection = collections.find(({ slug }) => slug === userSlug);

    const onSuccess = (data) => {
        if (data.token) {
            setToken(data.token);
            history.push(admin);
        }
    };

    if (user) {
        return (
            <MinimalTemplate className={baseClass}>
                <Meta
                    title={adminUILocale.views.Login.MetaTitleLabel}
                    description={adminUILocale.views.Login.MetaDescriptionLabel}
                    keywords={adminUILocale.views.Login.MetaKeywordsLabel}
                />
                <div className={`${baseClass}__wrap`}>
                    <h1>
                        {adminUILocale.views.Login.AlreadyLoggedInLabels[0]}
                    </h1>
                    <p>
                        {adminUILocale.views.Login.AlreadyLoggedInLabels[1]}{' '}
                        <Link to={`${admin}/logout`}>
                            {adminUILocale.views.Login.AlreadyLoggedInLabels[2]}
                        </Link>{' '}
                        {adminUILocale.views.Login.AlreadyLoggedInLabels[3]}
                    </p>
                    <br />
                    <Button el="link" buttonStyle="secondary" to={admin}>
                        {adminUILocale.views.Login.BackToDashboardLabel}
                    </Button>
                </div>
            </MinimalTemplate>
        );
    }

    return (
        <MinimalTemplate className={baseClass}>
            <Meta
                title={adminUILocale.views.Login.MetaTitleLabel}
                description={adminUILocale.views.Login.MetaDescriptionLabel}
                keywords={adminUILocale.views.Login.MetaKeywordsLabel}
            />
            <div className={`${baseClass}__brand`}>
                <Logo />
            </div>
            {Array.isArray(beforeLogin) &&
                beforeLogin.map((Component, i) => <Component key={i} />)}
            {!collection.auth.disableLocalStrategy && (
                <Form
                    disableSuccessStatus
                    waitForAutocomplete
                    onSuccess={onSuccess}
                    method="post"
                    action={`${serverURL}${api}/${userSlug}/login`}
                >
                    <Email
                        label={adminUILocale.views.Login.EmailAddressLabel}
                        name="email"
                        admin={{ autoComplete: 'email' }}
                        required
                    />
                    <Password
                        label={adminUILocale.views.Login.PasswordLabel}
                        name="password"
                        autoComplete="off"
                        required
                    />
                    <Link to={`${admin}/forgot`}>
                        {adminUILocale.views.Login.ForgotPasswordLabel}
                    </Link>
                    <FormSubmit>
                        {adminUILocale.views.Login.LoginLabel}
                    </FormSubmit>
                </Form>
            )}
            {Array.isArray(afterLogin) &&
                afterLogin.map((Component, i) => <Component key={i} />)}
        </MinimalTemplate>
    );
};

export default Login;
