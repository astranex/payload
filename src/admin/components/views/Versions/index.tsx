import React, { useEffect, useState } from 'react';
import { useRouteMatch } from 'react-router-dom';
import format from 'date-fns/format';
import { useConfig } from '../../utilities/Config';
import usePayloadAPI from '../../../hooks/usePayloadAPI';
import Eyebrow from '../../elements/Eyebrow';
import Loading from '../../elements/Loading';
import { useStepNav } from '../../elements/StepNav';
import { StepNavItem } from '../../elements/StepNav/types';
import Meta from '../../utilities/Meta';
import { Props } from './types';
import IDLabel from '../../elements/IDLabel';
import { getColumns } from './columns';
import Table from '../../elements/Table';
import Paginator from '../../elements/Paginator';
import PerPage from '../../elements/PerPage';
import { useSearchParams } from '../../utilities/SearchParams';
import { Banner, Pill } from '../..';
import { SanitizedCollectionConfig } from '../../../../collections/config/types';
import { SanitizedGlobalConfig } from '../../../../globals/config/types';
import { shouldIncrementVersionCount } from '../../../../versions/shouldIncrementVersionCount';
import { Gutter } from '../../elements/Gutter';

import './index.scss';

const baseClass = 'versions';

const Versions: React.FC<Props> = ({ collection, global }) => {
    const {
        serverURL,
        routes: { admin, api },
        admin: { dateFormat, dateLocale, locale: adminUILocale }
    } = useConfig();

    const { setStepNav } = useStepNav();
    const {
        params: { id }
    } = useRouteMatch<{ id: string }>();
    const [tableColumns] = useState(() => getColumns(collection, global));
    const [fetchURL, setFetchURL] = useState('');
    const { page, sort, limit } = useSearchParams();

    let docURL: string;
    let entityLabel: string;
    let slug: string;
    let entity: SanitizedCollectionConfig | SanitizedGlobalConfig;
    let editURL: string;

    if (collection) {
        ({ slug } = collection);
        docURL = `${serverURL}${api}/${slug}/${id}`;
        entityLabel = collection.labels.singular;
        entity = collection;
        editURL = `${admin}/collections/${collection.slug}/${id}`;
    }

    if (global) {
        ({ slug } = global);
        docURL = `${serverURL}${api}/globals/${slug}`;
        entityLabel = global.label;
        entity = global;
        editURL = `${admin}/globals/${global.slug}`;
    }

    const useAsTitle = collection?.admin?.useAsTitle || 'id';
    const [{ data: doc }] = usePayloadAPI(docURL, {
        initialParams: { draft: 'true' }
    });
    const [
        { data: versionsData, isLoading: isLoadingVersions },
        { setParams }
    ] = usePayloadAPI(fetchURL);

    useEffect(() => {
        let nav: StepNavItem[] = [];

        if (collection) {
            let docLabel = '';

            if (doc) {
                if (useAsTitle) {
                    if (doc[useAsTitle]) {
                        docLabel = doc[useAsTitle];
                    } else {
                        docLabel = adminUILocale.views.Versions.UntitledLabel;
                    }
                } else {
                    docLabel = doc.id;
                }
            }

            nav = [
                {
                    url: `${admin}/collections/${collection.slug}`,
                    label: collection.labels.plural
                },
                {
                    label: docLabel,
                    url: editURL
                },
                {
                    label: adminUILocale.views.Versions.VersionsLabel
                }
            ];
        }

        if (global) {
            nav = [
                {
                    url: editURL,
                    label: global.label
                },
                {
                    label: adminUILocale.views.Versions.VersionsLabel
                }
            ];
        }

        setStepNav(nav);
    }, [setStepNav, collection, global, useAsTitle, doc, admin, id, editURL]);

    useEffect(() => {
        const params = {
            depth: 1,
            page: undefined,
            sort: undefined,
            limit,
            where: {}
        };

        if (page) params.page = page;
        if (sort) params.sort = sort;

        let fetchURLToSet: string;

        if (collection) {
            fetchURLToSet = `${serverURL}${api}/${collection.slug}/versions`;
            params.where = {
                parent: {
                    equals: id
                }
            };
        }

        if (global) {
            fetchURLToSet = `${serverURL}${api}/globals/${global.slug}/versions`;
        }

        // Performance enhancement
        // Setting the Fetch URL this way
        // prevents a double-fetch

        setFetchURL(fetchURLToSet);

        setParams(params);
    }, [setParams, page, sort, limit, serverURL, api, id, global, collection]);

    let useIDLabel = doc[useAsTitle] === doc?.id;
    let heading: string;
    let metaDesc: string;
    let metaTitle: string;

    if (collection) {
        metaTitle = `${adminUILocale.views.Versions.VersionsLabel} - ${doc[useAsTitle]} - ${entityLabel}`;
        metaDesc = `${adminUILocale.views.Versions.ViewingVersionsCollectionsLabel} ${entityLabel} ${doc[useAsTitle]}`;
        heading =
            doc?.[useAsTitle] || adminUILocale.views.Versions.UntitledLabel;
    }

    if (global) {
        metaTitle = `${adminUILocale.views.Versions.VersionsLabel} - ${entityLabel}`;
        metaDesc = `${adminUILocale.views.Versions.ViewingVersionsGlobalsLabel} ${entityLabel}`;
        heading = entityLabel;
        useIDLabel = false;
    }

    const docStatus = doc?._status;
    const docUpdatedAt = doc?.updatedAt;
    const showParentDoc =
        versionsData?.page === 1 &&
        shouldIncrementVersionCount({
            entity,
            docStatus,
            versions: versionsData
        });

    return (
        <div className={baseClass}>
            <Meta title={metaTitle} description={metaDesc} />
            <Eyebrow />
            <Gutter className={`${baseClass}__wrap`}>
                <header className={`${baseClass}__header`}>
                    <div className={`${baseClass}__intro`}>
                        {adminUILocale.views.Versions.ShowingVersionsLabel}
                    </div>
                    {useIDLabel && <IDLabel id={doc?.id} />}
                    {!useIDLabel && <h1>{heading}</h1>}
                </header>
                {isLoadingVersions && <Loading />}
                {showParentDoc && (
                    <Banner
                        type={docStatus === 'published' ? 'success' : undefined}
                        className={`${baseClass}__parent-doc`}
                    >
                        {adminUILocale.views.Versions.CurrentStatusLabels[0]}{' '}
                        {docStatus}{' '}
                        {adminUILocale.views.Versions.CurrentStatusLabels[1]}{' '}
                        {format(new Date(docUpdatedAt), dateFormat, {
                            locale: dateLocale
                        })}
                        <div className={`${baseClass}__parent-doc-pills`}>
                            &nbsp;&nbsp;
                            <Pill pillStyle="white" to={editURL}>
                                {adminUILocale.views.Versions.EditLabel}
                            </Pill>
                        </div>
                    </Banner>
                )}
                {versionsData?.totalDocs > 0 && (
                    <React.Fragment>
                        <Table
                            data={versionsData?.docs}
                            columns={tableColumns}
                        />
                        <div className={`${baseClass}__page-controls`}>
                            <Paginator
                                limit={versionsData.limit}
                                totalPages={versionsData.totalPages}
                                page={versionsData.page}
                                hasPrevPage={versionsData.hasPrevPage}
                                hasNextPage={versionsData.hasNextPage}
                                prevPage={versionsData.prevPage}
                                nextPage={versionsData.nextPage}
                                numberOfNeighbors={1}
                            />
                            {versionsData?.totalDocs > 0 && (
                                <React.Fragment>
                                    <div className={`${baseClass}__page-info`}>
                                        {versionsData.page *
                                            versionsData.limit -
                                            (versionsData.limit - 1)}
                                        -
                                        {versionsData.totalPages > 1 &&
                                        versionsData.totalPages !==
                                            versionsData.page
                                            ? versionsData.limit *
                                              versionsData.page
                                            : versionsData.totalDocs}{' '}
                                        {adminUILocale.views.Versions.OfLabel}{' '}
                                        {versionsData.totalDocs}
                                    </div>
                                    <PerPage
                                        limits={
                                            collection?.admin?.pagination
                                                ?.limits
                                        }
                                        limit={limit ? Number(limit) : 10}
                                    />
                                </React.Fragment>
                            )}
                        </div>
                    </React.Fragment>
                )}
                {versionsData?.totalDocs === 0 && (
                    <div className={`${baseClass}__no-versions`}>
                        {adminUILocale.views.Versions.NoVersionsFoundLabel}
                    </div>
                )}
            </Gutter>
        </div>
    );
};

export default Versions;
