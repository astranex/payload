import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useConfig } from '../../utilities/Config';
import { useAuth } from '../../utilities/Auth';
import Logo from '../../graphics/Logo';
import MinimalTemplate from '../../templates/Minimal';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';
import { SanitizedCollectionConfig } from '../../../../collections/config/types';

import Login from '../Login';
import './index.scss';

const baseClass = 'verify';

const Verify: React.FC<{ collection: SanitizedCollectionConfig }> = ({
    collection
}) => {
    const { slug: collectionSlug } = collection;

    const { user } = useAuth();
    const { token } = useParams<{ token?: string }>();
    const {
        serverURL,
        routes: { admin: adminRoute },
        admin: { user: adminUser, locale: adminUILocale }
    } = useConfig();

    const isAdminUser = collectionSlug === adminUser;
    const [verifyResult, setVerifyResult] = useState(null);

    useEffect(() => {
        async function verifyToken() {
            const result = await fetch(
                `${serverURL}/api/${collectionSlug}/verify/${token}`,
                { method: 'POST' }
            );
            setVerifyResult(result);
        }
        verifyToken();
    }, [setVerifyResult, collectionSlug, serverURL, token]);

    if (user) {
        return <Login />;
    }

    const getText = () => {
        if (verifyResult?.status === 200)
            return adminUILocale.views.Verify.VerifiedSuccessfullyLabel;
        if (verifyResult?.status === 202)
            return adminUILocale.views.Verify.AlreadyActivatedLabel;
        return adminUILocale.views.Verify.UnableToVerifyLabel;
    };

    return (
        <MinimalTemplate className={baseClass}>
            <Meta
                title={adminUILocale.views.Verify.MetaTitleLabel}
                description={adminUILocale.views.Verify.MetaDescriptionLabel}
                keywords={adminUILocale.views.Verify.MetaKeywordsLabel}
            />
            <div className={`${baseClass}__brand`}>
                <Logo />
            </div>
            <h2>{getText()}</h2>
            {isAdminUser && verifyResult?.status === 200 && (
                <Button
                    el="link"
                    buttonStyle="secondary"
                    to={`${adminRoute}/login`}
                >
                    {adminUILocale.views.Verify.LoginLabel}
                </Button>
            )}
        </MinimalTemplate>
    );
};
export default Verify;
