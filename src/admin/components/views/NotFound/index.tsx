import React, { useEffect } from 'react';
import { useConfig } from '../../utilities/Config';
import Eyebrow from '../../elements/Eyebrow';
import { useStepNav } from '../../elements/StepNav';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';
import { Gutter } from '../../elements/Gutter';

const baseClass = 'not-found';

const NotFound: React.FC = () => {
    const { setStepNav } = useStepNav();
    const {
        routes: { admin },
        admin: { locale: adminUILocale }
    } = useConfig();

    useEffect(() => {
        setStepNav([
            {
                label: adminUILocale.views.NotFound.NotFoundLabel
            }
        ]);
    }, [setStepNav]);

    return (
        <div className={baseClass}>
            <Meta
                title={adminUILocale.views.NotFound.MetaTitleLabel}
                description={adminUILocale.views.NotFound.MetaDescriptionLabel}
                keywords={adminUILocale.views.NotFound.MetaKeywordsLabel}
            />
            <Eyebrow />
            <Gutter className={`${baseClass}__wrap`}>
                <h1>{adminUILocale.views.NotFound.NothingFoundLabels[0]}</h1>
                <p>{adminUILocale.views.NotFound.NothingFoundLabels[1]}</p>
                <Button el="link" to={`${admin}`}>
                    {adminUILocale.views.NotFound.BackToDashboardLabel}
                </Button>
            </Gutter>
        </div>
    );
};

export default NotFound;
