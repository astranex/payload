import React from 'react';
import { ArrayField } from '../../../../../../../../fields/config/types';
import { useConfig } from '../../../../../../utilities/Config';

type Props = {
    data: Record<string, unknown>;
    field: ArrayField;
};

const ArrayCell: React.FC<Props> = ({ data, field }) => {
    const {
        admin: { locale: adminUILocale }
    } = useConfig();

    const arrayFields = data ?? [];
    const label = `${arrayFields.length} ${field?.labels?.plural || adminUILocale.views.collections.List.Cell.fieldTypes.Array.RowsLabel}`;

    return <span>{label}</span>;
};

export default ArrayCell;
