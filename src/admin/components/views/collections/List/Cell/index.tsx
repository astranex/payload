import React from 'react';
import { Link } from 'react-router-dom';
import { useConfig } from '../../../../utilities/Config';
import RenderCustomComponent from '../../../../utilities/RenderCustomComponent';
import cellComponents from './field-types';
import { Props } from './types';

const DefaultCell: React.FC<Props> = (props) => {
    const {
        field,
        colIndex,
        collection: { slug },
        cellData,
        rowData: { id } = {}
    } = props;

    const {
        routes: { admin },
        admin: { locale: adminUILocale }
    } = useConfig();

    let WrapElement: React.ComponentType<any> | string = 'span';

    const wrapElementProps: {
        to?: string;
    } = {};

    if (colIndex === 0) {
        WrapElement = Link;
        wrapElementProps.to = `${admin}/collections/${slug}/${id}`;
    }

    const CellComponent = cellData && cellComponents[field.type];

    if (!CellComponent) {
        return (
            <WrapElement {...wrapElementProps}>
                {(cellData === '' || typeof cellData === 'undefined') &&
                    `${
                        adminUILocale.views.collections.List.Cell
                            .NoFieldLabels[0]
                    }${typeof field.label === 'string' ? field.label : 'data'}${
                        adminUILocale.views.collections.List.Cell
                            .NoFieldLabels[1]
                    }`}
                {typeof cellData === 'string' && cellData}
                {typeof cellData === 'number' && cellData}
                {typeof cellData === 'object' && JSON.stringify(cellData)}
            </WrapElement>
        );
    }

    return (
        <WrapElement {...wrapElementProps}>
            <CellComponent field={field} data={cellData} />
        </WrapElement>
    );
};

const Cell: React.FC<Props> = (props) => {
    const {
        colIndex,
        collection,
        cellData,
        rowData,
        field,
        field: { admin: { components: { Cell: CustomCell } = {} } = {} }
    } = props;

    return (
        <RenderCustomComponent
            componentProps={{
                rowData,
                colIndex,
                cellData,
                collection,
                field
            }}
            CustomComponent={CustomCell}
            DefaultComponent={DefaultCell}
        />
    );
};

export default Cell;
