import React from 'react';
import { useConfig } from '../../utilities/Config';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';
import MinimalTemplate from '../../templates/Minimal';

const Unauthorized: React.FC = () => {
    const {
        routes: { admin },
        admin: { locale: adminUILocale }
    } = useConfig();

    return (
        <MinimalTemplate className="unauthorized">
            <Meta
                title={adminUILocale.views.Unauthorized.MetaTitleLabel}
                description={
                    adminUILocale.views.Unauthorized.MetaDescriptionLabel
                }
                keywords={adminUILocale.views.Unauthorized.MetaKeywordsLabel}
            />
            <h2>{adminUILocale.views.Unauthorized.UnauthorizedLabel}</h2>
            <p>{adminUILocale.views.Unauthorized.NotAllowedLabel}</p>
            <br />
            <Button el="link" to={`${admin}/logout`}>
                {adminUILocale.views.Unauthorized.LogOutLabel}
            </Button>
        </MinimalTemplate>
    );
};

export default Unauthorized;
