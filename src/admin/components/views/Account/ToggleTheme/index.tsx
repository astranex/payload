import React, { useCallback } from 'react';
import RadioGroupInput from '../../../forms/field-types/RadioGroup/Input';
import { OnChange } from '../../../forms/field-types/RadioGroup/types';
import { useConfig } from '../../../utilities/Config';
import { Theme, useTheme } from '../../../utilities/Theme';

export const ToggleTheme: React.FC = () => {
    const {
        admin: { locale: adminUILocale }
    } = useConfig();

    const { theme, setTheme, autoMode } = useTheme();

    const onChange = useCallback<OnChange<Theme>>(
        (newTheme) => {
            setTheme(newTheme);
        },
        [setTheme]
    );

    return (
        <RadioGroupInput
            name="theme"
            label={adminUILocale.views.Account.RadioGroupInputLabel}
            value={autoMode ? 'auto' : theme}
            onChange={onChange}
            options={[
                {
                    label: adminUILocale.views.Account.AutomaticLabel,
                    value: 'auto'
                },
                {
                    label: adminUILocale.views.Account.LightLabel,
                    value: 'light'
                },
                {
                    label: adminUILocale.views.Account.DarkLabel,
                    value: 'dark'
                }
            ]}
        />
    );
};
