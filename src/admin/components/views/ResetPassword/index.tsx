import React from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useConfig } from '../../utilities/Config';
import { useAuth } from '../../utilities/Auth';
import MinimalTemplate from '../../templates/Minimal';
import Form from '../../forms/Form';
import Password from '../../forms/field-types/Password';
import ConfirmPassword from '../../forms/field-types/ConfirmPassword';
import FormSubmit from '../../forms/Submit';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';

import './index.scss';
import HiddenInput from '../../forms/field-types/HiddenInput';

const baseClass = 'reset-password';

const ResetPassword: React.FC = () => {
    const {
        admin: { user: userSlug, locale: adminUILocale },
        serverURL,
        routes: { admin, api }
    } = useConfig();
    const { token } = useParams<{ token?: string }>();
    const history = useHistory();
    const { user, setToken } = useAuth();

    const onSuccess = (data) => {
        if (data.token) {
            setToken(data.token);
            history.push(`${admin}`);
        }
    };

    if (user) {
        return (
            <MinimalTemplate className={baseClass}>
                <Meta
                    title={adminUILocale.views.ResetPassword.MetaTitleLabel}
                    description={
                        adminUILocale.views.ResetPassword.MetaDescriptionLabel
                    }
                    keywords={
                        adminUILocale.views.ResetPassword.MetaKeywordsLabel
                    }
                />

                <div className={`${baseClass}__wrap`}>
                    <h1>
                        {
                            adminUILocale.views.ResetPassword
                                .AlreadyLoggedInLabels[0]
                        }
                    </h1>
                    <p>
                        {
                            adminUILocale.views.ResetPassword
                                .AlreadyLoggedInLabels[1]
                        }{' '}
                        <Link to={`${admin}/logout`}>
                            {
                                adminUILocale.views.ResetPassword
                                    .AlreadyLoggedInLabels[2]
                            }
                        </Link>{' '}
                        {
                            adminUILocale.views.ResetPassword
                                .AlreadyLoggedInLabels[3]
                        }
                    </p>
                    <br />
                    <Button el="link" buttonStyle="secondary" to={admin}>
                        {adminUILocale.views.ResetPassword.BackToDashboardLabel}
                    </Button>
                </div>
            </MinimalTemplate>
        );
    }

    return (
        <MinimalTemplate className={baseClass}>
            <div className={`${baseClass}__wrap`}>
                <h1>{adminUILocale.views.ResetPassword.ResetPasswordLabel}</h1>
                <Form
                    onSuccess={onSuccess}
                    method="post"
                    action={`${serverURL}${api}/${userSlug}/reset-password`}
                    redirect={admin}
                >
                    <Password
                        label={
                            adminUILocale.views.ResetPassword.NewPasswordLabel
                        }
                        name="password"
                        autoComplete="off"
                        required
                    />
                    <ConfirmPassword />
                    <HiddenInput name="token" value={token} />
                    <FormSubmit>
                        {
                            adminUILocale.views.ResetPassword
                                .ResetPasswordButtonLabel
                        }
                    </FormSubmit>
                </Form>
            </div>
        </MinimalTemplate>
    );
};

export default ResetPassword;
