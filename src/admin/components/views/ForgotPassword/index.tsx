import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useConfig } from '../../utilities/Config';
import { useAuth } from '../../utilities/Auth';
import MinimalTemplate from '../../templates/Minimal';
import Form from '../../forms/Form';
import Email from '../../forms/field-types/Email';
import FormSubmit from '../../forms/Submit';
import Button from '../../elements/Button';
import Meta from '../../utilities/Meta';

import './index.scss';

const baseClass = 'forgot-password';

const ForgotPassword: React.FC = () => {
    const {
        admin: { locale: adminUILocale }
    } = useConfig();

    const [hasSubmitted, setHasSubmitted] = useState(false);
    const { user } = useAuth();
    const {
        admin: { user: userSlug },
        serverURL,
        routes: { admin, api }
    } = useConfig();

    const handleResponse = (res) => {
        res.json().then(
            () => {
                setHasSubmitted(true);
            },
            () => {
                toast.error(adminUILocale.views.ForgotPassword.ErrorLabel);
            }
        );
    };

    if (user) {
        return (
            <MinimalTemplate className={baseClass}>
                <Meta
                    title={adminUILocale.views.ForgotPassword.MetaTitleLabel}
                    description={
                        adminUILocale.views.ForgotPassword.MetaDescriptionLabel
                    }
                    keywords={
                        adminUILocale.views.ForgotPassword.MetaKeywordsLabel
                    }
                />

                <h1>
                    {adminUILocale.views.ForgotPassword.ForgotPasswordLabels[0]}
                </h1>
                <p>
                    {adminUILocale.views.ForgotPassword.ForgotPasswordLabels[1]}{' '}
                    <Link to={`${admin}/account`}>
                        {
                            adminUILocale.views.ForgotPassword
                                .ForgotPasswordLabels[2]
                        }
                    </Link>{' '}
                    {adminUILocale.views.ForgotPassword.ForgotPasswordLabels[3]}
                </p>
                <br />
                <Button el="link" buttonStyle="secondary" to={admin}>
                    {adminUILocale.views.ForgotPassword.BackToDashboardLabel}
                </Button>
            </MinimalTemplate>
        );
    }

    if (hasSubmitted) {
        return (
            <MinimalTemplate className={baseClass}>
                <h1>{adminUILocale.views.ForgotPassword.EmailSentLabel}</h1>
                <p>{adminUILocale.views.ForgotPassword.CheckEmailLabel}</p>
            </MinimalTemplate>
        );
    }

    return (
        <MinimalTemplate className={baseClass}>
            <Form
                handleResponse={handleResponse}
                method="post"
                action={`${serverURL}${api}/${userSlug}/forgot-password`}
            >
                <h1>
                    {adminUILocale.views.ForgotPassword.ForgotPasswordLabel}
                </h1>
                <p>{adminUILocale.views.ForgotPassword.EnterEmailLabel}</p>
                <Email
                    label={adminUILocale.views.ForgotPassword.EmailAddressLabel}
                    name="email"
                    admin={{ autoComplete: 'email' }}
                    required
                />
                <FormSubmit>
                    {adminUILocale.views.ForgotPassword.SubmitLabel}
                </FormSubmit>
            </Form>
            <Link to={`${admin}/login`}>
                {adminUILocale.views.ForgotPassword.BackToLogin}
            </Link>
        </MinimalTemplate>
    );
};

export default ForgotPassword;
