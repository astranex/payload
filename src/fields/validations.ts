import defaultRichTextValue from './richText/defaultValue';
import {
    ArrayField,
    BlockField,
    CheckboxField,
    CodeField,
    DateField,
    EmailField,
    NumberField,
    PointField,
    RadioField,
    RelationshipField,
    RelationshipValue,
    RichTextField,
    SelectField,
    TextareaField,
    TextField,
    UploadField,
    Validate,
    fieldAffectsData
} from './config/types';
import { TypeWithID } from '../collections/config/types';
import canUseDOM from '../utilities/canUseDOM';
import { isValidID } from '../utilities/isValidID';
import { getIDType } from '../utilities/getIDType';

export const number: Validate<unknown, unknown, NumberField> = (
    value: string,
    { required, min, max, adminUILocale }
) => {
    const parsedValue = parseFloat(value);

    if (
        (value && typeof parsedValue !== 'number') ||
        (required && Number.isNaN(parsedValue)) ||
        (value && Number.isNaN(parsedValue))
    ) {
        return (
            adminUILocale?.fields.validations.EnterValidNumberLabel ||
            'Please enter a valid number.'
        );
    }

    if (typeof max === 'number' && parsedValue > max) {
        return `"${value}" ${
            adminUILocale?.fields.validations.NumberGreaterLabel ||
            'is greater than the max allowed value of'
        } ${max}.`;
    }

    if (typeof min === 'number' && parsedValue < min) {
        return `"${value}" ${
            adminUILocale?.fields.validations.NumberLessLabel ||
            'is less than the min allowed value of'
        } ${min}.`;
    }

    if (required && typeof parsedValue !== 'number') {
        return (
            adminUILocale?.fields.validations.DefaultMessageLabel ||
            'This field is required.'
        );
    }

    return true;
};

export const text: Validate<unknown, unknown, TextField> = (
    value: string,
    { minLength, maxLength, required, adminUILocale }
) => {
    if (value && maxLength && value.length > maxLength) {
        return `${
            adminUILocale?.fields.validations.TextShorterLabels[0] ||
            'This value must be shorter than the max length of'
        } ${maxLength} ${
            adminUILocale?.fields.validations.TextShorterLabels[1] ||
            'characters.'
        }`;
    }

    if (value && minLength && value?.length < minLength) {
        return `${
            adminUILocale?.fields.validations.TextLongerLabels[0] ||
            'This value must be longer than the minimum length of'
        } ${minLength} ${
            adminUILocale?.fields.validations.TextLongerLabels[1] ||
            'characters.'
        }`;
    }

    if (required) {
        if (typeof value !== 'string' || value?.length === 0) {
            return (
                adminUILocale?.fields.validations.DefaultMessageLabel ||
                'This field is required.'
            );
        }
    }

    return true;
};

export const password: Validate<unknown, unknown, TextField> = (
    value: string,
    { required, maxLength, minLength, adminUILocale }
) => {
    if (value && maxLength && value.length > maxLength) {
        return `${
            adminUILocale?.fields.validations.PasswordShorterLabels[0] ||
            'This value must be shorter than the max length of'
        } ${maxLength} ${
            adminUILocale?.fields.validations.PasswordShorterLabels[1] ||
            'characters.'
        }`;
    }

    if (value && minLength && value.length < minLength) {
        return `${
            adminUILocale?.fields.validations.PasswordLongerLabels[0] ||
            'This value must be longer than the minimum length of'
        } ${minLength} ${
            adminUILocale?.fields.validations.PasswordLongerLabels[1] ||
            'characters.'
        }`;
    }

    if (required && !value) {
        return (
            adminUILocale?.fields.validations.DefaultMessageLabel ||
            'This field is required.'
        );
    }

    return true;
};

export const email: Validate<unknown, unknown, EmailField> = (
    value: string,
    { required, adminUILocale }
) => {
    if ((value && !/\S+@\S+\.\S+/.test(value)) || (!value && required)) {
        return (
            adminUILocale?.fields.validations.EnterValidEmailLabel ||
            'Please enter a valid email address.'
        );
    }

    return true;
};

export const textarea: Validate<unknown, unknown, TextareaField> = (
    value: string,
    { required, maxLength, minLength, adminUILocale }
) => {
    if (value && maxLength && value.length > maxLength) {
        return `${
            adminUILocale?.fields.validations.TextareaShorterLabels[0] ||
            'This value must be shorter than the max length of'
        } ${maxLength} ${
            adminUILocale?.fields.validations.TextareaShorterLabels[1] ||
            'characters.'
        }`;
    }

    if (value && minLength && value.length < minLength) {
        return `${
            adminUILocale?.fields.validations.TextareaLongerLabels[0] ||
            'This value must be longer than the minimum length of'
        } ${minLength} ${
            adminUILocale?.fields.validations.TextareaLongerLabels[1] ||
            'characters.'
        }`;
    }

    if (required && !value) {
        return (
            adminUILocale?.fields.validations.DefaultMessageLabel ||
            'This field is required.'
        );
    }

    return true;
};

export const code: Validate<unknown, unknown, CodeField> = (
    value: string,
    { required, adminUILocale }
) => {
    if (required && value === undefined) {
        return (
            adminUILocale?.fields.validations.DefaultMessageLabel ||
            'This field is required.'
        );
    }

    return true;
};

export const richText: Validate<unknown, unknown, RichTextField> = (
    value,
    { required, adminUILocale }
) => {
    if (required) {
        const stringifiedDefaultValue = JSON.stringify(defaultRichTextValue);
        if (value && JSON.stringify(value) !== stringifiedDefaultValue)
            return true;
        return (
            adminUILocale?.fields.validations.DefaultMessageLabel ||
            'This field is required.'
        );
    }

    return true;
};

export const checkbox: Validate<unknown, unknown, CheckboxField> = (
    value: boolean,
    { required, adminUILocale }
) => {
    if (
        (value && typeof value !== 'boolean') ||
        (required && typeof value !== 'boolean')
    ) {
        return (
            adminUILocale?.fields.validations.InvalidCheckboxLabel ||
            'This field can only be equal to true or false.'
        );
    }

    return true;
};

export const date: Validate<unknown, unknown, DateField> = (
    value,
    { required, adminUILocale }
) => {
    if (value && !isNaN(Date.parse(value.toString()))) {
        /* eslint-disable-line */
        return true;
    }

    if (value) {
        return `"${value}" ${
            adminUILocale?.fields.validations.InvalidDateLabel ||
            'is not a valid date.'
        }`;
    }

    if (required) {
        return (
            adminUILocale?.fields.validations.DefaultMessageLabel ||
            'This field is required.'
        );
    }

    return true;
};

const validateFilterOptions: Validate = async (
    value,
    { filterOptions, id, user, data, siblingData, relationTo, payload }
) => {
    if (!canUseDOM && typeof filterOptions !== 'undefined' && value) {
        const options: {
            [collection: string]: (string | number)[];
        } = {};

        const collections =
            typeof relationTo === 'string' ? [relationTo] : relationTo;
        const values = Array.isArray(value) ? value : [value];

        await Promise.all(
            collections.map(async (collection) => {
                const optionFilter =
                    typeof filterOptions === 'function'
                        ? filterOptions({
                              id,
                              data,
                              siblingData,
                              user,
                              relationTo: collection
                          })
                        : filterOptions;

                const valueIDs: (string | number)[] = [];

                values.forEach((val) => {
                    if (typeof val === 'object' && val?.value) {
                        valueIDs.push(val.value);
                    }

                    if (typeof val === 'string' || typeof val === 'number') {
                        valueIDs.push(val);
                    }
                });

                const result = await payload.find<TypeWithID>({
                    collection,
                    depth: 0,
                    where: {
                        and: [{ id: { in: valueIDs } }, optionFilter]
                    }
                });

                options[collection] = result.docs.map((doc) => doc.id);
            })
        );

        const invalidRelationships = values.filter((val) => {
            let collection: string;
            let requestedID: string | number;

            if (typeof relationTo === 'string') {
                collection = relationTo;

                if (typeof val === 'string' || typeof val === 'number') {
                    requestedID = val;
                }
            }

            if (
                Array.isArray(relationTo) &&
                typeof val === 'object' &&
                val?.relationTo
            ) {
                collection = val.relationTo;
                requestedID = val.value;
            }

            return options[collection].indexOf(requestedID) === -1;
        });

        if (invalidRelationships.length > 0) {
            return invalidRelationships.reduce((err, invalid, i) => {
                return `${err} ${JSON.stringify(invalid)}${
                    invalidRelationships.length === i + 1 ? ',' : ''
                } `;
            }, 'This field has the following invalid selections:') as string;
        }

        return true;
    }

    return true;
};

export const upload: Validate<unknown, unknown, UploadField> = (
    value: string,
    options
) => {
    const { adminUILocale } = options;

    if (!value && options.required) {
        return (
            adminUILocale?.fields.validations.DefaultMessageLabel ||
            'This field is required.'
        );
    }

    if (!canUseDOM && typeof value !== 'undefined' && value !== null) {
        const idField = options.payload.collections[
            options.relationTo
        ].config.fields.find(
            (field) => fieldAffectsData(field) && field.name === 'id'
        );
        const type = getIDType(idField);

        if (!isValidID(value, type)) {
            return (
                adminUILocale?.fields.validations.InvalidUploadLabel ||
                'This field is not a valid upload ID'
            );
        }
    }

    return validateFilterOptions(value, options);
};

export const relationship: Validate<
    unknown,
    unknown,
    RelationshipField
> = async (value: RelationshipValue, options) => {
    const { adminUILocale } = options;
    if (
        (!value || (Array.isArray(value) && value.length === 0)) &&
        options.required
    ) {
        return (
            adminUILocale?.fields.validations.DefaultMessageLabel ||
            'This field is required.'
        );
    }

    if (!canUseDOM && typeof value !== 'undefined' && value !== null) {
        const values = Array.isArray(value) ? value : [value];

        const invalidRelationships = values.filter((val) => {
            let collection: string;
            let requestedID: string | number;

            if (typeof options.relationTo === 'string') {
                collection = options.relationTo;

                // custom id
                if (typeof val === 'string' || typeof val === 'number') {
                    requestedID = val;
                }
            }

            if (
                Array.isArray(options.relationTo) &&
                typeof val === 'object' &&
                val?.relationTo
            ) {
                collection = val.relationTo;
                requestedID = val.value;
            }

            const idField = options.payload.collections[
                collection
            ].config.fields.find(
                (field) => fieldAffectsData(field) && field.name === 'id'
            );
            let type;
            if (idField) {
                type = idField.type === 'number' ? 'number' : 'text';
            } else {
                type = 'ObjectID';
            }

            return !isValidID(requestedID, type);
        });

        if (invalidRelationships.length > 0) {
            return `${
                adminUILocale?.fields.validations.InvalidSelectionsLabel ||
                'This field has the following invalid selections:'
            } ${invalidRelationships
                .map((err, invalid) => {
                    return `${err} ${JSON.stringify(invalid)}`;
                })
                .join(', ')}` as string;
        }
    }

    return validateFilterOptions(value, options);
};

export const array: Validate<unknown, unknown, ArrayField> = (
    value,
    { minRows, maxRows, required, adminUILocale }
) => {
    if (minRows && value < minRows) {
        return `${
            adminUILocale?.fields.validations.ArrayRequiresAtLeastLabels[0] ||
            'This field requires at least'
        } ${minRows} ${
            adminUILocale?.fields.validations.ArrayRequiresAtLeastLabels[1] ||
            'row(s).'
        }`;
    }

    if (maxRows && value > maxRows) {
        return `${
            adminUILocale?.fields.validations.ArrayRequiresNoMoreLabels[0] ||
            'This field requires no more than'
        } ${maxRows} ${
            adminUILocale?.fields.validations.ArrayRequiresNoMoreLabels[1] ||
            'row(s).'
        }`;
    }

    if (!value && required) {
        return (
            adminUILocale?.fields.validations.ArrayRequiresOneRowLabel ||
            'This field requires at least one row.'
        );
    }

    return true;
};

export const select: Validate<unknown, unknown, SelectField> = (
    value,
    { options, hasMany, required, adminUILocale }
) => {
    if (
        Array.isArray(value) &&
        value.some(
            (input) =>
                !options.some(
                    (option) =>
                        option === input ||
                        (typeof option !== 'string' && option?.value === input)
                )
        )
    ) {
        return (
            adminUILocale?.fields.validations.InvalidSelectionLabel ||
            'This field has an invalid selection'
        );
    }

    if (
        typeof value === 'string' &&
        !options.some(
            (option) =>
                option === value ||
                (typeof option !== 'string' && option.value === value)
        )
    ) {
        return (
            adminUILocale?.fields.validations.InvalidSelectionLabel ||
            'This field has an invalid selection'
        );
    }

    if (
        required &&
        (typeof value === 'undefined' ||
            value === null ||
            (hasMany && Array.isArray(value) && (value as [])?.length === 0))
    ) {
        return (
            adminUILocale?.fields.validations.DefaultMessageLabel ||
            'This field is required.'
        );
    }

    return true;
};

export const radio: Validate<unknown, unknown, RadioField> = (
    value,
    { options, required, adminUILocale }
) => {
    const stringValue = String(value);
    if (
        (typeof value !== 'undefined' || !required) &&
        options.find(
            (option) =>
                String(typeof option !== 'string' && option?.value) ===
                stringValue
        )
    )
        return true;
    return (
        adminUILocale?.fields.validations.DefaultMessageLabel ||
        'This field is required.'
    );
};

export const blocks: Validate<unknown, unknown, BlockField> = (
    value,
    { maxRows, minRows, required, adminUILocale }
) => {
    if (minRows && value < minRows) {
        return `${
            adminUILocale?.fields.validations.BlockRequiresAtLeastLabels[0] ||
            'This field requires at least'
        } ${minRows} ${
            adminUILocale?.fields.validations.BlockRequiresAtLeastLabels[1] ||
            'row(s).'
        }`;
    }

    if (maxRows && value > maxRows) {
        return `${
            adminUILocale?.fields.validations.BlockRequiresNoMoreLabels[0] ||
            'This field requires no more than'
        } ${maxRows} ${
            adminUILocale?.fields.validations.BlockRequiresNoMoreLabels[1] ||
            'row(s).'
        }`;
    }

    if (!value && required) {
        return (
            adminUILocale?.fields.validations.BlockRequiresOneRowLabel ||
            'This field requires at least one row.'
        );
    }

    return true;
};

export const point: Validate<unknown, unknown, PointField> = (
    value: [number | string, number | string] = ['', ''],
    { required, adminUILocale }
) => {
    const lng = parseFloat(String(value[0]));
    const lat = parseFloat(String(value[1]));
    if (
        required &&
        ((value[0] &&
            value[1] &&
            typeof lng !== 'number' &&
            typeof lat !== 'number') ||
            Number.isNaN(lng) ||
            Number.isNaN(lat) ||
            (Array.isArray(value) && value.length !== 2))
    ) {
        return (
            adminUILocale?.fields.validations.PointRequiresTwoNumbersLabel ||
            'This field requires two numbers'
        );
    }

    if ((value[1] && Number.isNaN(lng)) || (value[0] && Number.isNaN(lat))) {
        return (
            adminUILocale?.fields.validations.PointInvalidInputLabel ||
            'This field has an invalid input'
        );
    }

    return true;
};

export default {
    number,
    text,
    password,
    email,
    textarea,
    code,
    richText,
    checkbox,
    date,
    upload,
    relationship,
    array,
    select,
    radio,
    blocks,
    point
};
