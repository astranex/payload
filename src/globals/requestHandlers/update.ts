import payload from '../..';
import { Response, NextFunction } from 'express';
import httpStatus from 'http-status';
import { PayloadRequest } from '../../express/types';
import { SanitizedGlobalConfig } from '../config/types';
import { Document } from '../../types';
import update from '../operations/update';

export type UpdateGlobalResult = Promise<Response<Document> | void>;
export type UpdateGlobalResponse = (
    req: PayloadRequest,
    res: Response,
    next: NextFunction
) => UpdateGlobalResult;

export default function updateHandler(
    globalConfig: SanitizedGlobalConfig
): UpdateGlobalResponse {
    const adminUILocale = payload.config.admin.locale;

    return async function handler(
        req: PayloadRequest,
        res: Response,
        next: NextFunction
    ) {
        try {
            const { slug } = globalConfig;
            const draft = req.query.draft === 'true';
            const autosave = req.query.autosave === 'true';

            const result = await update({
                req,
                globalConfig,
                slug,
                depth: Number(req.query.depth),
                data: req.body,
                draft,
                autosave
            });

            let message =
                adminUILocale.requestHandlers.Update.SavedSuccessfullyLabel;

            if (draft)
                message =
                    adminUILocale.requestHandlers.Update
                        .DraftSavedSuccessfullyLabel;
            if (autosave)
                message =
                    adminUILocale.requestHandlers.Update
                        .AutosavedSuccessfullyLabel;

            return res.status(httpStatus.OK).json({ message, result });
        } catch (error) {
            return next(error);
        }
    };
}
